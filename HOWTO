********************************************************************************
                           PSI3 : UNIX VERSION 1.3.1
********************************************************************************
                                   June 2015

INTRODUCTION
================

	PSi3 is the next generation of PSi2
	Improvement of PSi3:
      - New software structure, more modulable and more extensible
      - New configuration input file standard: YAML. More friendly and faster
        parser
      - More performance gain on simulation method "Coupling from the past" (x4
        faster)
      - New simulation kernel : Simple Forward simulates Markov system
        trajectory in finite time without guaranty of steady state 
      - New simulation kernel : Simple Forward Parallel has the same objectif
        of Simple Forward and is used on multi-cores system to reduce
        simulation time compared with Simple Forward kernel				


INSTALL
=================

	Requirements:

      - C compiler supporting OpenMP 3.0
         - GCC > 4.4
         Note: if your compiler doesn't support OpenMP 3.0, method Simple
         Forward Parallel won't be available.
		- CMake > 3.0
		 
   Install commands:

	$ DEST="$HOME/psi"
	$ mkdir build
	$ cd build
	$ cmake -D CMAKE_INSTALL_PREFIX="$DEST" ..
	$ make
	$ make install
	
   Note: if you install in /usr or /usr/local (the default if
   CMAKE_INSTALL_PREFIX is not set), then you wont need to set the following.

	$ export PATH="$DEST/bin:$PATH"
	$ psi3_unix -h

	$ export MANPATH="$MANPATH:$DEST/man"
	$ man psi3_unix
	

EXAMPLES
================

$ cd Examples

	1. M/M/1 queue by using Simple Forward method

		$ cd SimpleForward/
		$ psi3_unix -m simple-queue.yaml -p param.yaml -k method.yaml

	2. Double Queue by using Simple Forward Parallel method

		$ cd SimpleForwardParallel/
		$ psi3_unix -m double-queue.yaml -k method.yaml -p param.yaml

	3. Double Queue by using Backward Monotone method

		$ cd BackwardMonotone/
		$ psi3_unix -m double-queue.yaml -k method.yaml -p param.yaml

	4. Negative Customer by using Backward Non-monotone method with envelop

		$ cd BackwardEnvelop/
		$ psi3_unix -m negative_customer.yaml -k method.yaml -p param.yaml

	5. Multi-server by using Backward Non-monotone method with envelop/splitting

		$ cd BackwardEnvelop/
		$ psi3_unix -m multi-server.yaml -k method.yaml -p param.yaml


CONTACT 
=============

Jean-Marc Vincent <jean-marc.vincent@imag.fr>
Benjamin Briot <benjamin.briot@inria.fr>
Florian Leveque <fleveque@imag.fr>
Minh Quan HO <minh-quan.ho@imag.fr>
