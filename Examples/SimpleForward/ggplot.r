#! /usr/bin/env Rscript

# For nice plotting
library (ggplot2)

# To plot easily on the same board
library (gridExtra)


# Read the trajectory
data = read.table ("output.txt")

# Iterations are on the 1st column
iterations = data$V1

# States are on the 3rd column
states = data$V3

# Get states range
stateRange = range (states)

# Output to pdf
pdf ("ex1_r.pdf", width = 15)


# Build trajectory
trajectory = ggplot (data, aes (x = V1, y = V3)) +
               geom_line (stat = "identity",
                          position = "identity",
                          colour = "black",
                          width = "3") +
               ylim (0, max (stateRange)) +
               xlab (paste ("Trajectory\n(length: ", length (iterations), ")")) +
               ylab ("States")

# Build histogram
histogram = ggplot (data, aes (x = V3)) +
               geom_bar (binwidth = 1,
                         colour = "black",
                         fill = "white") +
               xlim (0, max (stateRange)) +
               xlab ("States") +
               ylab ("Frequency")

# Plot the trajectory and the frequency histogram on the same board
grid.arrange (trajectory, histogram, ncol = 2)

# Close output file
dev.off()
