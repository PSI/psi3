#!/usr/bin/perl

use strict;
use warnings;

use Getopt::Long;
use Pod::Usage;

my $options= {
    'nb-queues' => 4,
    'global-arrival-rate' => 1,
};

GetOptions (
    $options,
    "help",# => sub { HelpMessage() },
    "nb-queues=i",
    "departure-rate=f@",
    "global-arrival-rate=f",
    "order-balancing!",
    "queue-size=i@",
    "verbose",
    "man") or pod2usage(2);
pod2usage(1) if $options->{"help"};
pod2usage(-exitval => 0, -verbose => 2) if $options->{"man"};

sub build_queues {
    my $nb = shift;
    my $qsize = shift;
    my $qsize_orig = [ @$qsize ]; 
    my @res;
    
    for(my $i=0; $i<$nb; $i++) {
	if (scalar(@$qsize) == 0) {
	    $qsize = [ @$qsize_orig ]; 
	}
	my $max = shift @$qsize;
	push @res, {
	    'id' => 'queue'.$i,
	    'min' => 0,
	    'max' => $max,
	};
    }
    return \@res;
}

sub build_events {
    my $nb = shift;
    my $drate = shift;
    my $arate = shift;
    my $bal = shift;

    my @res;
    
    my $nb_arev = $nb * ($nb - 1);
    $nb_arev /= 2 if not $bal;
    
    my $arate_arev = $arate / $nb_arev;
    
    if (not defined($drate)) {
	$drate = [ 1 / ($nb + 1) ];
    }
    my $drate_orig = [ @$drate ];

    for (my $q=0; $q < $nb; $q++) {
	my $drateq = shift @$drate;
	if (scalar(@$drate) == 0) {
	    $drate = [ @$drate_orig ];
	}
	push @res, {
	    'id' => 'evdep'.$q,
	    'type' => 'Default$ext_departure',
	    'rate' => $drateq,
	    'from' => "[queue$q]",
	    'indexFctOrigin' => '~',
	    'to' => "[drop]",
	    'indexFctDest' => '~',
	    'parameters' => '~',
	}
    }
    for (my $q1=0; $q1 < $nb; $q1++) {
	for (my $q2=0; $q2 < $nb; $q2++) {
	    next if $q1 == $q2;
	    next if $q1 > $q2 && not $bal;
	    push @res, {
		'id' => 'jsq'.$q1."-".$q2,
		'type' => 'Default$JSQ_rejet',
		'rate' => $arate_arev,
		'from' => "[outside]",
		'indexFctOrigin' => '~',
		'to' => "[queue$q1,queue$q2,drop]",
		'indexFctDest' => '~',
		'parameters' => '~',
	    }
	}
    }
    return \@res;
}

my $data={
    'Queues' => build_queues(
	$options->{'nb-queues'},
	$options->{'queue-size'} // [ 50 ],
	),
    'Events' => build_events(
	$options->{'nb-queues'},
	$options->{'departure-rate'},
	$options->{'global-arrival-rate'} // 1,
	$options->{'order-balancing'} // 1,
    ),
};

#use Data::Dumper;
#print Dumper($data);
    
use YAML;

#print Dump($data),"\n";
# Workaround PSI3 bugs

sub reformat_yaml {
    my $str = shift;
    $str =~ s/'//g;
    $str =~ s/^--- *$//mg;
    return $str;
}

print reformat_yaml(
    Dump( {
	'Queues' => $data->{'Queues'},
	  }
    )), reformat_yaml(
    Dump( {
	'Events' => $data->{'Events'},
	  }
    )),
    "\n";

__END__

=head1 NAME

create-jrqr-model - Generate a psi3 model for JSQ randomized

=head1 SYNOPSIS

create-jrqr-model [options]

 Options:
   -help            brief help message
   -man             full documentation

=head1 OPTIONS

=over 8

=item B<-help>

Print a brief help message and exits.

=item B<-man>

Prints the manual page and exits.

=back

=head1 DESCRIPTION

B<This program> will read the given input file(s) and do something
useful with the contents thereof.

=cut
