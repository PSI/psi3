/**
 * \file        output.c
 * \author      Florian LEVEQUE
 * \author      Minh Quan HO
 * \author      Jean-Marc.Vincent@imag.fr
 * \author      Benjamin.Briot@inria.fr
 * \version     1.3.0
 * \date        08/04/2015
 * \brief       Contains default output function
 */

/*
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <stdio.h>
#include <psi.h>

extern int nb_queues;
extern int nb_sample;
extern int* capmaxs;
extern int nb_AV;

/* Global var *****************************************************************/
int last_written_sample = 0;
int initialState = 0; /* used to remember if the initial state has been
                       written */
/******************************************************************************/

void
print_state (FILE * f, int *state)
{
    int i;
    
    for (i = 0; i < nb_queues; i++) {
        fprintf (f, "%d ", state[i]);
    }
}


void
MyOutput (FILE * f, int **state, int sample, int *log2stop_time)
{
    int i;
    int reward0 = 0;
    
    fprintf (f, "%d \t", sample);
    // calcul du reward pour l'etat min
    for (i = (nb_queues - 4); i < nb_queues; i++) {
        reward0=(reward0 ||(state[0][i] == capmaxs[i]));
    }
    fprintf (f, " %d ", reward0);
    
    fprintf (f, " %d ", log2stop_time[0]);
    fprintf (f, "\n");
}



