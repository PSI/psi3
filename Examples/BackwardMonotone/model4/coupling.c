#include <stdio.h>
#include <psi.h>

extern int *capmaxs;
extern int nb_queues;
extern int nb_sample;
extern int nb_AV;

int min2 (int val1, int val2)
{
    if (val1 <= val2)
        return val1;
    else
        return val2;
}

int max2 (int val1, int val2)
{
    if (val1 >= val2)
        return val1;
    else
        return val2;
}


/**
 * \brief      Check if the queues have coupled for backward monotone
 
 * \details    Check if the queues have coupled, save this time and check if
 *             all queues have coupled
 
 * \param      states         (vector of) states of queues
 * \param      log2stop_time  time iteration (= current simulation position in
 states vector)
 * \param      reward         save the coupling time for each queues
 * \param      state_saved    save the state at coupling time
 
 * \return     int            representing if all queues have coupled:
 *                               0 means no
 *                               other means yes.
 */


short int couplingLastStage (T_state *states,
                             int log2stop_time,
                             int *reward,
                             int *state_saved)
{
    int i = 0;
    int reward0 = 0;
    int reward1 = 0;
    
    // calcul du reward pour l'etat min
    for (i = (nb_queues - 4); i < nb_queues; i++) {
        reward0 = (reward0 || (states->queues[0][i] == capmaxs[i]));
        reward1 = (reward1 || (states->queues[1][i] == capmaxs[i]));
    }
    if (reward0 == reward1) {
        for (i = 0; i < nb_queues; i++) {
            state_saved[i]= states->queues[0][i] ;
        }
    }
    return (reward0 == reward1);
}
