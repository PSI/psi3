#!/bin/bash
#set -x

if [ ! -r results.txt ]; then

   psi3_unix -m model4.yaml -k method.yaml -p param.yaml

   if [ $? -ne 0 ]; then

      exit 1;
   fi

fi

rate=`cat results.txt | grep -v "^#" | awk '{print $2}'\
   | awk '/1/ {Full++} /0/ {NotFull++} END {print Full " / " NotFull}' | bc -l`

echo "loss rate [$rate]"
