#!/usr/bin/python
################################################################################
#                                                                              #
#            Script to parse a standard litterature queuing model.             #
#                          http://psi.gforge.inria.fr                          #
#                                                                              #
################################################################################

################################################################################
###                                GLOBAL VAR                                ###
################################################################################

import re, sys, getopt

Debug_lvl = 0
InputFile = None
OutputFile = sys.stdout
EvtId = 0

################################################################################
###                            Converter (class)                             ###
################################################################################

def print_queue (queueId, queueMin, queueMax):

    output ("   - id:     queue" + str (queueId) + "\n")
    output ("     min:    " + str (queueMin) + "\n")
    output ("     max:    " + str (queueMax) + "\n")


def print_event (fromQueue, toQueue, rate, eventType):

    global EvtId

    output ("   - id:                evt" + str (EvtId) + "\n")
    output ("     type:              " + eventType + "\n")
    output ("     rate:              " + str (rate) + "\n")

    if (fromQueue == -1):
      output ("     from:              [outside]\n")
    else:
      output ("     from:              [queue" + str (fromQueue) + "]\n")

    output ("     indexFctOrigin:    ~\n")

    if (toQueue == -1):
      output ("     to:                [drop]\n")
    else:
      output ("     to:                [queue" + str (toQueue) +  "]\n")

    output ("     indexFctDest:      ~\n")
    output ("     parameters:        ~\n")

    EvtId += 1


def make_queue (model):

    output ("Queues:\n")

    for queueId in range (0, model.N()):

        print_queue (queueId, 0, model.C(queueId))


def make_event (model):

    output ("Events:\n")

    # external arrival generation
    queueId = 0
    for rate in model.getLambda():
        if (rate != 0):
            arrival (queueId, rate)

        queueId += 1
   
    # routing
    for vector in model.getRouting():
        if vector[1] == -1:
            departure(vector[0], model.Mu(vector[0]) * vector[2])
        else:
            connect(vector[0], vector[1], model.Mu(vector[0]) * vector[2])


def arrival (queueId, rate):

    evtType = 'Default$ext_arrival_reject'
    print_event (-1, queueId, rate, evtType)


def departure (queueId, rate):

    evtType = 'Default$ext_departure'
    print_event (queueId, -1, rate, evtType)


def connect (queueFrom, queueTo, rate):

    evtType = 'Default$routing_n_queues_reject'
    print_event (queueFrom, queueTo, rate, evtType)

class literatureToPsi3:

    def toPsi3 (self, model, OutputFile):

        make_queue (model)
        make_event (model)

################################################################################
###                              Model (class)                               ###
################################################################################

def str_vector (vector):
    string = '('
    for component in vector:
        string = string + str (component) + ", "

    string = string.rstrip(", ") + ')'

    return string


def removeName (string):
    string = string [ string.find ("=") + 1 :].strip()
    string = string.lstrip('(').rstrip(')').replace (" ", "")

    return string


def isComment (string):
    pattern = re.compile("^[ ]*#.*")

    return pattern.match(string)


def extractInteger (string):
    value = removeName(string)
    pattern = re.compile("^\d+\d*$")
    if not pattern.match(value):
        return None

    return int (value)


def extractIntVector (string):

    string = removeName(string)
    pattern = re.compile("^\d+\d*(,\d+\d*)*$")
    if not pattern.match(string):
        return None

    vector = string.split(',')

    i = 0
    for i in range(0, len(vector)):
       vector[i] = int (vector[i]) 

    return vector


def extractFloatVector (string):

    string = removeName(string)
    # check if the vector is composed of float (always with a digit before
    # point)
    pattern = re.compile("^\d+[.]?\d*(,\d+[.]?\d*)*$")
    if not pattern.match(string):
        return None

    vector = string.split(',')

    i = 0
    for i in range(0, len(vector)):
       vector[i] = float (vector[i]) 

    return vector


def extractRoutingVectorList (InputFile):

    pattern = re.compile("^\d+\d*,\d+\d*,\d+[.]?\d*$")
    vectorList = []

    while True:
        line = InputFile.readline().strip().lower().replace(' ', '')

        if not line:
            break

        if isComment (line):
            continue

        line = line.lstrip('(').rstrip(')').replace (" ", "")

        if not pattern.match(line):
            return None

        vector = line.split(',')

        vector[0] = int (vector[0])
        vector[1] = int (vector[1])
        vector[2] = float (vector[2])

        vectorList.append(vector)

    return vectorList


def readValue (varName, regex, expectedType, InputFile):
    err = 1
    InputFile.seek(0, 0)
    pattern = re.compile(regex)

    while True:
        line = InputFile.readline().strip().lower().replace(' ', '')

        if not line:
            break

        if isComment (line):
            continue

        if pattern.match(line):
            if expectedType == 1:
                value = extractInteger(line)
            elif expectedType == 2:
                value = extractIntVector(line)
            elif expectedType == 3:
                value = extractFloatVector(line)
            elif expectedType == 4:
                value = extractRoutingVectorList(InputFile)

            if value:
                err = 0

    if not err:
        return value
    else:
        error ("Retrieving " + varName + " value (check syntax?)")


class literature:

    def __init__ (self):
        self.size = 0               # N
        self.capacityVector = []    # C
        self.serviceRateVector = [] # Mu
        self.arrivalRateVector = [] # Lambda
        self.routing = []           # Routing vector list


    def N (self):
        return self.size

    def str_N (self):
        return str (self.size)

    def C (self, i):
        return self.capacityVector [i]

    def str_C (self):
        return str_vector(self.capacityVector)

    def Mu (self, i):
        return self.serviceRateVector [i]

    def str_Mu (self):
        return str_vector(self.serviceRateVector)

    def getLambda (self):
        return self.arrivalRateVector

    def Lambda (self, i):
        return self.arrivalRateVector [i]

    def str_Lambda (self):
        return str_vector(self.arrivalRateVector)

    def getRouting (self):
        return self.routing

    def Routing (self, i):
        return self.routing[i]

    def str_Routing (self):
        string = ""
        for route in self.routing:
            string = string + "\n" + str_vector(route)

        return string

    def readN (self, InputFile):
        self.size = readValue ('N', "^n=.*", 1, InputFile)

    def readC (self, InputFile):
        self.capacityVector = readValue ('C', "^c=.*", 2, InputFile)

    def readMu (self, InputFile):
        self.serviceRateVector = readValue ('Mu', "^mu=.*", 3, InputFile)

    def readLambda (self, InputFile):
        self.arrivalRateVector = readValue ('Lambda', "^lambda=.*", 3, InputFile)

    def readRouting (self, InputFile):
        self.routing = readValue ('Routing', "^routing=.*", 4, InputFile)
        self.finalizeRouting()

    # Add links to fill the missing probabilities in the routing table
    def finalizeRouting (self):
        self.routing.sort()
        extraLinks = [] #for new links

        for i in range(0, self.size): # For each queue

            pSum = 0
            found = 0

            # We sum the probabilitie for the current queue
            for vector in self.routing:
                if i == vector[0]:
                    found = 1
                    pSum += vector[2]
                elif i > vector[0] and found == 1: # (list is sorted)
                    break
            
            if pSum != 1:
                extraLinks.append([i, -1, 1 - pSum])

        for vector in extraLinks:
            self.routing.append(vector)



    def checkConsistancy (self):
        if len(self.capacityVector) != self.size:
            error ("C vector hasn't the proper length")
        elif len(self.serviceRateVector) != self.size:
            error ("Mu vector hasn't the proper length")
        elif len(self.arrivalRateVector) != self.size:
            error ("Lambda vector hasn't the proper length")

        for vector in self.routing:
            if (vector[0] < 0) or (vector[0] > self.size - 1):
                error ("Error in Routing vector " + str_vector(vector)
                        + ": queue " + str(vector[0]) + " doesn't exist")
            elif (vector[1] < -1) or (vector[1] > self.size - 1):
                error ("Error in Routing vector " + str_vector(vector)
                        + ": queue " + str(vector[1]) + " doesn't exist")
            elif (vector[2] < 0) or (vector[2] > 1):
                error ("Error in Routing vector " + str_vector(vector)
                        + ": probability should be in [0:1]")


    def importFromFile (self, InputFile):
        self.readN (InputFile)
        self.readC (InputFile)
        self.readMu (InputFile)
        self.readLambda (InputFile)
        self.readRouting (InputFile)

        self.checkConsistancy ()


################################################################################

def debug (message):

   global Debug_lvl

   if (Debug_lvl == 1):
      print ('DEBUG [' + message + ']')


def error (message):

   print ("ERROR: " + message + "")
   sys.exit(1)


def output (message):

   OutputFile.write (message)


def print_info ():

   output("\nScript to generate a PSI3 model from a standard litterature ")
   output("queuing model.\n")
   output("http://psi.gforge.inria.fr\n\n")


def print_help ():

   output("gen.py -i [formal description filename] -o [PSI3 model filename]\n\n")
   output("   -h, --help:\n")
   output("\t\tEcho this help.\n")
   output("   -i, --input:\n")
   output("\t\tInput file. File containing a formal description.\n")
   output("   -o, --output:\n")
   output("\t\tOutput file. The file containing the PSI3 generated model.\n")
   output("   -d, --debug:\n")
   output("\t\tAllows debug information.\n")


def handle_arg (argv):

   global Debug_lvl
   global InputFile
   global OutputFile

   if (len(argv) == 0):
      error ("Argument problem: --help to show usage")

   try:
      opts, args = getopt.getopt (
                     argv,
                     "dhi:o:",
                     ["debug", "help","input=","output="])
   except getopt.GetoptError:
      error ("Argument problem: --help to show usage")

   for opt, arg in opts:

      if (opt in ("-h", "--help")):
         print_info ()
         print_help ()
         sys.exit (0)

      elif (opt in ("-d", "--debug")):
         Debug_lvl = 1

      elif (opt in ("-i", "--input")):
         try:
            InputFile = open (arg, 'r')
         except IOError:
            error ("Problem opening: [" + arg + "]. File exist?")

      elif (opt in ("-o", "--output")):
         try:
            OutputFile = open (arg, 'w')
         except IOError:
            error ("Problem writing to: [" + arg + "].")

   if (InputFile == None):
       error ("No input file specified")


################################################################################

def main (argv):

   handle_arg (argv)
   
   lmodel = literature ()
   lmodel.importFromFile (InputFile)

   debug ("N = " + lmodel.str_N ())
   debug ("C = " + lmodel.str_C ())
   debug ("Mu = " + lmodel.str_Mu ())
   debug ("Lambda = " + lmodel.str_Lambda ())
   debug ("Routing = " + lmodel.str_Routing ())

   converter = literatureToPsi3 ()
   converter.toPsi3 (lmodel, OutputFile)

if __name__ == "__main__":
   main (sys.argv[1:])

