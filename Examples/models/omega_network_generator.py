#!/usr/bin/python
################################################################################
#                                                                              #
#                  Script to generate Omega network for PSI 3.                 #
#                          http://psi.gforge.inria.fr                          #
#                                                                              #
################################################################################

import sys, getopt, math

Debug_lvl = 0
QueueNb = 0
StageQueueNb = 0
StageNb = 0
EvtId = 0
ArrivalRate = 1.0
RoutingRate = 1.0
ExitRate = 1.0
QueueMin = 0
QueueMax = 100

################################################################################

def debug (message):

   global Debug_lvl

   if (Debug_lvl == 1):
      print 'DEBUG [' + message + ']'


def is_power2 (num):

   return num != 0 and ((num & (num - 1)) == 0)


def print_info ():

   print '\nScript to generate Omega network for PSI 3.'
   print 'http://psi.gforge.inria.fr\n'


def print_help ():

   print 'omega_network_generator.py -q [queue Number] -a [arrival rate] '\
         '-r [routing rate] -e [exit rate] -m [max queue capacity]\n'
   print '   -h, --help:' 
   print '\t\tEcho this help.\n'
   print '   -o, --output:'
   print '\t\tOutput file name. Generated model will be printed to this file.\n'
   print '   -q, --queue-number:'
   print '\t\tSet the number of queues per stage. This number sould be power '\
         'of 2.\n'
   print '   -a, --arrival-rate:'
   print '\t\tSet the rate of clients arriving from exterior to the first '\
         'queue stage.\n'
   print '   -r, --routing-rate:'
   print '\t\tSet the rate of routing connections in the omega network.\n'
   print '   -e, --exit-rate:'
   print '\t\tSet the rate of clients leaving the network (last queue stage).\n'
   print '   -m, --max-capacity:'
   print '\t\tSet the max capacity of queues in the network.\n'
   print '   -d, --debug:'
   print '\t\tAllows debug information.\n'


def handle_arg (argv):

   global StageQueueNb
   global Debug_lvl
   global ArrivalRate
   global RoutingRate
   global ExitRate
   global QueueMin
   global QueueMax

   if (len(argv) == 0):
      print_help ()
      sys.exit (1)

   try:
      opts, args = getopt.getopt (
                     argv,
                     "dho:q:a:r:e:m:",
                     ["debug", "help","queue-number=","arrival-rate=",
                        "routing-rate=","exit-rate=","max-capacity="])
   except getopt.GetoptError:
      print 'Argument problem:\n'
      print_help ()
      sys.exit(2)

   for opt, arg in opts:

      if (opt in ("-h", "--help")):
         print_info ()
         print_help ()
         sys.exit (0)

      elif (opt in ("-d", "--debug")):
         Debug_lvl = 1

      elif (opt in ("-o", "--output")):
         sys.stdout = open (arg, 'w')

      elif (opt in ("-q", "--queue-number")):
         if (is_power2 (int (arg))):
            StageQueueNb = int (arg)
         else:
            print 'Queue number (-q) should be a not null power of 2'
            sys.exit (1)

      elif (opt in ("-a", "--arrival-rate")):
         ArrivalRate = float (arg)

      elif (opt in ("-r", "--routing-rate")):
         RoutingRate = float (arg)

      elif (opt in ("-e", "--exit-rate")):
         ExitRate = float (arg)

      elif (opt in ("-m", "--max-capacity")):
         QueueMax = int (arg)


def print_queue (queueId, queueMin, queueMax):

   print '   - id:     queue' + str (queueId)
   print '     min:    ' + str (queueMin)
   print '     max:    ' + str (queueMax)


def print_event (fromQueue, toQueue, rate, evtId, eventType):

   print '   - id:                evt' + str (evtId)
   print '     type:              ' + eventType
   print '     rate:              ' + str (rate)

   if (fromQueue == -1):
      print '     from:              [outside]'
   else:
      print '     from:              [queue' + str (fromQueue) + ']'

   print '     indexFctOrigin:   ~'

   if (toQueue == -1):
      print '     to:                [drop]'
   else:
      print '     to:                [queue' + str (toQueue) +  ']'

   print '     indexFctDest:     ~'
   print '     parameters:       ~'


def compute_tree_parameters ():

   global QueueNb
   global StageQueueNb
   global StageNb

   StageNb = int (math.log (StageQueueNb, 2) + 1)
   QueueNb = int (StageNb * StageQueueNb)


def make_queue ():

   global QueueNb
   global StageQueueNb
   global StageNb
   global QueueMin
   global QueueMax

   print 'Queues:'

   for stageId in range (0, StageNb):

      queueIdMin = stageId * StageQueueNb
      queueIdMax = stageId * StageQueueNb + StageQueueNb

      for queueId in range (queueIdMin, queueIdMax):

         print_queue (queueId, QueueMin, QueueMax)


def make_event ():

   global QueueNb
   global StageQueueNb
   global StageNb

   print 'Events:'

   for stageId in range (0, StageNb):

      queueIdMin = stageId * StageQueueNb
      queueIdMax = stageId * StageQueueNb + StageQueueNb

      for queueId in range (queueIdMin, queueIdMax):

         if (stageId == 0):
            arrival (stageId, queueId)
            connect (stageId, queueId)

         elif (stageId == StageNb - 1):
            departure (stageId, queueId)

         else:
            connect (stageId, queueId)


def arrival (stageId, queueId):

   global EvtId

   evtType = 'Default$ext_arrival_reject'
   EvtId += 1
   evtRate = ArrivalRate
   print_event (-1, queueId, evtRate, EvtId, evtType)


def departure (stageId, queueId):

   global EvtId

   evtType = 'Default$ext_departure'
   EvtId += 1
   evtRate = ExitRate
   print_event (queueId, -1, evtRate, EvtId, evtType)


def connect (stageId, queueId):

   global StageNb
   global EvtId

   EvtId += 1
   evtRate = RoutingRate

   evtType = 'Default$routing_n_queues_reject'

   fils1 = queueId + StageQueueNb

   if ((queueId % pow (2, stageId + 1)) < pow (2, stageId)):
      fils2 = queueId + StageQueueNb + pow (2, stageId)
   else:
      fils2 = queueId + StageQueueNb - pow (2, stageId)

   print_event (queueId, fils1, evtRate, EvtId, evtType)

   EvtId += 1
   print_event (queueId, fils2, evtRate, EvtId, evtType)


################################################################################

def main (argv):

   handle_arg (argv)

   compute_tree_parameters ()

   debug ('Number of queues per stage is [' + str (StageQueueNb) + ']')
   debug ('Total number of queues is [' + str (QueueNb) + ']')
   debug ('Number of stages is [' + str (StageNb) + ']')
   debug ('ArrivalRate is [' + str (ArrivalRate) + ']')
   debug ('RoutingRate [' + str (RoutingRate) + ']')
   debug ('ExitRate is [' + str (ExitRate) + ']')

   make_queue ()

   make_event ()

if __name__ == "__main__":
   main (sys.argv[1:])
