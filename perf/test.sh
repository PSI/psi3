#!/bin/bash
#set -x

# PATH
CUR_PATH=`pwd`

# LOG
LOG="$CUR_PATH/test_log.txt"

PSI3="psi3_unix"

# Available kernels
KERNEL[1]="simpleforward"
KERNEL[2]="simpleforwardparallel"
KERNEL[3]="backwardmonotone"
KERNEL[4]="backwardenvelope"
KERNEL[5]="backwardenvelopesplitting"
KERNEL[6]="multipleforward"
KERNEL[7]="backwardmonotoneparallel"

# Set a table with a list of tests for a specific kernel
list_tests () {

   if [ ! -d "$ROOT_PATH/${KERNEL[$kernel_id]}" ]; then
      echo "Error: no dir [$ROOT_PATH/${KERNEL[$kernel_id]}]"
      exit 1
   fi

   MODEL_LIST=`find $ROOT_PATH/${KERNEL[$kernel_id]} -type f | grep "model" | sort`
   NB_MODEL=`echo $MODEL_LIST | wc -w`

   METHOD_LIST=`find $ROOT_PATH/${KERNEL[$kernel_id]} -type f | grep "method" | sort`
   NB_METHOD=`echo $METHOD_LIST | wc -w`

   PARAM_LIST=`find $ROOT_PATH/${KERNEL[$kernel_id]} -type f | grep "param" | sort`
   NB_PARAM=`echo $PARAM_LIST | wc -w`

   if [ -z "$METHOD_LIST" ]; then # param or model could have be tested...
      echo "Error: no test files"
      exit 1
   fi

   if [ $NB_MODEL -ne $NB_METHOD -o $NB_METHOD -ne $NB_PARAM ]; then
      echo "Error: input files (model: $NB_MODEL, method: $NB_METHOD, param: $NB_PARAM)"
      exit 1
   fi

   NB_TEST=$NB_METHOD

   test_id=0
   for item in $MODEL_LIST; do
      item=`basename $item`
      TEST_NAME[$test_id]=`echo "$item" | sed 's/_model//'`
      let test_id=test_id+1
   done

   test_id=0
   for item in $MODEL_LIST; do
      MODEL[$test_id]=$item
      let test_id=test_id+1
   done

   test_id=0
   for item in $METHOD_LIST; do
      METHOD[$test_id]=$item
      let test_id=test_id+1
   done

   test_id=0
   for item in $PARAM_LIST; do
      PARAM[$test_id]=$item
      let test_id=test_id+1
   done

}

# Set a table of result files 
list_results () {

   if [ ! -d "$ROOT_PATH/${KERNEL[$kernel_id]}/output" ]; then
      echo "Error: no dir [$ROOT_PATH/${KERNEL[$kernel_id]}/output]"
      exit 1
   fi

   RESULTS_LIST=`find $ROOT_PATH/${KERNEL[$kernel_id]}/output -type f`

   if [ -z "$RESULTS_LIST" ]; then
      echo "Error: no result files"
      exit 1
   fi

   test_id=0
   for result_file in $RESULTS_LIST
   do
      RESULTS[$test_id]=$result_file
      let test_id=test_id+1
   done

}

prepare_tests () {

   clean

   list_tests $kernel_id

}

comment_filter () {

   cat $1 | sed '/^#/d' | sed '/^$/d' > $2
}

run () {

#   cd $ROOT_PATH/${KERNEL[$kernel_id]}/input &>/dev/null # because of model relative path...

   model_file=${MODEL[$1]}
   parameters_file=${PARAM[$1]}
   method_file=${METHOD[$1]}
   iteration_file="`echo \"$model_file\" | sed 's/_model$//'`_iteration"

   cmd="$PSI3 -m $model_file -p $parameters_file -k $method_file"
   PASSED=0 # store return code of PSI

   TIMING=0
   TIMING_SUM=0
   TIMING_LIST=""

   AVERAGE=0
   STD_DEVIATION=0
   MIN=0
   MAX=0

   NB_ITERATION=5 # by default

   if [ -s $iteration_file ]; then
      iteration_tmp=`head -n1 $iteration_file | grep "^[ [:digit:] ]*$"`

      if [ $iteration_tmp ]; then
         NB_ITERATION=$iteration_tmp

         if [ $NB_ITERATION -eq 0 ]; then
            PASSED=1
            INFO="Iteration number set to 0"
         fi

      else
         PASSED=1
         NB_ITERATION=0
         INFO="Iteration number is not a numeric valeur"
      fi
   fi

   for iteration in `seq 1 $NB_ITERATION`
   do
      $PSI3 -m $model_file -p $parameters_file -k $method_file &>$LOG

      PASSED=$?

      if [ $PASSED -ne 0 ];then
         INFO="Exec error"
      else
         INFO="Exec OK"
         TIMING=`grep "PERF:" $LOG | awk '{print $3}'`

         TIMING_LIST="$TIMING_LIST $TIMING"

         TIMING_SUM=`echo "$TIMING + $TIMING_SUM" | bc -l`

         if [ $iteration -eq 1 ]; then
            MIN=$TIMING
            MAX=$MIN
         fi

         if [ $(echo "$TIMING > $MAX" | bc) -eq 1 ]; then
            MAX=$TIMING
         elif [ $(echo "$TIMING < $MIN" | bc) -eq 1 ]; then
            MIN=$TIMING
         fi

      fi

      AVERAGE=`echo "$TIMING_SUM / $NB_ITERATION" | bc -l`

   done

   STD_DEVIATION=$(echo "$TIMING_LIST" | awk 'NF {sum=0;ssq=0;for (i=1;i<=NF;i++){sum+=$i;ssq+=$i**2}; print (ssq/NF-(sum/NF)**2)**0.5}')

#   rm -f ${RESULTS[$1]}_tmp
#
#   cd - &>/dev/null

}

send_to_codespeed () {

   SEND_SCRIPT_FILE="send.py"

   COMMITID=`svn info --xml | grep revision | head -n1 | sed -n 's/.*"\([^"]*\)".*/\1/p'`
   PROJECT="PSI3"
   EXECUTABLE=${KERNEL[$kernel_id]}
   BENCHMARK=${TEST_NAME[$testId]}
   ENVIRONMENT=`hostname`

   exec 6>&1
   exec > $SEND_SCRIPT_FILE

   cat $CUR_PATH/codespeed/send_script_header.py

   echo -e "data = {"
   echo -e "\t'commitid': '$COMMITID',"
   echo -e "\t'branch': '$BRANCH',"
   echo -e "\t'project': '$PROJECT',"
   echo -e "\t'executable': '$EXECUTABLE',"
   echo -e "\t'benchmark': '$BENCHMARK',"
   echo -e "\t'environment': \"$ENVIRONMENT\","
   echo -e "\t'result_value': $AVERAGE,"
   echo -e "}"

   echo -e "data.update({"
   echo -e "\t'revision_date': current_date,"
   echo -e "\t'result_date': current_date,"
   echo -e "\t'std_dev': $STD_DEVIATION,"
   echo -e "\t'max': $MAX,"
   echo -e "\t'min': $MIN,"
   echo -e "})"

   cat $CUR_PATH/codespeed/send_script_footer.py
   
   exec 1>&6 6>&-

   python $SEND_SCRIPT_FILE

   rm -f $SEND_SCRIPT_FILE
}

clean () {

   rm -f `find $ROOT_PATH | grep "_tmp"`

}

print_help () {

   echo -e "Perf engine for PSI3"
   echo -e ""
   echo -e "options are:"
   echo -e "\t-k [kernel ID list]"
   echo -e "\t\texecute perf tests from the given kernel list"
   echo -e "\t\tKernel list:"
   echo -e "\t\t\t1  simpleforward"
   echo -e "\t\t\t2  simpleforwardparallel"
   echo -e "\t\t\t3  backwardmonotone"
   echo -e "\t\t\t4  backwardenvelope"
   echo -e "\t\t\t5  backwardenvelopesplitting"
   echo -e "\t\t\t6  multipleforward"
   echo -e "\t\t\t7  backwardmonotoneparallel"
   echo -e ""
   echo -e "\t-d [perf test root path]"
   echo -e "\t\tpath where to find perf tests directories"
   echo -e ""
   echo -e "\t-b [branch name]"
   echo -e "\t\tName of the svn branch from which the psi3 binary has been built."
   echo -e "\t\tThis information is mandatory to send information to codespeed server."
   echo -e ""
   echo -e "\t-s"
   echo -e "\t\tSend the results to the codespeed server (nothing is sent by default)."
   echo -e ""
   echo -e "\t-h"
   echo -e "\t\tprint this help"

}
 
ROOT_PATH=`pwd` # by default
CODESPEED=0

if [ $# -eq 0 ]; then
   print_help
fi

while getopts "d:k:b:sh" opt; do
   case $opt in
      d)
         ROOT_PATH="$OPTARG"
         ;;
      k)
         KERNEL_LIST=`echo "$OPTARG" | sed 's/,/ /g'`
         ;;
      b)
         BRANCH="$OPTARG"
         ;;
      s)
         CODESPEED=1
         ;;
      h)
         print_help
         ;;
      \?)
         echo "Invalid option: -$OPTARG"
         exit 1
         ;;
   esac
done

if [ $CODESPEED -eq 1 -a -z "$BRANCH" ]; then
   echo "Branch name is mandatory to send bench results to codespeed server."
   exit 1;
fi

for kernel_id in $KERNEL_LIST
do
   echo "Running perf tests for kernel [${KERNEL[$kernel_id]}]"

   prepare_tests

   for testId in `seq 0 $(($NB_TEST - 1))`
   do

      echo -n "Bench [${TEST_NAME[$testId]}] "

      run $testId

      if [ $PASSED -eq 0 ]; then
         echo "AV: $AVERAGE, STDDEV: $STD_DEVIATION, MAX: $MAX, MIN: $MIN"

         if [ $CODESPEED -eq 1 ]; then

            send_to_codespeed

         fi

      else
         echo "FAILED ($INFO)"
      fi

      let testId=testId+1

   done

done

exit 0
