 /**
 * \file		generator.c
 * \author		Minh Quan HO
 * \author		Florian LEVEQUE
 * \author      Jean-Marc.Vincent@imag.fr
 * \version   	1.0
 * \date       	26/06/2012
 * \brief       Contains all functions of generating random events
 */

/* 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include "generator.h"

#define EPS 1e-12

double *probe = NULL;           // probabilites of events
double *thresholds = NULL;      // event thresholds
int *alter = NULL;              // alternative event when the threshold of a
                                // event is passed

                                /* vector used to initialize the Rng */
unsigned long seed_vector [SEED_VECTOR_SIZE] = {0, 0, 0, 0, 0, 0};

RngStream   Rng_IS;             /* Rng stream for initial state(s) */
RngStream  *Rng_Time;           /* Rng stream(s) for continuous time generation */
RngStream  *Rng_U01;            /* Rng stream(s) to compute u01(). There
                                 * are as many streams as threads. */

/**
 * \brief      Initialize the pseudo Random Number Generator system
 *
 * \details    - initialize the seed vector for the whole RNG system
 *             - create a stream for initial states generation: Rng_IS
 *             - create a default stream for u01 ()
 *                -> if several threads call u01 (), there is a need to create
 *                more streams: generator_addstream (). FIXME: good solution?
 *
 * \param      seed value given by user
 *
 * \return     nothing
 */
void
generator_seed (int seed_int)
{
   unsigned long seed_ul = (unsigned long) seed_int;

   seed_vector [0] = ((seed_ul * 14) + 3) % 4294967087;
   seed_vector [1] = ((seed_ul * 15) + 3) % 4294967087;
   seed_vector [2] = ((seed_ul * 92) + 3) % 4294967087;
   seed_vector [3] = ((seed_ul * 65) + 3) % 4294944443;
   seed_vector [4] = ((seed_ul * 35) + 3) % 4294944443;
   seed_vector [5] = ((seed_ul * 89) + 3) % 4294944443;

#if (DEBUG_LVL >= DEBUG_INFO)
   int i;
   fprintf (stderr, "DEBUG: Seed: [%lu]\n", seed_ul);
   fprintf (stderr, "DEBUG: Seed vector: [%lu", seed_vector [0]);
   for (i = 1; i < SEED_VECTOR_SIZE; i++) {
      fprintf (stderr, " %lu", seed_vector [i]);
   }
   fprintf (stderr, "]\n");
#endif

   RngStream_SetPackageSeed (seed_vector);

   Rng_IS = RngStream_CreateStream ("Rng_IS");

   Rng_U01 = malloc (sizeof (RngStream) * 1);
   if (Rng_U01 == NULL) PSI3_ERROR ("Rng_U01 Allocation");

   Rng_U01[0] = RngStream_CreateStream ("default");

   Rng_Time = malloc (sizeof (RngStream) * 1);
   if (Rng_U01 == NULL) PSI3_ERROR ("Rng_Time Allocation");

   Rng_Time[0] = RngStream_CreateStream ("Rng_Time");
}


/**
 * \brief       Free the different table used by the generator
 * \return     void
 */
void
free_generator ()
{
   if (probe != NULL) {
      free (probe);
   }
   if (thresholds != NULL) {
      free (thresholds);
   }
   if (alter != NULL) {
      free (alter);
   }
}


/**
 * \brief       Construct the static generator based on Walker algorithm
 * \details    
 * \param    nb_evts        Number of events
 * \return     void
 */
void
construction (int nb_evts)
{
   int j, k;                    /* j point on the first Low and k on the first
                                 * High */
   int *L;                      /* Liste des objets a traiter (indice suivant
                                 * dans la liste L et H) */
   int j_loop, l_aux;
   double *thresholds_tmp;

   j = nb_evts;                 // Test if L is empty 
   k = nb_evts;                 // Test if L is empty

   L = (int *) malloc (nb_evts * sizeof (int));
   thresholds_tmp = (double *) malloc (nb_evts * sizeof (double));

   for (j_loop = 0; j_loop < nb_evts; j_loop++) {
      thresholds_tmp[j_loop] = *(probe + j_loop) * nb_evts;
      *(thresholds + j_loop) = thresholds_tmp[j_loop];
      // Initialize thresholds with probabilites of events
      if (thresholds_tmp[j_loop] < 1)
         /* l'indice j_loop est place dans la liste L le premier element est
          * pointe par j */
      {
         L[j_loop] = j;
         j = j_loop;
      }
      else {                    /* l'indice j_loop est place dans la liste H le 
                                 * premier element est pointe par k */

         // if (thresholds_tmp[j_loop] > 1)
         // {
         L[j_loop] = k;
         k = j_loop;
         // }
      }
   }

   while (j != nb_evts) {
      *(alter + j) = k;
      thresholds_tmp[k] = thresholds_tmp[k] + thresholds_tmp[j] - 1;
      if (fabs (thresholds_tmp[k] - 1) < EPS) { /* j et k sont bons et on
                                                 * repart avec les suivants */
         *(thresholds + k) = 1;
         k = L[k];
         j = L[j];
      }
      else {
         if (thresholds_tmp[k] < 1) {  /* on insere k dans la liste L (ici on
                                        * le place en tete ) */
            *(thresholds + k) = thresholds_tmp[k];
            l_aux = L[k];
            L[k] = L[j];
            j = k;
            k = l_aux;
         }
         else {
/*** thresholds_tmp[k] >= 1 *****/
            /* on supprime l'element j des listes L et H */
            j = L[j];
         }
      }
   }
   free (L);                    // /ajout des frees
   free (thresholds_tmp);
}                               /* fin de la generation de la matrice des
                                 * seuils */

/**
 * \brief       Return a random double in [0 .. 1] 
 * \details    
 * \return     double
 */
inline double
u01 ()
{
   return RngStream_RandU01 (Rng_U01[0]);
}

/**
 * \brief       Return a random double in [0 .. 1] 
 * \details    
 * \return     double
 */
inline double
u01_time ()
{
   return RngStream_RandU01 (Rng_Time[0]);
}

/**
 * \brief       return a random int list 
 * \details    result into res[] : res[i] is a random int between capmins[i] and capmaxs[i]
 * \param    size        size of int list
 * \param    res         result int list
 * \param    capmaxs     limit sup
 * \param    capmins     limit inf
 * \param    seed        random seed
 * \return     int*
 */
inline int *
random_int_list_NEW (int size, int *capmaxs, int *capmins)
{
   int i;
   int *res;

   res = (int *) malloc (size * sizeof (int));

   for (i = 0; i < size; i++) {
      res[i] = RngStream_RandInt (Rng_IS, capmins[i], capmaxs[i]);
   }

   return res;
}

/**
 * \brief       return a random int list 
 * \details    result into res[] : res[i] is a random int between capmins[i] and capmaxs[i]
 * \param    size        size of int list
 * \param    res         result int list
 * \param    capmaxs     limit sup
 * \param    capmins     limit inf
 * \param    seed        random seed
 * \return     int*
 */
inline int *
random_int_list (int size, int *capmaxs, int *capmins, int seed)
{
   int i;
   int *res;

   res = (int *) malloc (size * sizeof (int));

   for (i = 0; i < size; i++) {
      res[i] = RngStream_RandInt (Rng_IS, capmins[i], capmaxs[i]);
   }

   return res;
}

/**
 * \brief      Called by kernel : Return a random event from aliasing
 * \param      random         Random double in [0 .. 1]
 * \param      nb_events      Number of events
 * \return     int index of the chosen event
 */
inline int
genere_evenement (double random, int nb_events)
{
   int evenement;
   double x;

   x = nb_events * random;
   evenement = (int) x;
   x = x - evenement;

   return (x <= *(thresholds + evenement)) ? evenement : *(alter + evenement);
}

/**
 * \brief       Initialize event generator
 * \details 	Calculate probabilites to have each events 
 * \param    nb_evts        Number of events
 * \param    evts           Set of events
 * \return     void
 */
void
init_generator (int nb_evts, T_event * evts)
{
   double somme = 0.0;
   int i_loop;
   probe = (double *) malloc (nb_evts * sizeof (double));
   thresholds = (double *) malloc (nb_evts * sizeof (double));
   alter = (int *) malloc (nb_evts * sizeof (int));

   for (i_loop = 0; i_loop < nb_evts; i_loop++)
      somme += (evts + i_loop)->rate;
   for (i_loop = 0; i_loop < nb_evts; i_loop++) {
      *(probe + i_loop) = ((evts + i_loop)->rate) / somme;
   }
   construction (nb_evts);
}

/**
 * \brief      Return a random double in [0 .. 1] 
 *
 * \details    Is used (only) for continuous time generation
 *
 * \return     double
 */
inline double
u01_time_p (int streamId)
{
   return RngStream_RandU01 (Rng_Time[streamId]);
}

/**
 * \brief      Return a random double in [0 .. 1] 
 *
 * \details    This function is designed for multi stream computation. Before
 *             any call to u01_p, be sure that the Rng is ready to generate
 *             ramdom stream (see function generator_set_multistream).
 *
 * \param      streamId    stream identifier (stream should be numbered in
 *                         [0...N]).
 *
 * \return     double
 */
inline double
u01_p (int streamId)
{
   return RngStream_RandU01 (Rng_U01[streamId]);
}

/**
 * \brief      Put the Rng in multiStream mode
 *
 * \details    This function has to be called before any call to u01_p. It
 *             prepares the Rgn to generate numStream streams.
 *
 * \param      numStream   Number of stream to generate
 *
 * \return     void
 */
void
generator_set_multistream (int numStream)
{
   int stream;

   if (numStream <= 0) {
      PSI3_ERROR ("Rng: numStream should be more than 0");
      return;
   }

   Rng_U01 = realloc (Rng_U01, numStream * sizeof (RngStream));
   if (Rng_U01 == NULL) PSI3_ERROR ("Rng_U01 realloc failed");

   for (stream = 1; stream < numStream; stream++) {
      Rng_U01[stream] = RngStream_CreateStream ("test");
   }

   Rng_Time = realloc (Rng_Time, numStream * sizeof (RngStream));
   if (Rng_Time == NULL) PSI3_ERROR ("Rng_Time realloc failed");

   for (stream = 1; stream < numStream; stream++) {
      Rng_Time[stream] = RngStream_CreateStream ("test");
   }
}

/**
 * \brief      Get the internal state associated to a Rng stream
 *
 * \details    This return the stream state. Calling it after any u01 call
 *             return the stream seed.
 *
 * \param      streamId    stream identifier (stream should be numbered in
 *                         [0...N]).
 * \param      seed        where to store de seed
 *
 * \return     void
 */
void
generator_get_state (int streamId, unsigned long *seed)
{
   RngStream_GetState (Rng_U01[streamId], seed);
}

void
generator_time_get_state (int streamId, unsigned long *seed)
{
   RngStream_GetState (Rng_Time[streamId], seed);
}

/**
 * \brief      Set the internal state associated to a Rng stream
 *
 * \details    It allows to reproduce a Rng stream.
 *
 * \param      streamId    stream identifier (stream should be numbered in
 *                         [0...N]).
 * \param      seed        where to store de seed
 *
 * \return     void
 */
void
generator_set_state (int streamId, unsigned long *seed)
{
   RngStream_SetSeed (Rng_U01[streamId], seed);
}

void
generator_time_set_state (int streamId, unsigned long *seed)
{
   RngStream_SetSeed (Rng_Time[streamId], seed);
}


int
empty_seed (unsigned long *seed)
{
   return (seed[0] == 0
            && seed[1] == 0
            && seed[2] == 0
            && seed[3] == 0
            && seed[4] == 0
            && seed[5] == 0);
}

void
generator_time_reset (int streamId)
{
   RngStream_ResetStartStream (Rng_Time[streamId]);
}
