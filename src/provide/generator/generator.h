 /**
 * \file 		generator.h
 * \author		Minh Quan HO
 * \author		Florian LEVEQUE 
 * \author      Jean-Marc.Vincent@imag.fr 
 * \version   	1.0
 * \date       	26/06/2012
 * \brief       Declare all functions of generating random events
 */

 /* 
  * This library is free software; you can redistribute it and/or
  * modify it under the terms of the GNU Lesser General Public
  * License as published by the Free Software Foundation; either
  * version 2.1 of the License, or any later version.
  * 
  * This library is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  * Lesser General Public License for more details.
  * 
  * You should have received a copy of the GNU Lesser General Public
  * License along with this library; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
  */

#ifndef PSI3_GENERATOR_H
#define PSI3_GENERATOR_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <psi.h>
#include <unistd.h>
#include "RngStream.h"
#include "psi_common.h"

#define SEED_VECTOR_SIZE 6

void free_generator ();
void generator_seed (int seed);
void construction (int nb_evts);
int *random_int_list (int size, int *capmaxs, int *capmins, int seed);
int *random_int_list_NEW (int size, int *capmaxs, int *capmins);
void init_generator (int nb_evts, T_event * evts);
int genere_evenement (double u, int nb_evenements);
double u01 ();
void generator_set_multistream (int numStreams);
double u01_time ();
double u01_time_p (int streamId);
double u01_p (int streamId);
void generator_get_state (int streamId, unsigned long *seed);
void generator_set_state (int streamId, unsigned long *seed);
void generator_time_get_state (int streamId, unsigned long *seed);
void generator_time_set_state (int streamId, unsigned long *seed);
int empty_seed (unsigned long *seed);
void generator_time_reset (int streamId);

#endif // PSI3_GENERATOR_H
