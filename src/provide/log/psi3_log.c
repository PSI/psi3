#include "psi3_log.h"

/* Colors */
#define KNRM   "\x1B[0m"
#define KRED   "\x1B[31m"
#define KRED_B "\033[1m\033[31m"
#define KGRN   "\x1B[32m"
#define KYEL   "\x1B[33m"
#define KBLU   "\x1B[34m"
#define KMAG   "\x1B[35m"
#define KCYN   "\x1B[36m"
#define KWHT   "\x1B[37m"

/* Header string configuration */
static const char *LOG_STRING_ERROR = KRED_B"ERROR"KNRM;
static const char *LOG_STRING_WARNING = KYEL"WARNING"KNRM;
static const char *LOG_STRING_INFO = "INFO";
static const char *LOG_STRING_DEBUG = "DEBUG";
static const char *LOG_STRING_UNDEFINED = "UNDEFINED";

void
psi3_log (int type, ...)
{
   const char *header = NULL;
   FILE *output;
   va_list arg;

   /* Allocating like this allows to have one buffer per thread */
   static __thread char LOG_BUFFER [LOG_BUFFER_SIZE];

   /* Get user message and put in the log buffer */
   va_start (arg, type);
   char *format = va_arg (arg, char *);
   vsnprintf (LOG_BUFFER, LOG_BUFFER_SIZE, format, arg);
   va_end (arg);

   switch (type) {
      case LOG_ERROR:
         header = LOG_STRING_ERROR;
         output = stderr;
         break;
      case LOG_WARNING:
         header = LOG_STRING_WARNING;
         output = stderr;
         break;
      case LOG_INFO:
         header = LOG_STRING_INFO;
         output = stderr;
         break;
      case LOG_DEBUG:
         header = LOG_STRING_DEBUG;
         output = stderr;
         break;
      default:
         header = LOG_STRING_UNDEFINED;
         output = stderr;
         break;
   }

   fprintf (output, "%s: %s\n", header, LOG_BUFFER);
   fflush (output);
}
