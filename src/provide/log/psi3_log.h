#include <string.h>
#include <stdio.h>
#include <stdarg.h>

#ifndef PSI3_LOG_H
#define PSI3_LOG_H

#define LOG_ERROR 0
#define LOG_WARNING 1
#define LOG_INFO 2
#define LOG_DEBUG 3

#ifndef LOG_LVL
#define LOG_LVL 1
#endif

#define LOG_BUFFER_SIZE 1024
#define LOG_HEADER_SIZE 128

void
psi3_log (int type, ...);

#if (LOG_LVL >= LOG_ERROR)
#define PSI3_ERROR(...) \
   psi3_log (LOG_ERROR, __VA_ARGS__);
#else
#define PSI3_ERROR(...) // Nothing
#endif
#if (LOG_LVL >= LOG_WARNING)
#define PSI3_WARNING(...) \
   psi3_log (LOG_WARNING, __VA_ARGS__);
#else
#define PSI3_WARNING(...) // Nothing
#endif
#if (LOG_LVL >= LOG_INFO)
#define PSI3_INFO(...) \
   psi3_log (LOG_INFO, __VA_ARGS__);
#else
#define PSI3_INFO(...) // Nothing
#endif
#if (LOG_LVL >= LOG_DEBUG)
#define PSI3_DEBUG(...) \
   psi3_log (LOG_DEBUG, __VA_ARGS__);
#else
#define PSI3_DEBUG(...) // Nothing
#endif

#endif // PSI3_LOG_H
