
#ifndef PSI3_TIMING_H
#define PSI3_TIMING_H

#include <psi3_log.h>
#include <psi_common.h>
#include <math.h>

typedef struct st_timing {
   double value;
   double lambda;
   unsigned long *seed;
   int streamId;
} st_timing, *st_timing_ptr;

st_timing_ptr
psi3_timing_init (int streamId, unsigned long *seed, st_model_ptr model);

double
psi3_timing_next (st_timing_ptr timing);

void
psi3_timing_close (st_timing_ptr timing);

void
psi3_timing_print (FILE *fd, st_timing_ptr timing);

void
psi3_timing_reset (st_timing_ptr timing);

#endif // PSI3_TIMING_H
