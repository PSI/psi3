#include <psi3_timing.h>
#include <float.h>

st_timing_ptr
psi3_timing_init (int streamId, unsigned long *seed, st_model_ptr model)
{
   int event;

   st_timing_ptr timing;

   timing = malloc (sizeof(st_timing));

   if (timing == NULL) {
      PSI3_ERROR ("Timing Allocation");
      return NULL;
   }

   timing->value = 0;
   timing->lambda = 0;
   timing->seed = seed;
   timing->streamId = streamId;

   if (timing->seed != NULL) {
      generator_time_set_state (timing->streamId, timing->seed);
   }
   else {
      timing->seed = malloc (SEED_VECTOR_SIZE * sizeof (unsigned long));
      generator_time_get_state (0, timing->seed);
   }

   for (event = 0; event < model->events->nb_evts; event++) {
      timing->lambda += model->events->evts[event].rate;
   }

   return timing;
}

double
psi3_timing_next (st_timing_ptr timing)
{
   double delta = 0;

   delta = -(log (u01_time_p (timing->streamId)) / timing->lambda);
   timing->value = timing->value + delta;

   return timing->value;
}

void
psi3_timing_print (FILE *fd, st_timing_ptr timing)
{
   fprintf (fd, "# time: %f\n", timing->value);
}

void
psi3_timing_close (st_timing_ptr timing)
{
   free (timing);
}

void
psi3_timing_reset (st_timing_ptr timing)
{
   timing->value = 0;
   generator_time_reset (timing->streamId);
}
