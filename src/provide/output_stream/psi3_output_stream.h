#include <stdio.h>
#include <stdlib.h>
#include <psi.h>

#ifndef PSI3_OUTPUT_STREAM_H
#define PSI3_OUTPUT_STREAM_H

void
psi3_output_set_multistream (st_general_ptr general, int numStreams);

FILE *
psi3_get_output (int streamId);

int
psi3_output_concatenate ();

#endif // PSI3_OUTPUT_STREAM_H
