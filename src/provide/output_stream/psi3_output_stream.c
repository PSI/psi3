#include <psi3_output_stream.h>
#include <psi3_log.h>
#include <psi_common.h>

FILE **output_table;
int output_table_size;
char **output_table_filename;

/*
 * Create [numStreams] output streams (that correspond to as many files).
 * It is used for parallel thread(s) to output in different places.
 */
void
psi3_output_set_multistream (st_general_ptr general, int numStreams)
{
   int streamId;
   int stringLength = 0;
   char filename_tmp [1024];

   output_table_size = numStreams;

   output_table = malloc ((output_table_size) * sizeof (FILE *));
   if (output_table == NULL) {
      PSI3_ERROR ("Ouput table allocation");
   }

   /* if user didn't set anything for output file name */
   if (strcmp (general->output->outputfilename, PSI3_DEFAULT_OUTPUT_FILENAME) == 0) {
      general->output->outputfilename = "trajectory";
   }

   output_table_filename = malloc (numStreams * sizeof (char *));

   output_table[0] = general->output_file;

   for (streamId = 1; streamId < output_table_size; streamId++) {
      /* create a new file name derived from the original one:
       * originName_[streamId]
       */
      stringLength = snprintf (filename_tmp, 1024, "%s_%d", general->output->outputfilename,
            streamId);

      output_table_filename [streamId] = malloc
         ((stringLength * sizeof (char)) + 1);

      strncpy (output_table_filename [streamId],
            filename_tmp, stringLength + 1); // Null byte

      PSI3_DEBUG ("filename: [%s]", output_table_filename [streamId]);
      output_table[streamId] = fopen (filename_tmp, "w");
   }
}

/*
 * Return the output stream associated to stream Id.
 * TODO: this output stream is a file descriptor, this can be abstracted...
 */
FILE *
psi3_get_output (int streamId)
{
   if (output_table == NULL) {
      PSI3_ERROR ("Ouput table NULL");
   }

   return output_table[streamId];
}

/*
 * Used at the end of a computation to gather all output stream in one file.
 */
int
psi3_output_concatenate ()
{
   int streamId;
   FILE *output;
   char buf[8192]; // temp buffer of 8K (goal is to fit in L1 cache)

   PSI3_DEBUG ("Concatenating...");
   PSI3_DEBUG ("output_table_size [%d]", output_table_size);

   output = output_table[0];

   for (streamId = 1; streamId < output_table_size; streamId++) {

      fseek (output, 0, SEEK_END); // concatenate at the end of the output file

      fsync (fileno (output_table[streamId])); /* ensure everything is in the
                                                * file before closing it */
      fclose (output_table[streamId]);

      /* we reopen the file in read mode */
      output_table[streamId] = fopen (output_table_filename[streamId], "r");

      while (1) {

         size_t rbytes = fread (buf, 1, sizeof (buf), output_table[streamId]);

         if (! rbytes) break;

         if (! (rbytes == fwrite (buf, 1, rbytes, output))) {
            PSI3_ERROR ("Concatenation error");
            break;
         }
      }
      
      fclose (output_table[streamId]);
      remove (output_table_filename[streamId]);
      PSI3_DEBUG ("Remove file: [%s]", output_table_filename[streamId]);
   }
   PSI3_DEBUG ("Concatenating... OK");

   return 0;
}

