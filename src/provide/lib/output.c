 /**
 * \file        output.c
 * \author      Florian LEVEQUE
 * \author      Minh Quan HO
 * \author      Jean-Marc.Vincent@imag.fr
 * \author      Benjamin.Briot@inria.fr
 * \version     1.3.0
 * \date        08/04/2015
 * \brief       Contains default output function  
 */

 /* 
  * This library is free software; you can redistribute it and/or
  * modify it under the terms of the GNU Lesser General Public
  * License as published by the Free Software Foundation; either
  * version 2.1 of the License, or any later version.
  * 
  * This library is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  * Lesser General Public License for more details.
  * 
  * You should have received a copy of the GNU Lesser General Public
  * License along with this library; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
  */

#include <stdio.h>
#include <psi.h>

extern int nb_queues;
extern int nb_sample;
extern int nb_AV;

/* Global var *****************************************************************/
int last_written_sample = 0;
int initialState = 0; /* used to remember if the initial state has been
                          written */
/******************************************************************************/

void
print_state (FILE * f, int *state)
{
   int i;

   for (i = 0; i < nb_queues; i++) {
      fprintf (f, "%d ", state[i]);
   }
}

/**
 * \brief      Function defining how data will be diplayed in Backward methods
 * \details    Write in file f the state at coupling time using this pattern:
 *             [sample number] - [queue 1 state] ... [queue N state]
 * \param      f              output file
 * \param      state          (vector of) state of queues
 * \param      sample         sample number
 * \param      log2stop_time  coupling time iteration
 * \return     void
 */
void
output_B (FILE * f, int **state, int sample, int *log2stop_time)
{
   int i, n;

   fprintf (f, "%d \t", sample);

   /* FIXME: Antithetique variable... why? */
   for (n = 0; n < nb_AV; n++) {

      for (i = 0; i < nb_queues; i++) {
         fprintf (f, " %d ", state[n][i]);
      }

      fprintf (f, " %d ", log2stop_time[n]);
      fprintf (f, "\t");
   }

   fprintf (f, "\n");
}

/**
 * \brief      function used by simpleforward method to ouput data
 * \details    write to output_file the log_size number of states with the 
 *             following pattern:
 *             [Iteration Nb] [Time] [Evt Number] [Queue1 State] ... [QueueN State]
 *
 * \param      output_file    output file descriptor
 * \param      log_states     2 dimensional array describing queues states in
 *                            function of iteration (1st dim: time, 2nd dim:
 *                            queue states)
 * \param      log_size       number of states to write
 * \param      events_list    array of generated events in function of iteration
 *
 * \return     void
 */
void
output_SF (FILE * output_file, int **trajectory,
           int log_size, int *events_list, double *timing)
{
   int i = 0, k;

   if (last_written_sample == 0) {  /* initial state => no event */

      fprintf (output_file, "#\n# Output data:\n");
      fprintf (output_file, "# [iteration] [time] [transition]");

      for (k = 0; k < nb_queues; k++) {
         fprintf (output_file, " [queue %d]", k);
      }
      fprintf (output_file, "\n");

      fprintf (output_file, "0\t%f\tNa\t", 0.0); /* Na: null value for R */

      for (k = 0; k < nb_queues; k++) {
         fprintf (output_file, "%d ", trajectory[0][k]);
      }
      fprintf (output_file, "\n");

      i = 1;
   }

   for (; i < log_size; i++) {

      fprintf (output_file,
               "%d\t%f\t%d\t",
               last_written_sample + i,
               timing[i],
               events_list[last_written_sample + i - 1]);

      for (k = 0; k < nb_queues; k++) {
         fprintf (output_file, "%d ", trajectory[i][k]);
      }

      fprintf (output_file, "\n");
   }

   last_written_sample += log_size;
}

/**
 * \brief      function used by simpleforward method to ouput data
 * \details    write to output_file the log_size number of states with the 
 *             following pattern:
 *             [Transition Number] [Event Number] [Queue1 State] ... [QueueN State]
 *
 * \param      output_file    output file descriptor
 * \param      log_states     2 dimensional array describing queues states in
 *                            function of iteration (1st dim: time, 2nd dim:
 *                            queue states)
 * \param      log_size       number of states to write
 * \param      events_list    array of generated events in function of iteration
 *
 * \return     void
 */
void
output_SF_noTime (FILE * output_file, int **trajectory,
           int log_size, int *events_list, double *timing)
{
   int i = 0, k;

   if (last_written_sample == 0) {  /* initial state => no event */

      fprintf (output_file, "#\n# Output data:\n");
      fprintf (output_file, "# [iteration] [transition]");

      for (k = 0; k < nb_queues; k++) {
         fprintf (output_file, " [queue %d]", k);
      }
      fprintf (output_file, "\n0\tNa\t"); /* Na: null value for R */

      for (k = 0; k < nb_queues; k++) {
         fprintf (output_file, "%d ", trajectory[0][k]);
      }
      fprintf (output_file, "\n");

      i = 1;
   }

   for (; i < log_size; i++) {

      fprintf (output_file,
               "%d\t%d\t",
               last_written_sample + i,
               events_list[last_written_sample + i - 1]);

      for (k = 0; k < nb_queues; k++) {
         fprintf (output_file, "%d ", trajectory[i][k]);
      }

      fprintf (output_file, "\n");
   }

   last_written_sample += log_size;
}

/**
 * \brief      function used by simpleforward method to ouput data
 * \details    write to output_file the log_size number of states with the 
 *             following pattern:
 *             [Transition Number] [Event Number] [Queue1 State] ... [QueueN State]
 *
 * \param      output_file    output file descriptor
 * \param      log_states     2 dimensional array describing queues states in
 *                            function of iteration (1st dim: time, 2nd dim:
 *                            queue states)
 * \param      log_size       number of states to write
 * \param      events_list    array of generated events in function of iteration
 *
 * \return     void
 */
void
output_MF (FILE * output_file, int **trajectory,
           int log_size, int *events_list,
           double *timing, int transitionNb, int nbTransitions)
{
   int i = 0, k;

   if (transitionNb == log_size - 1) {  /* initial state => no event */

      fprintf (output_file, "#\n# Output data:\n");
      fprintf (output_file, "# [iteration] [time] [transition]");

      for (k = 0; k < nb_queues; k++) {
         fprintf (output_file, " [queue %d]", k);
      }
      fprintf (output_file, "\n");

      fprintf (output_file, "0\t%f\tNa\t", 0.0); /* Na: null value for R */

      for (k = 0; k < nb_queues; k++) {
         fprintf (output_file, "%d ", trajectory[0][k]);
      }
      fprintf (output_file, "\n");

      i = 1;
   }

   for (; i < log_size; i++) {

      fprintf (output_file,
               "%d\t%f\t%d\t",
               transitionNb - log_size + i + 1,
               timing[i],
               events_list[transitionNb - log_size + i]);

      for (k = 0; k < nb_queues; k++) {
         fprintf (output_file, "%d ", trajectory[i][k]);
      }

      fprintf (output_file, "\n");
   }

   last_written_sample += log_size;
}

/**
 * \brief      function used by simpleforward method to ouput the last state
 * \details    write to output_file the last state with the following pattern:
 *             [Transition Number] [Event Number] [Queue1 State] ... [QueueN State]
 *
 * \param      output_file    output file descriptor
 * \param      log_states     2 dimensional array describing queues states in
 *                            function of iteration (1st dim: time, 2nd dim:
 *                            queue states)
 * \param      log_size       number of states to write
 * \param      events_list    array of generated events in function of iteration
 *
 * \return     void
 */
void
last_state (FILE * output_file, int **trajectory,
           int log_size, int *events_list,
           double *timing, int transitionNb, int nbTransitions,
           st_simulation_ptr simulation)
{
   int k;

   if (transitionNb == nbTransitions) { // simulation is finished

      fprintf (output_file, "#\n# Output data:\n");
      fprintf (output_file, "# [iteration] [time] [transition]");

      for (k = 0; k < nb_queues; k++) {
         fprintf (output_file, " [queue %d]", k);
      }
      fprintf (output_file, "\n");

      fprintf (output_file,
               "%d\t%f\t%d\t",
               transitionNb,
               timing[log_size - 1],
               events_list[transitionNb - 1]);

      for (k = 0; k < nb_queues; k++) {
         fprintf (output_file, "%d ", trajectory[log_size - 1][k]);
      }

      fprintf (output_file, "\n");
   }
}

/**
 * \brief      Function defining how data will be diplayed in method Simple
 *             Forward Parallel
 *
 * \details    Write into stream_out the given sub_trajectory associated to an
 *             interval, from 'begin' in the global trajectory and with
 *             'length'
 *
 * \param      stream_out        Stream out.
 * \param      sub_trajectory    Sub trajectory of the interval
 * \param      events_list       List of events that have been generated in
 *                               this interval
 * \param      begin             Position where this sub_trajectory starts in
 *                               the global trajectory
 * \param      length            Length of sub_trajectory
 *
 * \return     void
 */
void
output_SFP (FILE * stream_out,
            int **sub_trajectory, int *events_list, int begin, int length)
{
   int i = 0, j;

   if (initialState == 0) {
      fprintf (stream_out, "%d\tNa\t", begin + i);

      for (j = 0; j < nb_queues; j++) {
         fprintf (stream_out, "%d ", sub_trajectory[i][j]);
      }

      fprintf (stream_out, "\n");
      initialState = 1;         /* mark initial state: written */
      i = 1;
   }


   for (; i < length; i++) {
      fprintf (stream_out, "%d\t%d\t", begin + i, events_list[i - 1]);

      for (j = 0; j < nb_queues; j++) {
         fprintf (stream_out, "%d ", sub_trajectory[i][j]);
      }

      fprintf (stream_out, "\n");
   }
}

/**
 * This function exists for dev purpose
 */
void
no_Output ()
{
   return;
}

void
output_SFP_timing (FILE * stream_out, int **sub_trajectory, int *events_list,
                   int begin, int length, double *timing)
{
   int i = 0, j = 0;

   if (initialState == 0) {
      fprintf (stream_out, "#\n# Output data:\n");
      fprintf (stream_out, "# [iteration] [time] [transition]");

      for (j = 0; j < nb_queues; j++) {
         fprintf (stream_out, " [queue %d]", j);
      }
      fprintf (stream_out, "\n");

      fprintf (stream_out, "%d\t%f\tNa\t", 0, 0.0);
      for (j = 0; j < nb_queues; j++) {
         fprintf (stream_out, "%d ", sub_trajectory[i][j]);
      }
      fprintf (stream_out, "\n");

      initialState = 1;         /* mark initial state: written */
      i++;
   }

   for (; i < length; i++) {
      fprintf (stream_out, "%d\t%f\t%d\t",
               begin + i,
               timing[begin + i],
               events_list[begin + i - 1]);

      for (j = 0; j < nb_queues; j++) {
         fprintf (stream_out, "%d ", sub_trajectory[i][j]);
      }

      fprintf (stream_out, "\n");
   }
}

void
output_SFP_noTiming (FILE * stream_out, int **sub_trajectory, int *events_list,
                     int begin, int length, double *timing)
{
   int i = 0, j = 0;

   if (initialState == 0) {
      fprintf (stream_out, "#\n# Output data:\n");
      fprintf (stream_out, "# [iteration] [transition]");

      for (j = 0; j < nb_queues; j++) {
         fprintf (stream_out, " [queue %d]", j);
      }
      fprintf (stream_out, "\n");

      fprintf (stream_out, "%d\tNa\t", begin);
      for (j = 0; j < nb_queues; j++) {
         fprintf (stream_out, "%d ", sub_trajectory[i][j]);
      }
      fprintf (stream_out, "\n");

      initialState = 1;         /* mark initial state: written */
      i++;
   }

   for (; i < length; i++) {
      fprintf (stream_out, "%d\t%d\t", begin + i, events_list[begin + i - 1]);

      for (j = 0; j < nb_queues; j++) {
         fprintf (stream_out, "%d ", sub_trajectory[i][j]);
      }

      fprintf (stream_out, "\n");
   }
}
