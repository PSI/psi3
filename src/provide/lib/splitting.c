 /**
 * \file      splitting.c
 * \author    Florian LEVEQUE
 * \author      Jean-Marc.Vincent@imag.fr
 * \version   1.0
 * \date      10/07/2012
 * \brief     Contains function for splitting 
 */

 /* 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/
 
 
#include <stdio.h> 
#include <stdlib.h>
#include <limits.h>
#include <psi.h>

extern int nb_queues;


/**
 * \brief	Check the number of states included in the envelop 
 * \details    	Calculate the number of state include in the envelop and check if 
 *				this number is lower than the split threshold
 * \param    	state		Vector of states of queues
 * \param    	int		Upper bound of initial split state number
 * \return   	An \e int representing the number of state after the splitting 
 *				or 0 if it is not possible to split
 */
int check_splitting(T_state* states, int splitThreshold){
    int nbTraj=1;
    int i=0;
     for (i=0; i<nb_queues && nbTraj<=splitThreshold; i++){ 
	  nbTraj *= (states->queues[1][i]-states->queues[0][i]+1);
    }
    //Check if we have not exceed the threshold and return  
    return (i==nb_queues && nbTraj<=splitThreshold) * nbTraj;
}

/**
 * \brief	Initialize structure for splitting 
 * \details    	Save all the permutations 
 * \param    	state         Vector of states of queues
 * \param    	int         Number of state 
 * \return   	An \e T_state* representing new state updated.
 */
T_state* init_splitting(T_state* states, int size_splitting){
    int current_nb_state,value,cmpt,min_states,max_states,i=0,j=0;
    states->splitting = 1;
    current_nb_state = 1;
    for (j=0; j<nb_queues; j++){
	    min_states=states->queues[0][j];
	    max_states=states->queues[1][j];
	    value = min_states;
	    cmpt=0;
	    for (i=0; i<size_splitting; i++) {
		    states->queues[i][j] = value;
		    cmpt++;
		    if (cmpt == current_nb_state) {
			    cmpt = 0;
			    if (value == max_states) value = min_states;
			    else  value++;
		    }
	    }
	    current_nb_state = current_nb_state*(max_states -min_states + 1);		
    }
    states->nb_vectors=size_splitting;
    return states;
}
