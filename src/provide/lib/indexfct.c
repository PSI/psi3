 /**
 * \file      	indexfct.c
 * \author    	Florian LEVEQUE
 * \author      Jean-Marc.Vincent@imag.fr
 * \version   	1.0
 * \date      	10/07/2012
 * \brief     	Contains functions to calculate index for routing 
 */

 /* 
  * This library is free software; you can redistribute it and/or
  * modify it under the terms of the GNU Lesser General Public
  * License as published by the Free Software Foundation; either
  * version 2.1 of the License, or any later version.
  * 
  * This library is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  * Lesser General Public License for more details.
  * 
  * You should have received a copy of the GNU Lesser General Public
  * License along with this library; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
  */

#include <stdio.h>
#include <psi.h>

#define MIN_VAL -1.0
#define MIN_MIN_VAL -2.0
#define MAX_VAL 10000.0
#define MAX_MAX_VAL 1000.0

#define THR 1
#define THR 1

/* Overload/Under-load Threshold for each hierarchy level*/
#define THR_1 1
#define THR_2 3
#define THR_3 5

/*pre-defined index values for each hierarchy level*/
#define IDX_1 3.0
#define IDX_2 2.0
#define IDX_3 1.0

extern int nb_queues;

double fct0[1] = { 11. };
double fct1[12] = { 0., 3., 4., 5., 6., 7., 8., 9., 10., 11., 12., 13. };
double fct2[12] = { 1., 2., 3., 4., 5., 6., 7., 8., 9., 10., 11., 12. };
double fct3[11] = { 0., 1., 2., 3., 4., 5., 6., 7., 8., 9., 10. };
double fct4[11] = { 0., 1., 2., 3., 4., 5., 6., 7., 8., 9., 10. };
double fct5[11] = { 0.1, 1.1, 2.1, 3.1, 4.1, 5.1, 6.1, 7.1, 8.1, 9.1, 10.1 };
double fct6[11] = { 0.2, 1.2, 2.2, 3.2, 4.2, 5.2, 6.2, 7.2, 8.2, 9.2, 10.2 };

double
index0 (int value)
{
   return fct0[value];
}

double
index1 (int value)
{
   return fct1[value];
}

double
index2 (int value)
{
   return fct2[value];
}

double
index3 (int value)
{
   return fct3[value];
}

double
index4 (int value)
{
   return fct4[value];
}

double
index5 (int value)
{
   return fct5[value];
}

double
index6 (int value)
{
   return fct6[value];
}

double
ws_index (int num_ori, int num_cible, int dist, int ch_cible, int *rand_permut)
{
   if (num_ori == num_cible) {
      if (ch_cible < THR)
         return MIN_VAL;
      else
         return MAX_VAL;

   }
   else {                       // num_ori != num_cible
      if (ch_cible <= THR || dist > 2)
         return MIN_MIN_VAL;
      else
         return (double) ch_cible + (double) num_cible / nb_queues;
   }
}


double
ws_index2 (int num_ori, int num_cible, int dist, int ch_cible, int *priority)
{

   if (num_ori == num_cible) {
      if (ch_cible < THR)
         return MIN_VAL;
      else
         return MAX_VAL;

   }
   else {                       // num_ori != num_cible
      if (ch_cible <= THR)
         return MIN_MIN_VAL;
      else
         return (double) nb_queues - (double) priority[num_cible];
   }
}

double
ws_index3 (int num_ori, int num_cible, int dist, int ch_cible, int *rand_permut)
{
   if (num_ori == num_cible) {
      if (ch_cible > THR)
         return MAX_VAL;
      else
         return MIN_VAL;

   }
   else {                       // num_ori != num_cible
      if (ch_cible >= THR)
         return MAX_MAX_VAL;
      else
         return (double) rand_permut[num_cible];
   }
}




double
wsindex4 (int num_ori, int num_cible, int dist, int ch_cible, int *priority)
{
   if (num_ori == num_cible) {
      if (ch_cible < THR_1)
         return MIN_VAL;
      else
         return MAX_VAL;

   }
   else {                       // num_ori != num_cible
      if (dist == 2 && ch_cible > THR_1)
         return IDX_1 + (double) num_cible / nb_queues;

      else if (dist == 4 && ch_cible > THR_2)
         return IDX_2 + (double) num_cible / nb_queues;

      else if (dist == 6 && ch_cible > THR_3)
         return IDX_3 + (double) num_cible / nb_queues;

      else
         return MIN_MIN_VAL;
   }
}
