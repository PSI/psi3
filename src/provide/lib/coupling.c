 /**
 * \file    	coupling.c
 * \author		Florian LEVEQUE 
 * \author      Jean-Marc.Vincent@imag.fr 
 * \version   	1.0
 * \date      	10/07/2012
 * \brief       Contains default reward function
 */

 /* 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include <stdio.h>
#include <psi.h>

extern int nb_queues;


/**
 * \brief      Check if the queues have coupled for backward monotone 
 * \details    Check if the queues have coupled, save this time and check if
 *             all queues have coupled 
 * \param      states         (vector of) states of queues
 * \param      log2stop_time  time iteration (= current simulation position in
 *                            states vector)
 * \param      reward         save the coupling time for each queues
 * \param      state_saved    save the state at coupling time
 * \return     int            representing if all queues have coupled:
 *                               0 means no
 *                               other means yes.
 */
short int coupling (T_state *states,
                  int log2stop_time,
                  int *reward,
                  int *state_saved)
{
   int i = 0;
   int i_boucle, val_test;

   /* check if i queue has coupled */
   for (i = 0; i < nb_queues; i++) {
      /* because of monotony we check two trajectories (queue[0/1]) */
      if (!reward[i] && (states->queues[0][i] == states->queues[1][i])) {
         *(reward + i) = log2stop_time;
         *(state_saved + i) = states->queues[0][i];
      }
   }

   val_test = 1;

   /* check if all queues have coupled */
   for (i_boucle = 0; i_boucle < nb_queues; i_boucle++) {
      val_test = val_test && reward[i_boucle];
   }

   return val_test;   
}

/**
 * \brief      Check if the queues have coupled for backward monotone 
 * \details    Check if the queues have coupled, save this time and check if
 *             all queues have coupled 
 * \param      states         (vector of) states of queues
 * \param      log2stop_time  time iteration (= current simulation position in
 *                            states vector)
 * \param      reward         save the coupling time for each queues
 * \param      state_saved    save the state at coupling time
 * \return     int            representing if all queues have coupled:
 *                               0 means no
 *                               other means yes.
 */
short int reward_ENV (T_state *states,
                      int log2stop_time,
                      int *reward,
                      int *state_saved)
{
	int i = 0;
	int i_boucle, val_test;

   for (i = 0; i < nb_queues; i++) {
      if (!reward[i] && (states->queues[0][i] == states->queues[1][i])) {
         *(reward+i) = log2stop_time;
         *(state_saved+i) = states->queues[0][i];
      }
   }

   val_test = 1;

   for (i_boucle = 0; i_boucle < nb_queues; i_boucle++) {
      val_test = val_test && reward[i_boucle];
   }

   return val_test;   
}

/**
 * \brief      Check if the queues have coupled for backward envelop splitting
 * \details    Check if the queues have coupled, save this time and check if all queues have coupled or if there is only one remaining queue (after splitting)
 * \param    state		Vector of states of queues
 * \param    log2stop_time	state of queues (trajectory).
 * \param    reward		save the coupling time 
 * \param    state_saved	save the state at coupling time
 * \return   \e int representing if all queues have coupled, 0 means no, other means yes.
 */
short int reward_ENV_SPLIT(T_state *states,
                           int log2stop_time,
                           int *reward,
                           int *state_saved)
{
	int i = 0;
	int i_boucle, val_test;

   for (i = 0; i < nb_queues; i++) {
      //check if i queue has coupled or if there is only one remaining queue
      if (!reward[i]
            && (states->nb_vectors == 1 
            || (states->nb_vectors == 2
            && states->queues[0][i] == states->queues[1][i])))
      {
         *(reward+i) = log2stop_time;
         *(state_saved+i) = states->queues[0][i];
      }
   }

   val_test = 1;

   for (i_boucle = 0; i_boucle < nb_queues; i_boucle++) {
      val_test=val_test && reward[i_boucle];
   }

   return val_test;   
}
