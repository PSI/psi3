 /**
 * \file      	transition.c
 * \author    	Minh Quan HO
 * \author		Florian LEVEQUE
 * \author      Jean-Marc.Vincent@imag.fr
 * \version   	1.0
 * \date       	01/07/2012
 * \brief       Contains default transition functions
 */

 /* 
  * This library is free software; you can redistribute it and/or
  * modify it under the terms of the GNU Lesser General Public
  * License as published by the Free Software Foundation; either
  * version 2.1 of the License, or any later version.
  * 
  * This library is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  * Lesser General Public License for more details.
  * 
  * You should have received a copy of the GNU Lesser General Public
  * License along with this library; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
  */

#include <stdio.h>
#include <limits.h>
#include <string.h>
#include <stdlib.h>
#include <psi.h>

#define max(a,b) a>b?a:b
#define min(a,b) a<b?a:b



/* Declare here what will be used for transition functions:  */
extern int FLAG;
//extern int *capmins;            // capacity min of queues
//extern int *capmaxs;            // capacity max of queues 
extern int nb_queues;           // Number of queues
extern int **node_dist;         // Distance matrix
extern int ls_mode;             // Load Sharing mode
extern struct st_neighbors *node_neighbors;  // Neighbors calculated with Load
                                             // Sharing
/***********************************************************/
int *candidates;
int *priority;
int *cur_neighbors;

// if a destination negative, compute new state and return directly
// ALL | DROP 
int *
destination_negative (int dest, int *state, st_model_ptr model)
{
   int i;
   switch (dest) {
   case (ALL):{
         for (i = 0; i < nb_queues; i++) {
            if (state[i] < model->queues->maxs[i])
               state[i]++;
         }
      }
      break;
   default:
      break;
   }
   return state;
}

/**
 * \brief      Compute a new state on an external arrival 
 * \details    Try to send to the first available queue. Reject if destination
 *             queues are full : no change
 *
 * \param      state    Vector of states of queues
 * \param      event    event parameters
 * \param      model    the model
 *
 * \return     An \e int* representing new state updated.
 */
T_state *
ext_arrival_reject (T_state * state, T_event * event, st_model_ptr model)
{
   int k = 0, i = 0;

   while (i < state->nb_vectors) {  // for all vectors in T_state 
      k = 0;

      do {
         if (event->to[k] < 0) { // if destination is negative (outside or all)
            state->queues[i] =
               destination_negative (event->to[k], state->queues[i], model);
            break;
         }

         /* packet accepted by the queue */
         if (state->queues[i][event->to[k]] < model->queues->maxs[event->to[k]]) {
            state->queues[i][event->to[k]] = state->queues[i][event->to[k]] + 1;
            break;
         }
         k++;                   // if not accepted, goto next queue

      } while (k < event->nb_queues_dest);

      i++;
   }
   return state;
}

/**
 * \brief      Compute a new state on an external departure
 * \details    Try to remove a packet from given queues.
 *
 * \param      state    Vector of states of queues
 * \param      event    event parameters
 * \param      model    the model
 *
 * \return     An \e int* representing new state updated.
 */
T_state *
ext_departure (T_state * state, T_event * event, st_model_ptr model)
{
   int i, j = 0;
   for (j = 0; j < state->nb_vectors; j++) { // for all vectors in T_state 

      for (i = 0; i < event->nb_queues_origin; i++) {

         if (state->queues[j][event->from[i]] > 0) // queue not empty
            state->queues[j][event->from[i]] =
                        state->queues[j][event->from[i]] - 1; /* remove 1
                                                               * client from
                                                               * queue */
      }
   }
   return state;
}


/**
 * \brief      Routing n queues from origin queue to destination queues
 * \details    Remove 1 packet from each origin queue and send to destination
 *             queues. Reject if destination queues are full.
 *
 * \param      state    Vector of states of queues
 * \param      event    event parameters
 * \param      model    the model
 *
 * \return     An \e int* representing new state updated.
 */
T_state *
routing_n_queues_reject (T_state * state, T_event * event, st_model_ptr model)
{
   int i;
   int j = 0;
   int k = 0;
   while (k < state->nb_vectors) {  // for all vectors in T_state 
      for (j = 0; j < event->nb_queues_origin; j++) {
         if (state->queues[k][event->from[j]] > 0) {  // if queue not empty
            // remove 1 of origin queue
            state->queues[k][event->from[j]]--;
            // send to others queues
            i = 0;
            do {
               if (event->to[i] < 0) {
                  state->queues[k] =
                     destination_negative (event->to[i], state->queues[k], model);
                  break;
               }
               if (state->queues[k][event->to[i]] <
                     model->queues->maxs[event->to[i]]) { /* packet accepted */

                  state->queues[k][event->to[i]]++;
                  break;
               }
               i++;             // if not accepted, goto next queue
            } while (i < event->nb_queues_dest);
         }
      }
      k++;
   }
   return state;
}

/**
 * \brief      Routing to the least customer queues 
 * \details    Remove 1 packet from origin queue and send to destination
 *             queues. \e Reject if all destination queues are full
 *
 * \param      state    Vector of states of queues
 * \param      event    event parameters
 * \param      model    the model
 *
 * \return     An \e T_state* representing new state updated.
 */
T_state *
JSQ_rejet (T_state * state, T_event * event, st_model_ptr model)
{
   int min_queue;
   int i = 0, k = 0;
   while (i < state->nb_vectors) {
      k = 0;

      if (*(event->from) >= 0) { // Origin queue != ALL/DROP
         if (state->queues[i][*(event->from)] == 0) { /* Origin queue is empty:
                                                      *  do nothing */
            i++;
            continue;
         }
         else {
            state->queues[i][*(event->from)]--; // remove packet from queue
         }
      }

      /* Research the first queue which can receive the packet */
      /* TODO: BUG on the while condition */
      while (k < event->nb_queues_dest
             && event->to[k] >= 0 /* != drop */
             && state->queues[i][event->to[k]] == model->queues->maxs[event->to[k]]) {
         k++;
      }

      /* If there is no queue which can receive the packet: reject */
      if (event->to[k] < 0 || k >= event->nb_queues_dest - 1) {
         i++;
         continue;
      }

      /* min_queue take value of the first not full queue */
      min_queue = event->to[k];
      k++;
      /* Research a queue with less customers */
      while (k < event->nb_queues_dest) {
         if (event->to[k] >= 0
             && state->queues[i][min_queue] > state->queues[i][event->to[k]]
             && state->queues[i][event->to[k]] < model->queues->maxs[event->to[k]]) {
            min_queue = event->to[k];
         }
         k++;
      }

      /* Add the packet in the queue with less customers in the system */
      if (state->queues[i][min_queue] < model->queues->maxs[min_queue]) {
         state->queues[i][min_queue] += 1;
      }
      i++;
   }
   return state;
}

/**
 * \brief      Routing to a multi server network 
 * \details    Remove 1 packet from origin queue and send to destination
 *             queues. It is like a "cache" queue, it means that when a client
 *             arrives, if all queues (except the last) are busy, he will wait
 *             in the last one. Reject if all destination queues are full.
 *
 * \param      state    Vector of states of queues
 * \param      event    event parameters
 * \param      model    the model
 *
 * \return     An \e T_state* representing new state updated.
 */

T_state *
Depart_multi_serveur (T_state * state, T_event * event, st_model_ptr model)
{
   int i = 0, k = 0;

   while (k < state->nb_vectors) {

      if (event->from[0] < 0 && event->from[1] < 0) {
         k++;
         continue;
      }

      /* If there is 1 client or more in the "cache" queue, we extract one of
       * them */
      if (state->queues[k][event->from[0]] != 0
          && state->queues[k][event->from[1]] != 0) {
         state->queues[k][event->from[0]] -= 1;
      }
      else {
         /* else we check if there is a client in the queue */
         if (state->queues[k][event->from[0]] == 0
             && state->queues[k][event->from[1]] != 0) {
            state->queues[k][event->from[1]] -= 1;
         }
         else {
            /* If it's the case, we extract the client from the queue and we
             * put it in the "cache" queue */
            if (state->queues[k][event->from[0]] != 0
                && state->queues[k][event->from[1]] == 0) {
               state->queues[k][event->from[0]] -= 1;
               state->queues[k][event->from[1]] += 1;
            }
            k++;
            continue;
         }
      }
      // Research in destination queues the first not full queue
      while (i < event->nb_queues_dest && event->to[i] != -1) {
         if (state->queues[k][event->to[i]] < model->queues->maxs[event->to[i]]) {
            state->queues[k][event->to[i]] += 1;
            i = event->nb_queues_dest;
         }
         i++;
      }
      k++;
   }
   return state;
}

/**
 * \brief      Routing n queues from origin queue to destination queues
 * \details    Remove 1 packet from origin queue and send to destination
 *             queues. If all destination queues are full, the packet is not
 *             sent
 *
 * \param      state    Vector of states of queues
 * \param      event    event parameters
 * \param      model    the model
 *
 * \return     An \e T_state* representing new state updated.
 */

T_state *
routage_nfile_bloc (T_state * state, T_event * event, st_model_ptr model)
{
   int i, j, k = 0;

   while (k < state->nb_vectors) {
      i = 0; /* destination queue itarator */
      j = 0; /* source queue iterator */

      if (state->queues[k][event->from[j]] > 0) {  /* if queue not empty: try
                                                    * to send to others queues
                                                    */
         do {

            if (event->to[i] == ALL) {
               state->queues[k] =
                  destination_negative (event->to[i], state->queues[k], model);
               break;
            }

            if (state->queues[k][event->to[i]] <
                  model->queues->maxs[event->to[i]]) { /* packet accepted in
                                                        * queue and remove it
                                                        * from the origin queue
                                                        */
               state->queues[k][event->from[j]]--;
               state->queues[k][event->to[i]]++;
               break;
            }
            i++;                /* if the packet is not accepted, goto next
                                 * queue */
         } while (i < event->nb_queues_dest);
      }

      k++;
   }

   return state;
}

/**
 * \brief      Call Center Departure
 * \details    Remove 1 packet from the first not empty queue
 *
 * \param      state    Vector of states of queues
 * \param      event    event parameters
 * \param      model    the model
 *
 * \return     An \e T_state* representing new state updated.
 */

T_state *
Depart_call_center (T_state * state, T_event * event, st_model_ptr model)
{
   int i = 0, k = 0;

   while (k < state->nb_vectors) {

      /* Research the first not empty queue */
      while (i < event->nb_queues_origin && event->from[i] >= 0
             && state->queues[k][event->from[i]] == 0) {
         i++;
      }

      /* Check if we are in a queue of the system */
      if (event->from[i] >= 0) {
         /* Check if the choosen queue is not empty */
         if (state->queues[k][event->from[i]] > 0) {
            state->queues[k][event->from[i]] -= 1;
         }
      }
      k++;
   }
   return state;
}

/**
 * \brief      Routing to the least customer queues according to index tables 
 * \details    Remove 1 packet from origin queue and send to destination
 *             queues. Reject if all destination queues are full.
 *
 * \param      state    Vector of states of queues
 * \param      event    event parameters
 * \param      model    the model
 *
 * \return     An \e T_state* representing new state updated.
 */

T_state *
Reject_Index_Arrival (T_state * state, T_event * event, st_model_ptr model)
{
   int min_queue, j;
   int k = 0, i;

   for (i = 0; i < state->nb_vectors; i++) {
      k = 0;
      j = 0;

      // If the first queue is a queue of the system (not the outside...)
      if (*(event->from) >= 0) {
         // Check if the queue is empty 
         if (state->queues[i][*(event->from)] > 0) {
            // If it's not empty, a client/packet leaves the original queue
            state->queues[i][*(event->from)] -= 1;
         }
         else {
            // If the queue is empty we pass to the next vector 
            continue;
         }
      }
      // Research the first, not drop, not full queue in the list of destination 
      while (k < event->nb_queues_dest && event->to[k] > 0 /* != drop */
             && state->queues[i][event->to[k]] == model->queues->maxs[event->to[k]]) {
         k++;
      }
      if (event->to[k] < 0 || k == event->nb_queues_dest) {
         continue;
      }
      // min_queue has the value of the first not empty queue
      min_queue = event->to[k];
      j = k;
      // Research if there is a better queue according to the index function
      while (k < event->nb_queues_dest) {
         // if the min queue is NOT the "outside"
         if (min_queue >= 0) {
            // if the current queue is better according to the index function
            if (event->to[k] != -1
                && (*((T_ptr_indexfct) event->indexFctDest[k]))
                                          (state-> queues[i] [event->to[k]])
                < (*((T_ptr_indexfct) event->indexFctDest[j]))
                                          (state-> queues[i] [min_queue])
                && (state->queues[i][event->to[k]] < model->queues->maxs[event->to[k]])) {
               // min_queue takes the value of this queue
               min_queue = event->to[k];
               j = k;
            }
            // if the current queue is the "outside"
            if (event->to[k] == -1
                && (*((T_ptr_indexfct) event->indexFctDest[k])) (0) <
                ((*((T_ptr_indexfct) event->indexFctDest[j]))
                 (state->queues[i][min_queue])) + 1) {
               min_queue = -1;
            }
         }                      // if the min queue is the "outside"
         else {
            // if the current queue is better than the min queue
            if ((*((T_ptr_indexfct) event->indexFctDest[k]))
                (state->queues[i][event->to[k]]) <
                (*((T_ptr_indexfct) event->indexFctDest[j])) (0)) {
               min_queue = event->to[k];
               j = k;
            }
         }
         k++;
      };
      // Check if the queue is really not empty
      if ((min_queue != -1) && state->queues[i][min_queue] < model->queues->maxs[min_queue]) {
         state->queues[i][min_queue] = state->queues[i][min_queue] + 1;
      }
   }
   return state;
}

/**
 * \brief      Routing to the least customer queues with index table in multi
 *             server network.
 * \details    Remove 1 packet from origin queue and send to destination
 *             queues. Reject if all destination queues are full.
 *
 * \param      state    Vector of states of queues
 * \param      event    event parameters
 * \param      model    the model
 *
 * \return     An \e T_state* representing new state updated.
 */

T_state *
Reject_Multi_Server_Index_Arrival (T_state * state, T_event * event, st_model_ptr model)
{
   int i = 0, j = 0, k = 0;
   int sum_state = 0, sum_capacity = 0;
   int nb_queues_grp;           // Number of queues for each group
   int index_min_queue = -1;
   double min_value = 100000000.;   // minimum state groups 
   int nb_queue_min_grps;
   int nb_queues_total = 0;     // group counter
   int error = 0;

   while (k < state->nb_vectors) {
      // If the first queue is a queue of the system 
      if (*(event->from) != -1) {
         // Check if the queue is empty 
         if (state->queues[k][*(event->from)] > 0) {
            // If it's not empty, a client leave the original queue
            state->queues[k][*(event->from)] -= 1;
         }
         else {
            // If the queue is empty we pass to the next vector 
            k++;
            continue;
         }
      }

      i = 0;
      j = 0;
      error = 0;
      nb_queues_grp = -1;       // Number of queues for each group
      index_min_queue = 0;
      min_value = 100000000.;   // minimum state groups 
      nb_queue_min_grps = -1;
      nb_queues_total = 0;      // group counter

      do {
         if (i < event->nb_params) {
            nb_queues_grp = atoi (event->param[i]);
            if ((nb_queues_grp <= 0 && nb_queues_grp != -1)
                || nb_queues_total + nb_queues_grp > event->nb_queues_dest) {
               fprintf (stderr, "There is a problem in event definition, parameters are "
                        "too big\n");
               error = 1;
               break;
            }
         }
         else {
            fprintf (stderr, "There is a problem in event definition, there is not "
                     "enough parameters %d\n", i);
            error = 1;
            break;
         }
         sum_state = 0;
         sum_capacity = 0;
         if (nb_queues_grp > 0) {
            for (j = nb_queues_total; j < nb_queues_total + nb_queues_grp; j++) {
               // accumulation of number of clients in the queues of each group
               if (event->to[j] >= 0) {
                  sum_state += state->queues[k][event->to[j]];
                  // accumulation of number of clients
                  sum_capacity += model->queues->maxs[event->to[j]];
               }
               else {
                  sum_state += 0;
                  // accumulation of number of clients
                  sum_capacity += 1;
               }
            }
         }
         else {
            sum_state += 0;
            // accumulation of number of clients
            sum_capacity += 1;
         }
         // if the group can receive packet 
         if (sum_state < sum_capacity) {

            if (i < event->nb_fct_index_dest) {
               // if the current group has smaller value 
               if ((*((T_ptr_indexfct) event->indexFctDest[i])) (sum_state) <
                   min_value) {

                  min_value =
                     (*((T_ptr_indexfct) event->indexFctDest[i])) (sum_state);
                  nb_queue_min_grps = nb_queues_grp;
                  index_min_queue = nb_queues_total;
               }
            }
            else {
               fprintf (stderr, "There is a problem in event definition, there are "
                        "not enough functions \n");
               error = 1;
               break;
            }
         }
         i++;
         nb_queues_total += nb_queues_grp;
         // Travel all the groups to find out the destination group and the
         // final queue in this group
      } while (nb_queues_grp > 0 && nb_queues_total < event->nb_queues_dest);

      // if an error occured, we pass on the next vector
      if (error) {
         k++;
         continue;
      }
      if (nb_queues_total > event->nb_queues_dest) {
         fprintf (stderr, "There is a problem in event definition,Number of groups does "
                  "not correspond to number of queues \n");
         k++;
         continue;
      }
      else {
         if (nb_queue_min_grps < 0) {
            k++;
            continue;
         }
         else {                 // A index multi server is found 
            // Research of the first not full queue
            for (j = index_min_queue; j < index_min_queue + nb_queue_min_grps;
                 j++) {
               if (event->to[j] >= 0
                   && state->queues[k][event->to[j]] < model->queues->maxs[event->to[j]]) {
                  state->queues[k][event->to[j]] += 1;
                  break;
               }
               if (event->to[j] < 0) {
                  break;
               }
            }
         }
      }
      k++;
   } /* while */

   return state;
}


int
get_elem (int mode, int a, int b)
{

   int elem;

   if (mode) {                  /* Tree */
      elem = node_dist[max (a, b)][min (a, b)];
   }
   else {                       /* Matrix */
      elem = node_dist[a][b];
   }

   return elem;
}

/**
 * \brief      Stealing a task on another target queue
 * \details    Choose a queue where steals a task according to index function
 *             and a load sharing hierarchy
 *
 * \param      state    Vector of states of queues
 * \param      event    event parameters
 * \param      model    the model
 *
 * \return     An \e T_state* representing new state updated.
 */

T_state *
Independent_Pull (T_state * state, T_event * event, st_model_ptr model)
{

   int i, j, next_val, max_value, target_num;   // for index argmax calculation
   int nb_candidates;
   int rand_candidate;          // a randomly choosen candidate

   if (node_dist == NULL) {
      // fprintf(stderr, "Initialise Load Sharing structure\n");
      return state;
   }
   // Recover the number probe limit
   if (event->nb_params >= 1) {
      event->nb_rand = atof (*(event->param));
      if (event->nb_rand <= 0) {
         fprintf (stderr, "Problems in the parameters of the independant pull event");
         return state;
      }
   }
   if (event->nb_rand > nb_queues) {
      fprintf (stderr, "Warning : the prob limit is greater than the number of queue\n");
   }
   /* allocation of data structures if never allocated by some other event
    * before */
   if (candidates == NULL)
      candidates = (int *) malloc (nb_queues * sizeof (int));
   if (priority == NULL)
      priority = (int *) malloc (nb_queues * sizeof (int));
   /******************************************************************************/

   /* initialization of random parameter */
   if (event->rand_par == NULL) {
      event->rand_par = (int *) malloc (event->nb_rand * sizeof (int));
      for (i = 0; i < event->nb_rand; i++) {
         event->rand_par[i] = random ();
      }
   }
   /*************************************/
   j = 0;
   while (j < state->nb_vectors) {
      nb_candidates = nb_queues;
      for (i = 0; i < nb_queues; i++) {
         priority[i] = INFINITE_PRIORITY;
      }
      for (i = 0; i < nb_candidates; i++) {
         candidates[i] = i;
      }
      nb_candidates--;
      candidates[*(event->from)] = candidates[nb_candidates]; /* origin queue
                                                               * is removed
                                                               * from the set
                                                               * of candidates
                                                               **/


      /* allocation of a priority to a sub-set of candidate of size nb_rand */
      i = 0;
      while (i < event->nb_rand && nb_candidates >= 0) {

         rand_candidate =
            (int) ((double) event->rand_par[i] / (double) RAND_MAX *
                   nb_candidates);
         priority[candidates[rand_candidate]] = i;

         nb_candidates--;
         candidates[rand_candidate] = candidates[nb_candidates];

         i++;
      }
      /************************************************************************/
      /* calculation of the target for a task theft by index functions */
      if (event->nb_fct_index_origin < 1) {
         fprintf (stderr, "Problem in definition of index function of independent pull "
                  "event\n");
         break;
      }
      target_num = 0;
      max_value =
         (*((T_ptr_indexfct) * (event->indexFctOrigin)))
               (*(event->from),
                0,
                get_elem (ls_mode, *(event-> from), 0),
                state->queues[j][0],
                priority);

      for (i = 1; i < nb_queues; i++) {
         next_val =
            (*((T_ptr_indexfct) * (event->indexFctOrigin)))
               (*(event->from),
                i,
                get_elem (ls_mode, *(event-> from), i),
                state-> queues[j][i],
                priority);

         if (next_val > max_value) {
            max_value = next_val;
            target_num = i;
         }
      }
      /************************************************************************/

      /* the origin steals a task on the target */
      if (target_num != *(event->from)) {
         state->queues[j][target_num] -= 1;
         state->queues[j][*(event->from)] += 1;
      }
      j++;
   }
   return state;
}

/**
 * \brief      Giving a trask on another target queue
 * \details    The origin queue tries to transfer a task from itself to another
 *             queue according to index functions and a load sharing hierarchy
 *
 * \param      state    Vector of states of queues
 * \param      event    event parameters
 * \param      model    the model
 *
 * \return     An \e T_state* representing new state updated.
 */

T_state *
Independent_Push (T_state * state, T_event * event, st_model_ptr model)
{

   int i, j, next_val, min_value, target_num;   // for index argmax calculation

   int nb_candidates;           // to manage a set of potential target with the 
                                // global int[] candidates array
   int rand_candidate;          // a randomly choosen candidate

   if (node_dist == NULL) {
      return state;
   }
   if (event->nb_params == 1) {
      event->nb_rand = atof (*(event->param));
      if (event->nb_rand <= 0) {
         fprintf (stderr, "Problems in the parameters of the independant pull event");
         return state;
      }
   }
   if (event->nb_rand > nb_queues) {
      fprintf (stderr, "Warning : the prob limit is greater than the number of queue\n");
   }
   /* allocation of data structures if never allocated by some other event
      before */
   if (candidates == NULL)
      candidates = (int *) malloc (nb_queues * sizeof (int));
   if (priority == NULL)
      priority = (int *) malloc (nb_queues * sizeof (int));
   /******************************************************************************/

   /* initialization of random parameter */
   if (event->rand_par == NULL) {
      event->rand_par = (int *) malloc (event->nb_rand * sizeof (int));
      for (i = 0; i < event->nb_rand; i++) {
         event->rand_par[i] = random ();
      }
   }
   /*************************************/
   j = 0;
   while (j < state->nb_vectors) {
      nb_candidates = nb_queues;
      for (i = 0; i < nb_queues; i++) {
         priority[i] = INFINITE_PRIORITY;
      }
      for (i = 0; i < nb_candidates; i++) {
         candidates[i] = i;
      }
      nb_candidates--;
      candidates[*(event->from)] = candidates[nb_candidates];  // origin queues 
                                                               // is removed
                                                               // from the set
                                                               // of candidates


      /* allocation of a priority to a sub-set of candidate of size nb_rand */
      i = 0;
      while (i < event->nb_rand && nb_candidates >= 0) {

         rand_candidate =
            (int) ((double) event->rand_par[i] / (double) RAND_MAX *
                   nb_candidates);
         priority[candidates[rand_candidate]] = i;

         nb_candidates--;
         candidates[rand_candidate] = candidates[nb_candidates];

         i++;
      }
       /***************************************************************************/
      /* calculation of the target for a task theft by index functions
         (index.c) */
      if (event->nb_fct_index_origin < 1) {
         fprintf
            (stderr, "Problem in definition of index function of independent pull event");
         continue;
      }
      target_num = 0;
      min_value =
         (*((T_ptr_indexfct) * (event->indexFctOrigin))) (*(event->from), 0,
                                                          get_elem (ls_mode,
                                                                    *(event->
                                                                      from), 0),
                                                          state->queues[j][0],
                                                          priority);
      for (i = 1; i < nb_queues; i++) {
         next_val =
            (*((T_ptr_indexfct) * (event->indexFctOrigin))) (*(event->from), i,
                                                             get_elem (ls_mode,
                                                                       *(event->
                                                                         from),
                                                                       i),
                                                             state->
                                                             queues[j][i],
                                                             priority);
         if (next_val < min_value) {
            min_value = next_val;
            target_num = i;
         }
      }
       /**************************************************************************/

      /* the origin gives a task on the target */
      if (target_num != *(event->from)) {
         state->queues[j][target_num] += 1;
         state->queues[j][*(event->from)] -= 1;
      }
      j++;
   }
   return state;
}

/**
 * \brief      Giving a task on another target queue when an arrival occurs 
 *
 * \details    Lazy Queue : when a packet arrives to queue A, it tries to
 *             transfer it to another queue B.  if B refuses, A keeps the
 *             packet if A is not full
 *
 * \param      state    Vector of states of queues
 * \param      event    event parameters
 * \param      model    the model
 *
 * \return     An \e T_state* representing new state updated.
 */

T_state *
Arrival_Push (T_state * state, T_event * event, st_model_ptr model)
{

   int i, j, next_val, min_value, target_num;   // for index argmin calculation

   int prev_state_ori;          // to save the origin state before the task
                                // arrival 
   // enables to distinguish the cases etat[num_ori] = capacite_file(num_ori)
   // and etat[num_ori] = capacite_file(num_ori) - 1.

   int nb_candidates;           // for managing a set of potential target
   // with the global int[] candidates array

   int rand_candidate;          // a randomly choosen candidate
   if (node_dist == NULL) {
      fprintf
         (stderr, "Problems in parameters file, Load Sharing is not initialized \n");
      return state;
   }
   if (event->nb_params == 1) {
      event->nb_rand = atof (*(event->param));
      if (event->nb_rand <= 0) {
         fprintf (stderr, "Problems in the parameters of the independant pull event");
         return state;
      }
   }
   if (event->nb_rand > nb_queues) {
      fprintf (stderr, "Warning : the prob limit is greater than the number of queue\n");
   }
   /* allocation of data structures if never allocated by some other event
      before */
   if (candidates == NULL)
      candidates = (int *) malloc (nb_queues * sizeof (int));
   if (priority == NULL)
      priority = (int *) malloc (nb_queues * sizeof (int));
   /***************************************************************************/

   /* initialization of random parameter */
   if (event->rand_par == NULL) {
      event->rand_par = (int *) malloc (event->nb_rand * sizeof (int));
      for (i = 0; i < event->nb_rand; i++) {
         event->rand_par[i] = random ();
      }
   }
   /*************************************/
   j = 0;
   while (j < state->nb_vectors) {
      nb_candidates = nb_queues;
      prev_state_ori = state->queues[j][*(event->from)];

      if (state->queues[j][*(event->from)] < model->queues->maxs[*(event->from)])
         state->queues[j][*(event->from)] += 1; // arrival on the origin queue

      for (i = 0; i < nb_queues; i++) {
         priority[i] = INFINITE_PRIORITY;
      }
      for (i = 0; i < nb_candidates; i++) {
         candidates[i] = i;
      }

      nb_candidates--;
      candidates[*(event->from)] = candidates[nb_candidates];  // num_ori is
                                                               // removed from
                                                               // the set of
                                                               // candidates

      /* allocation of a priority to a sub-set of candidate of size nb_rand */
      i = 0;
      while (i < event->nb_rand && nb_candidates >= 0) {

         rand_candidate =
            (int) ((double) event->rand_par[i] / (double) RAND_MAX *
                   nb_candidates);
         priority[candidates[rand_candidate]] = i;
         nb_candidates--;
         candidates[rand_candidate] = candidates[nb_candidates];
         i++;
      }
      /************************************************************************/

      /* calculation of the target for a task theft by index functions */
      target_num = 0;
      min_value =
         (*((T_ptr_indexfct) * (event->indexFctOrigin)))
            (*(event->from),
             0,
             get_elem (ls_mode, *(event-> from), 0),
             state->queues[j][0],
             priority);

      for (i = 1; i < nb_queues; i++) {
         next_val =
            (*((T_ptr_indexfct) * (event->indexFctOrigin)))
               (*(event->from),
                i,
                get_elem (ls_mode, *(event->from), i),
                state->queues[j][i],
                priority);

         if (next_val < min_value) {
            min_value = next_val;
            target_num = i;
         }
      }
      /************************************************************************/
      /* If the target_num queue is not the origin queue */
      if (target_num != *(event->from)) {

         /* If the target_num queue can receive the packet */
         if (state->queues[j][target_num] < model->queues->maxs[target_num]) {
            state->queues[j][target_num] += 1;

            /* the task doesn't make an overflow on the origin queue */
            if (prev_state_ori < state->queues[j][*(event->from)])
               state->queues[j][*(event->from)] -= 1;
         }
      }
      j++;
   }
   return state;
}

/**
 * \brief      Random cascading to another queue when an arrival occurs 
 * \details    Exterior arrival on the origin queue with random cascading to
 *             the final destination according to the random numbers memorized
 *             in rand_par in event structure (if this random numbers have not
 *             been initialize, they are initialize in the function)
 *
 * \param      state    Vector of states of queues
 * \param      event    event parameters
 * \param      model    the model
 *
 * \return     An \e T_state* representing new state updated.
 */

T_state *
LTM_arrival (T_state * state, T_event * event, st_model_ptr model)
{

   int i, j;                    // loop counters
   int cur_node;                // the current evaluated node in the main loop
   int next_target;             // the next neighbor where we try to push the
                                // arriving task 
   int next_u;                  // the next memorized random number
   int index_u, index_target;   // the next index of next_u, next_target
   int cpt_neighbors;           // variable counting the number of current non
                                // visited neighbors
   j = 0;
   if (node_neighbors == NULL) {
      return state;
   }

   while (j < state->nb_vectors) {

      if (state->queues[j][*(event->from)] != model->queues->maxs[*(event->from)]) {
        /****** initialisation *********/
         cur_node = *(event->from);

         if (cur_neighbors == NULL)
            cur_neighbors = (int *) malloc (nb_queues * sizeof (int));
         // size of cur_neighbors is bounded by the number of queues
         // but we use values of which index are between 0 and cpt_neighbors-1
         if (cur_neighbors == NULL)
            perror ("cur_neighbors allocation");

         memcpy (cur_neighbors, node_neighbors[*(event->from)].neighbors,
                 node_neighbors[*(event->from)].nb_neighbors * sizeof (int));
         cpt_neighbors = node_neighbors[*(event->from)].nb_neighbors;
         index_u = 0;
            /*******************************/
         if (event->rand_par == NULL) {
            for (i = 0; i < cpt_neighbors; i++)
               event->rand_par[index_u] = random ();
         }

         while (cpt_neighbors > 0) {
            next_u = event->rand_par[index_u];
            index_target =
               (int) ((double) next_u / (double) RAND_MAX * cpt_neighbors);
            next_target = cur_neighbors[index_target];
            if (state->queues[j][next_target] < state->queues[j][cur_node]) {
               cur_node = next_target;
               cpt_neighbors = node_neighbors[cur_node].nb_neighbors;
               memcpy (cur_neighbors, node_neighbors[cur_node].neighbors,
                       node_neighbors[cur_node].nb_neighbors * sizeof (int));
            }
            else {              // state->queues[next_target] >=
                                // state->queues[cur_node]
               cpt_neighbors--;
               cur_neighbors[index_target] = cur_neighbors[cpt_neighbors];
            }
            index_u++;
         }

         event->nb_rand = index_u;
            /*** update of the load of cur_node ***/
         state->queues[j][cur_node] += 1;

            /**************************************/
      }
      j++;
   }
   return state;
}

/**
 * \brief      Random cascading to another queue when a departure occurs 
 * \details    Departure on the origin queue with random cascading from the
 *             final destination according to the random numbers memorized in
 *             rand_par in event structure (if this random numbers have not
 *             been initialize, they are initialize in the function)
 *
 * \param      state    Vector of states of queues
 * \param      event    event parameters
 * \param      model    the model
 *
 * \return     An \e T_state* representing new state updated.
 */
T_state *
LTM_departure (T_state * state, T_event * event, st_model_ptr model)
{

   int i = 0, j = 0;            // loop counters
   int cur_node;                // the current evaluated node in the main loop
   int next_target;             // the next neighbor where we try to pull the
                                // leaving task 
   int next_u;                  // the next memorized random number
   int index_u, index_target;   // the next index of next_u, next_target
   int cpt_neighbors;           // variable counting the number of current non
                                // visited neighbors
   if (node_neighbors == NULL) {
      return state;
   }
   while (j < state->nb_vectors) {

      if (state->queues[j][*(event->from)] > 0) {  // we have to find a hole
                                                   // where to push the
                                                   // arriving task
          /****** initialisation *********/
         cur_node = *(event->from);

         if (cur_neighbors == NULL)
            cur_neighbors = (int *) malloc (nb_queues * sizeof (int));
         // size of cur_neighbors is bounded by the number of queues
         // but we use values of which index are between 0 and cpt_neighbors-1
         if (cur_neighbors == NULL)
            perror ("cur_neighbors allocation");

         memcpy (cur_neighbors, node_neighbors[*(event->from)].neighbors,
                 node_neighbors[*(event->from)].nb_neighbors * sizeof (int));
         cpt_neighbors = node_neighbors[*(event->from)].nb_neighbors;
         index_u = 0;
          /*******************************/
         if (event->rand_par == NULL) {
            for (i = 0; i < cpt_neighbors; i++)
               event->rand_par[index_u] = random ();
         }

         while (cpt_neighbors > 0) {
            next_u = event->rand_par[index_u];
            index_target =
               (int) ((double) next_u / (double) RAND_MAX * cpt_neighbors);
            next_target = cur_neighbors[index_target];
            if (state->queues[j][next_target] > state->queues[j][cur_node]) {
               cur_node = next_target;
               cpt_neighbors = node_neighbors[cur_node].nb_neighbors;
               memcpy (cur_neighbors, node_neighbors[cur_node].neighbors,
                       node_neighbors[cur_node].nb_neighbors * sizeof (int));
            }
            else {              // etat[next_target] >= etat[cur_node]
               cpt_neighbors--;
               cur_neighbors[index_target] = cur_neighbors[cpt_neighbors];
            }

            index_u++;
         }
         event->nb_rand = index_u;
            /*** update of the load of cur_node ***/
         state->queues[j][cur_node] -= 1;

          /**************************************/
      }
      j++;
   }
   return state;
}


/**
 * \brief      Batch arrival/routing/service 
 * \details    (non decomposable); The batch size is in event parameters
 *
 * \param      state    Vector of states of queues
 * \param      event    event parameters
 * \param      model    the model
 *
 * \return     An \e T_state* representing new state updated.
 */
T_state *
batch (T_state * state, T_event * event, st_model_ptr model)
{
   int k = 0;
   int tmp;
   int flagInf = 0;
   int size = 0;
   if (event->nb_params > 0 && (size = atoi (*(event->param))) <= 0) {
      fprintf (stderr, "Problem with batch size in parameters of the event");
      return state;
   }

   /* For Simple Forward kernels */
   if (state->nb_vectors == 1) {

   }
   else {
      /* For other kernels */
      if (!state->splitting) {
         // If the first queue is different of -1
         if (*(event->from) >= 0) {
            // Check if the Sup queue has at least size clients
            if (state->queues[1][*(event->from)] >= size) { // If it's not
                                                            // empty, a client
                                                            // leave the
                                                            // original queue
               if (state->queues[0][*(event->from)] >= size) {
                  state->queues[0][*(event->from)] -= size;
                  state->queues[1][*(event->from)] -= size;
               }
               else {
                  state->queues[0][*(event->from)] = 0;
                  state->queues[1][*(event->from)] = size - 1;
                  flagInf = 1;  // There is at least one state for which there
                                // is no batch
               }
            }
            else {
               // If the Sup queue doesn't have at least size clients, we
               // return the original states
               return state;
            }
         }
         // Check if destination can receive the clients
         if (*(event->to) >= 0) {
            if (state->queues[1][*(event->to)] + size <= model->queues->maxs[*(event->to)]) {
               if (flagInf == 0)
                  state->queues[0][*(event->to)] += size;
               state->queues[1][*(event->to)] += size;
            }
            else if (state->queues[0][*(event->to)] + size >
                     model->queues->maxs[*(event->to)]) {
            }
            else {
               if (flagInf == 0) {
                  state->queues[0][*(event->to)] += size;
                  tmp = model->queues->maxs[*(event->to)] - size + 1;
                  if (tmp < state->queues[0][*(event->to)])
                     state->queues[0][*(event->to)] = tmp;
               }
               state->queues[1][*(event->to)] = model->queues->maxs[*(event->to)];
            }
         }

      }
      else {
         while (k < state->nb_vectors) {
            if (*(event->from) != -1) {
               // Check if the queue has at least size clients
               if (state->queues[k][*(event->from)] >= size) {
                  // If yes, size clients leave the original queue
                  state->queues[k][*(event->from)] -= size;
               }
            }
            if ((*(event->to) != -1)
                && (state->queues[k][*(event->to)] + size <=
                    model->queues->maxs[*(event->to)]))
               state->queues[k][*(event->to)] += size;
            k++;
         }
      }

   }

   return state;
}

int
inf_batch (int x, int y, int C, int k)
{

   if (y + k <= C)
      return x + k;
   else if (x + k > C)
      return x;
   else if (x + k <= C - k + 1)
      return x + k;
   else
      return C - k + 1;
}

int
sup_batch (int x, int y, int C, int k)
{

   if (y + k <= C)
      return y + k;
   else if (x + k > C)
      return y;
   else
      return C;
}

/**
 * \brief      Batch index routing with rejection
 * \details    If the batch cannot be accepted due to the finite capacity (no
 *             partial acceptance) . The batch size is in event parameters
 *
 * \param      state    Vector of states of queues
 * \param      event    event parameters
 * \param      model    the model
 *
 * \return     An \e T_state* representing new state updated.
 */

T_state *
batch_index_routing (T_state * state, T_event * event, st_model_ptr model)
{
   int minInf, minSup, jMinInf, jMinSup;
   int k;
   int b, flag, val, i, j = 0, tmp;
   int *etatInfOld = NULL, *etatSupOld = NULL;
   int flagInf = 0;
   int size = 0;
   if (event->nb_params > 0) {
      size = atoi (*(event->param));
      if (size <= 0) {
         fprintf (stderr, "Problem with batch size in parameters of the event");
         return state;
      }
   }
   else {
      fprintf (stderr, "There is no batch size in parameters");
      return state;
   }
   if (state->splitting) {
      while (j < state->nb_vectors) {
         // Create copy of Inf 
         etatInfOld = (int *) calloc (nb_queues, sizeof (int));

         for (i = 0; i < nb_queues; i++) {
            etatInfOld[i] = state->queues[j][i];
         }

         // If the first queue is different of -1
         if (*(event->from) != -1) {
            // Check if the queue has at least size clients
            if (etatInfOld[*(event->from)] >= size) {
               // If yes, size clients leave the original queue
               state->queues[j][*(event->from)] -= size;
            }
            else {
               // If the queue has less than size clients, we return the
               // original state
               return state;
            }
         }

         // minInf has the value of the first queue with minimal value of index 
         // function
         k = 0;
         minInf = event->to[k];
         jMinInf = k;
         while (k < event->nb_queues_dest) {
            if (event->to[k] != -1 &&
                (*((T_ptr_indexfct) event->indexFctDest[k])) (etatInfOld
                                                              [event->to[k]])
                <
                (*((T_ptr_indexfct) event->indexFctDest[jMinInf])) (etatInfOld
                                                                    [minInf])) {
               minInf = event->to[k];
               jMinInf = k;
            }
            if (event->to[k] == -1)
               fprintf
                  (stderr, "Warning: batch index routing ignores -1 as destination\n");
            k++;
         };

         // Check if the batch is not too big
         if ((minInf != -1) && etatInfOld[minInf] + size <= model->queues->maxs[minInf]) {
            state->queues[j][minInf] = etatInfOld[minInf] + size;
         }
         j++;
      }
      free (etatInfOld);
   }
   else {
      // Create copies of Inf and Sup
      etatInfOld = (int *) calloc (nb_queues, sizeof (int));
      etatSupOld = (int *) calloc (nb_queues, sizeof (int));

      for (i = 0; i < nb_queues; i++) {
         etatInfOld[i] = state->queues[0][i];
         etatSupOld[i] = state->queues[1][i];
      }
      // If the first queue is different of -1
      if (*(event->from) != -1) {
         // Check if the Sup queue has at least size clients 
         if (etatSupOld[*(event->from)] >= size) { // If it's not empty, a
                                                   // client leave the original 
                                                   // queue
            if (etatInfOld[*(event->from)] >= size) {
               state->queues[0][*(event->from)] -= size;
               state->queues[1][*(event->from)] -= size;
            }
            else {
               state->queues[0][*(event->from)] = 0;
               state->queues[1][*(event->from)] = size - 1;
               flagInf = 1;     // There is at least one state for which there
                                // is no batch
            }
         }
         else {
            // If the Sup queue doesn't have at least size clients, we return
            // the original states
            return state;
         }
      }

      // minInf (minSup) has the value of the first queue with minimal value of 
      // index function for state Inf (Sup)
      k = 0;
      minInf = event->to[k];
      jMinInf = k;
      minSup = event->to[k];
      jMinSup = k;
      while (k < event->nb_queues_dest) {
         // Inf 
         if (event->to[k] != -1
             &&
             (*((T_ptr_indexfct) event->indexFctDest[k])) (etatInfOld
                                                           [event->to[k]])
             <
             (*((T_ptr_indexfct) event->indexFctDest[jMinInf])) (etatInfOld
                                                                 [minInf])) {
            minInf = event->to[k];
            jMinInf = k;
         }

         // Sup
         if (event->to[k] != -1
             &&
             (*((T_ptr_indexfct) event->indexFctDest[k])) (etatSupOld
                                                           [event->to[k]])
             <
             (*((T_ptr_indexfct) event->indexFctDest[jMinSup])) (etatSupOld
                                                                 [minSup])) {
            minSup = event->to[k];
            jMinSup = k;
         }

         if (event->to[k] == -1)
            fprintf (stderr, "Warning: batch index routing ignores -1 as destination\n");
         k++;
      };

      k = 0;
      while (k < event->nb_queues_dest) {
         if (event->to[k] != -1
             &&
             (*((T_ptr_indexfct) event->indexFctDest[k])) (etatInfOld
                                                           [event->to[k]])
             <
             (*((T_ptr_indexfct) event->indexFctDest[jMinSup])) (etatSupOld
                                                                 [minSup])) {

            // Sup
            if (k == jMinSup) {

               state->queues[1][minSup] =
                  sup_batch (etatInfOld[minSup], etatSupOld[minSup],
                             model->queues->maxs[minSup], size);
            }
            else {

               b = 0;
               flag = 1;
               while (flag) {
                  b++;
                  if ((etatInfOld[event->to[k]] + b > etatSupOld[event->to[k]])
                      ||
                      (*((T_ptr_indexfct) event->indexFctDest[k])) (etatInfOld
                                                                    [event->
                                                                     to[k]] + b)
                      >
                      (*((T_ptr_indexfct) event->indexFctDest[jMinSup]))
                      (etatSupOld[minSup])) {
                     flag = 0;
                     b--;
                  }
               }
               state->queues[1][event->to[k]] =
                  sup_batch (etatInfOld[event->to[k]],
                             etatInfOld[event->to[k]] + b,
                             model->queues->maxs[event->to[k]], size);
               if (state->queues[1][event->to[k]] < etatSupOld[event->to[k]])
                  state->queues[1][event->to[k]] = etatSupOld[event->to[k]];
            }

            // Inf
            if (k == jMinInf && flagInf == 0) {
               j = 0;
               // second smallest
               if (jMinInf != 0)
                  val =
                     (*((T_ptr_indexfct) event->indexFctDest[0])) (etatInfOld
                                                                   [event->
                                                                    to[0]]);
               else
                  val =
                     (*((T_ptr_indexfct) event->indexFctDest[1])) (etatInfOld
                                                                   [event->
                                                                    to[1]]);
               while (j < event->nb_queues_dest) {
                  if (j != jMinInf && event->to[j] >= 0
                      &&
                      (*((T_ptr_indexfct) event->indexFctDest[j])) (etatInfOld
                                                                    [event->
                                                                     to[j]]) <
                      val) {
                     val =
                        (*((T_ptr_indexfct) event->indexFctDest[j])) (etatInfOld
                                                                      [event->
                                                                       to[j]]);
                  }
                  j++;
               }
               if ((*((T_ptr_indexfct) event->indexFctDest[jMinInf]))
                   (etatInfOld[event->to[jMinInf]]) <= val) {
                  state->queues[0][minInf] =
                     inf_batch (etatInfOld[event->to[jMinInf]],
                                etatSupOld[event->to[jMinInf]],
                                model->queues->maxs[event->to[jMinInf]], size);
               }

               else {
                  b = 0;
                  flag = 1;
                  while (flag) {
                     b++;
                     if ((*((T_ptr_indexfct) event->indexFctDest[jMinInf]))
                         (etatInfOld[event->to[jMinInf]] + b) > val) {
                        flag = 0;
                        b--;
                     }
                  }
                  tmp =
                     inf_batch (etatInfOld[event->to[jMinInf]],
                                etatInfOld[event->to[jMinInf]] + b,
                                model->queues->maxs[event->to[jMinInf]], size);
                  if (tmp <= etatInfOld[event->to[jMinInf]] + b + 1)
                     state->queues[0][event->to[jMinInf]] = tmp;
                  else
                     state->queues[0][event->to[jMinInf]] =
                        etatInfOld[event->to[jMinInf]] + b + 1;
               }
            }
         }
         k++;
      }
      free (etatInfOld);
      free (etatSupOld);
   }

   return state;
}

/**
 * \brief      Decomposable batch arrival/routing/service
 * \details    The batch size is in event parameters. 
 *
 * \param      state    Vector of states of queues
 * \param      event    event parameters
 * \param      model    the model
 *
 * \return     An \e T_state* representing new state updated.
 */

T_state *
dec_batch (T_state * state, T_event * event, st_model_ptr model)
{
   int size = 0, j = 0;
   if (event->nb_params > 0) {
      size = atoi (*(event->param));
      if (size <= 0) {
         fprintf (stderr, "Problem with batch size in parameters of the event");
         return state;
      }
   }
   else {
      fprintf (stderr, "There is no batch size in parameters");
      return state;
   }

   while (j < state->nb_vectors) {

      if (*(event->from) != -1) {
         /* Check if the batch is not too big for the origin queue, this queue
          * is cleared */
         if (state->queues[j][*(event->from)] - size >= 0)
            state->queues[j][*(event->from)] -= size;
         else {
            size = state->queues[j][*(event->from)];
            state->queues[j][*(event->from)] = 0;
         }
      }
      /* Check if the batch is not too big for the destination queue, this
       * queue is filled */
      if (*(event->to) != -1) {
         if (state->queues[j][*(event->to)] + size <= model->queues->maxs[*(event->to)])
            state->queues[j][*(event->to)] += size;
         else
            state->queues[j][*(event->to)] = model->queues->maxs[*(event->to)];
      }
      j++;
   }
   return state;
}


/**
 * \brief      Negative customer with routing 
 * \details    negative customer external arrival (source == -1), negative
 *             customer routing (source != dest) or impatience (source == dest)
 *
 * \param      state    Vector of states of queues
 * \param      event    event parameters
 * \param      model    the model
 *
 * \return     An \e T_state* representing new state updated.
 */

T_state *
negative_customer (T_state * state, T_event * event, st_model_ptr model)
{
   // int j =0;

   if (FLAG != 2) {
      fprintf (stderr, "Error: You must use a non-monotone algorithm.\n");
      return state;
   }

   if (*(event->to) == -1) {
      fprintf (stderr, "Error: negative customer destination cannot be exterior.\n");
      return state;
   }

   if (state->splitting) {
      // /* negative customer external arrival (source == -1) or impatience
      // (source == dest) */
      // while (j<state->nb_vectors){
      // if ((*(event->from) == -1) || (*(event->from)==*(event->to))){
      // //simple departure
      // if (state->queues[j][*(event->to)] > 0) state->queues[j][*(event->to)] 
      // = state->queues[j][*(event->to)]-1;
      // }

      // else { /* negative customer routing (source != dest) */
      // if (state->queues[j][*(event->from)] && state->queues[j][*(event->to)] 
      // > 0 ) state->queues[j][*(event->to)]--;
      // if (state->queues[j][*(event->from)] > 0)
      // state->queues[j][*(event->from)]--;
      // }
      // j++;
      // }
   }
   else {
      /* negative customer external arrival (source == -1) or impatience
         (source == dest) */

      if ((*(event->from) == -1) || (*(event->from) == *(event->to))) { // simple 
                                                                        // departure
         if (state->queues[0][*(event->to)] > 0)
            state->queues[0][*(event->to)] = state->queues[0][*(event->to)] - 1;

         if (state->queues[1][*(event->to)] > 0)
            state->queues[1][*(event->to)] = state->queues[1][*(event->to)] - 1;
      }

      else {                    /* negative customer routing (source != dest) */

         if (state->queues[0][*(event->from)] > 0) {  /* negative customer in
                                                         both Inf and Sup state 
                                                       */
            state->queues[0][*(event->from)] =
               state->queues[0][*(event->from)] - 1;
            if (state->queues[0][*(event->to)] > 0)
               state->queues[0][*(event->to)] =
                  state->queues[0][*(event->to)] - 1;

            state->queues[1][*(event->from)] =
               state->queues[1][*(event->from)] - 1;
            if (state->queues[1][*(event->to)] > 0)
               state->queues[1][*(event->to)] =
                  state->queues[1][*(event->to)] - 1;
         }
         else if (state->queues[1][*(event->from)] == 0);   /* negative
                                                               customer in
                                                               neither Inf nor
                                                               Sup state */
         else {                 /* negative customer only in Sup state */
            /* the only case that breaks the monotonicity property */
            state->queues[1][*(event->from)] =
               state->queues[1][*(event->from)] - 1;
            if (state->queues[0][*(event->to)] < state->queues[1][*(event->to)])
               state->queues[1][*(event->to)] =
                  state->queues[1][*(event->to)] - 1;
            else if (state->queues[0][*(event->to)] > 0) {
               state->queues[0][*(event->to)] =
                  state->queues[0][*(event->to)] - 1;
            }
         }
      }
   }
   return state;
}
/* TODO: RELECT FROM HERE */
/**
 * \brief	Pull on Departure 
 * \details    	On a task completion (client departure), the origin queue where the event occurs tries  to steal a task on another queue 
 * according to index functions and load sharing hierarchy. If the queue is empty, no attempt of transfer is done (this is done by the event EmptyQueue_Pull)   
 * \param    	state         Vector of states of queues
 * \param    	event         Event.
 * \return   	An \e T_state* representing new state updated.
 */
T_state *
TF_Departure_Pull (T_state * state, T_event * event, st_model_ptr model)
{


   int i, j = 0, next_val, max_value, target_num;  // for index argmax
                                                   // calculation

   int nb_candidates;           // for managing a set of potential target
   // with the global int[] candidates array
   int rand_candidate;          // a randomly choosen candidate

   if (event->nb_params >= 1) {
      event->nb_rand = atof (*(event->param));
      if (event->nb_rand <= 0) {
         fprintf (stderr, "Problems in the parameters of the independant pull event");
         return state;
      }
   }
   if (event->nb_rand > nb_queues) {
      fprintf (stderr, "Warning : the prob limit is greater than the number of queue\n");
   }
   /* allocation of data structures if never allocated by some other event
      before */
   if (candidates == NULL)
      candidates = (int *) malloc (nb_queues * sizeof (int));
   if (priority == NULL)
      priority = (int *) malloc (nb_queues * sizeof (int));
   /******************************************************************************/

   /* initialization of random parameter */
   if (event->rand_par == NULL) {
      event->rand_par = (int *) malloc (event->nb_rand * sizeof (int));
      for (i = 0; i < event->nb_rand; i++) {
         event->rand_par[i] = random ();
      }
   }

   while (j < state->nb_vectors) {
      nb_candidates = nb_queues;
      if (state->queues[j][*(event->from)] > 0) {  // event application
                                                   // condition

         state->queues[j][*(event->from)] -= 1; // end of service on origin
      /**********************************************************************/
         for (i = 0; i < nb_queues; i++) {
            priority[i] = INFINITE_PRIORITY;
         }

         for (i = 0; i < nb_candidates; i++) {
            candidates[i] = i;
         }

         nb_candidates--;
         candidates[*(event->from)] = candidates[nb_candidates];  // origin
                                                                  // queue is
                                                                  // removed
                                                                  // from the
                                                                  // set of
                                                                  // candidates

         i = 0;
         while (i < event->nb_rand && nb_candidates >= 0) {

            rand_candidate =
               (int) ((double) event->rand_par[i] / (double) RAND_MAX *
                      nb_candidates);
            priority[candidates[rand_candidate]] = i;

            nb_candidates--;
            candidates[rand_candidate] = candidates[nb_candidates];

            i++;
         }


         /* calculation of the target for a task theft by index functions */
         if (event->nb_fct_index_origin < 1) {
            fprintf
               (stderr, "Problem in definition of index function of independent pull event");
            break;
         }
         target_num = 0;
         max_value = (*((T_ptr_indexfct)
                        * (event->indexFctOrigin)))
            (*(event->from),
             0,
             get_elem (ls_mode, *(event->from), 0),
             state->queues[j][0], priority);

         for (i = 1; i < nb_queues; i++) {
            next_val =
               (*((T_ptr_indexfct) * (event->indexFctOrigin))) (*(event->from),
                                                                i,
                                                                get_elem
                                                                (ls_mode,
                                                                 *(event->from),
                                                                 i),
                                                                state->
                                                                queues[j][i],
                                                                priority);
            if (next_val > max_value) {
               max_value = next_val;
               target_num = i;
            }
         }

            /**************************************************/

         /* task theft if the target is not the origin */
         /* the origin steals a task on the target */
         if (target_num != *(event->from)) {
            state->queues[j][target_num] -= 1;
            state->queues[j][*(event->from)] += 1;
         }
      }
      j++;
   }
   return state;
}

/**
 * \brief	Pull on Departure  (NOT Monotone with envelop)
 * \details    	On a task completion (client departure), the origin queue where the event occurs tries  to steal a task on another queue 
 * according to index functions and load sharing hierarchy. If the queue is empty, no attempt of transfer is done (this is done by the event EmptyQueue_Pull)   
 * \param    	state         Vector of states of queues
 * \param    	event         Event.
 * \return   	An \e T_state* representing new state updated.
 */

T_state *
ENV_Departure_Pull (T_state * state, T_event * event, st_model_ptr model)
{

   int i, next_val, max_value;  // for index argmax calculation
   int target_num_Sup;          // the target of the theft for etatSup
   int target_num_NearInf;      // the target of the theft for the state where
                                // all value are equal
   // to etatInf except for the origin queue

   int nb_candidates = nb_queues;   // for managing a set of potential target
   // with the global int[] candidates array
   int rand_candidate;
   if (FLAG != 2) {
      fprintf (stderr, "Error: You must use a non-monotone algorithm.\n");
      return state;
   }
   if (!state->splitting && state->queues[1][*(event->from)] > 0 && state->queues[0][*(event->from)] == 0) {   // envelope 
                                                                                                               // case
      /* initialization of random parameter */
      if (event->nb_params == 1) {
         event->nb_rand = atof (*(event->param));
         if (event->nb_rand <= 0) {
            fprintf (stderr, "Problems in the parameters of the independant pull event");
            return state;
         }
      }
      if (event->nb_rand > nb_queues) {
         fprintf
            (stderr, "Warning : the prob limit is greater than the number of queue\n");
      }
      state->queues[1][*(event->from)] -= 1; // end of service on origin

      /* allocation of data structures if never allocated by some other event
         before */
      if (candidates == NULL)
         candidates = (int *) malloc (nb_queues * sizeof (int));

      if (priority == NULL)
         priority = (int *) malloc (nb_queues * sizeof (int));
      /******************************************************************************/

      for (i = 0; i < nb_queues; i++) {
         priority[i] = INFINITE_PRIORITY;
      }

      for (i = 0; i < nb_candidates; i++) {
         candidates[i] = i;
      }
      if (event->rand_par == NULL) {
         event->rand_par = (int *) malloc (event->nb_rand * sizeof (int));
         for (i = 0; i < event->nb_rand; i++) {
            event->rand_par[i] = random ();
         }
      }
      nb_candidates--;
      candidates[*(event->from)] = candidates[nb_candidates];

      i = 0;
      while (i < event->nb_rand && nb_candidates >= 0) {

         rand_candidate =
            (int) ((double) event->rand_par[i] / (double) RAND_MAX *
                   nb_candidates);
         priority[candidates[rand_candidate]] = i;
         nb_candidates--;
         candidates[rand_candidate] = candidates[nb_candidates];
         i++;
      }

      /* calculation of the target for a task theft by index functions */
      target_num_Sup = 0;
      max_value =
         (*((T_ptr_indexfct) * (event->indexFctOrigin))) (*(event->from), 0,
                                                          get_elem (ls_mode,
                                                                    *(event->
                                                                      from), 0),
                                                          state->queues[1][0],
                                                          priority);

      for (i = 1; i < nb_queues; i++) {

         next_val =
            (*((T_ptr_indexfct) * (event->indexFctOrigin))) (*(event->from), i,
                                                             get_elem (ls_mode,
                                                                       *(event->
                                                                         from),
                                                                       i),
                                                             state->
                                                             queues[1][i],
                                                             priority);
         if (next_val > max_value) {
            max_value = next_val;
            target_num_Sup = i;
         }
      }


      if (target_num_Sup != *(event->from)) {
         state->queues[1][*(event->from)] += 1; // like if a task had been
                                                // stolen from target_num_Sup 
         // uppper envelope let etatSup[target_num_Sup] unchanged

      }

      /* calculation of the target for a task theft by index functions */
      target_num_NearInf = 0;
      if (*(event->from) == 0)
         max_value =
            (*((T_ptr_indexfct) * (event->indexFctOrigin))) (*(event->from), 0,
                                                             get_elem (ls_mode,
                                                                       *(event->
                                                                         from),
                                                                       0), 1,
                                                             priority);
      else
         max_value =
            (*((T_ptr_indexfct) * (event->indexFctOrigin))) (*(event->from), 0,
                                                             get_elem (ls_mode,
                                                                       *(event->
                                                                         from),
                                                                       0),
                                                             state->
                                                             queues[0][0],
                                                             priority);

      for (i = 1; i < nb_queues; i++) {

         if (*(event->from) == i)
            next_val =
               (*((T_ptr_indexfct) * (event->indexFctOrigin))) (*(event->from),
                                                                i,
                                                                get_elem
                                                                (ls_mode,
                                                                 *(event->from),
                                                                 i), 1,
                                                                priority);
         else
            next_val =
               (*((T_ptr_indexfct) * (event->indexFctOrigin))) (*(event->from),
                                                                i,
                                                                get_elem
                                                                (ls_mode,
                                                                 *(event->from),
                                                                 i),
                                                                state->
                                                                queues[0][i],
                                                                priority);

         if (next_val > max_value) {
            max_value = next_val;
            target_num_NearInf = i;
         }
      }

      if (target_num_NearInf != *(event->from)) {
         state->queues[0][target_num_NearInf] -= 1;   // lower envelope
      }

   }
   else {                       // normal case
      state = TF_Departure_Pull (state, event, model);
   }
   return state;
}

/**
 * \brief	Pull on Departure  (associated to TF_Departure_Pull() and ENV_Departure_Pull() to cover the case of pull control when the queue is empty.)
 * \details    	On a task completion (client departure), if the origin queue is idle, it tries to steal a task on another queue 
 * according to index functions and load sharing hierarchy. 
 * \param    	state         Vector of states of queues
 * \param    	event         Event.
 * \return   	An \e T_state* representing new state updated.
 */

T_state *
EmptyQueue_Pull (T_state * state, T_event * event, st_model_ptr model)
{

   int i, j = 0, next_val, max_value, target_num;  // for index argmax
                                                   // calculation

   int nb_candidates = nb_queues;   // for managing a set of potential target
   // with the global int[] candidates array
   int rand_candidate;          // a randomly choosen candidate
   if (event->nb_params >= 1) {
      event->nb_rand = atof (*(event->param));
      if (event->nb_rand <= 0) {
         fprintf (stderr, "Problems in the parameters of the independant pull event");
         return state;
      }
   }
   if (event->nb_rand > nb_queues) {
      fprintf (stderr, "Warning : the prob limit is greater than the number of queue\n");
   }
   /* allocation of data structures if never allocated by some other event
      before */
   if (candidates == NULL)
      candidates = (int *) malloc (nb_queues * sizeof (int));

   if (priority == NULL)
      priority = (int *) malloc (nb_queues * sizeof (int));
   /******************************************************************************/

   /* initialization of random parameter */
   if (event->rand_par == NULL) {
      event->rand_par = (int *) malloc (event->nb_rand * sizeof (int));
      for (i = 0; i < event->nb_rand; i++) {
         event->rand_par[i] = random ();
      }
   }
   j = 0;
   while (j < state->nb_vectors) {
      nb_candidates = nb_queues;
      if (state->queues[j][*(event->from)] == 0) { // event application
                                                   // condition

            /**************************************/
         for (i = 0; i < nb_queues; i++) {
            priority[i] = INFINITE_PRIORITY;
         }
         for (i = 0; i < nb_candidates; i++) {
            candidates[i] = i;
         }

         nb_candidates--;
         candidates[*(event->from)] = candidates[nb_candidates];

         i = 0;
         while (i < event->nb_rand && nb_candidates >= 0) {

            rand_candidate =
               (int) ((double) event->rand_par[i] / (double) RAND_MAX *
                      nb_candidates);
            priority[candidates[rand_candidate]] = i;

            nb_candidates--;
            candidates[rand_candidate] = candidates[nb_candidates];

            i++;
         }


         /* calculation of the target for a task theft by index functions */
         target_num = 0;
         max_value =
            (*((T_ptr_indexfct) * (event->indexFctOrigin))) (*(event->from), 0,
                                                             get_elem (ls_mode,
                                                                       *(event->
                                                                         from),
                                                                       0),
                                                             state->
                                                             queues[j][0],
                                                             priority);
         for (i = 1; i < nb_queues; i++) {

            next_val =
               (*((T_ptr_indexfct) * (event->indexFctOrigin))) (*(event->from),
                                                                i,
                                                                get_elem
                                                                (ls_mode,
                                                                 *(event->from),
                                                                 i),
                                                                state->
                                                                queues[1][i],
                                                                priority);

            if (next_val > max_value) {
               max_value = next_val;
               target_num = i;
            }
         }
            /************************************************************************/

         if (target_num != *(event->from)) {
            state->queues[j][target_num] -= 1;
            state->queues[j][*(event->from)] += 1;
         }
      }
      j++;
   }

   return state;

}

/**
 * \brief	TANDEM QUEUEING NETWORK phase 1
 * \details    	
 * \param    	state         Vector of states of queues
 * \param    	event         Event.
 * \return   	An \e T_state* representing new state updated.
 */

T_state *
TQN_phase1_service (T_state * state, T_event * event, st_model_ptr model)
{
   int j = 0;
   if (event->nb_queues_origin != 2) {
      fprintf (stderr, "Event of TQN_phase1_service event must have 2 origins queues ");
      return state;
   }
   while (j < state->nb_vectors) {
      if (state->queues[j][event->from[1]] == 1
          && state->queues[j][*(event->from)] > 0) {
         state->queues[j][event->from[1]] = 2;
         /* etat[queue1] -= 1; */
      }
      j++;
   }
   return state;
}

/**
 * \brief	TANDEM QUEUEING NETWORK phase 1 skip phase 2
 * \details    	
 * \param    	state         Vector of states of queues
 * \param    	event         Event.
 * \return   	An \e T_state* representing new state updated.
 */
T_state *
TF_TQN_phase1_service_skip_phase2 (T_state * state, T_event * event, st_model_ptr model)
{
   int j = 0;
   if (event->nb_queues_origin != 2) {
      fprintf (stderr, "Event of TQN_phase1_service event must have 2 origins queues ");
      return state;
   }
   while (j < state->nb_vectors) {
      // if(etat[phase] == 1 && etat[queue1] > 0){ // Overflow
      if (state->queues[j][event->from[1]] == 1 && state->queues[j][event->from[0]] > 0 && state->queues[j][*(event->to)] < model->queues->maxs[*(event->to)]) {  // Blocking
         state->queues[j][event->from[0]] -= 1;
         // if(etat[queue2] < capacite_file(queue2)) // Overflow
         state->queues[j][*(event->to)] += 1;
      }
      j++;
   }
   return state;
}

/**
 * \brief	TANDEM QUEUEING NETWORK phase 1 skip phase 2 (Not Monotone)
 * \details    	
 * \param    	state         Vector of states of queues
 * \param    	event         Event.
 * \return   	An \e T_state* representing new state updated.
 */
T_state *
ENV_TQN_phase1_service_skip_phase2 (T_state * state, T_event * event, st_model_ptr model)
{
   if (FLAG != 2) {
      fprintf (stderr, "Error: You must use a non-monotone algorithm.\n");
      return state;
   }
   if (!state->splitting && state->queues[1][event->from[1]] == 2
       && state->queues[0][event->from[1]] == 1
       && state->queues[0][event->from[0]] > 0
       && state->queues[0][*(event->to)] < model->queues->maxs[*(event->to)]) {
      // && etatSup[queue2] == etatInf[queue2]){ // non monotone case

      if (state->queues[1][*(event->to)] < model->queues->maxs[*(event->to)])
         state->queues[1][*(event->to)] += 1;   // Sup env

      state->queues[0][event->from[0]] -= 1;
      // etatInf[queue2] += 1; is not applied // Inf env

   }
   else {
      state = TF_TQN_phase1_service_skip_phase2 (state, event, model);
   }
   return state;
}

/**
 * \brief	TANDEM QUEUEING NETWORK phase 2
 * \details    	
 * \param    	state         Vector of states of queues
 * \param    	event         Event.
 * \return   	An \e T_state* representing new state updated.
 */
T_state *
TF_TQN_phase2_service (T_state * state, T_event * event, st_model_ptr model)
{

   int j = 0;
   while (j < state->nb_vectors) {
      if (state->queues[j][event->from[1]] == 2
          && state->queues[j][*(event->to)] < model->queues->maxs[*(event->to)]) {
         state->queues[j][event->from[1]] = 1;

         if (state->queues[j][event->from[0]] <= 0) {
            fprintf
               (stderr, "Problem in TF_TQN_phase2_service, state of queue %d <= 0 !\n",
                event->from[0]);
            break;
         }
         state->queues[j][event->from[0]] -= 1;

         // if(etat[queue2] < capacite_file(queue2)) // Overflow 
         state->queues[j][event->to[0]] += 1;
      }
      j++;
   }
   return state;
}

/**
 * \brief	TANDEM QUEUEING NETWORK phase 2 (Not monotone)
 * \details    	
 * \param    	state         Vector of states of queues
 * \param    	event         Event.
 * \return   	An \e T_state* representing new state updated.
 */
T_state *
ENV_TQN_phase2_service (T_state * state, T_event * event, st_model_ptr model)
{
   if (FLAG != 2) {
      fprintf (stderr, "Error: You must use a non-monotone algorithm.\n");
      return state;
   }
   // if(etatSup[phase] == 2 && etatInf[phase] == 1 && etatInf[queue1] > 0){ // 
   // non monotone case
   if (!state->splitting && state->queues[1][event->from[1]] == 2 && state->queues[0][event->from[1]] == 1 && state->queues[1][*(event->to)] < model->queues->maxs[*(event->to)]) { // non 
                                                                                                                                                                        // monotone 
                                                                                                                                                                        // case
      state->queues[1][event->from[1]] = 1;

      if (state->queues[0][event->from[0]] > 0)
         state->queues[0][event->from[0]] -= 1; // Inf env

      // if(etatSup[queue2] < capacite_file(queue2)) // Overflow
      state->queues[1][*(event->to)] += 1;
      // etatSup[queue1] -= 1; is not applied // Sup env

   }
   else {
      state = TF_TQN_phase2_service (state, event, model);
   }

   return state;
}
