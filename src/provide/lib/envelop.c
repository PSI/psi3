 /**
 * \file        envelop.c
 * \author      Florian LEVEQUE
 * \author      Jean-Marc.Vincent@imag.fr
 * \version     1.0
 * \date        20/07/2012
 * \brief       Contains default envelop initialize function 
 */

 /* 
  * This library is free software; you can redistribute it and/or
  * modify it under the terms of the GNU Lesser General Public
  * License as published by the Free Software Foundation; either
  * version 2.1 of the License, or any later version.
  * 
  * This library is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  * Lesser General Public License for more details.
  * 
  * You should have received a copy of the GNU Lesser General Public
  * License along with this library; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
  */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <psi.h>

extern int nb_queues;
extern int *capmins;
extern int *capmaxs;

/**
 * \brief      Initialize the state for envelop algorithm 
 * \details    Initialize the state to create an envelop
 *
 * \param      states      state of queues (trajectory)
 * \param      size        Number of vectors
 *
 * \return     void
 */
T_state *
init_envelop (T_state * states, int size)
{
   int i = 0;
   states = (T_state *) malloc (sizeof (T_state));
   states->queues = (int **) malloc (size * sizeof (int *));
   for (i = 0; i < size; i++)
      states->queues[i] = (int *) malloc (nb_queues * sizeof (int));
   states->nb_vectors = size;
   states->splitting = 0;
   return states;
}

/**
 * \brief      Modify the state for envelop algorithm 
 * \details    Copy the min max capacities of queues in vectors of states
 *
 * \param      states      state of queues (trajectory)
 * \param      size        Number of vectors
 *
 * \return     void
 */
T_state *
modify (T_state * states)
{
   int i = 0;
   for (i = 0; i < nb_queues; i++) {
      states->queues[0][i] = capmins[i];
      states->queues[1][i] = capmaxs[i];
   }
   return states;
}
