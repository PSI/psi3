 /**
 * \file        get_lib.c
 * \author      Minh Quan HO
 * \author      Florian LEVEQUE
 * \author      Jean-Marc.Vincent@imag.fr
 * \version     1.0
 * \date        26/06/2012
 * \brief       Parse to retrieve list of user source file  + name of simulation method
 * \details    This executable will store in lib_user.txt:
 * 		- simulation method name in the first line
 * 		- list of user source files in next lines (to generate lib_user.so later)
 */

 /*
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <dlfcn.h>
#include <getopt.h>
#include <errno.h>

#include "parser.h"
#include <psi_common.h>


#define ASSERT(condition, message) 				\
	if (!(condition)) { 					\
	    fprintf (stderr, "\n\t/!\\ ERROR : ");			\
	    fprintf (stderr, message); 					\
	    fprintf (stderr, "\n\tProgram exits. \n");			\
	    exit(1);						\
	}							\


/**
 * \brief      An element of the linked list to store strings
 */
typedef struct maillon {
   char *data;
   struct maillon *next;
} T_maillon;

/**
 * \brief      Linked list to store strings
 */
typedef struct {
   T_maillon *head;
   T_maillon *queue;
} T_chain;


/**
 * \brief       Merge two lists of string
 * \details    Merge the two lists \a l1  and \a l2 and store the new list in \a l1
 * \param    l1         List l1
 * \param    l2         List l2
 * \return     \e T_chain* representing the merged list.
 */
T_chain *merge (T_chain *l1, T_chain *l2)
{
   // concat two lists : l1 = l1 + l2

   if (l1 == NULL) {		// if l1 empty
      if (l2 == NULL) {		// if l2 empty
         return l1;			//return empty
      }
      else {			// l2 not empty
         l1 = l2;				// l1 = l2 and work on l1
      }
   }
   else {			// l1 not empty
      if (l2 != NULL) {		// if l2 not empty
         l1->queue->next = l2->head;
         l1->queue = l2->queue;
         free(l2);
      }
   }
   T_maillon *i, *j, *temp;
   // Invariant : N = total length of two lists
   // [0 .. i-1] : left part : is already treated (i.e no duplicata)
   // [i .. N]	: right part :  need to be treated.

   l1->queue = l1->head;
   i = l1->head->next;

   while (i != NULL) {
      // j will go from head to i
      j = l1->head;
      while (j != i && strcmp(j->data, i->data)){
         j = j->next;
      }

      if (j != i) { //i.e i is present in left : delete i and pass to next i
         temp = i;
         i = i->next;
         l1->queue->next = i;
         free(temp->data);
         free(temp);
      }
      else {		// i is not in left : add i in left and update l1->queue
         l1->queue = i;
         i = i->next;
      }
   }
   l1->queue->next = NULL;
   return l1;
}

/**
 * \brief       Called by get_my_lib : Parse and return a lists of string of the key "MyLib"
 * \details    Get the string list in the key "MyLib" of a YAML config file
 * \param    parser        Parser pointer
 * \param    token         Token pointer
 * \param    list          Result list
 * \return     \e T_chain* representing list.
 */
T_chain *get_chain_string (yaml_parser_t *parser, yaml_token_t *token, T_chain *list)
{
  int quit = 0;
  T_maillon* tmp;
  list = (T_chain *) malloc(sizeof(T_chain));
  do {
     yaml_parser_scan(parser, token);
     switch(token->type) {

        case YAML_FLOW_SEQUENCE_START_TOKEN: // "["
           list->head = (T_maillon*) malloc(sizeof(T_maillon));
           list->head->data = get_string(parser, token,list->head->data);
           list->queue = list->head;
           list->queue->next=NULL;
           break;

        case YAML_FLOW_SEQUENCE_END_TOKEN: // "]"
           break;

        case YAML_FLOW_ENTRY_TOKEN:
           // new string
           tmp = (T_maillon *) malloc(sizeof(T_maillon));
           tmp->data = get_string(parser, token,tmp->data);
           tmp->next = NULL;
           list->queue->next = tmp;
           list->queue = list->queue->next;
           break;

        case YAML_SCALAR_TOKEN: //in case of "~"
           free(list);
           return NULL;
           break;

        default:
           break;
     }
     if (token->type != YAML_FLOW_SEQUENCE_END_TOKEN) yaml_token_delete(token);

  } while (!quit && (token->type != YAML_FLOW_SEQUENCE_END_TOKEN));

  /*Delete the last token*/
  yaml_token_delete(token);

  return list;
}
/**
 * \brief       Parse a YAML config file return a lists of string of the key "MyLib"
 * \details    Get the string list in the key "MyLib" of a YAML config file
 * \param    fh        File pointer (*.yaml)
 * \return     \e T_chain* representing list.
 */
T_chain* get_my_lib(FILE *fh){
  yaml_parser_t parser;
  yaml_token_t  token;
  int quit = 0;
  char* keyname=NULL;
  T_chain* res=NULL;

  /* Initialize parser */
  if(!yaml_parser_initialize(&parser))
    fputs("Failed to initialize parser!\n", stderr);
  if(fh == NULL)
    fputs("Failed to open file!\n", stderr);

  /* Set input file */
  yaml_parser_set_input_file(&parser, fh);

  do{
    //get a token
    yaml_parser_scan(&parser, &token);
    switch(token.type){
	case YAML_KEY_TOKEN: {
	  keyname = get_string(&parser, &token, keyname);

	}break;
	case YAML_VALUE_TOKEN: {
	  if(!strcmp(keyname, "MyLib")) {
	    quit = 1;
	    res = get_chain_string(&parser, &token, res);
	  }
	  free(keyname);
	} break;
	default: break;
    }
    if(token.type != YAML_STREAM_END_TOKEN) yaml_token_delete(&token);
  } while(!quit && token.type != YAML_STREAM_END_TOKEN);
  /*Delete the last token*/
  yaml_token_delete(&token);
  /* Cleanup */
  yaml_parser_delete(&parser);

  return res;
}
/**
 * \brief      	Free the chain
 * \param    chain	the chain to free
 * \return     void
 */
void free_chain( T_chain* chain){
    T_maillon* cour;
    T_maillon* temp;
    cour = chain->head;
    while(cour != NULL){
      temp = cour;
      cour = cour->next;
      free(temp->data);
      free(temp);
    }
    free(cour);
    free(chain);
}


int main (int argc, char **argv)
{
   FILE *f;
   int i;
   char *method = NULL;
   T_chain *chain = NULL;
   T_maillon *cour;
   T_maillon *temp;

   for (i = 2; i < argc; i++){

      PSI3_DEBUG ("Opening file %s", argv[i]);

      f = fopen(argv[i], "r");
      if (f == NULL) {
         fprintf (stderr, "File %s does not exist\n", argv[i]);
         if (chain != NULL) free_chain(chain);
         exit(1);
      }
      chain = merge(chain, get_my_lib(f));

      if (method == NULL) {
         method = get_string_keyname(f, "Method");
      }
      fclose(f);
   }

   if (method == NULL) {
      fprintf (stderr, "Method not found\n");
      if (chain != NULL) free_chain(chain);
      exit(1);
   }

   PSI3_DEBUG ("Using method %s", method);

   f = fopen(argv[1], "w");
   if (f == NULL) {
      fprintf(stderr, "Fail to open file %s\n", argv[1]);
      exit(1);
   }
   fprintf(f, "%s\n", method);

   // if the merged chain is null i.e MyLib is empty => do not create lib_user.txt
   if (chain == NULL) {
      if (method != NULL) free(method);
      fclose(f);
      return 0;
   }

   cour = chain->head;
   while(cour != NULL) {
      fprintf(f, "%s\n", cour->data);
      temp = cour;
      cour = cour->next;
      free(temp->data);
      free(temp);
   }

   free(cour);
   free(chain);
   fclose(f);
   return 0;
}
