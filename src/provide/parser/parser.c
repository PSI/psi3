 /**
 * \file        parser.c
 * \author      Florian LEVEQUE
 * \author      Minh Quan HO
 * \author      Jean-Marc.Vincent@imag.fr
 * \version     1.0
 * \date        26/06/2012
 * \brief       Contains all functions of parsing YAML
 */

 /* 
  * This library is free software; you can redistribute it and/or
  * modify it under the terms of the GNU Lesser General Public
  * License as published by the Free Software Foundation; either
  * version 2.1 of the License, or any later version.
  * 
  * This library is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  * Lesser General Public License for more details.
  * 
  * You should have received a copy of the GNU Lesser General Public
  * License along with this library; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
  */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <dlfcn.h>
#include <getopt.h>
#include <errno.h>
#include <sys/time.h>
#include <time.h>
#include "parser.h"

#define STRING_BUFFER 128

/*****************************************************/
/*       Global variables                            */
/*****************************************************/

int nb_queues;

T_label *transcode = NULL;      // convert label-index of queues
int size_transcode;

/*****************************************************/

/**
 * \brief       Initialize struture queue
 * \details    Initialize struture queue
 * \param    queues       Pointer of structure T_queues
 * \return     void
 */
inline void
init_queues (T_queues * queues)
{
   queues->mins = NULL;
   queues->maxs = NULL;
   queues->nb_queues = 0;

}

/**
 * \brief       Initialize struture event
 * \details    Initialize struture event
 * \param    event       Pointer of structure T_event
 * \return     void
 */
inline void
init_event (T_event * event)
{
   event->id_evt = -1;
   event->ptr_transition = NULL;
   event->rate = 0.0;
   event->nb_queues_origin = 0;
   event->from = NULL;
   event->indexFctOrigin = NULL;
   event->nb_queues_dest = 0;
   event->to = NULL;
   event->indexFctDest = NULL;
   event->nb_params = 0;
   event->param = NULL;
   event->nb_rand = 0;
   event->rand_par = NULL;
}


/**
 * \brief       Initialize a set of events
 * \details    Initialize struture events
 * \param    events       Pointer of structure T_events
 * \return     void
 */
inline void
init_events (T_events * events)
{
   events->evts = NULL;
   events->nb_evts = 0;
}

/**
 * \brief       Initialize a model
 * \details    Initialize struture model
 * \param    model       Pointer of structure T_model
 * \return     void
 */
inline void
init_model (T_model * model)
{
   model->queues = NULL;
   model->events = NULL;
}


/**
 * \brief       Initialize a output
 * \details    Initialize struture output
 * \param    output       Pointer of structure T_output
 * \return     void
 */
inline void
init_output (T_output * output)
{


   output->outputfilename = NULL;
   output->outputFct = NULL;
   output->showModel = -1;
   output->showParam = -1;
   output->showsimulTime = -1;
}

inline st_general_ptr
init_general_param ()
{
   st_general_ptr general = (st_general_ptr) malloc (sizeof (st_general));
   if (general == NULL) {
      return NULL;
   }

   /* FIXME: will change */
   general->output = (st_output_ptr) malloc (sizeof (T_output));
   if (general->output == NULL) {
      free (general);
      return NULL;
   }

   general->output->outputfilename = NULL;
   general->output->outputFct = NULL;
   general->output->showModel = -1;
   general->output->showParam = -1;
   general->output->showsimulTime = -1;

   general->seed = 0;
   general->default_lib = NULL;
   general->user_lib = NULL;

   return general;
}

void
free_general_param (st_general_ptr general)
{
   /* FIXME: will change */
   free (general->output->outputfilename);
   free (general->output);
   free (general);
}

/**
 * \brief       Free table between identifier and name 
 * \return     void
 */
inline void
free_transcode ()
{
   int i;
   if (transcode != NULL) {
      for (i = NUMBER_DEFAULT_KEYWORDS; i < size_transcode; i++) {
         free (transcode[i].label);
      }
      free (transcode);
   }
}

/**
 * \brief       Free all the label of event structure  
 * \param    evt      	a event to free
 * \return     void
 */
inline void
free_label_event (T_event * evt)
{
   int i;
   if (evt != NULL) {
      // free "from"
      if (evt->name_evt != NULL) {
         free (evt->name_evt);
      }
      if (evt->name_transition != NULL) {
         free (evt->name_transition);
      }
      if (evt->name_from != NULL) {
         for (i = 0; i < evt->nb_queues_origin; i++)
            free (evt->name_from[i]);
         free (evt->name_from);
         if (evt->names_indexFctOrigin != NULL) {
            for (i = 0; i < evt->nb_fct_index_origin; i++)
               free (evt->names_indexFctOrigin[i]);
            free (evt->names_indexFctOrigin);
         }
      }
      // free "to"
      if (evt->name_to != NULL) {
         for (i = 0; i < evt->nb_queues_dest; i++)
            free (evt->name_to[i]);
         free (evt->name_to);
         if (evt->names_indexFctDest != NULL) {
            for (i = 0; i < evt->nb_fct_index_dest; i++) {
               free (evt->names_indexFctDest[i]);
            }
            free (evt->names_indexFctDest);
         }
      }
   }
}

/**
 * \brief       Free all labels of all events 
 * \param    	events      	all events to free
 * \return     void
 */
inline void
free_label_events (T_events * events)
{
   int i;
   if (events != NULL) {
      if (events->evts != NULL) {
         for (i = 0; i < events->nb_evts; i++) {
            free_label_event (events->evts + i);
         }
      }
   }
}

/**
 * \brief       Free event structure 
 * \param    evt      	a event to free
 * \return     void
 */
inline void
free_event (T_event * evt)
{
   int i;
   if (evt != NULL) {
      // free "from"
      if (evt->from != NULL) {
         free (evt->from);
         if (evt->indexFctOrigin != NULL) {
            for (i = 0; i < evt->nb_fct_index_origin; i++) {
               // free(*(evt->indexFctOrigin+i));
            }
            free (evt->indexFctOrigin);
         }
      }
      // free "to"
      if (evt->to != NULL) {
         free (evt->to);
         if (evt->indexFctDest != NULL) {
            for (i = 0; i < evt->nb_fct_index_dest; i++) {
               // free(*(evt->indexFctDest+i));
            }
            free (evt->indexFctDest);
         }
      }
      // free "param"
      if (evt->param != NULL) {
         for (i = 0; i < evt->nb_params; i++) {
            free (*(evt->param + i));
         }
         free (evt->param);

      }
      if (evt->rand_par != NULL) {
         free (evt->rand_par);
      }
      free_label_event (evt);
   }
}

/**
 * \brief       Free all label of all queues 
 * \param    queues      all queues
 * \return     void
 */
static inline void
free_label_queues (T_queues * queues)
{
   int i;
   if (queues->name_queues != NULL) {
      for (i = 0; i < queues->nb_queues; i++) {
         free (queues->name_queues[i]);
      }
      free (queues->name_queues);
   }
}

/**
 * \brief       Free all queues 
 * \param    queues      all queues
 * \return     void
 */
inline void
free_queues (T_queues * queues)
{
   if (queues != NULL) {
      if (queues->mins != NULL)
         free (queues->mins);
      if (queues->maxs != NULL)
         free (queues->maxs);
      free_label_queues (queues);
      free (queues);
   }
}

 /**
 * \brief       Free all events 
 * \param    events      all events to free
 * \return     void
 */
inline void
free_events (T_events * events)
{
   int i;
   if (events != NULL) {
      if (events->evts != NULL) {
         for (i = 0; i < events->nb_evts; i++) {
            free_event (events->evts + i);
         }
         free (events->evts);
      }
      free (events);
   }
}

/**
 * \brief       Free all label from the model 
 * \param    model     the model (queues + events)
 * \return     void
 */
//static inline void
//free_label_model (T_model * model)
//{
//   if (model != NULL) {
//      free_label_queues (model->queues);
//      free_label_events (model->events);
//   }
//}

/**
 * \brief       Free the model 
 * \param    model     the model (queues + events)
 * \return     void
 */
inline void
free_model (T_model * model)
{
   if (model != NULL) {
      free_queues (model->queues);
      free_events (model->events);
      free (model);
   }
}

/**
 * \brief       Free output structure 
 * \param    output     output structure 
 * \return     void
 */
inline void
free_output (T_output * output)
{
   if (output != NULL) {
      if (output->outputfilename != NULL)
         free (output->outputfilename);
      free (output);
   }
}

/**
 * \brief       Print a string list into a stream
 * \details    Print a string list into a stream : stdout ou a file
 * \param    stream_out       Pointer of ouput stream
 * \param    list             String list
 * \param    size             String list size
 * \return     void
 */
void
print_list_string (FILE * stream_out, char **list, int size)
{
   int i;
   if (list == NULL) {
      fprintf (stream_out, " null ");
   }
   else {
      for (i = 0; i < size; i++) {
         fprintf (stream_out, " %s ", *(list + i));
      }
   }
}

/**
 * \brief       Print a int list into a stream
 * \details    Print a int list into a stream : stdout ou a file
 * \param    stream_out       Pointer of ouput stream
 * \param    list             int list
 * \param    size             int list size
 * \return     void
 */
void
print_list_int (FILE * stream_out, int *list, int size)
{
   int i;
   if (list != NULL) {
      fprintf (stream_out, "%d", *(list));
      for (i = 1; i < size; i++) {
         fprintf (stream_out, ", %d", *(list + i));
      }
   }
}

/**
 * \brief       Parse and return an integer
 * \details    Parse and return an integer
 * \param    parser       Pointer of parser
 * \param    token        Pointer of token
 * \return     int
 */
inline int
get_int (yaml_parser_t * parser, yaml_token_t * token)
{
   yaml_parser_scan (parser, token);
   if (!strcmp ((const char *) token->data.scalar.value, "~")) {
      return INT_MIN;
   }
   int res = atoi ((const char *) token->data.scalar.value);
   yaml_token_delete (token);
   return res;
}

/**
 * \brief       Parse and return an integer
 * \details    Parse and return an integer
 * \param    parser       Pointer of parser
 * \param    token        Pointer of token
 * \return     int
 */
inline unsigned long
get_unsigned_long (yaml_parser_t * parser, yaml_token_t * token)
{
   yaml_parser_scan (parser, token);
   if (!strcmp ((const char *) token->data.scalar.value, "~")) {
      return INT_MIN;
   }
   unsigned long res = atol ((const char *) token->data.scalar.value);
   yaml_token_delete (token);
   return res;
}

/**
 * \brief       Parse and return a double
 * \details    Parse and return an double
 * \param    parser       Pointer of parser
 * \param    token        Pointer of token
 * \return     double
 */
inline double
get_double (yaml_parser_t * parser, yaml_token_t * token)
{
   yaml_parser_scan (parser, token);
   double res = atof ((const char *) token->data.scalar.value);
   yaml_token_delete (token);
   return res;
}

/**
 * \brief       Parse and return a string
 * \details    Parse and return an string
 * \param    parser       Pointer of parser
 * \param    token        Pointer of token
 * \return     char*
 */
inline char *
get_string (yaml_parser_t * parser, yaml_token_t * token, char *res)
{
   yaml_parser_scan (parser, token);
   if (!strcmp ((const char *) token->data.scalar.value, "~")) {
      yaml_token_delete (token);
      return NULL;
   }
   res = (char *) calloc (token->data.scalar.length + 1, sizeof (char));
   memcpy (res, token->data.scalar.value, token->data.scalar.length + 1);
   yaml_token_delete (token);
   return res;
}

/**
 * \brief       Parse and return a double of a key
 * \details    Parse and return an double of a keyname
 * \param    file       Pointer of YAML config file
 * \param    name       Keyname
 * \return     double
 */
double
get_double_keyname (FILE * file, char *name)
{
   yaml_parser_t parser;
   yaml_token_t token;
   char *keyname = NULL;
   short int quit = 0;
   double value = -1;

   /* start at the beginning of file */
   fseek (file, 0, 0);

   /* Initialize parser */
   if (!yaml_parser_initialize (&parser))
      fputs ("Failed to initialize parser!\n", stderr);
   /* Set input file */
   yaml_parser_set_input_file (&parser, file);
   do {
      // get a token
      yaml_parser_scan (&parser, &token);
      switch (token.type) {
      case YAML_KEY_TOKEN:{
            keyname = get_string (&parser, &token, keyname);
         }
         break;
      case YAML_VALUE_TOKEN:{
            if (!strcmp (keyname, name)) {
               value = get_double (&parser, &token);
               quit = 1;
            }
            free (keyname);
         }
         break;
      default:
         break;
      }
      if (token.type != YAML_STREAM_END_TOKEN)
         yaml_token_delete (&token);
   } while (!quit && (token.type != YAML_STREAM_END_TOKEN));
   /* Delete the last token */
   yaml_token_delete (&token);
   /* Cleanup */
   yaml_parser_delete (&parser);
   return value;
}


/**
 * \brief       Parse and return a integer of a key
 * \details    Parse and return an integer of a keyname
 * \param    file       Pointer of YAML config file
 * \param    name       Keyname
 * \return     int
 */
int
get_int_keyname (FILE * file, char *name)
{
   yaml_parser_t parser;
   yaml_token_t token;
   char *keyname = NULL;
   short int quit = 0;
   int value = -1;

   /* Start at the beginning of file */
   fseek (file, 0, 0);

   /* Initialize parser */
   if (!yaml_parser_initialize (&parser)) {
      fputs ("Failed to initialize parser!\n", stderr);
   }
   /* Set input file */
   yaml_parser_set_input_file (&parser, file);
   do {
      // get a token
      yaml_parser_scan (&parser, &token);
      switch (token.type) {
      case YAML_KEY_TOKEN:
         keyname = get_string (&parser, &token, keyname);
         break;

      case YAML_VALUE_TOKEN:
         if (!strcmp (keyname, name)) {
            value = get_int (&parser, &token);
            quit = 1;
         }
         free (keyname);
         break;

      default:
         break;
      }
      if (token.type != YAML_STREAM_END_TOKEN) {
         yaml_token_delete (&token);
      }
   } while (!quit && (token.type != YAML_STREAM_END_TOKEN));

   /* Delete the last token */
   yaml_token_delete (&token);
   /* Cleanup */
   yaml_parser_delete (&parser);

   return value;
}

/**
 * \brief      Parse and return a boolean of a key
 *
 * \param      file     Pointer of YAML config file
 * \param      name     Keyname
 * \return     1: Yes
 *             0: No
 *             -1: undef
 */
int
get_bool_keyname (FILE * file, char *name)
{
   yaml_parser_t parser;
   yaml_token_t token;
   char *keyname = NULL, *temp = NULL;
   short int quit = 0;
   int rc = -1;

   /* start at the beginning of file */
   fseek (file, 0, 0);

   /* Initialize parser */
   if (!yaml_parser_initialize (&parser)) {
      fputs ("Failed to initialize parser!\n", stderr);
   }
   /* Set input file */
   yaml_parser_set_input_file (&parser, file);

   do {
      // get a token
      yaml_parser_scan (&parser, &token);

      switch (token.type) {

      case YAML_KEY_TOKEN:
         keyname = get_string (&parser, &token, keyname);
         break;

      case YAML_VALUE_TOKEN:
         if (!strcmp (keyname, name)) {
            temp = get_string (&parser, &token, temp);

            if (strcmp (temp, "Yes") == 0) {
               rc = 1;
            }
            else if (strcmp (temp, "No") == 0) {
               rc = 0;
            }
            else {
               rc = -1;
            }

            free (temp);
            quit = 1;
         }
         free (keyname);
         break;

      default:
         break;
      }

      if (token.type != YAML_STREAM_END_TOKEN) {
         yaml_token_delete (&token);
      }

   } while (!quit && (token.type != YAML_STREAM_END_TOKEN));

   /* Delete the last token */
   yaml_token_delete (&token);
   /* Cleanup */
   yaml_parser_delete (&parser);

   return rc;
}


/**
 * \brief       Parse and return a string of a key
 * \details    Parse and return an string of a keyname
 * \param    file       Pointer of YAML config file
 * \param    name       Keyname
 * \param    my_chain   Result
 * \return     char*
 */
char *
get_string_keyname (FILE * file, char *name)
{
   yaml_parser_t parser;
   yaml_token_t token;
   char *keyname = NULL;
   char *value = NULL;
   short int quit = 0;

   /* start at the beginning of file */
   fseek (file, 0, 0);

   /* Initialize parser */
   if (!yaml_parser_initialize (&parser)) {
      fputs ("Failed to initialize parser!\n", stderr);
   }
   /* Set input file */
   yaml_parser_set_input_file (&parser, file);

   do {
      // get a token
      yaml_parser_scan (&parser, &token);

      switch (token.type) {

      case YAML_KEY_TOKEN:
         keyname = get_string (&parser, &token, keyname);
         break;

      case YAML_VALUE_TOKEN:
         if (!strcmp (keyname, name)) {
            value = get_string (&parser, &token, value);
            quit = 1;
         }
         free (keyname);
         break;

      default:
         break;
      }

      if (token.type != YAML_STREAM_END_TOKEN) {
         yaml_token_delete (&token);
      }

   } while (!quit && (token.type != YAML_STREAM_END_TOKEN));

   /* Delete the last token */
   yaml_token_delete (&token);
   /* Cleanup */
   yaml_parser_delete (&parser);

   return value;
}


/**
 * \brief       Parse and return a string list
 * \details    Parse and return an string of a keyname
 * \param    parser       Pointer of parser
 * \param    token        Pointer of token
 * \param    ptr_ptr      Result string list
 * \param    size         Size of string list to be updated
 * \return     char**
 */
char **
get_list_string (yaml_parser_t * parser, yaml_token_t * token, char **ptr_ptr,
                 int *size)
{
   short int quit = 0;
   int size_loc = 0;
   int i = 0;
   do {
      yaml_parser_scan (parser, token);
      switch (token->type) {

      case YAML_FLOW_SEQUENCE_START_TOKEN:{
                                // "["
            size_loc++;
            ptr_ptr = (char **) malloc (sizeof (char *));
            *(ptr_ptr) = get_string (parser, token, *(ptr_ptr));
            // fprintf(stderr, " %s ", *(ptr_ptr));
            if (*ptr_ptr == NULL) {
               *size = 0;
               return NULL;
            }
         }
         break;
      case YAML_FLOW_SEQUENCE_END_TOKEN:{
                                // "]"
         }
         break;

      case YAML_FLOW_ENTRY_TOKEN:{
            size_loc++;
            ptr_ptr = realloc (ptr_ptr, size_loc * sizeof (char *));
            *(ptr_ptr + size_loc - 1) =
               get_string (parser, token, *(ptr_ptr + size_loc - 1));
            // fprintf(stderr, " %s ", *(ptr_ptr+size_loc-1));
            if (*(ptr_ptr + size_loc - 1) == NULL) {
               for (i = 0; i < size_loc - 1; i++) {
                  free (ptr_ptr + i);
               }
               *size = 0;
               return NULL;
            }
         }
         break;
      case YAML_SCALAR_TOKEN:{ // in case of "~"
            ptr_ptr = NULL;
            *size = 0;
            quit = 1;           // quit the while because we have an empty list 
                                // "~"

         }
         break;
      default:
         ptr_ptr = NULL;
         break;
      }
   } while (!quit && (token->type != YAML_FLOW_SEQUENCE_END_TOKEN));
   // fprintf(stderr, "\n");
   *size = size_loc;
   return ptr_ptr;
}

/**
 * \brief       Parse and return a string list of a key
 * \details    Parse and return an string of a keyname
 * \param    file         Pointer of YAML config file
 * \param    name         Keyname
 * \param    my_list      Result string list
 * \param    size         Size of string list to be updated
 * \return     char**
 */
char **
get_string_list_keyname (FILE * file, char *name, char **my_list, int *size)
{
   yaml_parser_t parser;
   yaml_token_t token;
   char *keyname = NULL;
   short int quit = 0;
   /* Initialize parser */
   if (!yaml_parser_initialize (&parser))
      fputs ("Failed to initialize parser!\n", stderr);
   /* Set input file */
   yaml_parser_set_input_file (&parser, file);
   do {
      // get a token
      yaml_parser_scan (&parser, &token);
      switch (token.type) {
      case YAML_KEY_TOKEN:{
            keyname = get_string (&parser, &token, keyname);
         }
         break;
      case YAML_VALUE_TOKEN:{
            if (!strcmp (keyname, name)) {
               my_list = get_list_string (&parser, &token, my_list, size);
               quit = 1;
            }
            free (keyname);
         }
         break;
      default:
         break;
      }
      if (token.type != YAML_STREAM_END_TOKEN)
         yaml_token_delete (&token);
   } while (!quit && (token.type != YAML_STREAM_END_TOKEN));
   /* Delete the last token */
   yaml_token_delete (&token);

   /* Cleanup */
   yaml_parser_delete (&parser);
   return my_list;

}

/**
 * \brief       Parse and return a int list
 * \details    Parse and return an int list of a keyname
 * \param    parser       Pointer of parser
 * \param    token        Pointer of token
 * \param    ptr_ptr      Result int list
 * \param    size         Size of int list to be updated
 * \return     int*
 */
int *
get_list_int (yaml_parser_t * parser, yaml_token_t * token, int *ptr, int *size)
{
   short int quit = 0;
   int size_loc = 0;
   do {
      yaml_parser_scan (parser, token);
      switch (token->type) {

      case YAML_FLOW_SEQUENCE_START_TOKEN:{
                                // "["
            size_loc++;
            ptr = (int *) malloc (sizeof (int));
            *ptr = get_int (parser, token);
         } break;
      case YAML_FLOW_SEQUENCE_END_TOKEN:{
                                // "]"
         }
         break;
      case YAML_FLOW_ENTRY_TOKEN:{
            size_loc++;
            ptr = realloc (ptr, size_loc * sizeof (int));
            *(ptr + size_loc - 1) = get_int (parser, token);
         }
         break;
      case YAML_SCALAR_TOKEN:{ // in case of "~"
            ptr = NULL;
            *size = 0;
            quit = 1;           // quit the while because we have an empty list 
                                // "~"
         }
         break;
      default:
         break;
      }
      if (token->type != YAML_FLOW_SEQUENCE_END_TOKEN)
         yaml_token_delete (token);
   } while (!quit && (token->type != YAML_FLOW_SEQUENCE_END_TOKEN));
   // fprintf(stderr, "\n");
   *size = size_loc;
   return ptr;
}

/**
 * \brief       Parse and return a int list of a key
 * \details    Parse and return an int list of a keyname
 * \param    file         Pointer of YAML config file
 * \param    name         Keyname
 * \param    my_list      Result int list
 * \param    size         Size of int list to be updated
 * \return     int*
 */
int *
get_int_list_keyname (FILE * file, char *name, int **my_list, int *size)
{
   yaml_parser_t parser;
   yaml_token_t token;
   char *keyname = NULL;
   short int quit = 0;

   /* Start at the beginning of file */
   fseek (file, 0, 0);

   /* Initialize parser */
   if (!yaml_parser_initialize (&parser)) {
      fputs ("Failed to initialize parser!\n", stderr);
   }
   /* Set input file */
   yaml_parser_set_input_file (&parser, file);

   do {
      // get a token
      yaml_parser_scan (&parser, &token);

      switch (token.type) {

      case YAML_KEY_TOKEN:
         keyname = get_string (&parser, &token, keyname);
         break;

      case YAML_VALUE_TOKEN:
         if (!strcmp (keyname, name)) {
            *my_list = get_list_int (&parser, &token, *my_list, size);
            quit = 1;
         }
         free (keyname);
         break;

      default:
         break;
      }

      if (token.type != YAML_STREAM_END_TOKEN) {
         yaml_token_delete (&token);
      }

   } while (!quit && (token.type != YAML_STREAM_END_TOKEN));

   /* Delete the last token */
   yaml_token_delete (&token);
   /* Cleanup */
   yaml_parser_delete (&parser);

   return 0;
}

/**
 * \brief       Split a string
 * \details    Split a string "part1$part2" into a table : ["part1", "part2"]
 * \param    full         String
 * \param    delim        Deliminator
 * \return     char**
 */
char **
split (char *full, char delim)
{
   int length;

   if (full == NULL)
      return NULL;

   length = strlen (full);
   // fprintf(stderr, "Size of full is %d \n", length);
   int i = 0;
   char **res = (char **) malloc (2 * sizeof (char *));
   while (full[i] != delim && i < length) {
      i++;
   }
   if (i >= length) {           // means that full is not at form
                                // <lib>$<function>
      // clear what have been created in this function:
      free (res);
      return NULL;
   }


   res[0] = (char *) malloc ((i + 1) * sizeof (char));
   res[1] = (char *) malloc ((length - i + 1) * sizeof (char));
   memcpy (res[0], full, i);
   memcpy (res[1], full + i + 1, length - i - 1);
   res[0][i] = '\0';
   res[1][length - i - 1] = '\0';
   return res;
}

/**
 * \brief       Return the pointer of a function
 * \details    Look for the function in the two librarys : default_lib and user_lib
 * \param    default_lib         Default library
 * \param    user_lib            User library
 * \param    fctname             Function name
 * \return     void*
 */
void *
get_ptr_fct (void *default_lib, void *user_lib, char *fctname)
{
   void *ptr;
   char **res;
   res = split (fctname, '$');

   if (res == NULL) {
      fprintf (stderr, " /!\\ : Function name is : %s . "
              "It must follow this syntax: <lib>$<function_name>\n", fctname);
      return NULL;
   }


   if ((strcmp (res[0], "Default") == 0) && (default_lib != NULL)) {
      ptr = dlsym (default_lib, res[1]);
   }
   else if ((strcmp (res[0], "MyLib") == 0) && (user_lib != NULL)) {
      ptr = dlsym (user_lib, res[1]);
   }
   else {
      free (res[0]);
      free (res[1]);
      free (res);
      return NULL;
   }

   // clean up
   free (res[0]);
   free (res[1]);
   free (res);

   return (void *) ptr;
}

/**
 * \brief       Return the pointer of a function of a key
 * \details    Look for the function in the two librarys : default_lib and user_lib
 * \param    default_lib         Default library
 * \param    user_lib            User library
 * \param    file                File pointer
 * \param    name                Function name
 * \return     void*
 */
void *
get_fct_by_keyname (void *default_lib, void *user_lib, FILE * file, char *name)
{
   yaml_parser_t parser;
   yaml_token_t token;
   char *keyname = NULL;
   short int quit = 0;
   void *ptr = NULL;
   char *fctname = NULL;

   /* start at the beginning of file */
   fseek (file, 0, 0);

   /* Initialize parser */
   if (!yaml_parser_initialize (&parser)) {
      fputs ("Failed to initialize parser!\n", stderr);
   }
   /* Set input file */
   yaml_parser_set_input_file (&parser, file);

   do {
      // get a token
      yaml_parser_scan (&parser, &token);

      switch (token.type) {

      case YAML_KEY_TOKEN:
         keyname = get_string (&parser, &token, keyname);
         break;

      case YAML_VALUE_TOKEN:
         if (strcmp (keyname, name) == 0) {
            // get the function name
            fctname = get_string (&parser, &token, fctname);
            PSI3_DEBUG ("Look for [%s] in libs", fctname);
            //
            // get the pointer of function
            ptr = get_ptr_fct (default_lib, user_lib, fctname);

            // if the function does not exist :
            if (ptr == NULL) {
               // FAIL : clear all that have been created
               free (fctname);
               free (keyname);
               yaml_token_delete (&token);
               yaml_parser_delete (&parser);
               return NULL;
            }

            // clean the function name
            if (fctname != NULL)
               free (fctname);
            quit = 1;
         }
         free (keyname);
         break;

      default:
         break;
      }

      if (token.type != YAML_STREAM_END_TOKEN) {
         yaml_token_delete (&token);
      }

   } while (!quit && (token.type != YAML_STREAM_END_TOKEN));

   /* Delete the last token */
   yaml_token_delete (&token);
   /* Cleanup */
   yaml_parser_delete (&parser);

   return ptr;
}


/**
 * Return a queue index from its label 
 */
int
find_index_labels (T_label * list, int size, char *chain)
{
   int i = 0;
   do {
      if (!strcmp ((list + i)->label, chain)) {
         return (list + i)->index;
      }
      i++;
   } while (i < size);
   return ERROR;
}

/**
 * \brief       Parse and return a structure of T_queues
 * \details    
 * \param    parser         Pointer of parser
 * \param    token          Pointer token 
 * \return     T_queues*
 */
T_queues *
read_queues (yaml_parser_t * parser, yaml_token_t * token)
{

   int level = 0;
   char *keyname = NULL;
   T_queues *queues = (T_queues *) malloc (sizeof (T_queues));
   // initialize queues
   init_queues (queues);

  /************************/
   /* Malloc to initialize */
  /***********************/
   int cpt_queues = 0;
   queues->mins = (int *) malloc (sizeof (int));
   queues->maxs = (int *) malloc (sizeof (int));
   queues->name_queues = (char **) malloc (sizeof (char *));
   /* Transcode to convert from queue labels into an integer. Initialized with
    * 3 labels : "outside", "all" and "drop" */
   transcode = (T_label *) malloc (NUMBER_DEFAULT_KEYWORDS * sizeof (T_label));
   transcode[0] = (T_label) {
   "outside", -1};
   transcode[1] = (T_label) {
   "drop", -1};
   transcode[2] = (T_label) {
   "all", -2};
   size_transcode = NUMBER_DEFAULT_KEYWORDS;
  /***********************/

   do {
      // get a token
      yaml_parser_scan (parser, token);
      switch (token->type) {
      case YAML_BLOCK_SEQUENCE_START_TOKEN:
         level++;
         break;
      case YAML_BLOCK_ENTRY_TOKEN:
         level++;
         break;
      case YAML_BLOCK_END_TOKEN:
         level--;
         break;
      case YAML_BLOCK_MAPPING_START_TOKEN:
         break;
      case YAML_KEY_TOKEN:{
            keyname = get_string (parser, token, keyname);
         }
         break;
      case YAML_VALUE_TOKEN:{
            if (!strcmp (keyname, "id")) {
               cpt_queues++;
               size_transcode++;

               transcode =
                  (T_label *) realloc (transcode,
                                       size_transcode * sizeof (T_label));
               queues->name_queues =
                  (char **) realloc (queues->name_queues,
                                     cpt_queues * sizeof (char *));
               (transcode + size_transcode - 1)->label =
                  get_string (parser, token,
                              (transcode + size_transcode - 1)->label);
               queues->name_queues[cpt_queues - 1] =
                  (char *) malloc (sizeof (char) *
                                   (strlen
                                    ((transcode + size_transcode - 1)->label) +
                                    1));
               memcpy ((queues->name_queues[cpt_queues - 1]),
                       (transcode + size_transcode - 1)->label,
                       strlen ((transcode + size_transcode - 1)->label) + 1);
               (transcode + size_transcode - 1)->index = cpt_queues - 1;
            }
            else if (!strcmp (keyname, "min")) {
               queues->mins =
                  (int *) realloc (queues->mins, cpt_queues * sizeof (int));
               *(queues->mins + cpt_queues - 1) = get_int (parser, token);
            }
            else if (!strcmp (keyname, "max")) {
               queues->maxs =
                  (int *) realloc (queues->maxs, cpt_queues * sizeof (int));
               *(queues->maxs + cpt_queues - 1) = get_int (parser, token);
            }
            else {
               // FAIL : clear all what have been created in this function
               free (queues->mins);
               free (queues->maxs);
               free (queues);
               free (keyname);
               free_transcode ();
               return NULL;
            }
            free (keyname);
         }
         break;
      default:
         break;
      }

      if (level != 0 && (token->type != YAML_STREAM_END_TOKEN))
         yaml_token_delete (token);

   } while (level != 0 && (token->type != YAML_STREAM_END_TOKEN));
   // update the total number of queues
   queues->nb_queues = cpt_queues;

   /* Delete the last token */
   yaml_token_delete (token);

   return queues;
}

/**
 * \brief       Check if the events have the smallest configuration
 * \details    
 * \param    events 
 * \return     \e int representing if the events are ok (return 1) or not (return 0)
 */
int
check_event (T_events * events)
{
   int i = 0;
   while (i < events->nb_evts) {
      if (events->evts[i].ptr_transition == NULL) {
         fprintf (stderr, "There is not transition function in event %s",
                 events->evts[i].name_evt);
         return 0;
      }
      if (events->evts[i].from == NULL) {
         fprintf (stderr, "There is not origin queue in event %s",
                 events->evts[i].name_evt);
         return 0;
      }
      i++;
   }
   return 1;
}


/**
 * \brief       Parse and return a structure of T_events
 * \details    
 * \param    parser         Pointer of parser
 * \param    token          Pointer token 
 * \param    default_lib         Default library
 * \param    user_lib            User library
 * \return     T_events*
 */
T_events *
read_events (yaml_parser_t * parser, yaml_token_t * token, void *default_lib,
             void *user_lib)
{
   int cpt_events = 0;
   int level = 0;
   char *keyname = NULL;
   int param_list_size, from_list_size, to_list_size, indexFct_list_size;
   int i;

   /* Initialize events */
   T_events *events = (T_events *) malloc (sizeof (T_events));
   T_event *evts = (T_event *) malloc (sizeof (T_event));
   init_events (events);
   init_event (evts);

 /******************/
   do {
      // get a token
      yaml_parser_scan (parser, token);
      switch (token->type) {

      case YAML_BLOCK_SEQUENCE_START_TOKEN:
         level++;
         break;
      case YAML_BLOCK_ENTRY_TOKEN:
         level++;
         cpt_events++;
         break;
      case YAML_BLOCK_END_TOKEN:
         level--;
         break;
      case YAML_BLOCK_MAPPING_START_TOKEN:
         evts = (T_event *) realloc (evts, cpt_events * sizeof (T_event));
         init_event (evts + cpt_events - 1);
         break;
      case YAML_KEY_TOKEN:{
            keyname = get_string (parser, token, keyname);
         }
         break;
      case YAML_VALUE_TOKEN:{

            if (!strcmp (keyname, "id")) {
               // we don't care about the label of event but its id, so we save 
               // the id (cpt_events) and skip the label
               (evts + cpt_events - 1)->name_evt =
                  get_string (parser, token, (evts + cpt_events - 1)->name_evt);
               // save the id
               (evts + cpt_events - 1)->id_evt = cpt_events - 1;
            }

            else if (!strcmp (keyname, "type")) {
               (evts + cpt_events - 1)->name_transition =
                  get_string (parser, token,
                              (evts + cpt_events - 1)->name_transition);
               // get the function name
               // temp2 = get_string(parser, token, temp2);

               // get the pointer of function
               (evts + cpt_events - 1)->ptr_transition =
                  (T_ptr_transfct) get_ptr_fct (default_lib, user_lib,
                                                (evts + cpt_events -
                                                 1)->name_transition);

               // if the function does not exist :
               if ((evts + cpt_events - 1)->ptr_transition == NULL) {
                  fprintf
                     (stderr, "\n /!\\ : Function not found : %s at event : %d  - keyname : %s  \n",
                      (evts + cpt_events - 1)->name_transition, cpt_events,
                      keyname);
                  // FAIL : clear all that have been created
                  // free(temp2);
                  free (keyname);
                  events->evts = evts;
                  events->nb_evts = cpt_events;
                  free_events (events);
                  free_transcode ();
                  return NULL;
               }
               // clean the function name
               // free(temp2);
            }

            else if (!strcmp (keyname, "rate")) {
               (evts + cpt_events - 1)->rate = get_double (parser, token);
            }

            else if (!strcmp (keyname, "from")) {
               (evts + cpt_events - 1)->name_from =
                  get_list_string (parser, token,
                                   (evts + cpt_events - 1)->name_from,
                                   &from_list_size);
               (evts + cpt_events - 1)->from =
                  (int *) malloc (from_list_size * sizeof (int));

               for (i = 0; i < from_list_size; i++) {
                  *((evts + cpt_events - 1)->from + i) =
                     find_index_labels (transcode, size_transcode,
                                        (evts + cpt_events - 1)->name_from[i]);
                  if (*((evts + cpt_events - 1)->from + i) == ERROR) {
                     fprintf
                        (stderr, "\n /!\\ : Label not found : %s at event : %d  - keyname : %s  \n",
                         (evts + cpt_events - 1)->name_from[i], cpt_events,
                         keyname);
                     // FAIL : clear what have been created in this function:
//         for(j = 0; j < from_list_size; j++) free(temp[j]);
//         free(temp);
                     free (keyname);
                     events->evts = evts;
                     events->nb_evts = cpt_events;
                     free_events (events);
                     free_transcode ();
                     return NULL;
                  }
               }
               (evts + cpt_events - 1)->nb_queues_origin = from_list_size;
               // free temp:
//        for(j = 0; j < from_list_size; j++) free(temp[j]);
//        free(temp);
            }
            else if (!strcmp (keyname, "to")) {
               // fprintf(stderr, "\t\tDest list is : "); 

               (evts + cpt_events - 1)->name_to =
                  get_list_string (parser, token,
                                   (evts + cpt_events - 1)->name_to,
                                   &to_list_size);
               (evts + cpt_events - 1)->to =
                  (int *) malloc (to_list_size * sizeof (int));

               for (i = 0; i < to_list_size; i++) {
                  *((evts + cpt_events - 1)->to + i) =
                     find_index_labels (transcode, size_transcode,
                                        (evts + cpt_events - 1)->name_to[i]);
                  if (*((evts + cpt_events - 1)->to + i) == ERROR) {
                     fprintf
                        (stderr, "\n /!\\ : Label not found : %s at event : %d  - keyname : %s  \n",
                         (evts + cpt_events - 1)->name_to[i], cpt_events,
                         keyname);
                     // FAIL : clear what have been created in this function:
//         for(j = 0; j < to_list_size; j++) free(temp[j]);
//         free(temp);
                     free (keyname);
                     events->evts = evts;
                     events->nb_evts = cpt_events;
                     free_events (events);
                     free_transcode ();
                     return NULL;
                  }
               }
               (evts + cpt_events - 1)->nb_queues_dest = to_list_size;
               // free temp:
//        for(j = 0; j < to_list_size; j++) free(temp[j]);
//        free(temp);
            }
            else if (!strcmp (keyname, "indexFctOrigin")) {
               // (evts+cpt_events-1)->indexFctOrigin = get_list_string(parser, 
               // token, (evts+cpt_events-1)->indexFctOrigin,
               // &indexFct_list_size);
               // read the list of function names : 
               (evts + cpt_events - 1)->names_indexFctOrigin =
                  get_list_string (parser, token,
                                   (evts + cpt_events -
                                    1)->names_indexFctOrigin,
                                   &indexFct_list_size);
               // allocate a table of pointer :
               (evts + cpt_events - 1)->indexFctOrigin =
                  (void **) malloc (sizeof (void *) * indexFct_list_size);
               (evts + cpt_events - 1)->nb_fct_index_origin =
                  indexFct_list_size;
               // look for function pointers and put them into table
               for (i = 0; i < indexFct_list_size; i++) {
                  *((evts + cpt_events - 1)->indexFctOrigin + i) =
                     (T_ptr_indexfct) get_ptr_fct (default_lib, user_lib,
                                                   (evts + cpt_events -
                                                    1)->
                                                   names_indexFctOrigin[i]);
                  if (*((evts + cpt_events - 1)->indexFctOrigin + i) == NULL) {
                     fprintf
                        (stderr, "\n /!\\ : Function not found : %s at event : %d  - keyname : %s  \n",
                         (evts + cpt_events - 1)->names_indexFctOrigin[i],
                         cpt_events, keyname);
                     // FAIL : clear what have been created in this function:
//           for(j = 0; j < indexFct_list_size; j++) free(temp[j]);
//           free(temp);
                     free (keyname);
                     events->evts = evts;
                     events->nb_evts = cpt_events;
                     free_events (events);
                     free_transcode ();
                     return NULL;
                  }
               }
//          for(j = 0; j < indexFct_list_size; j++) free(temp[j]);
//          free(temp);
            }
            else if (!strcmp (keyname, "indexFctDest")) {
               (evts + cpt_events - 1)->names_indexFctDest =
                  get_list_string (parser, token,
                                   (evts + cpt_events - 1)->names_indexFctDest,
                                   &indexFct_list_size);

               // read the list of function names : 
//          temp = get_list_string(parser, token,temp, &indexFct_list_size);
               // allocate a table of pointer :
               (evts + cpt_events - 1)->indexFctDest =
                  (void **) malloc (sizeof (void *) * indexFct_list_size);
               (evts + cpt_events - 1)->nb_fct_index_dest = indexFct_list_size;
               // look for function pointers and put them into table
               for (i = 0; i < indexFct_list_size; i++) {
                  *((evts + cpt_events - 1)->indexFctDest + i) =
                     (T_ptr_indexfct *) get_ptr_fct (default_lib, user_lib,
                                                     (evts + cpt_events -
                                                      1)->
                                                     names_indexFctDest[i]);
                  if (*((evts + cpt_events - 1)->indexFctDest + i) == NULL) {
                     fprintf
                        (stderr, "\n /!\\ : Function not found : %s at event : %d  - keyname : %s  \n",
                         (evts + cpt_events - 1)->names_indexFctDest[i],
                         cpt_events, keyname);
                     // FAIL : clear what have been created in this function:
//           for(j = 0; j < indexFct_list_size; j++) free(temp[j]);
//           free(temp);
                     free (keyname);
                     events->evts = evts;
                     events->nb_evts = cpt_events;
                     free_events (events);
                     free_transcode ();
                     return NULL;
                  }
               }
//          for(j = 0; j < indexFct_list_size; j++) free(temp[j]);
//          free(temp);
            }
            else if (!strcmp (keyname, "parameters")) {
               (evts + cpt_events - 1)->param =
                  get_list_string (parser, token,
                                   (evts + cpt_events - 1)->param,
                                   &param_list_size);
               (evts + cpt_events - 1)->nb_params = param_list_size;
            }
            else {
               fprintf (stderr, "\t Label not found : %s \n", keyname);
               // FAIL : clear what have been created in this function:
               free (keyname);
               events->evts = evts;
               events->nb_evts = cpt_events;
               free_events (events);
               free_transcode ();
               return NULL;
            }
            free (keyname);
         }
         break;
      default:
         break;
      }
      if (level != 0 && (token->type != YAML_STREAM_END_TOKEN))
         yaml_token_delete (token);
   } while (level != 0 && (token->type != YAML_STREAM_END_TOKEN));
   /* Delete the last token */
   yaml_token_delete (token);
   // update events list
   events->evts = evts;
   // update the number total of events
   events->nb_evts = cpt_events;
   // Free the transcode table 
   free_transcode ();
   return events;
}

/**
 * \brief       Parse and return a structure of T_model
 * \details    This function will call read_queues and read_events. 
 * \param    model_cfgfile         Pointer of model config file
 * \param    default_lib         Default library
 * \param    user_lib            User library
 * \return     T_model*
 */

T_model *
read_model (char *model_cfgfile, void *default_lib, void *user_lib)
{
   yaml_parser_t parser;
   yaml_token_t token;
   char *keyname = NULL;
   FILE *model_cfgfile_fd;

   PSI3_DEBUG ("Opening model file: %s", model_cfgfile);

   model_cfgfile_fd = fopen (model_cfgfile, "r");
   if (model_cfgfile_fd == NULL) {
      fprintf (stderr, "Fail to open file %s\n", model_cfgfile);
      return NULL;
   }

   T_model *model = (T_model *) malloc (sizeof (T_model));

   // initialize the model
   init_model (model);

   /* Initialize parser */
   if (!yaml_parser_initialize (&parser)) {
      fputs ("Failed to initialize parser!\n", stderr);
   }
   /* Set input file */
   yaml_parser_set_input_file (&parser, model_cfgfile_fd);

   do {
      yaml_parser_scan (&parser, &token);

      switch (token.type) {

      case YAML_KEY_TOKEN:
         keyname = get_string (&parser, &token, keyname);
         break;

      case YAML_VALUE_TOKEN:
         if (!strcmp (keyname, "Queues")) {
            model->queues = read_queues (&parser, &token);
            if (model->queues == NULL) {
               fprintf (stderr, "\t Fail to initialize queues \n");
               free (keyname);
               yaml_token_delete (&token);
               yaml_parser_delete (&parser);
               free_model (model);
               return NULL;
            }
         }
         else if (!strcmp (keyname, "Events")) {
            model->events = read_events (&parser,
                                         &token, default_lib, user_lib);

            if (model->events == NULL || !check_event (model->events)) {
               fprintf (stderr, "\t Fail to initialize events \n");
               free (keyname);
               yaml_token_delete (&token);
               yaml_parser_delete (&parser);
               free_model (model);
               return NULL;
            }

         }
         else if (!strcmp (keyname, "MyLib")) {
            // we don't treat Mylib here because it's been treated by get_lib
            yaml_parser_scan (&parser, &token);
            yaml_token_delete (&token);
         }
         else {
            /* Unrognized label, we skip it */
            PSI3_WARNING ("Not recognized label: [%s]", keyname);
         }

         free (keyname);
         break;

         /* Others */
      default:
         break;
      }

      if (token.type != YAML_STREAM_END_TOKEN) {
         yaml_token_delete (&token);
      }

   } while (token.type != YAML_STREAM_END_TOKEN);

   /* Delete the last token */
   yaml_token_delete (&token);
   /* Cleanup */
   yaml_parser_delete (&parser);

   fclose (model_cfgfile_fd);

   return model;
}


/**
 * \brief       Print model data into  a stream out
 * \details    
 * \param    stream_out     Pointer of stream out
 * \param    model          Model to be printed
 * \return     void
 */
void
print_model (FILE * stream_out, T_model model)
{
   T_queues *queues = model.queues;
   T_events *events = model.events;
   int i;

   fprintf (stream_out, "# Queues: \n# nb_queues = %d \n", queues->nb_queues);
   for (i = 0; i < queues->nb_queues; i++) {
      fprintf (stream_out, "#\tid = %s - min = %d - max = %d \n",
               queues->name_queues[i], queues->mins[i], queues->maxs[i]);
   }

   T_event *evts = events->evts;
   fprintf (stream_out, "# Events: \n# nb_evts = %d \n", events->nb_evts);
   for (i = 0; i < events->nb_evts; i++) {
      // fprintf(stderr, "\tid = %d - transition = %s - rate = %f ", (evts+i)->id_evt,
      // (evts+i)->ptr_transition, (evts+i)->rate);
      fprintf (stream_out, "#\tid = %s", (evts + i)->name_evt);
      fprintf (stream_out, "\n#\t\trate = %f", (evts + i)->rate);
      fprintf (stream_out, "\n#\t\ttype = %s", (evts + i)->name_transition);
      fprintf (stream_out, "\n#\t\tOrigin =  ");
      // print_list_int(stream_out,(evts+i)->from, (evts+i)->nb_queues_origin);
      print_list_string (stream_out, (evts + i)->name_from,
                         (evts + i)->nb_queues_origin);
      fprintf (stream_out, "\n#\t\tDest = ");
      // print_list_int(stream_out,(evts+i)->to, (evts+i)->nb_queues_dest);
      print_list_string (stream_out, (evts + i)->name_to,
                         (evts + i)->nb_queues_dest);
      fprintf (stream_out, "\n#\t\tIndex Function Origin = ");
      // print_list_int(stream_out,(evts+i)->to, (evts+i)->nb_queues_dest);
      print_list_string (stream_out, (evts + i)->names_indexFctOrigin,
                         (evts + i)->nb_fct_index_origin);
      fprintf (stream_out, "\n#\t\tIndex Function Dest = ");
      // print_list_int(stream_out,(evts+i)->to, (evts+i)->nb_queues_dest);
      print_list_string (stream_out, (evts + i)->names_indexFctDest,
                         (evts + i)->nb_fct_index_dest);
      fprintf (stream_out, "\n#\t\tParam =  ");
      print_list_string (stream_out, (evts + i)->param, (evts + i)->nb_params);
      fprintf (stream_out, "\n");
   }
}



/**
 * \brief       Parse and return a structure of T_output
 * \details    
 * \param    param_file         Pointer of model config file
 * \param    default_lib         Default library
 * \param    user_lib            User library
 * \return     T_output*
 */
T_output *
read_output (char *output_cfgfile, void *default_lib, void *user_lib)
{
   yaml_parser_t parser;
   yaml_token_t token;
   char *keyname = NULL, *temp = NULL;
   FILE *output_cfgfile_fd;

   PSI3_DEBUG ("Opening output file: %s", output_cfgfile);

   output_cfgfile_fd = fopen (output_cfgfile, "r");
   if (output_cfgfile_fd == NULL) {
      fprintf (stderr, "Fail to open file %s\n", output_cfgfile);
      return 0;
   }

   T_output *output = (T_output *) malloc (sizeof (T_output));
   // initialize output :
   init_output (output);

   /* Initialize parser */
   if (!yaml_parser_initialize (&parser)) {
      fputs ("Failed to initialize parser!\n", stderr);
   }
   /* Set input file */
   yaml_parser_set_input_file (&parser, output_cfgfile_fd);

   do {
      yaml_parser_scan (&parser, &token);

      switch (token.type) {

      case YAML_KEY_TOKEN:
         keyname = get_string (&parser, &token, keyname);
         break;

      case YAML_VALUE_TOKEN:
         if (!strcmp (keyname, "OutputFileName")) {
            output->outputfilename = get_string (&parser, &token,
                                                 output->outputfilename);
            PSI3_DEBUG ("OutputFileName: %s", output->outputfilename);

         }
         else if (!strcmp (keyname, "ConfigModel")) {
            temp = get_string (&parser, &token, temp);
            // Yes = 1, No = 0
            output->showModel = !strcmp (temp, "Yes");
            free (temp);
            PSI3_DEBUG ("ConfigModel: %d", output->showModel);
         }
         else if (!strcmp (keyname, "Param")) {
            temp = get_string (&parser, &token, temp);
            output->showParam = !strcmp (temp, "Yes");
            free (temp);
            PSI3_DEBUG ("Param: %d", output->showParam);
         }
         else if (!strcmp (keyname, "SimulationTime")) {
            temp = get_string (&parser, &token, temp);
            output->showsimulTime = !strcmp (temp, "Yes");
            free (temp);
            PSI3_DEBUG ("SimulationTime: %d", output->showsimulTime);
         }
         else if (!strcmp (keyname, "CTODFct")) {
            // output->outputFct = get_string(&parser, &token,
            // output->outputFct);
            // get the function name
            temp = get_string (&parser, &token, temp);
            // get the pointer of function
            output->outputFct = (T_ptr_outputfct) get_ptr_fct (default_lib,
                                                               user_lib, temp);

            // if the function does not exist :
            if (output->outputFct == NULL) {
               fprintf (stderr, "\n /!\\ : Output Function not found : %s  \n", temp);
               // FAIL : clear all that have been created
               free (temp);
               free (keyname);
               yaml_token_delete (&token);
               yaml_parser_delete (&parser);
               free_output (output);
               return NULL;
            }
            // clean the function name
            free (temp);
         }
         else if (!strcmp (keyname, "MyLib")) {
            // we don't treat Mylib here because it's been treated by get_lib
            yaml_parser_scan (&parser, &token);
            yaml_token_delete (&token);
         }
         else {
            /* Unrognized label, we skip it */
            fprintf (stderr, "Not recognized label: [%s]\n", keyname);
         }

         free (keyname);
         break;

         /* Others */
      default:
         break;
      }

      if (token.type != YAML_STREAM_END_TOKEN) {
         yaml_token_delete (&token);
      }

   } while (token.type != YAML_STREAM_END_TOKEN);

   /* Delete the last token */
   yaml_token_delete (&token);
   /* Cleanup */
   yaml_parser_delete (&parser);

   fclose (output_cfgfile_fd);

   return output;
}

/**
 * \brief       Parse and return a structure of T_output
 * \param	output	Structure with all output information
 * \return	void
 */
void
print_output (T_output output)
{
   fprintf (stderr, "Output:  \n");
   fprintf
      (stderr, "\tFilename = %s\n\tShowModel = %s\n\tShow Param = %s\n\tShow simulation time = %d\n",
       output.outputfilename, bool_string (output.showModel),
       bool_string (output.showParam), output.showsimulTime);
}

/**
 * \brief       Add a son on a hierarchy tree [Load Sharing]
 * \param	node	Node where the son is added
 * \return	 struct st_hier_node * represents the "value" of children
 */
struct st_hier_node *
add_son (struct st_hier_node *node)
{
   struct st_linked_sons **P = &(node->sons);

   node->nbsons += 1;

   while (*P != NULL) {
      P = &((*P)->next);
   }
   *P = malloc (sizeof (struct st_linked_sons));
   (*P)->next = NULL;
   (*P)->val = malloc (sizeof (struct st_hier_node));
   (*P)->val->nbsons = 0;
   (*P)->val->sons = NULL;
   return (*P)->val;
}

/**
 * \brief       Convert a hierarchy tree to a distance matrix [Load Sharing]
 * \param	node_queue	Pointer on the hierarchy tree root
 * \param 	node_dist	Pointer on the matrix 
 * \param 	nb_queues 	Number of queues in the system
 * \return	int**		the new matrix 
 */
int **
make_table_node_dist (struct st_hier_node **node_queue,
                      int **node_dist, int nb_queues)
{
   int i, j, k;
   struct st_hier_node *P;
   struct st_hier_node *node;

   struct st_hier_node ***queue_path = (struct st_hier_node ***)
      malloc (nb_queues * sizeof (struct st_hier_node **));

   for (i = 0; i < nb_queues; i++) {
      node = node_queue[i];
      queue_path[i] = (struct st_hier_node **)
         malloc ((node->level + 1) * sizeof (struct st_hier_node *));
      P = node;
      j = node->level;

      while (P != NULL) {
         queue_path[i][j] = P;
         P = P->father;
         j--;
      }
   }

   for (i = 0; i < nb_queues; i++) {

      for (j = 0; j < i; j++) {
         k = 0;

         while (queue_path[i][k] == queue_path[j][k]
                && k < ((node_queue[i])->level + 1)) {
            k++;
         }

         node_dist[i][j] = (node_queue[i])->level + (node_queue[j])->level -
            2 * (queue_path[i][k - 1])->level;
      }

      node_dist[i][i] = 0;      /* by convention */
   }

   for (i = 0; i < nb_queues; i++) {
      free (queue_path[i]);
   }
   free (queue_path);

#if (DEBUG_LVL >= DEBUG_INFO)
   for (i = 0; i < nb_queues; i++) {
      fprintf (stderr, "[%2d] ", i);

      for (j = 0; j <= i; j++) {
         fprintf (stderr, "%3d ", node_dist[i][j]);
      }
      fprintf (stderr, "\n");
   }
#endif

   return node_dist;
}

/**
 * \brief       Free a hierarchy tree [Load Sharing]
 * \param	node	Pointer on the hierarchy tree root
 * \return	 void
 */
void
free_tree (struct st_hier_node *node)
{
   struct st_linked_sons *tmp;
   struct st_linked_sons *next;
   tmp = node->sons;
   if (node != NULL) {
      while (tmp != NULL) {
         free_tree (tmp->val);
         next = tmp->next;
         free (tmp);
         tmp = next;
      }
      free (tmp);
      free (node);
   }
}

/**
 * \brief      Parse a hierarchy tree in a file and transform it to a distance
 *             matrix [Load Sharing]
 *
 * \param      f              Stream where is 
 * \param      node_dist      Pointer on the future matrix 
 * \param      nb_queues      Number of queues in the system
 * \return     int**          the new matrix 
 */
int **
tree_parser (FILE * f, int **node_dist, int nb_queues)
{
   struct st_hier_node **node_queue;   /* [LOAD SHARING] pointer on the
                                        * queue_number<-->tree_node
                                        * correspondance array */
   int i;
   int num = 0;
   int level = 0;
   int queues = 0;
   char *c = malloc (100 * sizeof (char));
   int n;
   int fin_lect = 0;
   int readnext = 1;
   struct st_hier_node *cur = NULL;
   struct st_hier_node *son = NULL;
   struct st_hier_node *hier_root = NULL; /* [LOAD SHARING] pointer on the
                                           * hierarchy tree root */
   node_queue = (struct st_hier_node **)
      malloc (nb_queues * sizeof (struct st_hier_node *));

   while (!fin_lect) {

      if (readnext) {
         *c = (char) fgetc (f);
      }
      else
         readnext = 1;

      if (*c == ',') {          /* same level node enumeration */
         ;
      }
      else if (*c == '(') {
         if (level == 0 && hier_root == NULL) { // Begin of the reading
            hier_root = (struct st_hier_node *)
               malloc (sizeof (struct st_hier_node));
            hier_root->num = num;
            hier_root->level = level;
            hier_root->father = NULL;
            hier_root->nbsons = 0;
            hier_root->sons = NULL;
            cur = hier_root;
         }
         else {
            son = add_son (cur);
            num++;
            level++;
            son->num = num;
            son->level = level;
            son->father = cur;
            cur = son;
         }
      }
      else if (*c == ')') {
         cur = cur->father;
         level--;

         if (level == -1) {
            fin_lect = 1;
         }
      }
      else if (*c >= '0' && *c <= '9') {
         queues++;
         fprintf (stderr, "queues: %d/%d\n", queues, nb_queues);
         if (queues > nb_queues) {
            fprintf (stderr, "Problem in the tree of Load Sharing, there are too "
                    "queues.\n If you don't want to use LS, remove LS "
                    "parameter in param file\n");
            return NULL;
         }

         i = 0;
         while (*(c + i) >= '0' && *(c + i) <= '9') {
            i++;
            *(c + i) = (char) fgetc (f);
         }

         n = atoi (c);
         *c = *(c + i);
         readnext = 0;
         son = add_son (cur);
         num++;
         son->num = num;
         son->level = level + 1;
         son->father = cur;
         node_queue[n] = son;
      }
   }
   if (nb_queues > queues) {
      fprintf (stderr, "Problem in the tree, all queues are not defined.\n If you don't "
              "want to use LS, remove LS parameter in param file\n");
      return NULL;
   }

   free (c);
   node_dist = make_table_node_dist (node_queue, node_dist, nb_queues);
   free_tree (hier_root);
   free (node_queue);
   // free(hier_root);
   // 
   return node_dist;
}

 /**
 * \brief       Check the mode of Load Sharing ( Tree or Matrix) and switch on the right parser [Load Sharing]
 * \param	f		Stream where is 
 * \param 	node_dist	Pointer where is the future matrix 
 * \param 	nb_queues 	Number of queues in the system
 * \param 	mode		Pointer where save the mode 
 * \return	int**		the new matrix 
 */
int **
LS_parser (FILE * f, int **node_dist, int nb_queues, int *mode)
{
   char line[30];               /* or other suitable maximum line size */
   int i, j, result;

   if (!fscanf (f, "%s", line)) {
      fprintf (stderr, "Problem to read file\n");
      return NULL;
   }
   if (!strcmp ("Tree", line)) {
      node_dist = (int **) malloc (nb_queues * sizeof (int *));
      for (i = 0; i < nb_queues; i++)
         node_dist[i] = (int *) malloc ((i + 1) * sizeof (int));
      *mode = 1;
      node_dist = tree_parser (f, node_dist, nb_queues);
   }
   else if (!strcmp ("Matrix", line)) {
      *mode = 0;
      node_dist = (int **) malloc (nb_queues * sizeof (int *));
      for (i = 0; i < nb_queues; i++)
         node_dist[i] = (int *) malloc (nb_queues * sizeof (int));
      for (i = 0; i < nb_queues; i++) {
         for (j = 0; j < nb_queues; j++) {
            result = fscanf (f, "%d", &node_dist[i][j]);
            if (result == -1) {
               // for(j=0;j<i;j++)
               // free(node_dist[0]);
               free (node_dist);
               fprintf
                  (stderr, "The matrix is not enough big. If you don't want to use Load Sharing, remove it on the parameters file\n");
               return NULL;
            }
         }
      }
   }
   else {
      fprintf
         (stderr, "Problem in the load sharing file, mode is not correctly defined");
      for (i = 0; i < nb_queues; i++)
         free (node_dist[i]);
      free (node_dist);
      node_dist = NULL;
   }
   return node_dist;
}

 /**
 * \brief       Calculate neighbors hierarchy thanks to the distance matrix
 * \param 	node_dist		Pointer where is the future matrix 
 * \param 	node_neighbors		Pointer where is the future neighbors hierarchy
 * \param 	nb_queues 		Number of queues in the system
 * \return	struct st_neighbors *	the new neighbors hierarchy
 */
struct st_neighbors *node_dist_to_node_neighbors
   (int **node_dist, struct st_neighbors *node_neighbors, int nb_queues)
{
   int i, j, k;
   int nb_neigh;
   int *tmp_neigh = (int *) calloc (nb_queues, sizeof (int));

   node_neighbors = (struct st_neighbors *)
      calloc (nb_queues, sizeof (struct st_neighbors));

   for (i = 0; i < nb_queues; i++) {
      nb_neigh = 0;

//      for (j = 0; j < nb_queues; j++) {
      for (j = 0; j <= i; j++) {
         if (node_dist[i][j] == 1) {
            tmp_neigh[nb_neigh] = j;
            nb_neigh++;
         }
      }

      node_neighbors[i].nb_neighbors = nb_neigh;
      node_neighbors[i].neighbors = (int *) calloc (nb_neigh, sizeof (int));

      for (k = 0; k < nb_neigh; k++) {
         node_neighbors[i].neighbors[k] = tmp_neigh[k];
      }
   }

   free (tmp_neigh);

   return node_neighbors;
}

/**
 * \brief       Parse and return a int list of a key
 * \details    Parse and return an int list of a keyname
 * \param    file         Pointer of YAML config file
 * \param    name         Keyname
 * \param    my_list      Result int list
 * \param    size         Size of int list to be updated
 * \return     int*
 */
int
get_unsigned_long_list_keyname (FILE * file, char *name, unsigned long **my_list, int size)
{
   yaml_parser_t parser;
   yaml_token_t token;
   char *keyname = NULL;
   short int quit = 0;
   int rc = -4; // indicate the keyword is not found

   /* Start at the beginning of file */
   fseek (file, 0, 0);

   /* Initialize parser */
   if (!yaml_parser_initialize (&parser)) {
      fputs ("Failed to initialize parser!\n", stderr);
   }
   /* Set input file */
   yaml_parser_set_input_file (&parser, file);

   do {
      // get a token
      yaml_parser_scan (&parser, &token);

      switch (token.type) {

      case YAML_KEY_TOKEN:
         keyname = get_string (&parser, &token, keyname);
         break;

      case YAML_VALUE_TOKEN:
         if (!strcmp (keyname, name)) {
            rc = get_list_unsigned_long (&parser, &token, my_list, size);
            quit = 1;
         }
         free (keyname);
         break;

      default:
         break;
      }

      if (token.type != YAML_STREAM_END_TOKEN) {
         yaml_token_delete (&token);
      }

   } while (!quit && (token.type != YAML_STREAM_END_TOKEN));

   /* Delete the last token */
   yaml_token_delete (&token);
   /* Cleanup */
   yaml_parser_delete (&parser);

   return rc;
}

/**
 * \brief       Parse and return a int list
 * \details    Parse and return an int list of a keyname
 * \param    parser       Pointer of parser
 * \param    token        Pointer of token
 * \param    ptr_ptr      Result int list
 * \param    size         Size of int list to be updated
 * \return     int*
 */
int
get_list_unsigned_long (yaml_parser_t * parser, yaml_token_t * token, unsigned long **ptr, int size)
{
   short int quit = 0;
   int rc = -1;

   int vector = 0;
   int entry = 0;
   int componentId = 0;
   int nbComponent = 0;

   unsigned long *list = NULL;

   do {
      yaml_parser_scan (parser, token);
      switch (token->type) {

         case YAML_FLOW_SEQUENCE_START_TOKEN: // "["

            if (vector == 0) {
               vector = 1;
            }
            else { // vector == 1
               rc = -1;
               quit = 1;
               PSI3_ERROR ("get_list_unsigned_long: syntax error: [");
            }
            break;

         case YAML_FLOW_SEQUENCE_END_TOKEN: // "]"

            if (vector == 1 && nbComponent == size) {
               vector = 0;
               rc = 0;
               quit = 1;
               entry = 0;
            }
            else {
               rc = -3;
               quit = 1;
               PSI3_ERROR ("get_list_unsigned_long: size error [%d]", nbComponent);
            }
            break;

         case YAML_FLOW_ENTRY_TOKEN: // ","

            PSI3_DEBUG ("get_list_unsigned_long: separator");
            break;

         case YAML_SCALAR_TOKEN:

            if (!strcmp ((const char *) token->data.scalar.value, "~")) { // "~"
               rc = -2;
               quit = 1;
               break;
            }
            else if (vector == 0) {
               rc = -1;
               quit = 1;
               PSI3_ERROR ("get_list_unsigned_long: scalar problem [%s]", token->data.scalar.value);
               break;
            }

            if (entry == 0) {
               componentId = 0;
               list = malloc (size * sizeof (unsigned long));
               list [componentId] = atol ((const char *) token->data.scalar.value);
               entry = 1;
               nbComponent = 1;
            }
            else {
               list [componentId] = atol ((const char *) token->data.scalar.value);
               nbComponent++;
            }

            componentId++;

            break;

         default:
            break;
      }
      if (token->type != YAML_FLOW_SEQUENCE_END_TOKEN)
         yaml_token_delete (token);

   } while (!quit);

   *ptr = list;

   return rc;
}

int
get_seed_list_keyname (FILE * file, char *name, unsigned long ***list,
                       int *list_size, int vector_size)
{
   yaml_parser_t parser;
   yaml_token_t token;
   char *keyname = NULL;
   short int quit = 0;

   int rc = -4; // means not set

   /* Start at the beginning of file */
   fseek (file, 0, 0);

   /* Initialize parser */
   if (!yaml_parser_initialize (&parser)) {
      fputs ("Failed to initialize parser!\n", stderr);
   }
   /* Set input file */
   yaml_parser_set_input_file (&parser, file);

   do {
      // get a token
      yaml_parser_scan (&parser, &token);

      switch (token.type) {

      case YAML_KEY_TOKEN:
         keyname = get_string (&parser, &token, keyname);
         break;

      case YAML_VALUE_TOKEN:
         if (!strcmp (keyname, name)) {
            rc = get_seed_list (&parser, &token, list, list_size, vector_size);
            quit = 1;
         }
         free (keyname);
         break;

      default:
         break;
      }

      if (token.type != YAML_STREAM_END_TOKEN) {
         yaml_token_delete (&token);
      }

   } while (!quit && (token.type != YAML_STREAM_END_TOKEN));

   /* Delete the last token */
   yaml_token_delete (&token);
   /* Cleanup */
   yaml_parser_delete (&parser);

   if (rc == -4) { // not set, so the list is empty
      *list_size = 0;
   }

   return rc;
}

int
get_seed_list (yaml_parser_t * parser, yaml_token_t * token,
               unsigned long ***ptr, int *list_size, int vector_size)
{
   short int quit = 0;
   int rc = -1;

   int list = 0;
   int vector = 0;
   int entry = 0;

   unsigned long **seed_list = NULL;

   int vectorId = 0;
   int nbVector = 0;
   int componentId = 0;
   int nbComponent = 0;

   do {
      yaml_parser_scan (parser, token);

//      printf("Got token of type %d\n", token->type);

      switch (token->type) {

         case YAML_FLOW_SEQUENCE_START_TOKEN: // "["

            if (list == 0) {
               list = 1;
            }
            else if (vector == 0) {
               vector = 1;

               if (nbVector == 0) {
                  nbVector = 1;
                  vectorId = 0;
                  seed_list = malloc (sizeof (unsigned long *));
               }
               else {
                  vectorId++;
                  nbVector++;
                  seed_list = realloc (seed_list, nbVector * sizeof (unsigned long *));
               }
            }
            else {
               rc = -1;
               quit = 1;
               PSI3_ERROR ("Error: too much [");
            }

            break;

         case YAML_FLOW_SEQUENCE_END_TOKEN: // "]"

            if (list == 1 && vector == 1) {
               list = 1;
               vector = 0;
            }
            else if (list == 1 && vector == 0) {
               list = 0;

               if (nbVector == 1) {
                  rc = -2;
               }
               else if (nbVector >= *list_size) {
                  rc = 0;
               }
               else {
                  rc = -3;
               }

               *list_size = nbVector;
               quit = 1;
            }
            else {
               rc = -1;
               quit = 1;
               PSI3_ERROR ("Error: too much ]\n");
            }

            break;

         case YAML_FLOW_ENTRY_TOKEN: // ","

            break;

         case YAML_SCALAR_TOKEN: // value

            if (!strcmp ((const char *) token->data.scalar.value, "~")) { // "~"
               rc = -4;
               quit = 1;
               break;
            }
            else if (vector == 0) {
               rc = -1;
               quit = 1;
               PSI3_ERROR ("get_seed_list: scalar problem [%s]", token->data.scalar.value);
               break;
            }

            if (entry == 0) {
               componentId = 0;
               seed_list [vectorId] = malloc (6 * sizeof (unsigned long));
               seed_list [vectorId] [componentId] = atol ((const char *) token->data.scalar.value);
               entry = 1;
               nbComponent = 1;
            }
            else {
               seed_list [vectorId] [componentId] = atol ((const char *) token->data.scalar.value);
               nbComponent++;
            }

            componentId++;

            if (nbComponent >= 6) {
               entry = 0;
               nbComponent = 0;
            }
            break;

         default:
            break;
      }

   } while (!quit);

   *ptr = seed_list;

   return rc;
}

int
get_initialstate_list_keyname (FILE * file, char *name, int vector_size,
                               int ***my_list, int *size)
{
   int rc = -2; // by default -2 indicate that no InitialState is set
   yaml_parser_t parser;
   yaml_token_t token;
   char *keyname = NULL;
   short int quit = 0;

   /* Start at the beginning of file */
   fseek (file, 0, 0);

   /* Initialize parser */
   if (!yaml_parser_initialize (&parser)) {
      fputs ("Failed to initialize parser!\n", stderr);
   }
   /* Set input file */
   yaml_parser_set_input_file (&parser, file);

   do {
      // get a token
      yaml_parser_scan (&parser, &token);

      switch (token.type) {

      case YAML_KEY_TOKEN:
         keyname = get_string (&parser, &token, keyname);
         break;

      case YAML_VALUE_TOKEN:
         if (!strcmp (keyname, name)) {
            rc = get_initialstatelist (&parser, &token, vector_size, my_list, size);
            quit = 1;
         }
         free (keyname);
         break;

      default:
         break;
      }

      if (token.type != YAML_STREAM_END_TOKEN) {
         yaml_token_delete (&token);
      }

   } while (!quit && (token.type != YAML_STREAM_END_TOKEN));

   /* Delete the last token */
   yaml_token_delete (&token);
   /* Cleanup */
   yaml_parser_delete (&parser);

   return rc;
}

int
get_initialstatelist  (yaml_parser_t * parser, yaml_token_t * token,
                       int vector_size, int ***ptr, int *size)
{
   short int quit = 0;
   int rc = 0;

   int list = 0;
   int vector = 0;
   int entry = 0;

   int **initialState_list = NULL;

   int vectorId = 0;
   int nbVector = 0;
   int componentId = 0;
   int nbComponent = 0;

   do {
      yaml_parser_scan (parser, token);

//      printf("Got token of type %d\n", token->type);

      switch (token->type) {

         case YAML_FLOW_SEQUENCE_START_TOKEN: // "["

            if (list == 0) {
               list = 1;
            }
            else if (vector == 0) {
               vector = 1;

               if (nbVector == 0) {
                  nbVector = 1;
                  vectorId = 0;
                  initialState_list = malloc (sizeof (int *));
               }
               else {
                  vectorId++;
                  nbVector++;
                  initialState_list = realloc (initialState_list, nbVector * sizeof (int *));
               }
            }
            else {
               quit = 1;
               rc = -1;
            }

            break;

         case YAML_FLOW_SEQUENCE_END_TOKEN: // "]"

            if (list == 1 && vector == 1 && entry == 1) {

               if (nbComponent >= vector_size) {
                  entry = 0;
                  nbComponent = 0;
               }

               list = 1;
               vector = 0;

            }
            else if (list == 1 && vector == 0 && entry == 0) {
               list = 0;
               quit = 1;
            }
            else {
               quit = 1;
               rc = -1;
            }

            break;

         case YAML_FLOW_ENTRY_TOKEN: // ","

            break;

         case YAML_SCALAR_TOKEN: // value

            if (!strcmp ((const char *) token->data.scalar.value, "~")) { // "~"
               rc = -2;
               quit = 1;
               break;
            }
            else if (list != 1 || vector != 1) {
               quit = 1;
               rc = -1;
               break;
            }

            if (entry == 0) {
               componentId = 0;
               initialState_list [vectorId] = malloc (vector_size * sizeof (int));
               initialState_list [vectorId] [componentId] = atoi ((const char *) token->data.scalar.value);
               //printf ("Read: %d\n", initialState_list [vectorId] [componentId]);
               entry = 1;
               nbComponent = 1;
            }
            else {
               initialState_list [vectorId] [componentId] = atoi ((const char *) token->data.scalar.value);
               //printf ("Read: %d\n", initialState_list [vectorId] [componentId]);
               nbComponent++;
            }

            componentId++;

            break;

         default:
            quit = 1;
            rc = -1;
            break;
      }

   } while (!quit);

   *size = nbVector;
   *ptr = initialState_list;

   return rc;
}

