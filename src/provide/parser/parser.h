 /**
 * \file       	parser.h
 * \author    	Florian LEVEQUE
 * \author		Minh Quan HO
 * \author      Jean-Marc.Vincent@imag.fr
 * \version   	1.0
 * \date       	26/06/2012
 * \brief       Declare all functions of parsing YAML
 */

 /* 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */


#ifndef PARSER_H
#define PARSER_H

#include <limits.h>
#include <psi.h>
#include "./yaml/include/yaml.h"
#include <psi_common.h>


//#define PRINT_DATA_AFTER_PARSING 1
#define NUMBER_ALGOS 4
#define NUMBER_DEFAULT_KEYWORDS 3
#define ERROR -100
#define bool_string(x) x ? "Yes" : "No"

typedef struct t_label{
  char* label;
  int index;
}T_label;
	
void print_output(T_output output);
T_output *read_output (char *output_cfgfile, void *default_lib, void *user_lib);
void print_model(FILE *f,T_model model);
T_model *read_model (char *model_file, void *default_lib, void *user_lib);
int valid_version(double version);
T_events *read_events(yaml_parser_t *parser,yaml_token_t *token,void *default_lib,void *user_lib);
T_queues *read_queues(yaml_parser_t *parser,yaml_token_t *token);
int find_index_labels(T_label *list,int size,char *chain);
void *get_fct_by_keyname(void *default_lib,void *user_lib,FILE *file,char *name);
void *get_ptr_fct(void *default_lib,void *user_lib,char *fctname);
char **split(char *full,char delim);
int *get_int_list_keyname(FILE *file,char *name,int **my_list,int *size);
int *get_list_int(yaml_parser_t *parser,yaml_token_t *token,int *ptr,int *size);
char **get_string_list_keyname(FILE *file,char *name,char **my_list,int *size);
char **get_list_string(yaml_parser_t *parser,yaml_token_t *token,char **ptr_ptr,int *size);
char *get_string_keyname(FILE *file,char *my_chain);
int get_int_keyname(FILE *file,char *name);
int get_bool_keyname(FILE* file, char* name);
double get_double_keyname(FILE *file,char *name);
char *get_string(yaml_parser_t *parser,yaml_token_t *token,char *res);
double get_double(yaml_parser_t *parser,yaml_token_t *token);
int get_int(yaml_parser_t *parser,yaml_token_t *token);
void print_list_int(FILE *f,int *list,int size);
void print_list_string(FILE *f,char **list,int size);
st_general_ptr init_general_param ();
void free_general_param (st_general_ptr general);
void free_output(T_output *output);
void free_model(T_model *model);
void free_events(T_events *events);
void free_queues(T_queues *queues);
void free_event(T_event *evt);
void free_label_events(T_events* events);
void free_label_event(T_event* evt);
void free_transcode();
void init_output(T_output *output);
void init_model(T_model *model);
void init_events(T_events *events);
void init_event(T_event *event);
void init_queues(T_queues *queues);
int** tree_parser(FILE * f,int **matrix, int nb_queues);
int** LS_parser(FILE * f,int **matrix, int nb_queues,int *mode);
struct st_neighbors * node_dist_to_node_neighbors(int **node_dist,struct st_neighbors * node_neighbors,int nb_queues);
unsigned long
get_unsigned_long (yaml_parser_t * parser, yaml_token_t * token);

int
get_list_unsigned_long (yaml_parser_t * parser, yaml_token_t * token,
                        unsigned long **ptr, int size);

int
get_unsigned_long_list_keyname (FILE * file, char *name,
                                unsigned long **my_list, int size);

int
get_seed_list (yaml_parser_t * parser, yaml_token_t * token,
               unsigned long ***ptr, int *list_size, int vector_size);

int
get_seed_list_keyname (FILE * file, char *name, unsigned long ***list,
                       int *list_size, int vector_size);

int
get_initialstate_list_keyname (FILE * file, char *name, int vector_size,
                               int ***my_list, int *size);
int
get_initialstatelist  (yaml_parser_t * parser, yaml_token_t * token,
                       int vector_size, int ***ptr, int *size);


#endif//PARSER_H
