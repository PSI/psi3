 /**
 * \file          multipleforward.c
 * \author        Minh Quan HO
 * \author        Jean-Marc.Vincent@imag.fr
 * \author        Benjamin.Briot@inria.fr
 * \version       1.3.3
 * \date          17/06/2015
 * \brief         Multiple Simple forward kernel
 */

 /* 
  * This library is free software; you can redistribute it and/or
  * modify it under the terms of the GNU Lesser General Public
  * License as published by the Free Software Foundation; either
  * version 2.1 of the License, or any later version.
  *
  * This library is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  * Lesser General Public License for more details.
  *
  * You should have received a copy of the GNU Lesser General Public
  * License along with this library; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
  */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <dlfcn.h>
#include <sys/time.h>
#include <time.h>
#include <generator.h>
#include <parser.h>
#include "psi_common.h"

#include <omp.h>

#define OUTPUT_THRESHOLD 500    /* buffer trajectory before writing out */

/*****************************************************************************/
/*             Global variables used by transition or simulation             */

/* TRANSITION ---------- */
/* Model FIXME: should disapear from this space since it moved to a struct */
/* - Queues: */
int *capmins;                   // capacity min of queues
int *capmaxs;                   // capacity max of queues
int nb_queues;

/* Load sharing */
int ls_mode;
int **node_dist;                /* [LOAD SHARING] matrix of distance between
                                 * two queues : node_dist[i][j] = distance
                                 * between i and j */
struct st_neighbors *node_neighbors;   /* [LOAD SHARING] array containing for
                                        * each node, the neighbors of the node */
int nb_AV;
int FLAG;

/* SIMULATION ---------- */
/* FIXME: will probably move to a struct */
double simulation_time;

/*****************************************************************************/

/**
 * \brief      Procedure to initialize the kernel from a model
 * \details    Initialize global variables from the model
 * \param      model    system model
 * \return     void
 */
void
init_kernel (st_model_ptr model)
{
   /**
    * initialize the random event generator :
    */
   init_generator (model->events->nb_evts, model->events->evts);
   
   capmins = model->queues->mins;
   capmaxs = model->queues->maxs;
   nb_queues = model->queues->nb_queues;
   FLAG = ALGO_NONMONOTONE;
}

/**
 * \brief      Procedure to free allocated parameters
 * \return     void
 */
void
free_method (st_method_ptr method_ptr)
{
   st_multipleForward_ptr method = method_ptr;

   if (method_ptr) {
      method = method_ptr;
   }
   else
      return;

   if (method->InitialState != NULL) {
      free (method->InitialState);
   }
   if (method->StopFct != NULL) {
      free (method->StopFct);
   }
   if (method->OutputFct != NULL) {
      free (method->OutputFct);
   }
   free (method);
}

/**
 * \brief      Procedure to print method parameters to the outout file
 * \param      output_file    file descriptor to the output file
 * \param      method_ptr     method
 * \return     void
 */
void
print_method (FILE * output_file, st_method_ptr method_ptr)
{
//   int t;                       /* thread iterator */
   st_multipleForward_ptr method = (st_multipleForward_ptr) method_ptr;

   fprintf (output_file, "# Method: multipleforward\n");
   fprintf (output_file, "#\tTrajectoryLength: %d\n", method->TrajectoryLength);
   fprintf (output_file, "#\tNumThreads: %d\n", method->NumThreads);
   fprintf (output_file, "#\tMaxTime: %f\n", method->MaxTime);
   fprintf (output_file, "#\tTiming: %d\n", method->Timing);
}

void
print_trajectoryInfo (FILE * output_file, int trajectoryNumber,
                      unsigned long seed[6], st_multipleForward_ptr method,
                      st_model_ptr model)
{
   int *InitialState;

   fprintf (output_file, "\n# Trajectory %d:\n", trajectoryNumber);
   fprintf (output_file, "#\tSeed: ");
   fprintf (output_file,
            "[%lu, %lu, %lu, %lu, %lu, %lu]\n",
            seed[0], seed[1], seed[2], seed[3], seed[4], seed[5]);

   fprintf (output_file, "#\tInitialState: [");
   if (method->InitialStateCommon)
      InitialState = method->InitialState[0];
   else
      InitialState = method->InitialState[trajectoryNumber];

   print_list_int (output_file, InitialState, model->queues->nb_queues);
   fprintf (output_file, "]\n");
}

/**
 * \brief      Procedure to init the method structure
 * \return     a pointer to a method structure
 *             NULL if failed
 */
st_multipleForward_ptr
init_method ()
{
   st_multipleForward_ptr method = (st_multipleForward_ptr)
      calloc (1, sizeof (st_multipleForward));
   if (method == NULL)
      return NULL;
   method->StopFct = (st_function_ptr) calloc (1, sizeof (st_function));
   if (method->StopFct == NULL)
      return NULL;
   method->OutputFct = (st_function_ptr) calloc (1, sizeof (st_function));
   if (method->OutputFct == NULL)
      return NULL;

   method->TrajectoryLength = 0;
   method->MaxTime = -1;
   method->InitialState = NULL;
   method->InitialState_size = 0;
   method->InitialStateCommon = 0;
   method->UserTimingSeed = 0;
   method->Timing = 1;
   method->TimingSeed = NULL;
   method->TimingSeedSize = 0;
   method->TimingSeedCommon = 0;
   method->TrajectorySeed = NULL;
   method->TrajectorySeedCommon = 0;
   method->Seed_size = 0;
   method->UserSeed = 0;
   method->NumThreads = 1;
   method->output_threshold = 0;
   method->cpt_transitions = 0;
   method->Concatenate = 1; /* FIXME: could be set by user */

   return method;
}

/**
 * \brief      Function to read an initialize method simulation parameters
 * \param      method_param_cfgfile    method config file name
 * \param      general                 pointer to the general structure
 * \param      model                   pointer to the model structure
 * \return     a pointer to a initialized method structure
 *             NULL if failed
 */
st_multipleForward_ptr
read_method (char *method_param_cfgfile,
             st_general_ptr general, st_model_ptr model)
{
   int err = 0;
   int t;
   int rc = 0;
   FILE *method_param_fd;

   PSI3_DEBUG ("Opening method parameters file: %s", method_param_cfgfile);
   method_param_fd = fopen (method_param_cfgfile, "r");
   if (method_param_fd == NULL) {
      PSI3_ERROR ("Fail to open file %s", method_param_cfgfile);
      return NULL;
   }

   st_multipleForward_ptr method = init_method ();

   /**
    * Get TrajectoryLength
    */
   method->TrajectoryLength =
      get_int_keyname (method_param_fd, "TrajectoryLength");
   if (method->TrajectoryLength <= 0) {
      PSI3_ERROR ("TrajectoryLength must be positive");
      err++;
   }
   else {
      /* FIXME: output_threshold is currently 5% of the trajectory length */
      method->output_threshold = (method->TrajectoryLength * 5) / 100;
   }
   PSI3_DEBUG ("TrajectoryLength: [%d]", method->TrajectoryLength);

   if ((method->output_threshold <= 0)
       || (method->output_threshold > method->TrajectoryLength)) {

      method->output_threshold = method->TrajectoryLength;
   }
   PSI3_DEBUG ("OutputThreshold: automatic value [%d]",
              method->output_threshold);

   /**
    * Get NumThreads
    */
   method->NumThreads = get_int_keyname (method_param_fd, "NumThreads");
   if (method->NumThreads <= 0) {
      method->NumThreads = 1;
      PSI3_INFO ("NumThreads: automatic value [%d]", method->NumThreads);
   }
   else {
      PSI3_DEBUG ("NumThreads: %d", method->NumThreads);
   }

   /**
    * Get NumTrajectory: if seed has been set, we compute only one trajectory
    */
   method->NumTrajectory = get_int_keyname (method_param_fd, "NumTrajectory");

   if (method->NumTrajectory <= 0) {
      method->NumTrajectory = 1;
      PSI3_INFO ("NumTrajectory: automatic value [%d]", method->NumTrajectory);
   }
   else {
      PSI3_DEBUG ("NumTrajectory: %d", method->NumTrajectory);
   }

   method->TimingSeedSize = method->NumTrajectory;
   method->Seed_size = method->NumTrajectory;

   /**
    * Get TrajectorySeed: to reprocude several trajectories
    */
   rc = get_seed_list_keyname (method_param_fd,
                               "TrajectorySeed",
                               &(method->TrajectorySeed),
                               &(method->Seed_size),
                               SEED_VECTOR_SIZE);

   if (rc == 0) { /* exists and has the proper size */

      method->UserSeed = 1;

      for (t = 0; t < method->NumTrajectory; t++) {
         PSI3_DEBUG ("Trajectory [%d] Seed: [%lu, %lu, %lu, %lu, %lu, %lu]",
                     t,
                     method->TrajectorySeed[t][0],
                     method->TrajectorySeed[t][1],
                     method->TrajectorySeed[t][2],
                     method->TrajectorySeed[t][3],
                     method->TrajectorySeed[t][4],
                     method->TrajectorySeed[t][5]);
      }
   }
   else if (rc == -2) { /* exists and has only one vector => the vector is
                         * common to all trajectories */

      method->UserSeed = 1;
      method->TrajectorySeedCommon = 1;

      for (t = 0; t < method->NumTrajectory; t++) {

         PSI3_DEBUG ("Trajectory [%d] Seed: [%lu, %lu, %lu, %lu, %lu, %lu]",
                     t,
                     method->TrajectorySeed[0][0],
                     method->TrajectorySeed[0][1],
                     method->TrajectorySeed[0][2],
                     method->TrajectorySeed[0][3],
                     method->TrajectorySeed[0][4],
                     method->TrajectorySeed[0][5]);
      }
   }
   else if (rc == -3) {
      PSI3_ERROR ("TrajectorySeed: incorrect vector number");
      err++;
   }
   else if (rc == -1) {
      PSI3_ERROR ("TrajectorySeed: error parsing vector");
      err++;
   }
   else { /* "~" */
      PSI3_DEBUG ("TrajectorySeed: no user seed for trajectories");
   }

   /**
    * Get Timing: FIXME: impact the simulation
    */
   rc = get_bool_keyname (method_param_fd, "Timing");
   if (rc != 0) { /* Yes by default */
      method->Timing = 1;
   }
   else {
      method->Timing = 0;
   }
   PSI3_DEBUG ("Timing: %d", method->Timing);

   /**
    * Get TimingSeed:
    */
   rc = get_seed_list_keyname (method_param_fd,
                               "TimingSeed",
                               &(method->TimingSeed),
                               &(method->TimingSeedSize),
                               SEED_VECTOR_SIZE);

   if (rc == 0) { /* exists and has the proper size */

      method->UserTimingSeed = 1;

      for (t = 0; t < method->NumTrajectory; t++) {
         PSI3_DEBUG ("Trajectory: [%d] TimingSeed: [%lu, %lu, %lu, %lu, %lu, %lu]",
                     t,
                     method->TimingSeed[t][0],
                     method->TimingSeed[t][1],
                     method->TimingSeed[t][2],
                     method->TimingSeed[t][3],
                     method->TimingSeed[t][4],
                     method->TimingSeed[t][5]);
      }
   }
   else if (rc == -2) { /* exists and has only one vector => the vector is
                         * common to all trajectories */

      method->UserTimingSeed = 1;
      method->TimingSeedCommon = 1;

      for (t = 0; t < method->NumTrajectory; t++) {
         PSI3_DEBUG ("TimingSeed: [%lu, %lu, %lu, %lu, %lu, %lu]",
                     method->TimingSeed[0][0],
                     method->TimingSeed[0][1],
                     method->TimingSeed[0][2],
                     method->TimingSeed[0][3],
                     method->TimingSeed[0][4],
                     method->TimingSeed[0][5]);
      }
   }
   else if (rc == -3) {
      PSI3_ERROR ("TimingSeed: incorrect vector number");
      err++;
   }
   else if (rc == -1) {
      PSI3_ERROR ("TimingSeed: error parsing vector");
      err++;
   }
   else { /* "~" */
      PSI3_DEBUG ("TimingSeed: no user seed for timing");
   }

   /**
    * Get MaxTime
    */
   method->MaxTime = get_double_keyname (method_param_fd, "MaxTime");

   if (method->MaxTime > 0) {
      PSI3_DEBUG ("MaxTime: [%f]", method->MaxTime);
   }
   else {
      method->MaxTime = INFINITY;
      PSI3_INFO ("MaxTime: automatic value [%f] (should be >= 0)", method->MaxTime);
   }

   /**
    * Get initial state(s)
    */
   rc = get_initialstate_list_keyname (method_param_fd,
                                       "InitialState",
                                       model->queues->nb_queues,
                                       &method->InitialState,
                                       &method->InitialState_size);

   if (rc == 0 && method->InitialState != NULL) {

      if (method->InitialState_size == 1) {
         method->InitialStateCommon = 1;
      }
      else if (method->InitialState_size < method->NumTrajectory) {
         PSI3_ERROR ("InitialState list should have %d vectors",
                     method->NumTrajectory);
         err++;
      }

      for (t = 0; t < method->InitialState_size; t++) {
         if (check_initialState (method->InitialState[t], model)) {
            PSI3_ERROR ("InitialState not valid for trajectory", t);
            err++;
         }
      }

   }
   else if (rc == -2) {         /* InitialState not set by user, or ~ */

      method->InitialState = malloc (method->NumTrajectory * sizeof (int *));

      for (t = 0; t < method->NumTrajectory; t++) {
         method->InitialState[t] =
            random_int_list_NEW (model->queues->nb_queues, model->queues->maxs,
                  model->queues->mins);
      }
      method->InitialState_size = method->NumTrajectory;

      PSI3_INFO ("InitialState: no user initial states");
   }
   else {                       /* rc == -1 */
      PSI3_ERROR ("InitialState: problem when parsing");
      PSI3_ERROR ("InitialState: check the vector size in your list "
                  "or the number of lists", model->queues->nb_queues);
      err++;
   }
#if (DEBUG_LVL >= DEBUG_INFO)
   if (!err) {
      int i;
      if (method->InitialStateCommon) {
         fprintf (stderr, "DEBUG: InitialState: [%d",
                  method->InitialState[0][0]);
         for (i = 1; i < model->queues->nb_queues; i++) {
            fprintf (stderr, " %d", method->InitialState[0][i]);
         }
         fprintf (stderr, "]\n");
      }
      else {
         for (t = 0; t < method->NumTrajectory; t++) {
            fprintf (stderr, "DEBUG: Trajectory [%d] InitialState: [", t);
            for (i = 0; i < model->queues->nb_queues;
                 fprintf (stderr, " %d", method->InitialState[t][i++]));
            fprintf (stderr, "]\n");
         }
      }
   }
#endif

   /**
    * Get Stop Function
    */
   method->StopFct->name = get_string_keyname (method_param_fd, "StopFct");
   if (method->StopFct->name == NULL) {
      method->StopFct->name = "Default$stop";
      PSI3_DEBUG ("StopFct: automatic default value (%s)",
                  method->StopFct->name);
   }
   method->StopFct->ptr = (T_ptr_stopfct) get_ptr_fct (general->default_lib,
                                                       general->user_lib,
                                                       method->StopFct->name);
   if (method->StopFct->ptr == NULL) {
      err++;
   }
   PSI3_DEBUG ("StopFct: %s (%p)", method->StopFct->name, method->StopFct->ptr);

   /**
    * Get Output Function
    */
   method->OutputFct->name = get_string_keyname (method_param_fd, "OutputFct");
   if (method->OutputFct->name == NULL) {
      method->OutputFct->name = "Default$output_MF";
      PSI3_DEBUG ("OutputFct: automatic default value (%s)",
                  method->OutputFct->name);
   }
   method->OutputFct->ptr = (T_ptr_stopfct) get_ptr_fct (general->default_lib,
                                                         general->user_lib,
                                                         method->
                                                         OutputFct->name);
   if (method->OutputFct->ptr == NULL) {
      PSI3_ERROR ("OutputFct: not found..");
      err++;
   }
   PSI3_DEBUG ("OutputFct: %s (%p)", method->OutputFct->name,
               method->OutputFct->ptr);

   fclose (method_param_fd);

   if (err) {
      free (method->StopFct);
      free (method->OutputFct);
      free (method);
      return NULL;
   }

   return method;
}

/**
 * \brief      Procedure to run the simulation
 * \details    Run simulation and store updated global variables (e.g state,
 *             trajectory etc)
 *
 * \return     1 if ok
 *             -1 if error
 */
int
simulation (st_general_ptr general,
            st_multipleForward_ptr method,
            st_model_ptr model)
{
   /* state of the system */
   int **log_states = NULL; /* TODO: one day this will become:
                             * _Thread_local, but for now gcc 4.8 is
                             * the default compiler in ubuntu LTS and
                             * only understand __thread. */
   int *events_list = NULL;
   double *timing_states = NULL;
   T_state *states = NULL;
   T_event *event = NULL;
   unsigned long *seed = NULL;
   int *initialState = NULL;
   struct timeval ti, tf;
   int cpt_log_size = 0;
   int n = 0, t = 0;
   int nbCompleted = 0;
   int nbTransitions = method->TrajectoryLength - 1;  /* because of initial
                                                       * state */
   st_simulation_ptr simulation;
   simulation = malloc (sizeof (st_simulation));
   simulation->finished = 0;
   
   omp_set_num_threads (method->NumThreads);

   print_header (general->output_file, general, model, method, print_method);

   psi3_output_set_multistream (general, method->NumTrajectory);

   PSI3_DEBUG ("nbTransitions: [%d]", nbTransitions);
   PSI3_DEBUG ("outputfilename: [%s]", general->output->outputfilename);

   /* In this model, we use one stream per trajectory */
   generator_set_multistream (method->NumTrajectory);

#pragma omp parallel firstprivate (n, cpt_log_size, nbTransitions, events_list, states, event, seed, initialState, timing_states, log_states)
   {
      /* Memory allocation ----------------------------------------------------
       */
      st_simulation simulation;
      simulation.finished = 0;

      events_list = (int *) malloc (nbTransitions * sizeof (int));

      states = (T_state *) malloc (sizeof (T_state));
      states->nb_vectors = 1;
      states->splitting = 0;
      states->queues = (int **) malloc (sizeof (int *));
      states->queues[0] =
         (int *) malloc (model->queues->nb_queues * sizeof (int));

      timing_states = (double *) malloc (method->output_threshold * sizeof (double));

      log_states = (int **) malloc (method->output_threshold * sizeof (int *));
      for (n = 0; n < method->output_threshold; n++) {
         log_states[n] =
            (int *) malloc (model->queues->nb_queues * sizeof (int));
      }

      if (method->UserSeed == 0) {
         seed = calloc (SEED_VECTOR_SIZE, sizeof (unsigned long));
      }

#pragma omp single
      {
         loadbar (0, method->NumTrajectory); /* to indicate we start the
                                              * computation */
         gettimeofday (&ti, NULL);
      }

#pragma omp for
      for (t = 0; t < method->NumTrajectory; t++) {   /* PARALLEL LOOP
                                                       * ----------- */
//         int threadId = omp_get_thread_num ();
//         PSI3_DEBUG ("Thread: [%d] Trajectory: [%d]", threadId, t);
         int i, trajectoryLength;

         simulation.finished = 0;

         trajectoryLength = method->TrajectoryLength;

         /* Init continuous time -------------------------------------------- */
         st_timing_ptr timing;
      
         if (method->UserTimingSeed) {

            if (method->TimingSeedCommon) {
               timing = psi3_timing_init (t, method->TimingSeed[0], model);
            }
            else {
               timing = psi3_timing_init (t, method->TimingSeed[t], model);
            }
         }
         else {
            timing = psi3_timing_init (t, NULL, model);
         }

         /* Precompute continuous time -------------------------------------------- */

         /* We pre-compute only if there is a time limit: it allows to reduce the
          * trajectory length. The time will be computed during the simulation anyway
          * to associate a timing to each state (and allocate memory respecting the
          * number of states). */

         if (! isinf (method->MaxTime)) {

            for (i = 0; i < trajectoryLength; i++) {

               psi3_timing_next (timing);

               if (timing->value >= method->MaxTime
                     && ((i + 1) <= trajectoryLength)) {

                  trajectoryLength = i + 1;
                  PSI3_DEBUG ("Update TrajectoryLength due to MaxTime constraint");
                  PSI3_DEBUG ("TrajectoryLength: [%d]", trajectoryLength);
                  break;
               }
            }

            psi3_timing_reset (timing);

            nbTransitions = trajectoryLength;
         }

         /* Initial state --------------------------------------------------- */
         if (method->InitialStateCommon) {
            initialState = method->InitialState[0];
         }
         else {
            initialState = method->InitialState[t];
         }

         memcpy (states->queues[0],
                 initialState, model->queues->nb_queues * sizeof (int));
         memcpy (log_states[0],
                 initialState, model->queues->nb_queues * sizeof (int));

         timing_states[0] = 0;

//         PSI3_DEBUG ("Thread: [%d] initialState: [%d]@%p", omp_get_thread_num (),
//                     states->queues[0][0], initialState);

         /* Print info... --------------------------------------------------- */
         FILE *output_fd = psi3_get_output (t);

         if (method->UserSeed) {
            if (method->TrajectorySeedCommon) {
              seed = method->TrajectorySeed[0];
            }
            else {
              seed = method->TrajectorySeed[t];
            }
            generator_set_state (t, seed);
         }
         else {
            generator_get_state (t, seed);
         }

         print_trajectoryInfo (output_fd, t, seed, method, model);

         /* initialize the events_list */
         for (n = 0; n < nbTransitions; n++) {
            events_list[n] =
               genere_evenement (u01_p (t), model->events->nb_evts);
         }

         n = 0;
         cpt_log_size = 1;      /* because of initial state */

         do {

            if (cpt_log_size >= method->output_threshold) {
               // write out log_states and fetch to the beginning
               ((T_ptr_outputfct) (method->OutputFct->ptr)) (output_fd,
                                                             log_states,
                                                             cpt_log_size,
                                                             events_list,
                                                             timing_states,
                                                             n,
                                                             nbTransitions,
                                                             simulation);

               // reset cpt_log_size
               cpt_log_size = 0;
            }

            /* 1 . get event from events list */
            event = model->events->evts + events_list[n];

            /* 2 . e(x+1) = Transition(e(x),event) */
            (((T_ptr_transfct) event->ptr_transition)) (states, event, model);
            // FIXME method->cpt_transitions++;

            /* 3. Store state into log_states */
            memcpy (log_states[cpt_log_size],
                    states->queues[0], model->queues->nb_queues * sizeof (int));

            timing_states [cpt_log_size] = psi3_timing_next (timing);

            cpt_log_size++;
            n++;

            if (((T_ptr_stopfct) method->StopFct->ptr) (states->queues[0])) {
               simulation.finished = 1;
            }

            if (n >= nbTransitions) {
               simulation.finished = 1;
            }

         } while (! simulation.finished);

         // / write the last part of log_states if cpt_log_size != 0
         if (cpt_log_size > 0) {
            ((T_ptr_outputfct) (method->OutputFct->ptr)) (output_fd,
                                                          log_states,
                                                          cpt_log_size,
                                                          events_list,
                                                          timing_states,
                                                          n,
                                                          nbTransitions,
                                                          simulation);
         }

#pragma omp critical
               {
                  nbCompleted++;
                  loadbar (nbCompleted, method->NumTrajectory);
               }


      }                         // END PARALLEL LOOP -------------------
   }

   gettimeofday (&tf, NULL);
   
   simulation_time = ((double) ((tf.tv_sec - ti.tv_sec) * 1000.0
                                + (tf.tv_usec - ti.tv_usec) / 1000.0));

   simulation->time = simulation_time;

   print_footer (general, model, method, simulation_time);

   print_time (stderr, simulation_time);

   print_perf (stderr, general, simulation);

   if (method->Concatenate) {
      psi3_output_concatenate ();
   }
   // FREE
//   free(states.queues[0]);
//   free(states.queues);
//   for (n = 0; n < method->output_threshold; n++) {
//      free(log_states[n]);
//   }
//   free(log_states);
//   free(state);
//   free(events_list);

   return 1;
}

/**
 * \brief      Main
 * \details    - Load libraries
 *             - Parse config files
 *             - Initialize data structures
 *             - Run simulation & write into output file
 * \return     should return 0 if something hapened wrong
 */
int
main (int argc, char **argv)
{
   st_model_ptr model = NULL;
   st_multipleForward_ptr method = NULL;
   st_general_ptr general = NULL;
   int rc;

   rc = read_configuration (&general,
                            argv[2],
                            &model,
                            argv[1],
                            (void **) &method, argv[3], (void *) read_method);

   if (rc) {                    /* if reading configuration failed */
      close_psi (general, model, method, free_method);
      return 1;
   }

   /**
    * initialize kernel from the model
    */
   init_kernel (model);

   /**
    * Run the simulation and write the trajectories into output file:
    */
   simulation (general, method, model);

   /**
    * clean up the system:
    */
   return close_psi (general, model, method, free_method);
}
