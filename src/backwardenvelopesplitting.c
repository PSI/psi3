/**
 * \file          backwardenvelopesplitting.c
 * \author        Florian LEVEQUE
 * \author        Jean-Marc.Vincent@imag.fr
 * \version       1.0.0
 * \date          02/07/2012
 * \brief         Backward Envelop Splitting Kernel.
 */

 /* 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <dlfcn.h>
#include <sys/time.h>
#include <time.h>
#include <generator.h>
#include <parser.h>
#include "psi_common.h"

#define ENTRYHASH 3
#define ALLOCATION 1000

//  int FLAG = ALGO_NONMONOTONE;
//
//  /******************************************/
//  /*  SIMULATION PARAMETERS */
//  /******************************************/
//  // Simulation parameters of method :
//  int nb_sample; 		//numbers of samples
//  int nb_AV; 			//number of anthitetic value
//  int doubling;			//doubling or not the period
//  int** reward;			// Coupling table
//  int ls_mode;
//  int ** node_dist;// [LOAD SHARING] matrix of distance between two queues : node_dist[i][j] = distance between i and j
//  struct st_neighbors * node_neighbors;// [LOAD SHARING] array containing for each node, the neighbors of the node
//	    
//  /******************************************/
//  /*  SIMULATION DATA */
//  /******************************************/
//  
//  
//  // Queues
//  int* capmins ;			// capacity min of queues
//  int* capmaxs ;			// capacity max of queues 
////   int tot_caps;
//  int nb_queues;
//  
//  // Events
//  T_event* events = NULL;		//pool of events 
//  int nb_events;			//number of events
//  double simulation_time;

/*****************************************************************************/
/*             Global variables used by transition or simulation             */

/* TRANSITION ---------- */
/* Model */
/* - Queues: */
int* capmins ;                 // capacity min of queues
int* capmaxs ;                 // capacity max of queues 
int nb_queues;                 // number of queues	

/* Load sharing */
int ls_mode;
int **node_dist;     /* [LOAD SHARING] matrix of distance between two queues :
                      * node_dist[i][j] = distance between i and j */
struct st_neighbors *node_neighbors; /* [LOAD SHARING] array containing for
                                      * each node, the neighbors of the node */

/* SIMULATION ---------- */
double simulation_time;
int **reward;              // reward table
int nb_sample;             //numbers of samples
int nb_AV;                 // number of anthitetic value
int FLAG;

int *log2stop_time; /* number of iteration to finish the simulation
                     * (coupling time) for each anthitetic variable
                     */
int **state_saved; /* different queues states when coupling for each
                    * anthitetic variable
                    */

/*****************************************************************************/

/**
 * \brief      Procedure to free allocated parameters
 * \return     void
 */
void free_method (st_method_ptr method_ptr)
{
   st_backwardEnvelopeSplitting_ptr method = method_ptr;

   if (method_ptr) {
      method = method_ptr;
   }
   else return;

   if (method->RewardFct != NULL) free (method->RewardFct);
   if (method->EnvInit != NULL) free (method->EnvInit);
   if (method->EnvModif != NULL) free (method->EnvModif);
   if (method->SplitInitFct != NULL) free (method->SplitInitFct);
   if (method->SplitCheckFct != NULL) free (method->SplitCheckFct);
   if (method->OutputFct != NULL) free (method->OutputFct);

   free (method);
}

/**
 * \brief      Procedure to init the method structure
 * \return     a pointer to a method structure
 *             NULL if failed
 */
st_backwardEnvelopeSplitting_ptr init_method ()
{
   st_backwardEnvelopeSplitting_ptr method = (st_backwardEnvelopeSplitting_ptr)
                                 calloc (1, sizeof(st_backwardEnvelopeSplitting));
   if (method == NULL) return NULL;

   method->RewardFct = (st_function_ptr) calloc (1, sizeof (st_function));
   if (method->RewardFct == NULL) return NULL;

   method->EnvInit = (st_function_ptr) calloc (1, sizeof (st_function));
   if (method->EnvInit == NULL) return NULL;

   method->EnvModif = (st_function_ptr) calloc (1, sizeof (st_function));
   if (method->EnvModif == NULL) return NULL;

   method->SplitInitFct = (st_function_ptr) calloc (1, sizeof (st_function));
   if (method->SplitInitFct == NULL) return NULL;

   method->SplitCheckFct = (st_function_ptr) calloc (1, sizeof (st_function));
   if (method->SplitCheckFct == NULL) return NULL;

   method->OutputFct = (st_function_ptr) calloc (1, sizeof (st_function));
   if (method->OutputFct == NULL) return NULL;

   method->SampleNumber = 1;
   method->TrajectoryMax = 0;
   method->Antithetic = 0;
   method->Doubling = 0;
   method->InitMemoryLength = 0;
   method->SplitThreshold = 0;

   return method;
}

/**
 * \brief      Procedure to print method parameters to the outout file
 * \param      output_file    file descriptor to the output file
 * \param      method_ptr     method
 * \return     void
 */
void print_method (FILE *output_file, st_method_ptr method_ptr)
{
   st_backwardEnvelopeSplitting_ptr method = (st_backwardEnvelopeSplitting_ptr) method_ptr;

   fprintf(output_file, "# Method: backwardenvelopesplitting\n");
   fprintf(output_file, "#\tSampleNumber: %d\n", method->SampleNumber);
   fprintf(output_file, "#\tTrajectoryMax: %d\n", method->TrajectoryMax);
   fprintf(output_file, "#\tAntithetic: %d\n", method->Antithetic);
   fprintf(output_file, "#\tDoubling: %d\n", method->Doubling);
   fprintf(output_file, "#\tInitMemoryLength: %d\n", method->InitMemoryLength);
   fprintf(output_file, "#\tSplitThreshold: %d\n", method->SplitThreshold);
   fprintf(output_file, "#\tCouplingFct: %s\n", method->RewardFct->name);
   fprintf(output_file, "#\tEnvInit: %s\n", method->EnvInit->name);
   fprintf(output_file, "#\tEnvModif: %s\n", method->EnvModif->name);
   fprintf(output_file, "#\tSplitInitFct: %s\n", method->SplitInitFct->name);
   fprintf(output_file, "#\tSplitCheckFct: %s\n", method->SplitCheckFct->name);
   fprintf(output_file, "#\tOutputFct: %s\n", method->OutputFct->name);
}

/**
 * \brief       Procedure to initialize the kernel from a model
 * \details	Initialize global variables from the model
 * \param    model         model of system.
 * \return   void.
 */
//inline void init_kernel(T_model model){
//       //initialize queues & events from model
//      // queues:
//      capmins = model.queues->mins;
//      capmaxs = model.queues->maxs;
//      nb_queues = model.queues->nb_queues;
//      //events:
//      events = model.events->evts;
//      nb_events = model.events->nb_evts;  
//}

/**
 * \brief       Procedure to initialize the kernel from a model
 * \details	Initialize global variables from the model
 * \param    model         model of system.
 * \return   void.
 */
void init_kernel (st_model_ptr model)
{
   /**
    * initialize the random event generator :
    */
   init_generator (model->events->nb_evts, model->events->evts);

   capmins = model->queues->mins;
   capmaxs = model->queues->maxs;
   nb_queues = model->queues->nb_queues;
   FLAG = ALGO_NONMONOTONE;
}

/**
 * \brief       Procedure to free allocated parameters
 * \return   void.
 */
//static inline void free_param(){
//   int i;
//   if (reward != NULL){
//      for(i = 0; i < nb_AV; i++) {
//	free(reward[i]);
//      }
//      free(reward);
//   }
//   if (node_dist != NULL){
//     for(i=0;i<nb_queues;i++)
//	free(node_dist[i]);
//     free(node_dist);
//   }
//   if (node_neighbors != NULL){
//      for (i=0;i<nb_queues;i++){
//	      free(node_neighbors[i].neighbors);
//      }
//      free(node_neighbors);
//   }
//}

/**
 * \brief      Function to read an initialize method simulation parameters
 * \param      method_param_cfgfile    method config file name
 * \param      general                 pointer to the general structure
 * \param      model                   pointer to the model structure
 * \return     a pointer to a initialized method structure
 *             NULL if failed
 */
st_backwardEnvelopeSplitting_ptr read_method (char *method_param_cfgfile,
                                     st_general_ptr general,
                                     st_model_ptr model)
{
   int err = 0;
   FILE *method_param_fd;
   char *loadsharing_file_name;
   FILE *loadsharing_file;

#if (DEBUG_LVL >= DEBUG_IO)
   fprintf(stderr, "Opening method parameters file: %s\n", method_param_cfgfile);
#endif
   method_param_fd = fopen(method_param_cfgfile, "r");
   if (method_param_fd == NULL) {
      fprintf(stderr, "Fail to open file %s\n", method_param_cfgfile);
      return NULL;
   }

   st_backwardEnvelopeSplitting_ptr method = init_method ();

   /**
    * Get samplenumber
    */
   method->SampleNumber = get_int_keyname(method_param_fd, "SampleNumber");
   if (method->SampleNumber <= 0) {
      fprintf(stderr, "Number of sample must be positive\n");
      err++;
   }
#if (DEBUG_LVL >= DEBUG_INFO)
   fprintf(stderr, "SampleNumber: %d\n", method->SampleNumber);
#endif
   nb_sample = method->SampleNumber; /* FIXME */

   /**
    * Get InitMemoryLength: the initial allocated memory
    */
   method->InitMemoryLength = get_int_keyname(method_param_fd, "InitMemoryLength");
   if (method->InitMemoryLength <= 0){
      fprintf (stderr, "InitMemoryLength: automatic default value (%d)\n", ALLOCATION);
      method->InitMemoryLength = ALLOCATION;
   }
#if (DEBUG_LVL >= DEBUG_INFO)
   fprintf(stderr, "InitMemoryLength: %d\n", method->InitMemoryLength);
#endif

   /**
    * Get Antithetic: the number of anthitetic variables
    */
   method->Antithetic = get_int_keyname(method_param_fd, "Antithetic");
   if (method->Antithetic <= 0) {
      fprintf(stderr, "Number of antithetic variables must be positive\n");
      err++;
   }
#if (DEBUG_LVL >= DEBUG_INFO)
   fprintf(stderr, "Antithetic: %d\n", method->Antithetic);
#endif

   nb_AV = method->Antithetic; /* FIXME */

   /**
    * Get the split threshold
    */
   method->SplitThreshold = get_int_keyname (method_param_fd, "SplitThreshold");
   if (method->SplitThreshold <= 0) {
      fprintf(stderr, "SplitThreshold must be positive\n");
      err++;
   }

   /**
    * Get TrajectoryMax
    */
   method->TrajectoryMax = get_int_keyname(method_param_fd, "TrajectoryMax");
   if (method->TrajectoryMax <= 0) {
      fprintf(stderr, "TrajectoryMax must be positive\n");
      err++;
   }
   else {
#if (DEBUG_LVL >= DEBUG_INFO)
      fprintf(stderr, "TrajectoryMax: %d\n", method->TrajectoryMax);
#endif
   }

   /**
    * Get Doubling
    */
   method->Doubling = get_bool_keyname (method_param_fd, "Doubling");
   if (method->Doubling != 0) { /* Yes by default */
      method->Doubling = 1;
   }
#if (DEBUG_LVL >= DEBUG_INFO)
   fprintf (stderr, "Doubling: %d\n", method->Doubling);
#endif

   /**
    * Get reward function
    */
   method->RewardFct->name = get_string_keyname (method_param_fd, "CouplingFct");
   if (method->RewardFct->name == NULL) {
      method->RewardFct->name = "Default$reward_ENV_SPLIT";
#if (DEBUG_LVL >= DEBUG_INFO)
      fprintf (stderr, "CouplingFct: automatic default value (%s)\n",
              method->RewardFct->name);
#endif
   }
   method->RewardFct->ptr = (T_ptr_rewardfct)
                                 get_ptr_fct (general->default_lib,
                                              general->user_lib,
                                              method->RewardFct->name);
   if (method->RewardFct->ptr == NULL) {
      err++;
   }
#if (DEBUG_LVL >= DEBUG_INFO)
   fprintf (stderr, "CouplingFct: %s (%p)\n",
           method->RewardFct->name,
           method->RewardFct->ptr);
#endif

   /**
    * Get EnvInit function
    */
   method->EnvInit->name = get_string_keyname (method_param_fd, "EnvInit");
   if (method->EnvInit->name == NULL) {
      method->EnvInit->name = "Default$init_envelop";
#if (DEBUG_LVL >= DEBUG_INFO)
      fprintf (stderr, "EnvInit: automatic default value (%s)\n",
              method->EnvInit->name);
#endif
   }
   method->EnvInit->ptr = (T_ptr_envelop)
                                 get_ptr_fct (general->default_lib,
                                              general->user_lib,
                                              method->EnvInit->name);
   if (method->EnvInit->ptr == NULL) {
      err++;
   }
#if (DEBUG_LVL >= DEBUG_INFO)
   fprintf (stderr, "EnvInit: %s (%p)\n",
           method->EnvInit->name,
           method->EnvInit->ptr);
#endif

   /**
    * Get EnvModif function
    */
   method->EnvModif->name = get_string_keyname (method_param_fd, "EnvModif");
   if (method->EnvModif->name == NULL) {
      method->EnvModif->name = "Default$modify";
#if (DEBUG_LVL >= DEBUG_INFO)
      fprintf (stderr, "EnvModif: automatic default value (%s)\n",
              method->EnvModif->name);
#endif
   }
   method->EnvModif->ptr = (T_ptr_envelop)
                                 get_ptr_fct (general->default_lib,
                                              general->user_lib,
                                              method->EnvModif->name);
   if (method->EnvModif->ptr == NULL) {
      err++;
   }
#if (DEBUG_LVL >= DEBUG_INFO)
   fprintf (stderr, "EnvModif: %s (%p)\n",
           method->EnvModif->name,
           method->EnvModif->ptr);
#endif

   /**
    * Get SplitInitFct function
    */
   method->SplitInitFct->name = get_string_keyname (method_param_fd,
                                                    "SplitInitFct");
   if (method->SplitInitFct->name == NULL) {
      method->SplitInitFct->name = "Default$init_splitting";
#if (DEBUG_LVL >= DEBUG_INFO)
      fprintf (stderr, "SplitInitFct: automatic default value (%s)\n",
              method->SplitInitFct->name);
#endif
   }
   method->SplitInitFct->ptr = (T_ptr_splitInitfct)
                                 get_ptr_fct (general->default_lib,
                                              general->user_lib,
                                              method->SplitInitFct->name);
   if (method->SplitInitFct->ptr == NULL) {
      err++;
   }
#if (DEBUG_LVL >= DEBUG_INFO)
   fprintf (stderr, "SplitInitFct: %s (%p)\n",
           method->SplitInitFct->name,
           method->SplitInitFct->ptr);
#endif

   /**
    * Get SplitCheckFct function
    */
   method->SplitCheckFct->name = get_string_keyname (method_param_fd,
                                                     "SplitCheckFct");
   if (method->SplitCheckFct->name == NULL) {
      method->SplitCheckFct->name = "Default$check_splitting";
#if (DEBUG_LVL >= DEBUG_INFO)
      fprintf (stderr, "SplitCheckFct: automatic default value (%s)\n",
              method->SplitCheckFct->name);
#endif
   }
   method->SplitCheckFct->ptr = (T_ptr_splitfct)
                                 get_ptr_fct (general->default_lib,
                                              general->user_lib,
                                              method->SplitCheckFct->name);
   if (method->SplitCheckFct->ptr == NULL) {
      err++;
   }
#if (DEBUG_LVL >= DEBUG_INFO)
   fprintf (stderr, "SplitCheckFct: %s (%p)\n",
           method->SplitCheckFct->name,
           method->SplitCheckFct->ptr);
#endif

   /**
    * Get Output Function
    */
   method->OutputFct->name = get_string_keyname (method_param_fd, "OutputFct");
   if (method->OutputFct->name == NULL) {
      method->OutputFct->name = "Default$output_B";
#if (DEBUG_LVL >= DEBUG_INFO)
      fprintf(stderr, "OutputFct: automatic default value (%s)\n",
             method->OutputFct->name);
#endif
   }
   method->OutputFct->ptr = (T_ptr_outputfct) get_ptr_fct (general->default_lib,
                                                           general->user_lib,
                                                           method->OutputFct->name);
   if (method->OutputFct->ptr == NULL) {
      err++;
   }
#if (DEBUG_LVL >= DEBUG_INFO)
   fprintf(stderr, "OutputFct: %s (%p)\n",
          method->OutputFct->name,
          method->OutputFct->ptr);
#endif

   /**
    * Get the load sharing
    */
   loadsharing_file_name = get_string_keyname(method_param_fd, "LoadSharing");

   if (loadsharing_file_name != NULL) {
#if (DEBUG_LVL >= DEBUG_INFO)
      fprintf(stderr, "LoadSharing: [%s]\n", loadsharing_file_name);
#endif
      loadsharing_file = fopen (loadsharing_file_name, "r");
      if (loadsharing_file == NULL) {
         fprintf(stderr, "The Load Sharing file does not exist\n");
         err++;
      }
      else {
         //parser the load sharing file
         node_dist = LS_parser (loadsharing_file,
                                node_dist,
                                model->queues->nb_queues,
                                &ls_mode);
         if (node_dist == NULL) {
            err++;
         }
         else {
            //save the configuration of load sharing
            node_neighbors = node_dist_to_node_neighbors(node_dist,
                                                         node_neighbors,
                                                         model->queues->nb_queues);
#if (DEBUG_LVL >= DEBUG_INFO)
            fprintf(stderr, "Load Sharing: nb_queues: %d\n", model->queues->nb_queues);
#endif
         }
         fclose(loadsharing_file);
      }
   }
   else {
      //load sharing is not used
      node_dist = NULL;
      node_neighbors = NULL;
#if (DEBUG_LVL >= DEBUG_INFO)
      fprintf(stderr, "Load Sharing is not used\n");
#endif
   }

   fclose(method_param_fd);

   if (err) {
      free_method (method);
      return NULL;
   }

   return method;
}

///**
// * \brief       Procedure to check if two vectors are similar
// * \param    e1         first vector
// * \param    e2	        second vector
// * \return   O if not egal, 1 if egal.
// */ 
//static inline int equal(int *e1, int *e2)
//{
//  int i;
//  for (i=0; i<nb_queues; i++) if (e1[i]!=e2[i]) return 0;
//  return 1;
//}

/**
 * \brief       Procedure to hash a vector
 * \param    vector         a vector
 * \param    length	    length of the hashtable
 * \details  
 * \return   value of the hashcode.
 */ 
static inline int hash_vector (int *vector,int length){
      int s =0;
      int i =0;
      for (i = 0; i<nb_queues;i++){
	s = ((capmaxs[i]+1) * s + vector[i]);
      }
      return abs(s)%length;
}
/**
 * \brief       Procedure to compare hash in hash table
 * \param    hashtable		the hashtable
 * \param    hash	    	hash key to look up in the hash table
 * \param    vector_index	index in the list of vectors in T_state structure
 * \param    state		T_state structure representing the different vector 
 * \details  When a hash code is already in the table, the vector is compared with other ones in table with same hash code
 * \return   0 if vector represent by hash is not in table or 1 if vector represent by hash is in table  
 */ 
int compare_hash(elements *hashtable, int hash,int vector_index,T_state* state){
  int i,j;
  if(hashtable[hash].occupation == 0){ //this key has never been used
    hashtable[hash].index[hashtable[hash].occupation] = vector_index;  
    hashtable[hash].occupation++;
    return 0;
  }
  else{//There are some values associated to this key
    for(i=0;i<hashtable[hash].occupation;i++){//check if there is already the vector in hash table
      for (j=0; j<nb_queues; j++){ 
	if (state->queues[vector_index][j]!=state->queues[hashtable[hash].index[i]][j])break;
      }
      if(j==nb_queues)return 1;
    }
    //There is a colision in the hash
    if ((hashtable[hash].occupation) == hashtable[hash].length){//Check for available memory
      hashtable[hash].length *= 2;
      hashtable[hash].index = (int*)realloc(hashtable[hash].index,sizeof(int)*hashtable[hash].length);
    }
    hashtable[hash].index[hashtable[hash].occupation] = vector_index;  //Put <hash,vector_index> in the hash table
    hashtable[hash].occupation++;
    return 0;
  }
}
/**
 * \brief       Procedure to reset the table
 * \param    hashtable		the hashtable
 * \param    length		length of the hashtable
 * \details  hashtable structure has an number of occupation for each hash code (table cell) that is number of vectors with a hash code.
 * 		To reset hash table, we just have to reset this number. 
 * \return   void
 */ 
static inline void reset_table(elements *hashtable,int length){
  int i;
  for (i=0;i<length;i++){
       hashtable[i].occupation=0;
  }
}


/**
 * \brief       Procedure to run the simulation
 * \details      Run simulation and store updated global variables (e.g state, trajectory etc)
 * \return   void.
 */
int simulation_simple (st_general_ptr general,
                       st_backwardEnvelopeSplitting_ptr method,
                       st_model_ptr model)
{

   int current_length_memory = method->InitMemoryLength;
//save time a the beginning and at the end of the simulation  
  struct timeval ti,tf;
  struct timeval a,b;
  double time = 0;

//States of the system
  T_state* states=NULL;
//several iterator use in simulation
  int i =0,cmpt=0,place,n, temp, num,stop_time=0,indice;
//current event of the simulation  
  T_event* event;
  int ** Tab_VA;
  double *stat;
  double val;
//back up of the previous event 
  int* trajectory;
//current number of sample
  int sample;
//current random event number
  int id_random_evt;
//save number of iteration to finish the simulation (coupling time) for each anthitetic variable
  int *log2stop_time;
//different queues states when coupling for each anthitetic variable
  int ** state_saved;
//length of the splitting, allow to check if we can split or not
  int check_splitting;
//hash table uses in the simulation
  elements* hashtable= NULL;

  int sampleComputed = 0;        /* number of sample computed during the
                                  * simulation */


   st_simulation_ptr simulation;
   simulation = malloc (sizeof (st_simulation));
   simulation->finished = 0;
  
//initialize the T_state structure thanks to the function defined, allow to do different envelopp
  states = ((T_ptr_envelop) (method->EnvInit->ptr))(states,method->SplitThreshold);

  log2stop_time=(int*)calloc(nb_AV,sizeof(int));

  Tab_VA = (int **) calloc(nb_AV, sizeof(int*));
  for(n=0; n<nb_AV; n++)  Tab_VA[n]=(int*)calloc(current_length_memory,sizeof(int));

  stat=(double*)calloc(current_length_memory,sizeof(double));
 
  trajectory = (int*)malloc(current_length_memory * sizeof(int));
 
  reward=(int**)calloc(nb_AV, sizeof(int*));
  for (n=0; n< nb_AV; n++)  reward[n]=(int *)calloc(nb_queues,sizeof(int));  

  state_saved = (int **) calloc(nb_AV, sizeof(int*));
  for(i=0; i<nb_AV; i++){
	  state_saved[i]=(int*)calloc(nb_queues,sizeof(int));
  }
  //initialize the hash table, the length is 4 time longer than the max length of split state set
  hashtable = (elements*) malloc(sizeof(elements)*(method->SplitThreshold)*4);
  for (i=0;i<(method->SplitThreshold)*4;i++){
    hashtable[i].index = (int*) malloc(sizeof(int)*ENTRYHASH);
    hashtable[i].length = ENTRYHASH;
    hashtable[i].occupation = 0;
  }
  //save current time before simulation 
  gettimeofday(&ti,NULL);
  for (sample = 0 ; sample < method->SampleNumber ; sample++){
// 		//re-initialize reward table and reward time for each trajectory 
		for (n=0; n<nb_AV ;n++){
			log2stop_time[n] = 0;  
		}	
		for (n= 0; n<nb_AV; n++){
		  for (i = 0; i< nb_queues; i++){
		    reward[n][i]= 0;
		  }
		}
		
		stat[0] = u01();
		for(n=0; n<nb_AV; n++){
			Tab_VA[n][0]=n;
		}
		for (i=1; i<nb_AV; i++){
			place = ((int) (u01()*(nb_AV-1)))+1;
			temp = Tab_VA[place][0];
			Tab_VA[place][0] = Tab_VA[i][0];
			Tab_VA[i][0] = temp;
		}
		
		for (num=0; num<nb_AV ; num++){
			// x modulo y == x - ( ( (int)x/(int)y ) * y )
			val = (stat[0]+(double)Tab_VA[num][0]/(double)nb_AV) - ((int)(stat[0]+(double)Tab_VA[num][0]/(double)nb_AV)); 
			id_random_evt = genere_evenement(val, model->events->nb_evts);
			trajectory[0] =  id_random_evt;
			stop_time = 1;  
			
			do{
				states->splitting = 0;
				states->nb_vectors = 2;
				check_splitting = 0;
				indice = stop_time-1;
				//re initialize lower and upper state with lower and upper capacity 
				states = ((T_ptr_envelop) (method->EnvModif->ptr))(states);

				//check if we have to realloc the memory 
				if (current_length_memory < indice){
				    current_length_memory = current_length_memory*2;
				    for(n=0; n<nb_AV; n++)  Tab_VA[n]=(int*)realloc(Tab_VA[n],current_length_memory*sizeof(int));
				    stat=(double*)realloc(stat,current_length_memory*sizeof(double));
				    trajectory = (int*)realloc(trajectory,current_length_memory*sizeof(int));
				}
				//Doing permutations for anthitetic variables 
				stat[indice] = u01();
				for(n=0; n<nb_AV; n++){
					Tab_VA[n][indice]=n;
				}
				for (i=1; i<nb_AV; i++){
					place = ((int) (u01()*(nb_AV-1)))+1;
					temp = Tab_VA[place][indice];
					Tab_VA[place][indice] = Tab_VA[i][indice]; 
					Tab_VA[i][indice] = temp;
				}
				val = (stat[indice]+(double)Tab_VA[num][indice]/(double)nb_AV) - ((int)(stat[indice]+(double)Tab_VA[num][indice]/(double)nb_AV ));				
				  //Generate a random event
				id_random_evt = genere_evenement(val, model->events->nb_evts);
				//save the random number
				trajectory[indice] =  id_random_evt;
				event = model->events->evts+id_random_evt;
				// e(x+1) = Transition(e(x),event), doing the event 
				states = (*((T_ptr_transfct) event->ptr_transition)) (states, event, model);
			 
				if (states->splitting){ //If we have splitted before
				gettimeofday(&a,NULL);
				      //interator uses to compare the number of old vector and new vector 
				      cmpt=0;
				      for (i = 0;i<states->nb_vectors;i++){ //for each remaining vector
					//we compare the hash of the vector 
					if (!compare_hash(hashtable,hash_vector(states->queues[i],check_splitting),i,states)){
					  //if the iterator are different, we need to copy the vector at the right place
					  if(i != cmpt)memcpy(states->queues[cmpt],states->queues[i],sizeof(int)*nb_queues);
					  cmpt++;
					}
				      }
				      states->nb_vectors=cmpt;
				      reset_table(hashtable,check_splitting);
				      gettimeofday(&b,NULL);
				    time += ((double) ((b.tv_sec-a.tv_sec)*1000000 + (b.tv_usec-a.tv_usec)));
				}
				
				else{
				  //check if we can split, check_splitting = length of splitting if split, check_splitting = 0 if not split
				    check_splitting = ((T_ptr_splitfct) method->SplitCheckFct->ptr) (states,method->SplitThreshold);
				    if (check_splitting){
					states->splitting = 1;
				      //we can initialize the different vectors 
					states= ((T_ptr_splitInitfct) method->SplitInitFct->ptr) (states,check_splitting);
				    } 
				}
				 
				// from indice = stop_time-1 to O we take the same trajectory (previous saved event)
				for (indice = stop_time-2; indice>=0; indice--){
					//take a previous random event
					id_random_evt = trajectory[indice];
					event = model->events->evts+id_random_evt;
					// e(x+1) = Transition(e(x),event), doing the event 
					states = (*((T_ptr_transfct) event->ptr_transition)) (states, event, model);
					if (states->splitting){
					  gettimeofday(&a,NULL);
					      cmpt=0;
					      for (i = 0;i<states->nb_vectors;i++){
						if (!compare_hash(hashtable,hash_vector(states->queues[i],check_splitting),i,states)){
						  if(i != cmpt) memcpy(states->queues[cmpt],states->queues[i],sizeof(int)*nb_queues);
						  cmpt++;
						}
					      }
					      states->nb_vectors=cmpt;
 					      reset_table(hashtable,check_splitting);
					      gettimeofday(&b,NULL);
					      time += ((double) ((b.tv_sec-a.tv_sec)*1000000 + (b.tv_usec-a.tv_usec)));
					}
					else{
					    check_splitting = ((T_ptr_splitfct) method->SplitCheckFct->ptr) (states,method->SplitThreshold);
					     if (check_splitting){
						states->splitting = 1;
						states= ((T_ptr_splitInitfct) method->SplitInitFct->ptr) (states,check_splitting);
					    } 
					}
				}   
				//double the begin time
				stop_time=stop_time+1;;
				log2stop_time[num]+=1;
				//until we are not at the trajectory max length and we have not coupling in all queues
			}while((stop_time < method->TrajectoryMax) && !((T_ptr_rewardfct) (method->RewardFct->ptr))(states,log2stop_time[num],reward[num], state_saved[num]));
		}

      sampleComputed++;

		//print the result in output file
		if (stop_time >= method->TrajectoryMax){
		    fprintf(general->output_file, " echec  \n");
		}
		else
		{
		    ((T_ptr_outputfct) (method->OutputFct->ptr)) (general->output_file, state_saved,sample,log2stop_time);
		}
      loadbar (sampleComputed, method->SampleNumber);
	} 
 //save current time after simulation 
  gettimeofday(&tf,NULL);
   //Calculate simulation time by substracting end time and beginning time 
   simulation_time = delta_ms (ti, tf);

   simulation->time = simulation_time;

   print_footer (general, model, method, simulation->time);

   print_time (stderr, simulation->time);

   print_perf (stderr, general, simulation);
 
  //free all the used structures 
  for(n=0; n<nb_AV; n++){
    free(Tab_VA[n]);
    free(state_saved[n]);
  }
  for (i=0;i<method->SplitThreshold;i++) free(states->queues[i]);
  for (i=0;i<(method->SplitThreshold*4);i++) free(hashtable[i].index);
  free(hashtable);
  free(states->queues);
  free(states);
  free(Tab_VA );
  free(stat);
  free(log2stop_time);
  free(state_saved);   
  free(trajectory);
  return 0;
}

int simulation_doubling (st_general_ptr general,
                         st_backwardEnvelopeSplitting_ptr method,
                         st_model_ptr model)
{
   int current_length_memory = method->InitMemoryLength;
   //save time a the beginning and at the end of the simulation  
   struct timeval ti,tf,a,b;
   double time = 0.0;
   //States of the system
   T_state* states=NULL;
   //several iterator use in simulation
   int i =0,cmpt=0,place,n, temp, num,stop_time,indice,max_temps,indice2;
   //current event of the simulation  
   T_event* event;
   int ** Tab_VA;
   double *stat;
   double val;
   //back up of the previous event 
   int* trajectory;
   //current number of sample
   int sample;
   //current random event number
   int id_random_evt;
   //save number of iteration to finish the simulation (coupling time) for each anthitetic variable
   int *log2stop_time;
   //different queues states when coupling for each anthitetic variable
   int ** state_saved;
   //length of the splitting, allow to check if we can split or not
   int check_splitting;
   //hash table uses in the simulation
   elements* hashtable= NULL;

   int sampleComputed = 0;        /* number of sample computed during the
                                   * simulation */

   st_simulation_ptr simulation;
   simulation = malloc (sizeof (st_simulation));
   simulation->finished = 0;

   //initialize the T_state structure thanks to the function defined, allow to do different envelopp
   states = ((T_ptr_envelop) (method->EnvInit->ptr))(states,method->SplitThreshold);

   log2stop_time=(int*)calloc(nb_AV,sizeof(int));

   Tab_VA = (int **) calloc(nb_AV, sizeof(int*));
   for(n=0; n<nb_AV; n++)  Tab_VA[n]=(int*)calloc(current_length_memory,sizeof(int));

   stat=(double*)calloc(current_length_memory,sizeof(double));

   trajectory = (int*)malloc(current_length_memory * sizeof(int));

   reward=(int**)calloc(nb_AV, sizeof(int*));
   for (n=0; n< nb_AV; n++)  reward[n]=(int *)calloc(nb_queues,sizeof(int));  

   state_saved = (int **) calloc(nb_AV, sizeof(int*));
   for(i=0; i<nb_AV; i++){
      state_saved[i]=(int*)calloc(nb_queues,sizeof(int));
   }
   //initialize the hash table, the length is 4 time longer than the max length of split state set
   hashtable = (elements*) malloc(sizeof(elements)*(method->SplitThreshold)*4);
   for (i=0;i<(method->SplitThreshold)*4;i++){
      hashtable[i].index = (int*) malloc(sizeof(int)*ENTRYHASH);
      hashtable[i].length = ENTRYHASH;
      hashtable[i].occupation = 0;
   }
   //save current time before simulation 
   gettimeofday(&ti,NULL);
   for (sample = 0 ; sample < method->SampleNumber ; sample++){
      // 		//re-initialize reward table and reward time for each trajectory 
      for (n=0; n<nb_AV ;n++){
         log2stop_time[n] = 0;  
      }	
      for (n= 0; n<nb_AV; n++){
         for (i = 0; i< nb_queues; i++){
            reward[n][i]= 0;
         }
      }
      max_temps = 0;
      stop_time = 1;

      stat[0] = u01();
      for(n=0; n<nb_AV; n++){
         Tab_VA[n][0]=n;
      }
      for (i=1; i<nb_AV; i++){
         place = ((int) (u01()*(nb_AV-1)))+1;
         temp = Tab_VA[place][0];
         Tab_VA[place][0] = Tab_VA[i][0];
         Tab_VA[i][0] = temp;
      }

      for (num=0; num<nb_AV ; num++){
         // x modulo y == x - ( ( (int)x/(int)y ) * y )
         val = (stat[0]+(double)Tab_VA[num][0]/(double)nb_AV) - ((int)(stat[0]+(double)Tab_VA[num][0]/(double)nb_AV)); 
         id_random_evt = genere_evenement(val, model->events->nb_evts);
         trajectory[0] =  id_random_evt;
         stop_time = 1;  

         do{
            states->splitting = 0;
            states->nb_vectors = 2;
            check_splitting = 0;

            //re initialize lower and upper state with lower and upper capacity 
            states = ((T_ptr_envelop) (method->EnvModif->ptr))(states);

            for (indice = 2*stop_time-1; indice >= stop_time; indice--){
               //check if we have to realloc the memory 
               if (current_length_memory < indice){
                  current_length_memory = current_length_memory*2;
                  for(n=0; n<nb_AV; n++)  Tab_VA[n]=(int*)realloc(Tab_VA[n],current_length_memory*sizeof(int));
                  stat=(double*)realloc(stat,current_length_memory*sizeof(double));
                  trajectory = (int*)realloc(trajectory,current_length_memory*sizeof(int));
               }
               //Doing permutations for anthitetic variables 
               if(max_temps < indice){
                  for (indice2 = 2*stop_time-1; indice2 >= stop_time; indice2--){
                     stat[indice2] = u01();
                     for(n=0; n<nb_AV; n++){
                        Tab_VA[n][indice2]=n;
                     }
                     for (i=1; i<nb_AV; i++){
                        place = ((int) (u01()*(nb_AV-1)))+1;
                        temp = Tab_VA[place][indice2];
                        Tab_VA[place][indice2] = Tab_VA[i][indice2]; 
                        Tab_VA[i][indice2] = temp;
                     }
                  }
                  max_temps = indice;
               }
               val = (stat[indice]+(double)Tab_VA[num][indice]/(double)nb_AV) - ((int)(stat[indice]+(double)Tab_VA[num][indice]/(double)nb_AV ));				
               //Generate a random event
               id_random_evt = genere_evenement(val, model->events->nb_evts);
               //save the random number
               trajectory[indice] =  id_random_evt;
               event = model->events->evts + id_random_evt;
               // e(x+1) = Transition(e(x),event), doing the event 
               states = (*((T_ptr_transfct) event->ptr_transition)) (states, event, model);

               if (states->splitting){ //If we have splitted before
                  //interator uses to compare the number of old vector and new vector 
                  gettimeofday(&a,NULL);
                  cmpt=0;
                  for (i = 0;i<states->nb_vectors;i++){ //for each remaining vector
                     //we compare the hash of the vector 
                     if (!compare_hash(hashtable,hash_vector(states->queues[i],check_splitting),i,states)){
                        //if the iterator are different, we need to copy the vector at the right place
                        if(i != cmpt)memcpy(states->queues[cmpt],states->queues[i],sizeof(int)*nb_queues);
                        cmpt++;
                     }
                  }
                  states->nb_vectors=cmpt;
                  reset_table(hashtable,check_splitting);
                  gettimeofday(&b,NULL);
                  time += ((double) ((b.tv_sec-a.tv_sec)*1000000 + (b.tv_usec-a.tv_usec)));
               }

               else{
                  //check if we can split, check_splitting = length of splitting if split, check_splitting = 0 if not split
                  check_splitting = ((T_ptr_splitfct) method->SplitCheckFct->ptr) (states,method->SplitThreshold);
                  if (check_splitting){
                     // 						states->splitting = 1;
                     //we can initialize the different vectors 
                     states = ((T_ptr_splitInitfct) method->SplitInitFct->ptr) (states,check_splitting);
                  } 
               }
            }
            // from indice = stop_time-1 to O we take the same trajectory (previous saved event)
            for (indice = stop_time-1; indice>=0; indice--){
               //take a previous random event
               id_random_evt = trajectory[indice];
               event = model->events->evts + id_random_evt;
               // e(x+1) = Transition(e(x),event), doing the event 
               states = (*((T_ptr_transfct) event->ptr_transition)) (states, event, model);
               if (states->splitting){
                  gettimeofday(&a,NULL);
                  cmpt=0;
                  for (i = 0;i<states->nb_vectors;i++){
                     if (!compare_hash(hashtable,hash_vector(states->queues[i],check_splitting),i,states)){
                        if(i != cmpt) memcpy(states->queues[cmpt],states->queues[i],sizeof(int)*nb_queues);
                        cmpt++;
                     }
                  }
                  states->nb_vectors=cmpt;
                  reset_table(hashtable,check_splitting);
                  gettimeofday(&b,NULL);
                  time += ((double) ((b.tv_sec-a.tv_sec)*1000000 + (b.tv_usec-a.tv_usec)));
               }
               else{
                  check_splitting = ((T_ptr_splitfct) method->SplitCheckFct->ptr) (states,method->SplitThreshold);
                  if (check_splitting){
                     // 						states->splitting = 1;
                     states = ((T_ptr_splitInitfct) method->SplitInitFct->ptr) (states,check_splitting);
                  } 
               }
            }   
            //double the begin time
            stop_time=stop_time<<1;;
            log2stop_time[num]+=1;
            //until we are not at the trajectory max length and we have not coupling in all queues
         }while((2*stop_time < method->TrajectoryMax) && !(((T_ptr_rewardfct) method->RewardFct->ptr)(states,log2stop_time[num],reward[num], state_saved[num])));
      } 

      sampleComputed++;

      //print the result in output file
      if (2*stop_time >= method->TrajectoryMax){
         fprintf(general->output_file, " echec  \n");
      }
      else
      {
         ((T_ptr_outputfct) method->OutputFct->ptr) (general->output_file, state_saved,sample,log2stop_time);
      }
      loadbar (sampleComputed, method->SampleNumber);
   } 
   //save current time after simulation 
   gettimeofday(&tf,NULL);
   //Calculate simulation time by substracting end time and beginning time 
   simulation_time = delta_ms (ti, tf);

   simulation->time = simulation_time;

   print_footer (general, model, method, simulation->time);

   print_time (stderr, simulation->time);

   print_perf (stderr, general, simulation);

   //free all the used structures 
   for(n=0; n<nb_AV; n++){
      free(Tab_VA[n]);
      free(state_saved[n]);
   }
   for (i=0;i<method->SplitThreshold;i++) free(states->queues[i]);
   for (i=0;i<(method->SplitThreshold)*4;i++) free(hashtable[i].index);
   free(hashtable);
   free(states->queues);
   free(states);
   free(Tab_VA );
   free(stat);
   free(log2stop_time);
   free(state_saved);   
   free(trajectory);
   return 0;
}

/**
 * \brief       Main 
 * \details     - Load libraries
 *		- Parse config files
 * 		- Initialize data structures
 * 		- Run simulation
 * 		- Write results into output file
 * \return   0.
 */
int main (int argc, char **argv)
{
   st_model_ptr model = NULL;
   st_backwardEnvelopeSplitting_ptr method = NULL;
   st_general_ptr general = NULL;
   int rc;

   rc = read_configuration (&general,
                            argv[2],
                            &model,
                            argv[1],
                            (void **) &method,
                            argv[3],
                            (void *) read_method);

   if (rc) {                   /* if reading configuration failed */
      close_psi (general, model, method, free_method);
      return 1;
   }

   /**
    * initialize kernel from the model
    */
   init_kernel (model);

   /**
    * run the simulation
    */
   if (method->Doubling) {
      simulation_doubling (general, method, model);
   }
   else {
      simulation_simple (general, method, model);
   }

   /**
    * clean up the system:
    */
   return close_psi (general, model, method, free_method);
}
