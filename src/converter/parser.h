/**
 * \file        parser.c
 * \author      Florian LEVEQUE
 * \author      Jean-Marc.Vincent@imag.fr
 * \version     1.0
 * \date        30/07/2012
 * \brief       This code is used to convert config files from PSI2 into 
 *              config files of PSI3 1.0 in YAML format
 */

/* 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

typedef struct t_label{
  char* label;
  int index;
}T_label;

/* structure du tableau des evenements lu dans le fichier client ***/

struct st_evt{
    int id_evt;  
    int typ_evt;
    double lambda;
    int nbr_file_evt;
    int nb_param;
    int *param_evt;
    int *num_fct;
    int nb_fct_from;
    int *num_fct_from;
    int nb_fct_to;
    int *num_fct_to;
    int num_ori;
    int batch_size;
    int *from ;
    int nb_from;
    int *to ;
    int nb_to;
} ;

#endif /* _H_lect_model */
