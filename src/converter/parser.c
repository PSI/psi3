/**
 * \file      	parser.c
 * \author    	Florian LEVEQUE
 * \author      Jean-Marc.Vincent@imag.fr
 * \version   	1.0
 * \date      	30/07/2012
 * \brief     	This code is used to convert config files from PSI2 into 
 *				config files of PSI3 1.0 in YAML format
 */

/* 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <dlfcn.h>
#include <getopt.h>
#include <errno.h>
#include "parser.h"

#define version 1.0



/***
 * 
 * To use this parser, you have to give a file of the old version, and a name of the new file. 
 * ./parser old.dsc new.yml 
 * 
 */



/**
 * \brief	Function to split a string  and return the first part of it
 * \details	When it sees the delimiter, it splits the string into 2 parts and return the first part
 * \param	full	a string to split
 * \param 	delim	delimiter character 
 * \return   \e char* representing the first part of full
 */
char *
split (char *full, char delim)
{
   int length;

   if (full == NULL)
      return NULL;

   length = strlen (full);
   // fprintf(stderr, "Size of full is %d \n", length);
   int i = 0;
   char *res = NULL;
   while (full[i] != delim && i < length) {
      i++;
   }
   if (i >= length) {           // means that full is not at form
                                // <lib>$<function>
      // clear what have been created in this function:
      return NULL;
   }
   res = (char *) malloc ((i + 1) * sizeof (char));
   memcpy (res, full, i);
   res[i] = '\0';
   return res;
}

/**
 * \brief	Function to return a name of function thanks to an indentifier
 * \details	
 * \param	list	the correspondence table
 * \param 	size	size of the correspondence table
 * \param 	id 	function number in psi2
 * \return   \e char* representing the name of the function in new PSI. When the indentifier is not found, it return default string (ERROR)
 */
char *
find_index_labels (T_label * list, int size, int id)
{
   int i = 0;
   do {
      if ((list + i)->index == id) {
         return (list + i)->label;
      }
      i++;
   } while (i < size);
   return "(ERROR)";
}

/**
 * \brief       Function to parse a model on old psi2
 * \details    	
 * \param	nomf		name of the file to parse
 * \param	nomf_dest	name of the file where save the new model
 * \param	list 		correspondence table between old index number of transition function and the new name 
 * \param	size 		size of the correspondence table 
 * \return   \e void
 */


void
modele_parser (char *nomf, char *nomf_dest, T_label * list, int size)
{
   int i, j, k;
   // Recover data from file 
   char *var = malloc (50 * sizeof (char));
   // file descriptor of the model file 
   FILE *model_fd = NULL;
   // file descriptor of the index file 
   FILE *index_model_fd = NULL;
   // file descriptor of the destination file 
   FILE *dest = NULL;
   // name of the custom-built index function file 
   char file_ws[50];
   // name of the index file
   char file_index[50];
   // temporary variable 
   char *tmp = NULL;
   // pointer on file number 
   int *nbr_file = NULL;
   // pointer on queue capacity 
   int *capfile = NULL;
   // pointer on etat_init_sup 
   int *etat_init_supU = NULL;
   // pointeur on etat_init_inf 
   int *etat_init_infU = NULL;
   // pointer on event number 
   int *nb_evt = NULL;
   // pointer on structure event 
   struct st_evt *evt = NULL;
   // [LOAD SHARING] pointer on the prob_limit value which give the number of
   // concurent nodes in the index calculation 
   int *prob_limit = NULL;
   // Name of index function file 
   char *file_LS = NULL;
   char *commande = (char *) malloc (BUFSIZ * sizeof (char));


   // Remove all lines starting with #
   sprintf (commande, "grep -v \"#\" %s", nomf);

   if ((model_fd = popen (commande, "r")) == NULL) {
      fprintf (stderr, "Pb d'ouverture de fichier '%s' \n", nomf);
      perror ("Le Pb est : ");
      goto free;
   }
   else {
      fprintf (stderr, "Reading of the data file : %s\n", nomf);
      // Recover number of queues in the system
      nbr_file = (int *) malloc (sizeof (int));
      fscanf (model_fd, "%d", nbr_file);

      capfile = (int *) malloc ((*nbr_file) * sizeof (int));
      // Recover the capacity value of each queue
      for (j = 0; j < *nbr_file; j++) {
         fscanf (model_fd, "%d", capfile + j);  /* stocke a cette @ la valeur
                                                 * lue *** */
      }

      etat_init_infU = (int *) malloc ((*nbr_file) * sizeof (int));
      // Recover the min capacity value of each queue 
      for (j = 0; j < *nbr_file; j++)
         fscanf (model_fd, "%d", etat_init_infU + j);

      etat_init_supU = (int *) malloc ((*nbr_file) * sizeof (int));
      // Recover the max value of each queue 
      for (j = 0; j < *nbr_file; j++)
         fscanf (model_fd, "%d", etat_init_supU + j);

      // Open destination file 
      dest = fopen (nomf_dest, "w");
      if (dest == NULL) {
         fprintf (stderr, "Error to open destination file %s\n", nomf_dest);
         goto free;
      }
      // Write the version 
      fprintf (dest, "Version:  %f\n\n\n", version);

      // Write the queues
      fprintf (dest, "Queues : \n");
      for (j = 0; j < *nbr_file; j++) {
         fprintf (dest, " - id : queue%d \n", j);
         fprintf (dest, "   min : %d\n", etat_init_infU[j]);
         fprintf (dest, "   max : %d\n\n", etat_init_supU[j]);
      }
      fprintf (dest, "\n\n");
   }
   // Read the number of events
   nb_evt = (int *) malloc (sizeof (int));
   fscanf (model_fd, "%d", nb_evt);
   // Read the name of index file 
   fscanf (model_fd, "%s", var);
   if (strcmp ("File:", var)) {
      fprintf (stderr, "Error: \"File:\" waited, we have %s\n", var);
      goto free;
   }
   else {

      fscanf (model_fd, "%s", file_index);
      if (!strcmp ("N", file_index)) {
         fprintf (stderr, "There is no index file\n");
      }
      else {
         fprintf (stderr, "Index file : %s\n", file_index);
      }
   }


   // reading load sharing configuration file name 
   fscanf (model_fd, "%s", var);

   if (strcmp ("File:", var)) {
      fprintf (stderr, "Error: \"File:\" waited, we have %s\n", var);
   }
   else {
      fscanf (model_fd, "%s", file_ws);

      if (!strcmp ("N", file_ws)) {
         fprintf (stderr, "There is no load sharing file\n");
      }
      else {
         fprintf (stderr, "Reading of the load sharing file : %s\n", file_ws);

         sprintf (commande, "grep -v \"#\" %s", file_ws);

         if ((index_model_fd = popen (commande, "r")) == NULL) {
            fprintf (stderr, "Pb d'ouverture de fichier '%s' \n", file_ws);
         }
         else {
            // Read name of the custom-built index function file 
            file_LS = (char *) malloc (100 * sizeof (char));
            fscanf (index_model_fd, "%s", var);
            if (strcmp ("File:", var)) {
               fprintf (stderr, "Error: \"File:\" waited, we have %s\n", var);
            }
            else {
               // Read the prob limit value
               fscanf (index_model_fd, "%s", var);
               fscanf (index_model_fd, "%s", var);
               if (strcmp ("Prob-Limit:", var)) {
                  fprintf (stderr, "Error: \"Prob-Limit:\" waited, we have %s\n", var);
               }
               else {
                  prob_limit = (int *) malloc (sizeof (int));
                  fscanf (index_model_fd, "%d", prob_limit);

                  if (*prob_limit > *nbr_file)
                     fprintf
                        (stderr, "Warning : the prob limit %s is greater than the number of queue\n",
                         var);
               }
            }
         }

         pclose (index_model_fd);
         fprintf (stderr, "Reading of the load sharing file %s over\n", file_ws);
      }
   }

   // Allocate memory to store data events
   evt = (struct st_evt *) malloc ((*nb_evt) * sizeof (struct st_evt));
   // Looping to browse events 
   for (i = 0; i < (*nb_evt); i++) {
      // Recover event id, event type, event rate
      fscanf (model_fd, "%d %d %lf", &(evt + i)->id_evt, &(evt + i)->typ_evt,
              &(evt + i)->lambda);
      ((evt + i)->param_evt) = malloc (sizeof (int));
      // For some event, there is batch size after event rate
      if ((evt + i)->typ_evt == 22 || (evt + i)->typ_evt == 121
          || (evt + i)->typ_evt == 123) {
         fscanf (model_fd, "%s", var);
         ((evt + i)->param_evt[(evt + i)->nb_param]) = atoi (var);
         (evt + i)->nb_param++;
      }
      // Read the number of queues in the current event 
      fscanf (model_fd, "%s", var);
      (evt + i)->nbr_file_evt = atoi (var);
      // Allocate memory for the event data 
      ((evt + i)->param_evt) =
         realloc ((evt + i)->param_evt,
                  ((evt + i)->nbr_file_evt) * sizeof (int));
      ((evt + i)->num_fct) = malloc (((evt + i)->nbr_file_evt) * sizeof (int));
      ((evt + i)->from) = malloc (((evt + i)->nbr_file_evt) * sizeof (int));
      ((evt + i)->to) = malloc (((evt + i)->nbr_file_evt) * sizeof (int));
      ((evt + i)->num_fct_from) =
         malloc (((evt + i)->nbr_file_evt) * sizeof (int));
      ((evt + i)->num_fct_to) =
         malloc (((evt + i)->nbr_file_evt) * sizeof (int));
      (evt + i)->nb_from = 0;
      (evt + i)->nb_to = 0;
      (evt + i)->nb_fct_from = 0;
      // For some event, there is a prob limit so it is stored in event
      // parameters
      if (((evt + i)->typ_evt == 50 || (evt + i)->typ_evt == 51
           || (evt + i)->typ_evt == 52 || (evt + i)->typ_evt == 56
           || (evt + i)->typ_evt == 153) && prob_limit != NULL) {
         ((evt + i)->param_evt[(evt + i)->nb_param]) = prob_limit[0];
         (evt + i)->nb_param++;
         ((evt + i)->num_fct_from[0]) = 0;
         (evt + i)->nb_fct_from++;
      }
      // Index function has a specific structure 
      if ((evt + i)->typ_evt == 5 || (evt + i)->typ_evt == 123) {
         // Read origin queue
         fscanf (model_fd, "%s", var);
         *((evt + i)->from) = atoi (var);
         (evt + i)->nb_from++;
         fscanf (model_fd, "%s", var);
         if (strcmp (":", var)) {
            fprintf (stderr, "Erreur: On attendait un :\n");
            goto free;
         }
         // Read destination queues with their functions 
         for (j = 0; j < ((evt + i)->nbr_file_evt) - 1; j++) {
            fscanf (model_fd, " (%d,%d)", ((evt + i)->to + j),
                    ((evt + i)->num_fct_to + j));
            (evt + i)->nb_to++;
            (evt + i)->nb_fct_to++;
         }

      }
      // Multi server index function has also a specific structure 
      else if ((evt + i)->typ_evt == 8) {
         // Read origin queue
         fscanf (model_fd, "%s", var);
         *((evt + i)->from) = atoi (var);
         (evt + i)->nb_from++;
         fscanf (model_fd, "%s", var);
         if (strcmp (":", var)) {
            fprintf (stderr, "Erreur: On attendait un :\n");
            goto free;
         }
         // Read destination queues with their functions and groups
         for (j = 0; j < ((evt + i)->nbr_file_evt) - 1; j++) {
            // Read number of queues in the groups 
            fscanf (model_fd, " (%d,",
                    ((evt + i)->param_evt + ((evt + i)->nb_param)));
            // If it is not the drop value 
            if (evt[i].param_evt[(evt + i)->nb_param] != -1) {
               // Read destination queue value
               for (k = 0; k < evt[i].param_evt[(evt + i)->nb_param]; k++) {
                  fscanf (model_fd, "%d,",
                          ((evt + i)->to + ((evt + i)->nb_to)));
                  (evt + i)->nb_to++;
               }
               // Read function value
               fscanf (model_fd, "%d)", ((evt + i)->num_fct_to + j));
               (evt + i)->nb_fct_to++;
            }
            else {
               evt[i].to[j] = -1;
               (evt + i)->nb_to++;
               fscanf (model_fd, "%d)", ((evt + i)->num_fct_to + j));
               (evt + i)->nb_fct_to++;
            }
            (evt + i)->nb_param++;

         }
      }
      // Call Center 
      else if ((evt + i)->typ_evt == 9) {
         // Read origin queue
         for (j = 0; j < ((evt + i)->nbr_file_evt); j++) {
            fscanf (model_fd, "%s", var);

            if (strcmp (":", var) && !(evt + i)->nb_from) {
               *((evt + i)->from + j) = atoi (var);
            }
            else if (strcmp (":", var) && (evt + i)->nb_from) {
               *((evt + i)->to + j - (evt + i)->nb_from) = atoi (var);

            }
            else {
               (evt + i)->nb_from = j;
               // fprintf(stderr, "Evt: %d Nombre de file origine: %d \n",i,
               // (evt+i)->num_ori );
               // Ici on a rencontre le caractere :
               j--;
            }
         }
         (evt + i)->nb_to = (evt + i)->nbr_file_evt - (evt + i)->nb_from;

      }
      else {
         for (j = 0; j < ((evt + i)->nbr_file_evt); j++) {
            fscanf (model_fd, "%s", var);

            if (strcmp (":", var) && !(evt + i)->nb_from) {
               *((evt + i)->from + j) = atoi (var);
            }
            else if (strcmp (":", var) && (evt + i)->nb_from) {
               *((evt + i)->to + j - (evt + i)->nb_from) = atoi (var);
            }
            else {
               (evt + i)->nb_from = j;
               // fprintf(stderr, "Evt: %d Nombre de file origine: %d \n",i,
               // (evt+i)->num_ori );
               // Ici on a rencontre le caractere :
               j--;
            }
         }
         if (!(evt + i)->nb_from)
            (evt + i)->nb_from = (evt + i)->nbr_file_evt;
         else
            (evt + i)->nb_to = ((evt + i)->nbr_file_evt) - (evt + i)->nb_from;
      }



   }

   fprintf (dest, "Events : \n");
   for (j = 0; j < *nb_evt; j++) {
      fprintf (dest, " - id: evt%d \n", (evt + j)->id_evt);
      fprintf (dest, "   type:  Default$%s\n",
               find_index_labels (list, size, (evt + j)->typ_evt));
      fprintf (dest, "   rate: %f \n", (evt + j)->lambda);
      fprintf (dest, "   from:  ");
      if (!(evt + j)->nb_from)
         fprintf (dest, "~\n");
      else {
         fprintf (dest, "[");
         for (i = 0; i < (evt + j)->nb_from; i++) {
            if (*((evt + j)->from + i) == -1)
               fprintf (dest, "outside");
            else
               fprintf (dest, "queue%d", *((evt + j)->from + i));
            if (i < (evt + j)->nb_from - 1)
               fprintf (dest, ",");
         }
         fprintf (dest, "]\n");
      }
      fprintf (dest, "   indexFctOrigin: ");
      if (!(evt + j)->nb_fct_from)
         fprintf (dest, "~\n");
      else {
         fprintf (dest, "[");
         for (i = 0; i < (evt + j)->nb_fct_from; i++) {
            fprintf (dest, "MyLib$index%d", *((evt + j)->num_fct_from + i));
            if (i < (evt + j)->nb_fct_from - 1)
               fprintf (dest, ",");
         }
         fprintf (dest, "]\n");
      }
      fprintf (dest, "   to:  ");
      if (!(evt + j)->nb_to)
         fprintf (dest, "~\n");
      else {
         fprintf (dest, "[");
         for (i = 0; i < (evt + j)->nb_to; i++) {
            if (*((evt + j)->to + i) == -1)
               fprintf (dest, "drop");
            else
               fprintf (dest, "queue%d", *((evt + j)->to + i));
            if (i < (evt + j)->nb_to - 1)
               fprintf (dest, ",");
         }
         fprintf (dest, "]\n");
      }
      fprintf (dest, "   indexFctDest:  ");
      if (!(evt + j)->nb_fct_to)
         fprintf (dest, "~\n");
      else {
         fprintf (dest, "[");
         for (i = 0; i < (evt + j)->nb_fct_to; i++) {
            fprintf (dest, "MyLib$index%d", *((evt + j)->num_fct_to + i));
            if (i < (evt + j)->nb_fct_to - 1)
               fprintf (dest, ",");
         }
         fprintf (dest, "]\n");
      }
      fprintf (dest, "   parameters:  ");
      if (!(evt + j)->nb_param)
         fprintf (dest, "~\n");
      else {
         fprintf (dest, "[");
         for (i = 0; i < (evt + j)->nb_param; i++) {
            fprintf (dest, "%d", *((evt + j)->param_evt + i));
            if (i < (evt + j)->nb_param - 1)
               fprintf (dest, ",");
         }
         fprintf (dest, "]\n");
      }
      fprintf (dest, "\n");

   }
   if (file_LS == NULL)
      fprintf (dest, "MyLib:  ~\n\n\n");
   else {
      tmp = split (file_LS, '.');
      fprintf (dest, "MyLib:  [./%s]\n\n\n", tmp);
   }
   free (commande);
   free (var);
   pclose (model_fd);
   fprintf (stderr, "Reading of the data file : %s over\n", nomf);

 free:
   if (dest != NULL)
      fclose (dest);
   if (etat_init_supU != NULL)
      free (etat_init_supU);
   if (etat_init_infU != NULL)
      free (etat_init_infU);
   if (capfile != NULL)
      free (capfile);
   if (tmp != NULL)
      free (tmp);
   if (file_LS != NULL)
      free (file_LS);
   if (prob_limit != NULL)
      free (prob_limit);
   for (i = 0; i < *nb_evt; i++) {
      if (((evt + i)->param_evt) != NULL)
         free ((evt + i)->param_evt);
      if (((evt + i)->num_fct) != NULL)
         free ((evt + i)->num_fct);
      if (((evt + i)->from) != NULL)
         free ((evt + i)->from);
      if (((evt + i)->to) != NULL)
         free ((evt + i)->to);
      if (((evt + i)->num_fct_from) != NULL)
         free ((evt + i)->num_fct_from);
      if (((evt + i)->num_fct_to) != NULL)
         free ((evt + i)->num_fct_to);
   }
   if (evt != NULL)
      free (evt);
   if (nb_evt != NULL)
      free (nb_evt);
   if (nbr_file != NULL)
      free (nbr_file);
}

int
main (int argc, char *argv[])
{
   int size_transcode = 23;
   T_label *transcode;
   transcode = (T_label *) malloc (size_transcode * sizeof (T_label));
   transcode[0] = (T_label) {
   "ext_departure", 1};
   transcode[1] = (T_label) {
   "ext_arrival_reject", 2};
   transcode[2] = (T_label) {
   "Depart_multi_serveur", 3};
   transcode[3] = (T_label) {
   "JSQ_rejet", 4};
   transcode[4] = (T_label) {
   "Reject_Index_Arrival", 5};
   transcode[5] = (T_label) {
   "routing_n_queues_reject", 6};
   transcode[6] = (T_label) {
   "routage_nfile_bloc", 7};
   transcode[7] = (T_label) {
   "Reject_Multi_Server_Index_Arrival", 8};
   transcode[8] = (T_label) {
   "Depart_call_center", 9};
   transcode[9] = (T_label) {
   "negative_customer", 120};
   transcode[10] = (T_label) {
   "batch", 121};
   transcode[11] = (T_label) {
   "dec_batch", 22};
   transcode[12] = (T_label) {
   "batch_index_routing", 123};
   transcode[13] = (T_label) {
   "Independent_Pull", 50};
   transcode[14] = (T_label) {
   "Independent_Push", 51};
   transcode[15] = (T_label) {
   "Arrival_Push", 52};
   transcode[16] = (T_label) {
   "LTM_arrival", 54};
   transcode[17] = (T_label) {
   "LTM_departure", 55};
   transcode[18] = (T_label) {
   "ENV_Departure_Pull", 153};
   transcode[19] = (T_label) {
   "EmptyQueue_Pull", 56};
   transcode[20] = (T_label) {
   "TQN_phase1_service", 31};
   transcode[21] = (T_label) {
   "ENV_TQN_phase1_service_skip_phase2", 132};
   transcode[22] = (T_label) {
   "ENV_TQN_phase2_service", 133};
   modele_parser (argv[1], argv[2], transcode, size_transcode);
   free (transcode);
   return 0;
}
