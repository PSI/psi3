#Inclusion des en-têtes publics
INCLUDE_DIRECTORIES(include
   provide/generator
   provide/parser/
   provide/output_stream
   provide/log
   provide/timing .)

#Configuration de la bibliothèque
FILE (
    GLOB_RECURSE
    lib_files
    provide/parser/*
    provide/lib/*
    include/*
)

ADD_LIBRARY (
    psi3_default
    SHARED
    ${lib_files}
)

SET_TARGET_PROPERTIES (
   psi3_default PROPERTIES
   VERSION ${PSI3_VERSION}
   SOVERSION 1
)

SET (
   PARSER_FILES
   provide/parser/parser.c
   provide/parser/yaml/src/yaml.c
)

SET (
   OUTPUT_STREAM_FILES
   provide/output_stream/psi3_output_stream.c
   provide/output_stream/psi3_output_stream.h
)

SET (
   LOG_FILES
   provide/log/psi3_log.c
   provide/log/psi3_log.h
)

SET (
   GENERATOR_FILES
   provide/generator/generator.c
   provide/generator/RngStream.c
)

SET (
   TIMING_FILES
   provide/timing/psi3_timing.c
   provide/timing/psi3_timing.h
)

SET (
   COMMON_FILES
   psi_common.c
   psi_common.h
   ${GENERATOR_FILES}
   ${PARSER_FILES}
   ${LOG_FILES}
   ${OUTPUT_STREAM_FILES}
   ${TIMING_FILES}
)


SET (
   MATRIX_FILES
   ${COMMON_FILES}
   matrix.c
)

SET (
   SIMPLE_FW_KERNEL_FILES
   ${COMMON_FILES}
   simpleforward.c
)

SET (
   SIMPLE_FW2_KERNEL_FILES
   ${COMMON_FILES}
   multipleforward.c
)

SET (
   SIMPLE_FW3_KERNEL_FILES
   ${COMMON_FILES}
   simpleforward3.c
)

SET (
   SIMPLE_FW_PARALLEL_KERNEL_FILES
   ${COMMON_FILES}
   simpleforwardparallel.c
)

SET (
   BACK_MONO_KERNEL_FILES
   ${COMMON_FILES}
   backwardmonotone.c
)

SET (
   BACK_MONO_PAR_KERNEL_FILES
   ${COMMON_FILES}
   backwardmonotoneparallel.c
)

SET (
   BACK_MONO_EN_KERNEL_FILES
   ${COMMON_FILES}
   backwardenvelope.c
)

SET (
   BACK_MONO_EN_SPLIT_KERNEL_FILES
   ${COMMON_FILES}
   backwardenvelopesplitting.c
)

ADD_EXECUTABLE (
	get_lib 
	${PARSER_FILES}
	${COMMON_FILES}
	provide/parser/get_lib.c
)

ADD_EXECUTABLE (
	backwardmonotone 
	${BACK_MONO_KERNEL_FILES}
	${COMMON_FILES}
)

ADD_EXECUTABLE (
	matrix
   ${MATRIX_FILES}
	${COMMON_FILES}
)

ADD_EXECUTABLE (
	simpleforward
	${SIMPLE_FW_KERNEL_FILES}
	${COMMON_FILES}
)

ADD_EXECUTABLE(
	backwardenvelope
	${BACK_MONO_EN_KERNEL_FILES}
	${COMMON_FILES}
)

ADD_EXECUTABLE (
	backwardenvelopesplitting
	${BACK_MONO_EN_SPLIT_KERNEL_FILES}
	${COMMON_FILES}
)

# For now, only gcc is able to compile SFP properly if version is > 4.4...
if ("${CMAKE_C_COMPILER_ID}" STREQUAL "GNU" AND C_COMPILER_VERSION VERSION_GREATER 4.4)

   ADD_EXECUTABLE (
      simpleforwardparallel 
      ${SIMPLE_FW_PARALLEL_KERNEL_FILES}
      ${COMMON_FILES}
   )

   ADD_EXECUTABLE (
      simpleforward3
      ${SIMPLE_FW3_KERNEL_FILES}
      ${COMMON_FILES}
   )

   ADD_EXECUTABLE (
      multipleforward
      ${SIMPLE_FW2_KERNEL_FILES}
      ${COMMON_FILES}
   )

   ADD_EXECUTABLE (
      backwardmonotoneparallel
      ${BACK_MONO_PAR_KERNEL_FILES}
      ${COMMON_FILES}
   )


   # add target compile flag for simple forward parallel to be compatible with
   # OpenMP
   SET_TARGET_PROPERTIES (
      simpleforwardparallel
      PROPERTIES COMPILE_FLAGS "-fopenmp"
      LINK_FLAGS "-fopenmp"
   )
   SET_TARGET_PROPERTIES (
      simpleforward3
      PROPERTIES COMPILE_FLAGS "-fopenmp"
      LINK_FLAGS "-fopenmp"
   )
   SET_TARGET_PROPERTIES (
      multipleforward
      PROPERTIES COMPILE_FLAGS "-fopenmp"
      LINK_FLAGS "-fopenmp"
   )
   SET_TARGET_PROPERTIES (
      backwardmonotoneparallel
      PROPERTIES COMPILE_FLAGS "-fopenmp"
      LINK_FLAGS "-fopenmp"
   )

   INSTALL (
      TARGETS get_lib psi3_default matrix simpleforward simpleforward3 multipleforward backwardenvelope backwardmonotoneparallel backwardmonotone backwardenvelopesplitting simpleforwardparallel
      RUNTIME DESTINATION lib/psi3_unix/
      LIBRARY DESTINATION lib
   )

else ("${CMAKE_C_COMPILER_ID}" STREQUAL "GNU" AND C_COMPILER_VERSION VERSION_GREATER 4.4)

   if ("${CMAKE_C_COMPILER_ID}" STREQUAL "Clang")

      message (STATUS "Clang compiler : ${C_COMPILER_VERSION}"
                      "Method Simple Forward Parallel won't be available (GCC > "
                      "4.4 is required for parallelism).")

   elseif ("${CMAKE_C_COMPILER_ID}" STREQUAL "GNU")

      message(STATUS "GCC compiler out-of-date : ${C_COMPILER_VERSION}\n "
                     "Method Simple Forward Parallel requires at least GCC 4.4. "
                     "Please upgrade GCC to the latest version to get full "
                     "compatibility with PSi3.\n")

   endif ()


   INSTALL (
      TARGETS get_lib psi3_default matrix simpleforward backwardenvelope backwardmonotone backwardenvelopesplitting
      RUNTIME DESTINATION lib/psi3_unix/
      LIBRARY DESTINATION lib
   )

endif ()


INSTALL (
   FILES "${CMAKE_CURRENT_BINARY_DIR}/include/psi.h"
   DESTINATION include
)

