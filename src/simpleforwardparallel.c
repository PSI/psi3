 /**
 * \file          simpleforwardparallel.c
 * \author        Minh Quan HO
 * \author        Jean-Marc.Vincent@imag.fr
 * \version       1.0.0
 * \date          26/06/2012
 * \brief         Simple Forward Parallel kernel.
 */

 /* 
  * This library is free software; you can redistribute it and/or
  * modify it under the terms of the GNU Lesser General Public
  * License as published by the Free Software Foundation; either
  * version 2.1 of the License, or any later version.
  * 
  * This library is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  * Lesser General Public License for more details.
  * 
  * You should have received a copy of the GNU Lesser General Public
  * License along with this library; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
  */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <dlfcn.h>
#include <sys/time.h>
#include <time.h>
#include <generator.h>
#include <parser.h>
#include "psi_common.h"

#ifdef _OPENMP
#include <omp.h>
#endif

#define NB_SAVED_TRAJECTORY_IN_INTERVAL 2
#define NO_MORE_INTERVAL -1

#define MASTER 1
#define NO_MASTER 0
#define CHECK_COUPLING_AT_BEGIN_STATE 1
#define CHECK_COUPLING_IN_REALTIME 2
#define GET_RANDOM_BEGIN_STATE 1
#define GET_INITIAL_BEGIN_STATE 2
#define PRINT_OUT_TRAJECTORY 1
#define PRINT_TIME_AFTER_SIMULATION 1

/* The following is here for futur improvemenents */
#define CHECK_STOP_CONDITION 1      // default is 1
#define OPTIMISATION_TRAJECTORY 0   // default is 0
/*****************************************************************************/
/*             Global variables used by transition or simulation             */

/* TRANSITION ------------- */
/* Model */
/* - Queues: */
int *capmins;                   // capacity min of queues
int *capmaxs;                   // capacity max of queues
int nb_queues;

/* Load Sharing ---------- */
int ls_mode;

int **node_dist;                /* Matrix of distance between two queues :
                                 * node_dist[i][j] = distance between i and j */

struct st_neighbors *node_neighbors;   /* Array containing for each node, the
                                        * neighbors of the node */

/* SIMULATION ------------ */
/* FIXME: will probably move to a struct */
int **log_states;
int *g_events_list;
int *begin_state;
double simulation_time;
int nb_AV;
int FLAG;

/* PARALLEL -------------- */
#ifdef _OPENMP
omp_lock_t *intervals_lock;
#endif
T_task_pool *taskpool;
T_interval *intervals;
int cpt_transition_master;
int cpt_transition_total;
int cpt_tasks_done;
int finished = 0;


/*****************************************************************************/

/**
 * Check equal between two integer vectors
 */
int
equal (int *l1, int *l2, int size)
{
   int i;
   for (i = 0; i < size; i++) {
      if (l1[i] != l2[i])
         return 0;
   }
   return 1;
}

/**
 * Initialize intervals
 */
void
init_intervals (int trajectoryLength, int granularity, int nb_events)
{
   int i, j, k;
   int length_avg, length_rest;

   PSI3_DEBUG ("Init memory");
   long unsigned debug_memory_cpt = 0;

#ifdef _OPENMP
   // initialize lock on each interval
   intervals_lock = (omp_lock_t *) malloc (granularity * sizeof (omp_lock_t));

   debug_memory_cpt += granularity * sizeof (omp_lock_t);

   for (i = 0; i < granularity; i++) {
      omp_init_lock (intervals_lock + i);
   }
#endif

   // calcul of interval length
   length_avg = (int) (trajectoryLength / granularity);
   length_rest = trajectoryLength % granularity;   // for the last interval

   g_events_list = (int *) malloc ((trajectoryLength) * sizeof (int));

   debug_memory_cpt += trajectoryLength * sizeof (int);

   for (i = 0; i < granularity; i++) {

      intervals[i].id_interval = i;
      intervals[i].begin = i * length_avg;

      intervals[i].length = (i == granularity - 1)
         ? length_avg + length_rest : length_avg;

      intervals[i].last_saved_trajectory = -1;

      // events list on each interval
      intervals[i].events_list = (int *) &(g_events_list[intervals[i].begin]);

/* TODO: May be parallelized */
      for (j = 0; j < intervals[i].length; j++) {
         intervals[i].events_list[j] = genere_evenement (u01 (), nb_events);
      }

      // pre-allocate for saved_trajectories
      intervals[i].saved_trajectories =
         (int ***) malloc (NB_SAVED_TRAJECTORY_IN_INTERVAL * sizeof (int **));

      debug_memory_cpt += NB_SAVED_TRAJECTORY_IN_INTERVAL * sizeof (int **);

      for (j = 0; j < NB_SAVED_TRAJECTORY_IN_INTERVAL; j++) {
         intervals[i].saved_trajectories[j] =
            (int **) malloc ((intervals[i].length + 1) * sizeof (int *));

         debug_memory_cpt += (intervals[i].length + 1) * sizeof (int *);

         for (k = 0; k < intervals[i].length + 1; k++) {
            intervals[i].saved_trajectories[j][k] =
               (int *) malloc (nb_queues * sizeof (int));
            // set all value to -1
            memset (intervals[i].saved_trajectories[j][k],
                    -1, nb_queues * sizeof (int));

            debug_memory_cpt += nb_queues * sizeof (int);
         }
      }
   }

   PSI3_DEBUG ("Init memory Ok");
   PSI3_DEBUG ("Total allocated memory for trajectory(ies): [%lu] MB", debug_memory_cpt / 1024 / 1024);
}

/**
 * Free intervals
 */
void
free_intervals (int granularity)
{
   int i, j, k;

   if (intervals != NULL) {

      for (i = 0; i < granularity; i++) {

         for (j = 0; j < NB_SAVED_TRAJECTORY_IN_INTERVAL; j++) {

            for (k = 0; k < intervals[i].length + 1; k++) {

               free (intervals[i].saved_trajectories[j][k]);
            }

            free (intervals[i].saved_trajectories[j]);
         }

         free (intervals[i].saved_trajectories);
      }

      free (intervals);
   }

   free (g_events_list);

#ifdef _OPENMP
   // destroy locks on intervals
   for (i = 0; i < granularity; i++)
      omp_destroy_lock (intervals_lock + i);
#endif
}


/**
 * Initialize task pool
 */
void
init_task_pool ()
{
   taskpool->nb_tasks = 0;
   taskpool->head = NULL;
   taskpool->queue = NULL;
}

/**
 * [CRITICAL] Empty task pool : delete all tasks in taskpool
 */
void
empty_taskpool ()
{
   T_task *temp, *cour;
   cour = taskpool->head;
   while (cour != NULL) {
      temp = cour;
      cour = cour->next;
      free (temp->data.begin_state);
      free (temp);
   }
   taskpool->nb_tasks = 0;
   taskpool->head = NULL;
   taskpool->queue = NULL;
}

/**
 * Free task pool
 */
void
free_taskpool ()
{
   if (taskpool != NULL)
      free (taskpool);
}

/**
 * Return a T_task* describing a task to push into taskpool 
 */
T_task *
create_task (int id_interval, int *begin_state, int is_master, int priority)
{
   T_task *new_task = (T_task *) malloc (sizeof (T_task));

   if (new_task != NULL) {
      /* data initialization */
      new_task->data.id_interval = id_interval;
      new_task->data.begin_state = begin_state;
      new_task->data.is_master = is_master;
      new_task->data.priority = priority;
   }

   return new_task;
}

/**
 * [CRITICAL] Push a task at the end of task pool
 */
void
push_task (T_task * new_task)
{
   if (new_task != NULL) {
      /* linkage */
      new_task->next = NULL;

      if (taskpool->head == NULL) { /* if empty taskpool, add new_task to head */
         new_task->prec = NULL;
         taskpool->head = new_task;
         taskpool->queue = new_task;
      }
      else {                    /* not empty, add new_task to queue */
         taskpool->queue->next = new_task;
         new_task->prec = taskpool->queue;
         taskpool->queue = new_task;
      }
      taskpool->nb_tasks++;
   }
}

/** Two possible methods to pop a task from taskpool :
 *	1. Pop_FIFO   : Pop in FIFO order 
 * 	2. Pop_Master : Look for the master task in taskpool. If master task found, pop this task; otherwise pop in FIFO order 
 * 
 * /!\ : task poping is now done by calling a user-defined function, so these functions below are commented 
 */
/// Pop in FIFO order
// T_task* pop_task_FIFO(){
//   T_task* task;
//   if(taskpool->nb_tasks <= 0){      /// taskpool is empty
//     return NULL;
//   }else{
//     task = taskpool->head;       // get the first task    
//     taskpool->head = taskpool->head->next;   // move head pointer forward
//     taskpool->nb_tasks--;
//     
//     if(task->data.is_master){
//       /// if master task is on the last interval means that this is the last task todo, no more task is needed
//       if(task->data.id_interval == method->Granularity-1){
//    finished++;
//    empty_taskpool();
//       }
//     }
//     return task;
//   }
// }

/// Look for master task, if there is no master task, pop in FIFO
// T_task* pop_task_MASTER(){
//   T_task* task;
//   if(taskpool->nb_tasks <= 0){      /// empty taskpool 
//     return NULL;
//   }else{
//     task = taskpool->head;
//     taskpool->nb_tasks--;        /// withdraw the task
//     /// look for the master task
//     while((task != NULL) && (!task->data.is_master)){
//       task = task->next;
//     }
//     if(task == NULL){            /// no master task, return the first task in taskpool
//       task = taskpool->head;
//       taskpool->head = (taskpool->head->next != NULL) ? taskpool->head->next : NULL;
//     }else{              /// master task found 
//       if(task == taskpool->head){      // master task at head
//    if(task->next == NULL){       // no task behind => taskpool becomes empty
//      taskpool->head = NULL;
//      taskpool->queue = NULL;
//    }else{            //there is a task behind => move head pointer forward
//      taskpool->head = task->next;
//    }
//       }else{               // master task not at head
//    if(task->next == NULL){       // no task behind => move queue pointer back
//      task->prec->next = NULL;
//      taskpool->queue = task->prec;
//    }else{            //there is a task behind => modify pointer linkages 
//      task->prec->next = task->next;
//      task->next->prec = task->prec;
//    }
//       }
//       /// if master task is on the last interval means that this is the last task todo, no more task is needed
//       if(task->data.id_interval == method->Granularity-1){
//    finished++;
//    empty_taskpool();
//       }
//     }
//     return task;
//   }
// }


/**
 * [CRITICAL] Pop a task at the head of task pool, return NULL if taskpool is empty
 */
// T_task* pop_task(int pop_method){
//   if(pop_method == POP_METHOD_FIFO) return pop_task_FIFO();
//   else return pop_task_MASTER();
// }


/**
 *	\brief      The task to be done inside a thread
 * \details    Calculate a sub-trajectory + maybe some extra actions : write
 *             out trajectory or create a new task and check for coupling on
 *             the begin_state
 * \param      task           task need to be done   
 * \param      stream_out     maybe need to write out trajectory
 * \return     void
 */
void task_OpenMP_check_coupling_by_beginState
   (T_task * task,
    FILE * stream_out, st_simpleForwardParallel_ptr method, st_model_ptr model)
{
   T_interval *this_interval;
   T_state *state;
   T_event *event;
   int next_interval, i_traject, skip, i;
   int **new_sub_traject;
   int *begin_state;

#ifdef PRINT_OUT_TRAJECTORY
   int valid_length;
#endif

#ifdef _OPENMP
   /* Check for lock to be able to work on this interval */
   if (omp_test_lock (intervals_lock + task->data.id_interval) == 0) {
      /* fail to acquire lock re-push: this task into taskpool to do it later
       * this may be better than blocking the current thread, risk of deadlock */
#pragma omp critical
      push_task (task);
   }
   else {                       /* lock acquired, do it! and don't forget to
                                 * release lock at the end */
#endif

      this_interval = intervals + task->data.id_interval;   /* get the current
                                                             * interval by
                                                             * pointer */

#ifdef PRINT_OUT_TRAJECTORY
      valid_length = this_interval->length;
#endif
      /* check if the new trajectory is already calculated (same begin_state
       * with one of the saved_trajectories): */
      skip = 0;
      i_traject = 0;
      do {
         PSI3_DEBUG ("Thread [%d] interval [%d] check traj [%d] @%p",
                     omp_get_thread_num(),
                     this_interval->id_interval,
                     i_traject,
                     this_interval->saved_trajectories[i_traject]);
         PSI3_DEBUG ("Thread [%d] interval [%d] begin [%d, ..., %d]",
                     omp_get_thread_num(),
                     this_interval->id_interval,
                     this_interval->saved_trajectories[i_traject][0],
                     this_interval->saved_trajectories[i_traject][nb_queues]);
         if (equal (task->data.begin_state,
                    this_interval->saved_trajectories[i_traject][0],
                    nb_queues)) {
            /* same begin_state of saved_trajectories[i_traject] */
            skip = 1;
         }
         else {
            i_traject++;
         }
      } while (!skip && (i_traject < NB_SAVED_TRAJECTORY_IN_INTERVAL));

      /* if trajectory has already been calculated */
      if (skip) {

         PSI3_DEBUG ("Thread [%d] gets interval [%d] %6s %4s",
                     omp_get_thread_num(),
                     this_interval->id_interval,
                     (task->data.is_master)? "master" : "",
                     skip? "SKIP" : "");

         if (task->data.is_master) {
            /* Validate the sub-trajectory + checking user-defined stop
             * condition */
#if CHECK_STOP_CONDITION
            i = 0;
            while ((i < this_interval->length)
                   && !((T_ptr_stopfct) method->StopFct->ptr)
                   (this_interval->saved_trajectories[i_traject][i])) {
               i++;
            }
#else
            i = this_interval->length;
#endif

            if (i < this_interval->length) { /* if stop condition is true */
#ifdef PRINT_OUT_TRAJECTORY
               valid_length = i + 1;
#endif
#pragma omp atomic
               finished++;
            }
            else {
               /* find the next interval to create new task: */
               next_interval =
                  (task->data.id_interval + 1) < method->Granularity
                     ? (task->data.id_interval + 1)
                     : NO_MORE_INTERVAL;
               if (next_interval == NO_MORE_INTERVAL) {
                  PSI3_DEBUG ("Thread [%d] NO_MORE_INTERVAL", omp_get_thread_num());
#pragma omp atomic
                  finished++;
               }
               else {
                  /* Create new task for the next interval as master last_state 
                   * of interval n becomes begin_state of interval n+1 */
                  begin_state = (int *) malloc (nb_queues * sizeof (int));
                  memcpy (begin_state,
                          this_interval->
                          saved_trajectories[i_traject][this_interval->length],
                          nb_queues * sizeof (int));

                  PSI3_DEBUG ("Thread [%d] new task for interval [%d]",
                              omp_get_thread_num(),  next_interval);
#pragma omp critical
                  push_task (create_task (next_interval,
                                          begin_state,
                                          task->data.is_master,
                                          task->data.priority + 1));
               }
            }
#ifdef PRINT_OUT_TRAJECTORY
#pragma omp critical
            {
               PSI3_DEBUG ("Thread [%d] writting interval [%d] results",
                           omp_get_thread_num(), this_interval->id_interval);
               /* user-defined output function call */
               ((T_ptr_outputfct)
                (method->OutputFct->ptr))
                  (stream_out,
                   this_interval->saved_trajectories[i_traject],
                   this_interval->events_list,
                   this_interval->begin, valid_length);
               PSI3_DEBUG ("Thread [%d] writting interval [%d] results FINISHED",
                           omp_get_thread_num(), this_interval->id_interval);
            }
#endif

         }
         else {                 /* skip and current interval is not master: do
                                 * nothing */
         }

      }
      else {
         /* sub-trajectory has never been calculated : Calculate the new
          * sub-trajectory with begin_state + checking user-defined stop
          * condition */

         PSI3_DEBUG ("Thread [%d] gets interval [%d] %6s %4s",
                     omp_get_thread_num(),
                     this_interval->id_interval,
                     (task->data.is_master)? "master" : "",
                     skip? "SKIP" : "");
/* TODO: remove this malloc */
         /* pre-allocate memory */
         state = (T_state *) malloc (sizeof (T_state));
         state->queues = (int **) malloc (sizeof (int *));
         state->nb_vectors = 1;
         state->splitting = 0;
         state->queues[0] = (int *) malloc (nb_queues * sizeof (int));

#if OPTIMISATION_TRAJECTORY
         this_interval->last_saved_trajectory =
            (this_interval->last_saved_trajectory + 1) %
            NB_SAVED_TRAJECTORY_IN_INTERVAL;
         new_sub_traject = this_interval->saved_trajectories [this_interval->last_saved_trajectory];
         PSI3_DEBUG ("Thread [%d] interval [%d] traj [%d] new_sub_traject [%p]",
                     omp_get_thread_num(),
                     this_interval->id_interval,
                     this_interval->last_saved_trajectory,
                     new_sub_traject);
#else
         new_sub_traject = (int **) malloc ((this_interval->length + 1) *
                                            sizeof (int *));

         for (i = 0; i < this_interval->length + 1; i++) {
            new_sub_traject[i] = (int *) malloc (nb_queues * sizeof (int));
            memset (new_sub_traject[i], -1, nb_queues * sizeof (int));
         }
#endif


         /* set begin_state for new_sub_traject and state */
         memcpy (state->queues[0],
                 task->data.begin_state, nb_queues * sizeof (int));
         memcpy (new_sub_traject[0],
                 task->data.begin_state, nb_queues * sizeof (int));

         /* calculate the sub-trajectory */
         i = 0;
         do {
            /* find event at step i of sub-trajectory */
            event = model->events->evts + this_interval->events_list[i];
            /* transition call */
            state =
               (((T_ptr_transfct) event->ptr_transition)) (state, event, model);
            /* count number of transition calls */
            if (task->data.is_master) {
#pragma omp critical
               {
                  cpt_transition_total++;
                  cpt_transition_master++;
               }
            }
            else {
#pragma omp atomic
               cpt_transition_total++;
            }
            /* save state into sub-trajectory */
            memcpy (new_sub_traject[i + 1],
                    state->queues[0], nb_queues * sizeof (int));
            i++;

            if ((task->data.is_master
                 &&
                 (((T_ptr_stopfct) method->StopFct->ptr) (state->queues[0]))))
               break;

         } while ((i < this_interval->length));

         if (i < this_interval->length) { /* stop condition is true in master
                                           * task */
#ifdef PRINT_OUT_TRAJECTORY
            valid_length = i + 1;
#endif
#pragma omp atomic
            finished++;
         }
         else {                 /* stop condition is not true */
            /* save the new_sub_traject into saved_trajectories */
#if ! OPTIMISATION_TRAJECTORY
            this_interval->last_saved_trajectory =
               (this_interval->last_saved_trajectory + 1) %
               NB_SAVED_TRAJECTORY_IN_INTERVAL;

            for (i = 0; i < this_interval->length + 1; i++) {
               memcpy (this_interval->
                       saved_trajectories[this_interval->
                                          last_saved_trajectory][i],
                       new_sub_traject[i], nb_queues * sizeof (int));
            }
#endif

            /* find the next interval to create new task: */
            next_interval = (task->data.id_interval + 1) < method->Granularity
               ? (task->data.id_interval + 1)
               : NO_MORE_INTERVAL;

            if (next_interval == NO_MORE_INTERVAL) {
               if (task->data.is_master) {
                  PSI3_DEBUG ("Thread [%d] NO_MORE_INTERVAL", omp_get_thread_num());
#pragma omp atomic
                  finished++;
               }
            }
            else {
               /* Create new task for the next interval as master */
               begin_state = (int *) malloc (nb_queues * sizeof (int));
               /* last_state of interval n becomes begin_state of interval n+1 */
               memcpy (begin_state,
                       new_sub_traject[this_interval->length],
                       nb_queues * sizeof (int));
               PSI3_DEBUG ("Thread [%d] new task for interval [%d]",
                           omp_get_thread_num(),  next_interval);
#pragma omp critical
               push_task (create_task (next_interval,
                                       begin_state,
                                       task->data.is_master,
                                       task->data.priority + 1));
            }
         }

         /* If interval is master, validate the sub-trajectory */
         if (task->data.is_master) {
#ifdef PRINT_OUT_TRAJECTORY
#pragma omp critical
            {
               PSI3_DEBUG ("Thread [%d] writting interval [%d] results",
                           omp_get_thread_num(), this_interval->id_interval);
               /* user-defined output function call */
               ((T_ptr_outputfct) (method->OutputFct->ptr))
                  (stream_out,
                   new_sub_traject,
                   this_interval->events_list,
                   this_interval->begin, valid_length);
               PSI3_DEBUG ("Thread [%d] writting interval [%d] results FINISHED",
                           omp_get_thread_num(), this_interval->id_interval);
            }
#endif
         }
         /* free what have been allocated for sub-trajactory calculating */
         free (state->queues[0]);
         free (state->queues);
         free (state);

#if ! OPTIMISATION_TRAJECTORY
         for (i = 0; i < this_interval->length + 1; i++) {
            free (new_sub_traject[i]);
         }
         free (new_sub_traject);
#endif

      }                         /* end if(skip) */
#ifdef _OPENMP
      /* release lock */
      omp_unset_lock (intervals_lock + this_interval->id_interval);
#endif
      /* free the task */
      free (task->data.begin_state);
      free (task);

      /* increment number of tasks done */
#pragma omp atomic
      cpt_tasks_done++;
#ifdef _OPENMP
   }                            /* end of task */
#endif
}


/**
 *	The task to be done inside a thread
 * \details    calculate a sub-trajectory + maybe some extra actions : write out trajectory or 
 *              create a new task and check for coupling in real time (i.e after each transition)
 * \param      task           T_task*      : task need to be done   
 * \param      stream_out     FILE*        : stream_out  (maybe need to write out trajectory)
 * \return    void
 */
void task_OpenMP_check_coupling_realtime
   (T_task * task,
    FILE * stream_out, st_simpleForwardParallel_ptr method, st_model_ptr model)
{
   T_interval *this_interval;
   T_state *state;
   T_event *event;
   int next_interval, i_traject, skip, i, j;
   int **new_sub_traject;
   int *begin_state;
#ifdef PRINT_OUT_TRAJECTORY
   int valid_length;
#endif

#ifdef _OPENMP
   /* check for lock to be able to work on this interval */
   if (omp_test_lock (intervals_lock + task->data.id_interval) == 0) {
      /* fail to acquire lock re-push this task into taskpool to do it later
       * this may be better than blocking the current thread, risk of deadlock */
#pragma omp critical
      push_task (task);
   }
   else {
      /* lock acquired, do it! and don't forget to release lock at the end */
#endif

      /* get the current interval by pointer */
      this_interval = intervals + task->data.id_interval;
#ifdef PRINT_OUT_TRAJECTORY
      valid_length = this_interval->length;
#endif
      /* check if the new trajectory is already calculated (same begin_state
       * with one of the saved_trajectories): */
      skip = 0;
      i_traject = 0;

      do {
         if (equal (task->data.begin_state,
                    this_interval->saved_trajectories[i_traject][0],
                    nb_queues)) {
            /* same begin_state of saved_trajectories[i_traject] */
            skip = 1;
         }
         else
            i_traject++;

      } while (!skip && (i_traject < NB_SAVED_TRAJECTORY_IN_INTERVAL));

      /* if trajectory has already been calculated */
      if (skip) {
         if (task->data.is_master) {
            /* Validate the sub-trajectory + checking user-defined stop
             * condition */
            i = 0;
            while ((i < this_interval->length)
                   && !((T_ptr_stopfct) method->StopFct->ptr)
                   (this_interval->saved_trajectories[i_traject][i])) {
               i++;
            }
            if (i < this_interval->length) { /* if stop condition is true */
#ifdef PRINT_OUT_TRAJECTORY
               valid_length = i + 1;
#endif
#pragma omp atomic
               finished++;
            }
            else {
               /* find the next interval to create new task: */
               next_interval =
                  (task->data.id_interval + 1) <
                  method->Granularity ? (task->data.id_interval + 1)
                  : NO_MORE_INTERVAL;
               if (next_interval == NO_MORE_INTERVAL) {
#pragma omp atomic
                  finished++;
               }
               else {
                  /* Create new task for the next interval as master last_state
                   * of interval n becomes begin_state of interval n+1 */
                  begin_state = (int *) malloc (nb_queues * sizeof (int));
                  memcpy (begin_state,
                          this_interval->
                          saved_trajectories[i_traject][this_interval->length],
                          nb_queues * sizeof (int));
#pragma omp critical
                  push_task (create_task (next_interval,
                                          begin_state,
                                          task->data.is_master,
                                          task->data.priority + 1));
               }
            }
#ifdef PRINT_OUT_TRAJECTORY
#pragma omp critical
            {
               /* user-defined output function call */
               ((T_ptr_outputfct) (method->OutputFct->ptr))
                  (stream_out,
                   this_interval->saved_trajectories[i_traject],
                   this_interval->events_list,
                   this_interval->begin, valid_length);
            }
#endif
         }
         else {                 /* skip and current interval is not master */
            /* do nothing */
         }
      }
      else {
         /* sub-trajectory has never been calculated : Calculate the new
          * sub-trajectory with begin_state + check for coupling + check stop
          * condition (if task is master) */

         /* pre-allocate memory */
         state = (T_state *) malloc (sizeof (T_state));
         state->queues = (int **) malloc (sizeof (int *));
         state->nb_vectors = 1;
         state->splitting = 0;
         state->queues[0] = (int *) malloc (nb_queues * sizeof (int));
         new_sub_traject =
            (int **) malloc ((this_interval->length + 1) * sizeof (int *));

         for (i = 0; i < this_interval->length + 1; i++) {
            new_sub_traject[i] = (int *) malloc (nb_queues * sizeof (int));
            memset (new_sub_traject[i], -1, nb_queues * sizeof (int));
         }

         /* set begin_state for new_sub_traject and state */
         memcpy (state->queues[0], task->data.begin_state,
                 nb_queues * sizeof (int));
         memcpy (new_sub_traject[0], task->data.begin_state,
                 nb_queues * sizeof (int));

         /* calculate the sub-trajectory (and check for coupling) */
         for (i = 0; i < this_interval->length; i++) {

            /* find event at step i of sub-trajectory */
            event = model->events->evts + this_interval->events_list[i];

            /* TRANSITION */
            state =
               (((T_ptr_transfct) event->ptr_transition)) (state, event, model);

            /* count number of transition calls */
            if (task->data.is_master) {
#pragma omp critical
               {
                  cpt_transition_total++;
                  cpt_transition_master++;
               }
            }
            else {
#pragma omp atomic
               cpt_transition_total++;
            }

            /* check for coupling after each transition (realtime) */
            for (i_traject = 0;
                 i_traject < NB_SAVED_TRAJECTORY_IN_INTERVAL; i_traject++) {

               if (equal (state->queues[0], this_interval->saved_trajectories[i_traject][i + 1], nb_queues)) { /* coupling 
                                                                                                                * found 
                                                                                                                * ! 
                                                                                                                */
                  /* copy the rest of sub-trajectory that have been caculated
                   * into new_sub_traject */
                  for (j = i + 1; j <= this_interval->length; j++) {
                     memcpy (new_sub_traject[j],
                             this_interval->saved_trajectories[i_traject][j],
                             nb_queues * sizeof (int));

                     /* stop condition is true */
                     if ((task->data.is_master
                          && (((T_ptr_stopfct) method->StopFct->ptr)
                              (new_sub_traject[j])))) {
#ifdef PRINT_OUT_TRAJECTORY
                        valid_length = j + 1;
#endif
#pragma omp atomic
                        finished++;
                        break;  /* stop copying trajectory because stop
                                 * condition is true in task master */
                     }
                  }
                  /* stop searching because coupling found */
                  break;
               }
            }

            /* coupling found ! */
            if (i_traject < NB_SAVED_TRAJECTORY_IN_INTERVAL)
               break;           /* break followed by break to quit nested for
                                 * loops */
            else {
               /* arriving here means there was no coupling. Keep saving state
                * into new_sub_traject and repeat transition call on state */
               memcpy (new_sub_traject[i + 1], state->queues[0],
                       nb_queues * sizeof (int));
               if ((task->data.is_master
                    && (((T_ptr_stopfct) method->StopFct->ptr)
                        (state->queues[0])))) { /* stop condition is true */
#ifdef PRINT_OUT_TRAJECTORY
                  valid_length = i + 2;
#endif
#pragma omp atomic
                  finished++;
                  break;        /* stop condition is true */
               }
            }
         }

         /* save the new_sub_traject into saved_trajectories */
         this_interval->last_saved_trajectory =
            (this_interval->last_saved_trajectory + 1) %
            NB_SAVED_TRAJECTORY_IN_INTERVAL;

         for (i = 0; i < this_interval->length + 1; i++) {
            memcpy (this_interval->
                    saved_trajectories[this_interval->last_saved_trajectory][i],
                    new_sub_traject[i], nb_queues * sizeof (int));
         }

         /* find the next interval to create new task: */
         next_interval = (task->data.id_interval + 1) < method->Granularity
            ? (task->data.id_interval + 1)
            : NO_MORE_INTERVAL;

         if (next_interval == NO_MORE_INTERVAL) {
            if (task->data.is_master) {
#pragma omp atomic
               finished++;
            }
         }
         else {
            /* Create new task for the next interval as master */
            begin_state = (int *) malloc (nb_queues * sizeof (int));
            /* last_state of interval n becomes begin_state of interval n+1 */
            memcpy (begin_state,
                    new_sub_traject[this_interval->length],
                    nb_queues * sizeof (int));
#pragma omp critical
            push_task (create_task (next_interval,
                                    begin_state,
                                    task->data.is_master,
                                    task->data.priority + 1));
         }

         /* If interval is master, validate the sub-trajectory */
         if (task->data.is_master) {
#ifdef PRINT_OUT_TRAJECTORY
#pragma omp critical
            {
               /* user-defined output function call */
               ((T_ptr_outputfct) (method->OutputFct->ptr))
                  (stream_out,
                   new_sub_traject,
                   this_interval->events_list,
                   this_interval->begin, valid_length);
            }
#endif
         }
         /* free what have been allocated for sub-trajactory calculating */
         free (state->queues[0]);
         free (state->queues);
         free (state);

         for (i = 0; i < this_interval->length + 1; i++) {
            free (new_sub_traject[i]);
         }
         free (new_sub_traject);

      }                         /* end if(skip) */

#ifdef _OPENMP
      /* release lock */
      omp_unset_lock (intervals_lock + this_interval->id_interval);
#endif

      /* free the task */
      free (task->data.begin_state);
      free (task);

      /* increment number of tasks done */
#pragma omp atomic
      cpt_tasks_done++;
#ifdef _OPENMP
   }                            /* end of task */
#endif
}

/*****************************************************************************/

/**
 * \brief      Procedure to initialize the kernel from a model
 * \details    Initialize global variables from the model
 * \param      model    system model
 * \return     void
 */
void
init_kernel (st_model_ptr model, st_simpleForwardParallel_ptr method)
{
   // initialize queues & events from model
   // queues:
   capmins = model->queues->mins;
   capmaxs = model->queues->maxs;
   nb_queues = model->queues->nb_queues;

   FLAG = ALGO_NONMONOTONE;
   begin_state = method->InitialState;
}

/**
 * \brief      Procedure to free allocated parameters
 * \return     void
 */
void
free_method (st_method_ptr method_ptr)
{
   st_simpleForwardParallel_ptr method;

   if (method_ptr) {
      method = method_ptr;
   }
   else
      return;

   if (method->InitialState != NULL) {
      free (method->InitialState);
   }
   if (method->StopFct != NULL) {
      free (method->StopFct);
   }
   if (method->TaskPopFct != NULL) {
      free (method->TaskPopFct);
   }
   if (method->OutputFct != NULL) {
      free (method->OutputFct);
   }
   free (method);
}

/**
 * \brief      Procedure to print method parameters to the outout file
 * \param      output_file    file descriptor to the output file
 * \param      method_ptr     method
 * \return     void
 */
void
print_method (FILE * output_file, st_method_ptr method_ptr)
{
   st_simpleForwardParallel_ptr method =
      (st_simpleForwardParallel_ptr) method_ptr;

   fprintf (output_file, "# Method: simpleforwardparallel\n");
   fprintf (output_file, "#\tTrajectoryLength: %d\n", method->TrajectoryLength);
   fprintf (output_file, "#\tGranularity: %d\n", method->Granularity);
   fprintf (output_file, "#\tNumThreads: %d\n", method->NumThreads);
   fprintf (output_file, "#\tCouplingMode: %d\n", method->CouplingMode);
   fprintf (output_file, "#\tMaxTime: %f\n", method->MaxTime);

   int elem_vector = 0;
   fprintf (output_file, "#\tSeed: [%lu", method->Seed[elem_vector]);
   for (elem_vector = 1; elem_vector < SEED_VECTOR_SIZE; elem_vector++) {
      fprintf (output_file, ", %lu", method->Seed[elem_vector]);
   }
   fprintf (output_file, "]\n");

   fprintf (output_file, "#\tInitialState: [");
   print_list_int (output_file, method->InitialState,
                   method->InitialState_size);
   fprintf (output_file, "]\n");
}

/**
 * \brief      Procedure to init the method structure
 * \return     a pointer to a method structure
 *             NULL if failed
 */
st_simpleForwardParallel_ptr
init_method ()
{
   st_simpleForwardParallel_ptr method = (st_simpleForwardParallel_ptr)
      calloc (1, sizeof (st_simpleForwardParallel));
   if (method == NULL)
      return NULL;

   method->StopFct = (st_function_ptr) calloc (1, sizeof (st_function));
   if (method->StopFct == NULL)
      return NULL;

   method->TaskPopFct = (st_function_ptr) calloc (1, sizeof (st_function));
   if (method->TaskPopFct == NULL)
      return NULL;

   method->OutputFct = (st_function_ptr) calloc (1, sizeof (st_function));
   if (method->OutputFct == NULL)
      return NULL;

   method->TrajectoryLength = 0;
   method->MaxTime = -1;
   method->InitialState = NULL;
   method->InitialState_size = 0;
   method->Granularity = 0;
   method->CouplingMode = 0;
   method->NumThreads = 0;
   method->output_threshold = 0;
   method->cpt_transitions = 0;
   method->Seed = NULL;
   method->Seed_size = 0;
   method->UserSeed = 0;

   return method;
}

/**
 * \brief      Function to read an initialize method simulation parameters
 * \param      method_param_cfgfile    method config file name
 * \param      general                 pointer to the general structure
 * \param      model                   pointer to the model structure
 * \return     a pointer to a initialized method structure
 *             NULL if failed
 */
st_simpleForwardParallel_ptr
read_method (char *method_param_cfgfile,
             st_general_ptr general, st_model_ptr model)
{
   int err = 0;
   char *temp;
   FILE *method_param_fd;

#if (DEBUG_LVL >= DEBUG_IO)
   fprintf (stderr, "Opening method parameters file: %s\n",
            method_param_cfgfile);
#endif
   method_param_fd = fopen (method_param_cfgfile, "r");
   if (method_param_fd == NULL) {
      fprintf (stderr, "Fail to open file %s\n", method_param_cfgfile);
      return NULL;
   }

   st_simpleForwardParallel_ptr method = init_method ();

   /**
    * Get TrajectoryLength
    */
   method->TrajectoryLength =
      get_int_keyname (method_param_fd, "TrajectoryLength");
   if (method->TrajectoryLength <= 0) {
      fprintf (stderr, "TrajectoryLength must be positive");
      err++;
   }
#if (DEBUG_LVL >= DEBUG_INFO)
   fprintf (stderr, "TrajectoryLength: %d\n", method->TrajectoryLength);
#endif

   /**
    * Get Seed: to reprocude a trajectory (not the same seed as the general
    * one)
    */
   get_unsigned_long_list_keyname (method_param_fd,
                                   "Seed",
                                   &(method->Seed), SEED_VECTOR_SIZE);

   if (method->Seed != NULL) {
      if (method->Seed_size != SEED_VECTOR_SIZE) {
         PSI3_ERROR ("Seed: incorrect size: [%d] (should be %d)",
                     method->Seed_size, SEED_VECTOR_SIZE);
      }
      else {
         method->UserSeed = 1;
         PSI3_DEBUG ("Seed: [%lu, %lu, %lu, %lu, %lu, %lu]",
                     method->Seed[0],
                     method->Seed[1],
                     method->Seed[2],
                     method->Seed[3], method->Seed[4], method->Seed[5]);
      }
   }
   else {
      method->UserSeed = 0;
      PSI3_INFO ("Seed: no user seed for trajectory");
   }

   /**
    * Get MaxTime
    */
   method->MaxTime = get_double_keyname (method_param_fd, "MaxTime");

   if (method->MaxTime > 0) {
      PSI3_DEBUG ("MaxTime: [%f]", method->MaxTime);
   }
   else {
      method->MaxTime = INFINITY;
      PSI3_INFO ("MaxTime: automatic value [%f] (should be >= 0)", method->MaxTime);
   }

   /**
    * Get initial state(s)
    */
   get_int_list_keyname (method_param_fd,
                         "InitialState",
                         &(method->InitialState), &(method->InitialState_size));

   if (method->InitialState != NULL) { // when user enter an <int> list

      if (method->InitialState_size != model->queues->nb_queues) {
         /* check the size consistency between this list and number of queues */
         fprintf (stderr,
                  " /!\\ : Number of initial states doesn't match with number of "
                  "queues : begin_size = %d - nb_queues = %d\n",
                  method->InitialState_size, model->queues->nb_queues);
         // Missmatch : clear what have been created in this function:
         free (method->InitialState);
         err++;
      }
   }
   else {
      method->InitialState = random_int_list (model->queues->nb_queues,
                                              model->queues->maxs,
                                              model->queues->mins,
                                              general->seed);
      method->InitialState_size = model->queues->nb_queues;
   }

#if (DEBUG_LVL >= DEBUG_INFO)
   int i = 0;
   fprintf (stderr, "InitialState: [");
   for (; i < model->queues->nb_queues;
        fprintf (stderr, " %d", method->InitialState[i++]));
   fprintf (stderr, "]\n");
#endif

   /**
    * Get Granularity
    */
   method->Granularity = get_int_keyname (method_param_fd, "Granularity");
   if (method->Granularity <= 0) {
      fprintf (stderr, "Granularity must be positive\n");
      err++;
   }
#if (DEBUG_LVL >= DEBUG_INFO)
   fprintf (stderr, "Granularity: %d\n", method->Granularity);
#endif

   /**
    * Get CouplingMode: 'BeginState' or in 'Realtime'
    */
   temp = get_string_keyname (method_param_fd, "CouplingMode");
   if (temp != NULL) {
      if (strcmp (temp, "BeginState") == 0) {
         method->CouplingMode = CHECK_COUPLING_AT_BEGIN_STATE;
#if (DEBUG_LVL >= DEBUG_INFO)
         fprintf (stderr, "CouplingMode: %s\n", temp);
#endif
      }
      else if (!strcmp (temp, "Realtime")) {
         method->CouplingMode = CHECK_COUPLING_IN_REALTIME;
#if (DEBUG_LVL >= DEBUG_INFO)
         fprintf (stderr, "CouplingMode: %s\n", temp);
#endif
      }
      else {
         fprintf (stderr,
                  " /!\\ : CouplingMode unrecognized. Possible modes are"
                  " 'BeginState' or 'Realtime'\n");
         err++;
      }
      free (temp);
   }
   else {
      fprintf (stderr, "CouplingMode must be set\n");
      err++;
   }

   /**
    * Get NumThreads
    */
   method->NumThreads = get_int_keyname (method_param_fd, "NumThreads");
   if (method->NumThreads <= 0) {
      fprintf (stderr, "NumThreads must be set to a number > 0\n");
      err++;
   }
   else {
#if (DEBUG_LVL >= DEBUG_INFO)
      fprintf (stderr, "NumThreads: %d\n", method->NumThreads);
#endif
   }

   /**
    * Get Stop Function
    */
   method->StopFct->name = get_string_keyname (method_param_fd, "StopFct");
   if (method->StopFct->name == NULL) {
      method->StopFct->name = "Default$stop";
#if (DEBUG_LVL >= DEBUG_INFO)
      fprintf (stderr, "StopFct: automatic default value (%s)\n",
               method->StopFct->name);
#endif
   }
   method->StopFct->ptr = (T_ptr_stopfct) get_ptr_fct (general->default_lib,
                                                       general->user_lib,
                                                       method->StopFct->name);
   if (method->StopFct->ptr == NULL) {
      err++;
   }
#if (DEBUG_LVL >= DEBUG_INFO)
   fprintf (stderr, "StopFct: %s (%p)\n", method->StopFct->name,
            method->StopFct->ptr);
#endif

   /**
    * Get TaskPopFct
    */
   method->TaskPopFct->name =
      get_string_keyname (method_param_fd, "TaskPopFct");
   if (method->TaskPopFct->name == NULL) {
      method->TaskPopFct->name = "Default$pop_task";
#if (DEBUG_LVL >= DEBUG_INFO)
      fprintf (stderr, "TaskPopFct: automatic default value (%s)\n",
               method->TaskPopFct->name);
#endif
   }
   method->TaskPopFct->ptr = (T_ptr_taskpopfct)
      get_ptr_fct (general->default_lib,
                   general->user_lib, method->TaskPopFct->name);
   if (method->TaskPopFct->ptr == NULL) {
      err++;
   }
#if (DEBUG_LVL >= DEBUG_INFO)
   fprintf (stderr, "TaskPopFct: %s (%p)\n",
            method->TaskPopFct->name, method->TaskPopFct->ptr);
#endif

   /**
    * Get Output Function
    */
   method->OutputFct->name = get_string_keyname (method_param_fd, "OutputFct");
   if (method->OutputFct->name == NULL) {
      method->OutputFct->name = "Default$output_SFP";
#if (DEBUG_LVL >= DEBUG_INFO)
      fprintf (stderr, "OutputFct: automatic default value (%s)\n",
               method->OutputFct->name);
#endif
   }
   method->OutputFct->ptr = (T_ptr_stopfct) get_ptr_fct (general->default_lib,
                                                         general->user_lib,
                                                         method->OutputFct->
                                                         name);
   if (method->OutputFct->ptr == NULL) {
      err++;
   }
#if (DEBUG_LVL >= DEBUG_INFO)
   fprintf (stderr, "OutputFct: %s (%p)\n",
            method->OutputFct->name, method->OutputFct->ptr);
#endif

   fclose (method_param_fd);

   if (err) {
      free_method (method);
      return NULL;
   }

   return method;
}

/**
 *  Return a integer vector representing a task's begin state depending on beginState_mode
 */
int *
generate_beginState (int beginState_mode)
{
   int *task_begin_state = NULL;
   if (beginState_mode == GET_INITIAL_BEGIN_STATE) {  // return a copy of
                                                      // global begin_state
      task_begin_state = (int *) malloc (nb_queues * sizeof (int));
      memcpy (task_begin_state, begin_state, nb_queues * sizeof (int));
   }
   else {
      if (beginState_mode == GET_RANDOM_BEGIN_STATE) {
         task_begin_state = random_int_list_NEW (nb_queues,
                                                 capmaxs,
                                                 capmins);
      }
      else {
         // never reach here
      }
   }
   return task_begin_state;
}

/**
 * \brief      Procedure to run the simulation
 * \details    Run simulation and store updated global variables (e.g state,
 *             trajectory etc)
 * \param stream_out               stream_out for output data
 * \param beginState_mode          mode for beginstate generation (cf in #define ...)
 * \param pop_method               mode for task popping          (cf in #define ...)
 * \return   void.
 */
void simulation
   (st_general_ptr general, int beginState_mode,
    st_simpleForwardParallel_ptr method, st_model_ptr model,
    st_simulation_ptr *simulationPtr)
{
   T_task *next_task;
   int i;
   struct timeval ti, tf;
   FILE *stream_out = general->output_file;

   st_simulation_ptr simulation;
   simulation = malloc (sizeof (st_simulation));
   simulation->finished = 0;
   *simulationPtr = simulation;

   if (method->UserSeed) {      /* if user has given a Seed to reproduce a
                                 * trajectory */
      generator_set_state (0, method->Seed);
   }
   else {
      method->Seed = calloc (SEED_VECTOR_SIZE, sizeof (unsigned long));
      generator_get_state (0, method->Seed);
   }

   /* Compute continuous time -------------------------------------------- */
   st_timing_ptr timing;
   timing = psi3_timing_init (0, NULL, model);

   for (i = 0; i < method->TrajectoryLength; i++) {

      psi3_timing_next (timing);

      if (timing->value >= method->MaxTime
            && ((i + 1) <= method->TrajectoryLength)) {

         method->TrajectoryLength = i + 1;
         PSI3_INFO ("Update TrajectoryLength due to MaxTime constraint");
         PSI3_INFO ("TrajectoryLength: [%d]", method->TrajectoryLength);
         break;
      }
   }

   PSI3_DEBUG ("trajectory time: [%f]", timing->value);
   psi3_timing_close (timing);

   print_header (general->output_file, general, model, method, print_method);

   /* Initialization of the simulation */
   intervals =
      (T_interval *) malloc (method->Granularity * sizeof (T_interval));
   init_intervals (method->TrajectoryLength, method->Granularity,
                   model->events->nb_evts);

#pragma omp parallel num_threads(method->NumThreads)
   {
#pragma omp master
      {
#pragma omp task
         {
            taskpool = (T_task_pool *) calloc (1, sizeof (T_task_pool));
            init_task_pool ();

            PSI3_DEBUG ("Task pool creation");

            /* begin pushing tasks into taskpool after initialization: first
             * task on interval 0 [MASTER] with the global (initial)
             * begin_state and priority 1 */
            push_task (create_task (0,
                                    generate_beginState
                                    (GET_INITIAL_BEGIN_STATE), MASTER, 1));

            /* and other tasks [NO_MASTER] of other intervals with random
             * begin_state and priority 0 */
            for (i = 1; i < method->Granularity; i++) {
               push_task (create_task (i, generate_beginState (beginState_mode),
                                       NO_MASTER, 0));
            }

            PSI3_DEBUG ("[%d] Tasks created", method->Granularity);
         }
#pragma omp task
         {
            finished = 0;
            cpt_transition_master = 0;
            cpt_transition_total = 0;
            cpt_tasks_done = 0;
            simulation_time = 0.0;
         }
      }
   }
   /* End of initialization */

   /* Begin simulation : */
   gettimeofday (&ti, NULL);

   while (!finished) {
#pragma omp parallel num_threads(method->NumThreads) private(next_task)
      {
#pragma omp critical
         {
            /* Pop a task from taskpool */
            next_task = ((T_ptr_taskpopfct) method->TaskPopFct->ptr) (taskpool);
         }

         if (next_task != NULL) {
            if (method->CouplingMode == CHECK_COUPLING_AT_BEGIN_STATE) {
#pragma omp task
               task_OpenMP_check_coupling_by_beginState (next_task,
                                                         stream_out,
                                                         method, model);
            }
            else {              /* CHECK_COUPLING_IN_REALTIME */
#pragma omp task
               task_OpenMP_check_coupling_realtime (next_task,
                                                    stream_out, method, model);
            }
         }
      }
   }

   gettimeofday (&tf, NULL);
   /* End of simulation */

   simulation_time += delta_ms (ti, tf);

   simulation->time = simulation_time;

   /* Free taskpool & intervals */
   free_taskpool ();
   free_intervals (method->Granularity);

   fprintf (stream_out,
            "\n# Number total of transition calls : %d\n"
            "# Number of Master transitions calls : %d\n",
            cpt_transition_total, cpt_transition_master);

   fprintf (stderr, "Number total of transition calls: %d\n"
            "Number of Master transition calls : %d\n",
            cpt_transition_total, cpt_transition_master);
}



/**
 * \brief       Main 
 * \details     - Load libraries
 *		- Parse config files
 * 		- Initialize data structures
 * 		- Run simulation & write into output file
 * \return   0.
 */
int
main (int argc, char **argv)
{
   st_model_ptr model = NULL;
   st_simpleForwardParallel_ptr method = NULL;
   st_general_ptr general = NULL;
   st_simulation_ptr simulationInfo = NULL;
   int rc;

   rc = read_configuration (&general,
                            argv[2],
                            &model,
                            argv[1],
                            (void **) &method, argv[3], (void *) read_method);

   if (rc) {                    /* if reading configuration failed */
      close_psi (general, model, method, free_method);
      return 1;
   }

   /**
    * initialize kernel from the model
    */
   init_kernel (model, method);

   /**
    * initialize the random event generator :
    */
   init_generator (model->events->nb_evts, model->events->evts);

   /**
    * Run the simulation and write the trajectories into output file:
    */
   simulation (general, GET_RANDOM_BEGIN_STATE, method, model, &simulationInfo);

   print_footer (general, model, method, simulationInfo->time);

   print_time (stderr, simulation_time);

   print_perf (stderr, general, simulationInfo);

   /**
    * clean up the system:
    */
   return close_psi (general, model, method, free_method);
}
