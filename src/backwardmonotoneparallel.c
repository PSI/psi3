/**
 * \file          backwardmonotone.c
 * \author        Florian LEVEQUE
 * \author        Jean-Marc.Vincent@imag.fr
 * \author        Benjamin.Briot@inria.fr
 * \version       1.3.0
 * \date          10/04/2015
 * \brief         Backward Monotone Kernel.
 */

 /* 
  * This library is free software; you can redistribute it and/or
  * modify it under the terms of the GNU Lesser General Public
  * License as published by the Free Software Foundation; either
  * version 2.1 of the License, or any later version.
  * 
  * This library is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  * Lesser General Public License for more details.
  * 
  * You should have received a copy of the GNU Lesser General Public
  * License along with this library; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
  */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <dlfcn.h>
#include <generator.h>
#include <parser.h>
#include "psi_common.h"

#include <omp.h>

#define ALLOCATION 1000

/*****************************************************************************/
/*             Global variables used by transition or simulation             */

/* TRANSITION ---------- */
/* Model */
/* - Queues: */
int *capmins;                   // capacity min of queues
int *capmaxs;                   // capacity max of queues 
int nb_queues;                  // number of queues 

/* Load sharing */
int ls_mode;
int **node_dist;                /* [LOAD SHARING] matrix of distance between
                                 * two queues : node_dist[i][j] = distance
                                 * between i and j */
struct st_neighbors *node_neighbors;   /* [LOAD SHARING] array containing for
                                        * each node, the neighbors of the node */

/* SIMULATION ---------- */
double simulation_time;
static __thread int **reward;                   // reward table
int nb_sample;                  // numbers of samples FIXME
int nb_AV;                      // number of anthitetic value
int FLAG;

/*****************************************************************************/

/**
 * \brief       Procedure to initialize the kernel from a model
 * \details	Initialize global variables from the model
 * \param    model         model of system.
 * \return   void.
 */
void
init_kernel (st_model_ptr model)
{
   /**
    * initialize the random event generator :
    */
   init_generator (model->events->nb_evts, model->events->evts);

   capmins = model->queues->mins;
   capmaxs = model->queues->maxs;
   nb_queues = model->queues->nb_queues;
   FLAG = ALGO_MONOTONE;
}

/**
 * \brief      Procedure to free allocated parameters
 * \return     void
 */
void
free_method (st_method_ptr method_ptr)
{
   st_backwardMonotoneParallel_ptr method = method_ptr;

   if (method_ptr) {
      method = method_ptr;
   }
   else return;

   if (method->RewardFct != NULL) {
      free (method->RewardFct);
   }
   if (method->OutputFct != NULL) {
      free (method->OutputFct);
   }
   free (method);
}

/**
 * \brief       Procedure to free allocated parameters
 * \return   void.
 */
void
free_param ()
{
   int i;
   if (reward != NULL) {
      for (i = 0; i < nb_AV; i++) {
         free (reward[i]);
      }
      free (reward);
   }
   if (node_dist != NULL) {
      for (i = 0; i < nb_queues; i++)
         free (node_dist[i]);
      free (node_dist);
   }
   if (node_neighbors != NULL) {
      for (i = 0; i < nb_queues; i++) {
         free (node_neighbors[i].neighbors);
      }
      free (node_neighbors);
   }
}

/**
 * \brief      Procedure to init the method structure
 * \return     a pointer to a method structure
 *             NULL if failed
 */
st_backwardMonotoneParallel_ptr
init_method ()
{
   st_backwardMonotoneParallel_ptr method = (st_backwardMonotoneParallel_ptr)
      calloc (1, sizeof (st_backwardMonotoneParallel));
   if (method == NULL)
      return NULL;
   method->RewardFct = (st_function_ptr) calloc (1, sizeof (st_function));
   if (method->RewardFct == NULL)
      return NULL;
   method->OutputFct = (st_function_ptr) calloc (1, sizeof (st_function));
   if (method->OutputFct == NULL)
      return NULL;

   method->SampleNumber = 1;
   method->TrajectoryMax = 0;
   method->Antithetic = 0;
   method->Doubling = 0;
   method->InitMemoryLength = 0;

   return method;
}

/**
 * \brief      Procedure to print method parameters to the outout file
 * \param      output_file    file descriptor to the output file
 * \param      method_ptr     method
 * \return     void
 */
void
print_method (FILE * output_file, st_method_ptr method_ptr)
{
   st_backwardMonotoneParallel_ptr method = (st_backwardMonotoneParallel_ptr) method_ptr;

   fprintf (output_file, "# Method: backwardmonotoneparallel\n");
   fprintf (output_file, "#\tSampleNumber: %d\n", method->SampleNumber);
   fprintf (output_file, "#\tNumThreads: %d\n", method->NumThreads);
   fprintf (output_file, "#\tTrajectoryMax: %d\n", method->TrajectoryMax);
   fprintf (output_file, "#\tAntithetic: %d\n", method->Antithetic);
   fprintf (output_file, "#\tDoubling: %d\n", method->Doubling);
   fprintf (output_file, "#\tInitMemoryLength: %d\n", method->InitMemoryLength);
   fprintf (output_file, "#\tCouplingfct: %s\n", method->RewardFct->name);
   fprintf (output_file, "#\tOutputFct: %s\n", method->OutputFct->name);
}

/**
 * \brief      Function to read an initialize method simulation parameters
 * \param      method_param_cfgfile    method config file name
 * \param      general                 pointer to the general structure
 * \param      model                   pointer to the model structure
 * \return     a pointer to a initialized method structure
 *             NULL if failed
 */
st_backwardMonotoneParallel_ptr
read_method (char *method_param_cfgfile,
             st_general_ptr general, st_model_ptr model)
{
   int err = 0;
   FILE *method_param_fd;
   char *loadsharing_file_name;
   FILE *loadsharing_file;

   PSI3_DEBUG ("Opening method parameters file: %s", method_param_cfgfile);
   method_param_fd = fopen (method_param_cfgfile, "r");
   if (method_param_fd == NULL) {
      PSI3_ERROR ("Fail to open file %s", method_param_cfgfile);
      return NULL;
   }

   st_backwardMonotoneParallel_ptr method = init_method ();

   /**
    * Get samplenumber
    */
   method->SampleNumber = get_int_keyname (method_param_fd, "SampleNumber");
   if (method->SampleNumber <= 0) {
      PSI3_ERROR ("Number of sample must be positive");
      err++;
   }
   PSI3_INFO ("SampleNumber: %d", method->SampleNumber);
   nb_sample = method->SampleNumber;   /* FIXME */
 
   /**
    * Get NumThreads
    */
   method->NumThreads = get_int_keyname (method_param_fd, "NumThreads");
   if (method->NumThreads <= 0
         || method->NumThreads == INT_MIN) { /* we let OMP decide: we get the
                                              * OMP number of threads in
                                              * parallel region (otherwise this
                                              * number is 1) */
#pragma omp parallel
      {
         method->NumThreads = omp_get_num_threads ();
      }
      PSI3_INFO ("NumThreads: automatic value [%d]", method->NumThreads);
   }
   else {
      PSI3_INFO ("NumThreads: %d", method->NumThreads);
   }

   /**
    * Get InitMemoryLength: the initial allocated memory
    */
   method->InitMemoryLength =
      get_int_keyname (method_param_fd, "InitMemoryLength");
   if (method->InitMemoryLength <= 0) {
      PSI3_DEBUG ("InitMemoryLength: automatic default value (%d)", ALLOCATION);
      method->InitMemoryLength = ALLOCATION;
   }
   PSI3_DEBUG ("InitMemoryLength: %d", method->InitMemoryLength);

   /**
    * Get Antithetic: the number of anthitetic variables
    */
   method->Antithetic = get_int_keyname (method_param_fd, "Antithetic");
   if (method->Antithetic <= 0) {
      PSI3_ERROR ("Number of antithetic variables must be positive");
      err++;
   }
   PSI3_INFO ("Antithetic: %d", method->Antithetic);

   nb_AV = method->Antithetic;  /* FIXME */

   /**
    * Get TrajectoryMax
    */
   method->TrajectoryMax = get_int_keyname (method_param_fd, "TrajectoryMax");
   if (method->TrajectoryMax <= 0) {
      PSI3_ERROR ("TrajectoryMax must be positive\n");
      err++;
   }
   else {
      PSI3_INFO ("TrajectoryMax: %d", method->TrajectoryMax);
   }

   /**
    * Get Doubling
    */
   method->Doubling = get_bool_keyname (method_param_fd, "Doubling");
   if (method->Doubling != 0) { /* Yes by default */
      method->Doubling = 1;
   }
   PSI3_INFO ("Doubling: %d", method->Doubling);

   /**
    * Get reward function
    */
   method->RewardFct->name = get_string_keyname (method_param_fd, "CouplingFct");
   if (method->RewardFct->name == NULL) {
      method->RewardFct->name = "Default$coupling";
      PSI3_INFO ("CouplingFct: automatic default value (%s)",
                 method->RewardFct->name);
   }
   method->RewardFct->ptr = (T_ptr_rewardfct)
      get_ptr_fct (general->default_lib,
                   general->user_lib, method->RewardFct->name);
   if (method->RewardFct->ptr == NULL) {
      err++;
   }
   PSI3_DEBUG ("CouplingFct: %s (%p)",
              method->RewardFct->name, method->RewardFct->ptr);

   /**
    * Get Output Function
    */
   method->OutputFct->name = get_string_keyname (method_param_fd, "OutputFct");
   if (method->OutputFct->name == NULL) {
      method->OutputFct->name = "Default$output_B";
      PSI3_INFO ("OutputFct: automatic default value (%s)",
                 method->OutputFct->name);
   }
   method->OutputFct->ptr = (T_ptr_stopfct) get_ptr_fct (general->default_lib,
                                                         general->user_lib,
                                                         method->OutputFct->
                                                         name);
   if (method->OutputFct->ptr == NULL) {
      err++;
   }
   PSI3_DEBUG ("OutputFct: %s (%p)",
              method->OutputFct->name, method->OutputFct->ptr);

   /**
    * Get the load sharing
    */
   loadsharing_file_name = get_string_keyname (method_param_fd, "LoadSharing");

   if (loadsharing_file_name != NULL) {
      PSI3_INFO ("LoadSharing: [%s]", loadsharing_file_name);
      loadsharing_file = fopen (loadsharing_file_name, "r");
      if (loadsharing_file == NULL) {
         PSI3_ERROR ("The Load Sharing file does not exist");
         err++;
      }
      else {
         // parser the load sharing file
         node_dist = LS_parser (loadsharing_file,
                                node_dist, model->queues->nb_queues, &ls_mode);
         if (node_dist == NULL) {
            err++;
         }
         else {
            // save the configuration of load sharing
            node_neighbors = node_dist_to_node_neighbors (node_dist,
                                                          node_neighbors,
                                                          model->queues->
                                                          nb_queues);
            PSI3_INFO ("Load Sharing: nb_queues: %d", model->queues->nb_queues);
         }
         fclose (loadsharing_file);
      }
   }
   else {
      // load sharing is not used
      node_dist = NULL;
      node_neighbors = NULL;
      PSI3_INFO ("Load Sharing is not used");
   }

   fclose (method_param_fd);

   if (err) {
      free_method (method);
      return NULL;
   }

   return method;
}

int
simulation_simple (st_general_ptr general,
                   st_backwardMonotoneParallel_ptr method,
                   st_model_ptr model)
{
   int sample;             /* current number of sample */
   struct timeval ti, tf;  /* save time a the beginning and at the end of the
                            * simulation */

   /* current allocated memory */
   int current_length_memory = method->InitMemoryLength;

   T_state *states = NULL;        /* States of the system */
   T_event *event = NULL;         /* current event of the simulation */

   int **Tab_VA = NULL;
   double *stat = NULL;
   int *trajectory = NULL;        /* back up of the previous event */

   int **log2stop_time = NULL;    /* save number of iteration to finish the
                                   * simulation for each anthitetic variable */

   int ***state_saved = NULL;     /* different queues states when coupling for
                                   * each anthitetic variable */

   int sampleComputed = 0;        /* number of sample computed during the
                                   * simulation */
   int n = 0, i = 0;

   st_simulation_ptr simulation;
   simulation = malloc (sizeof (st_simulation));
   simulation->finished = 0;

   /* results are stored in this table, and outputed at the end */
   state_saved = (int ***) calloc (method->SampleNumber, sizeof (int **));
   for (i = 0; i < method->SampleNumber; i++) {
      state_saved[i] = (int **) calloc (nb_AV, sizeof (int *));
      for (n = 0; n < nb_AV; n++) {
         state_saved[i][n] = (int *) calloc (nb_queues, sizeof (int));
      }
   }

   /* reward times are stored in this table */
   log2stop_time = (int **) calloc (method->SampleNumber, sizeof (int *));
   for (i = 0; i < method->SampleNumber; i++) {
      log2stop_time[i] = (int *) calloc (nb_AV, sizeof (int));
   }

   omp_set_num_threads (method->NumThreads);

   /* In this model, we use one stream per thread (for now) */
   generator_set_multistream (method->NumThreads);

#pragma omp parallel firstprivate (n, i, current_length_memory, states, event,\
                                   Tab_VA, stat, trajectory, log2stop_time,\
                                   state_saved)
   {
      int threadID = omp_get_thread_num ();

      /* initialize the structure with 2 vectors (lower et upper states) */
      states = (T_state *) malloc (sizeof (T_state));
      states->queues = (int **) malloc (2 * sizeof (int *));
      states->queues[0] = (int *) malloc (nb_queues * sizeof (int));
      states->queues[1] = (int *) malloc (nb_queues * sizeof (int));
      states->nb_vectors = 2;
      states->splitting = 0;

      Tab_VA = (int **) calloc (nb_AV, sizeof (int *));
      for (n = 0; n < nb_AV; n++)
         Tab_VA[n] = (int *) calloc (current_length_memory, sizeof (int));

      stat = (double *) calloc (current_length_memory, sizeof (double));

      trajectory = (int *) malloc (current_length_memory * sizeof (int));

      reward = (int **) calloc (nb_AV, sizeof (int *));
      for (n = 0; n < nb_AV; n++)
         reward[n] = (int *) calloc (nb_queues, sizeof (int));

#pragma omp single
      {
         gettimeofday (&ti, NULL);
      }

#pragma omp for
      for (sample = 0; sample < method->SampleNumber; sample++) {

         double val;
         int id_random_evt;      /* current random event number */

         /* several interator use in simulation */
         int place, i, temp, num, stop_time = 1, indice;

         // re-initialize reward table and reward time for each trajectory
         for (n = 0; n < nb_AV; n++) {
            log2stop_time[sample][n] = 0;
            for (i = 0; i < nb_queues; i++) {
               reward[n][i] = 0;
            }
         }

         stat[0] = u01_p (threadID);

         for (n = 0; n < nb_AV; n++) {
            Tab_VA[n][0] = n;
         }

         for (i = 1; i < nb_AV; i++) {
            place = ((int) (u01_p (threadID) * (nb_AV - 1))) + 1;
            temp = Tab_VA[place][0];
            Tab_VA[place][0] = Tab_VA[i][0];
            Tab_VA[i][0] = temp;
         }

         for (num = 0; num < nb_AV; num++) {
            // x modulo y == x - ( ( (int)x/(int)y ) * y )
            val = (stat[0] + (double) Tab_VA[num][0] / (double) nb_AV)
               - ((int) (stat[0] + (double) Tab_VA[num][0] / (double) nb_AV));

            // Generate a random event
            id_random_evt = genere_evenement (val, model->events->nb_evts);
            // save the random number
            trajectory[0] = id_random_evt;
            stop_time = 1;

            do {
               // re initialize lower and upper state with lower and upper
               // capacity 
               indice = stop_time - 1;
               memcpy (states->queues[0], capmins, sizeof (int) * nb_queues);
               memcpy (states->queues[1], capmaxs, sizeof (int) * nb_queues);

               // check if we have to realloc the memory
               if (current_length_memory <= indice) {
                  current_length_memory = current_length_memory * 2;
                  for (n = 0; n < nb_AV; n++) {
                     Tab_VA[n] = (int *) realloc (Tab_VA[n],
                           current_length_memory *
                           sizeof (int));
                  }
                  stat = (double *) realloc (stat,
                        current_length_memory *
                        sizeof (double));
                  trajectory =
                     (int *) realloc (trajectory,
                           current_length_memory * sizeof (int));
               }

               // Doing permutations for anthitetic variables 
               stat[indice] = u01_p (threadID);

               for (n = 0; n < nb_AV; n++) {
                  Tab_VA[n][indice] = n;
               }
               for (i = 1; i < nb_AV; i++) {
                  place = ((int) (u01_p (threadID) * (nb_AV - 1))) + 1;
                  temp = Tab_VA[place][indice];
                  Tab_VA[place][indice] = Tab_VA[i][indice];
                  Tab_VA[i][indice] = temp;
               }


               val = (stat[indice] + (double) Tab_VA[num][indice] / (double) nb_AV)
                  - ((int) (stat[indice]
                           + (double) Tab_VA[num][indice] / (double) nb_AV));
               // Generate a random event
               id_random_evt = genere_evenement (val, model->events->nb_evts);
               // save the random number
               trajectory[indice] = id_random_evt;
               event = model->events->evts + id_random_evt;
               // e(x+1) = Transition(e(x),event), doing the event 
               states =
                  (*((T_ptr_transfct) event->ptr_transition)) (states, event, model);

               // from indice = stop_time-1 to O we take the same trajectory
               // (previous saved event)
               for (indice = stop_time - 2; indice >= 0; indice--) {
                  // take a previous random event
                  id_random_evt = trajectory[indice];
                  event = model->events->evts + id_random_evt;
                  // e(x+1) = Transition(e(x),event), doing the event 
                  states =
                     (*((T_ptr_transfct) event->ptr_transition)) (states, event, model);
               }
               // Increment the begin time
               stop_time = stop_time + 1;
               log2stop_time[sample][num] += 1;

               // until we are not at the trajectory max and we have not coupling
               // in all queues
            }
            while ((stop_time < method->TrajectoryMax)
                  && !((T_ptr_rewardfct) method->RewardFct->ptr)
                  (states, log2stop_time[sample][num], reward[num], state_saved[sample][num]));

         }

         if (2 * stop_time >= method->TrajectoryMax) {
            log2stop_time[sample][0] = -1;
         }

#pragma omp atomic
         sampleComputed++;

#pragma omp critical
         {
            if (general->output_file != PSI3_DEFAULT_OUTPUT_FD) { /* stdout */
               loadbar (sampleComputed, method->SampleNumber);
            }
         }
      }
#pragma omp barrier
#pragma omp single
      {
         // print the result in output file
         for (i = 0; i < method->SampleNumber; i++) {
            if (log2stop_time[i][0] == -1) { // FIXME
               fprintf (general->output_file, " echec  \n");
            }
            else {
               ((T_ptr_outputfct) (method->OutputFct->ptr))
                  (general->output_file,
                   state_saved[i],
                   i,
                   log2stop_time[i]);
            }
         }

         gettimeofday (&tf, NULL); /* save current time after simulation */

         for (i = 0; i < method->SampleNumber; i++) {
            for (n = 0; n < nb_AV; n++) {
               free (state_saved[i][n]);
            }
            free (state_saved[i]);
         }
         free (state_saved);

         for (i = 0; i < method->SampleNumber; i++) {
            free (log2stop_time[i]);
         }
         free (log2stop_time);
      }

      for (n = 0; n < nb_AV; n++) {
         free (Tab_VA[n]);
      }
      free (Tab_VA);

      for (i = 0; i < states->nb_vectors; i++) {
         free (states->queues[i]);
      }
      free (states->queues);
      free (states);

      free (stat);

      free (trajectory);
   }

   // Calculate simulation time by substracting end time and beginning time 
   simulation_time = delta_ms (ti, tf);

   simulation->time = simulation_time;

   print_footer (general, model, method, simulation->time);

   print_time (stderr, simulation->time);

   print_perf (stderr, general, simulation);

   return 0;
}

/**
 * \brief       Procedure to run the simulation
 * \details      Run simulation and store updated global variables (e.g state, trajectory etc)
 * \return   void.
 */
int
simulation_doubling (st_general_ptr general,
                     st_backwardMonotoneParallel_ptr method,
                     st_model_ptr model)
{
   
   int sample;             /* current number of sample */
   struct timeval ti, tf;  /* save time a the beginning and at the end of the
                            * simulation */

   /* current allocated memory */
   int current_length_memory = method->InitMemoryLength;

   T_state *states = NULL;        /* States of the system */
   T_event *event = NULL;         /* current event of the simulation */

   int **Tab_VA = NULL;
   double *stat = NULL;
   int *trajectory = NULL;        /* back up of the previous event */

   int **log2stop_time = NULL;    /* save number of iteration to finish the
                                   * simulation for each anthitetic variable */

   int ***state_saved = NULL;     /* different queues states when coupling for
                                   * each anthitetic variable */

   int sampleComputed = 0;        /* number of sample computed during the
                                   * simulation */
   int n = 0, i = 0;

   st_simulation_ptr simulation;
   simulation = malloc (sizeof (st_simulation));
   simulation->finished = 0;

   print_header (general->output_file, general, model, method, print_method);

   /* results are stored in this table, and outputed at the end */
   state_saved = (int ***) calloc (method->SampleNumber, sizeof (int **));
   for (i = 0; i < method->SampleNumber; i++) {
      state_saved[i] = (int **) calloc (nb_AV, sizeof (int *));
      for (n = 0; n < nb_AV; n++) {
         state_saved[i][n] = (int *) calloc (nb_queues, sizeof (int));
      }
   }

   /* reward times are stored in this table */
   log2stop_time = (int **) calloc (method->SampleNumber, sizeof (int *));
   for (i = 0; i < method->SampleNumber; i++) {
      log2stop_time[i] = (int *) calloc (nb_AV, sizeof (int));
   }

   omp_set_num_threads (method->NumThreads);

   /* In this model, we use one stream per thread (for now) */
   generator_set_multistream (method->NumThreads);

#pragma omp parallel firstprivate (n, i, current_length_memory, states, event,\
                                   Tab_VA, stat, trajectory, log2stop_time,\
                                   state_saved)
   {
      int threadID = omp_get_thread_num ();

      /* initialize the structure with 2 vectors (lower et upper states) */
      states = (T_state *) malloc (sizeof (T_state));
      states->queues = (int **) malloc (2 * sizeof (int *));
      states->queues[0] = (int *) malloc (nb_queues * sizeof (int));
      states->queues[1] = (int *) malloc (nb_queues * sizeof (int));
      states->nb_vectors = 2;
      states->splitting = 0;

      Tab_VA = (int **) calloc (nb_AV, sizeof (int *));
      for (n = 0; n < nb_AV; n++)
         Tab_VA[n] = (int *) calloc (current_length_memory, sizeof (int));

      stat = (double *) calloc (current_length_memory, sizeof (double));

      trajectory = (int *) malloc (current_length_memory * sizeof (int));

      reward = (int **) calloc (nb_AV, sizeof (int *));
      for (n = 0; n < nb_AV; n++)
         reward[n] = (int *) calloc (nb_queues, sizeof (int));

#pragma omp single
      {
         gettimeofday (&ti, NULL); /* save current time before simulation */
      }

#pragma omp for
      for (sample = 0; sample < method->SampleNumber; sample++) {

         double val;
         int id_random_evt;      /* current random event number */

         /* several interator use in simulation */
         int place, i, temp, num, stop_time, indice, max_temps, indice2;

         // re-initialize reward table and reward time for each trajectory
         for (n = 0; n < nb_AV; n++) {
            log2stop_time[sample][n] = 0;
            for (i = 0; i < nb_queues; i++) {
               reward[n][i] = 0;
            }
         }
         max_temps = 0;
         stop_time = 1;

         stat[0] = u01_p (threadID);
         for (n = 0; n < nb_AV; n++) {
            Tab_VA[n][0] = n;
         }
         for (i = 1; i < nb_AV; i++) {
            place = ((int) (u01_p (threadID) * (nb_AV - 1))) + 1;
            temp = Tab_VA[place][0];
            Tab_VA[place][0] = Tab_VA[i][0];
            Tab_VA[i][0] = temp;
         }

         for (num = 0; num < nb_AV; num++) {
            // x modulo y == x - ( ( (int)x/(int)y ) * y )
            val =
               (stat[0] + (double) Tab_VA[num][0] / (double) nb_AV) -
               ((int) (stat[0] + (double) Tab_VA[num][0] / (double) nb_AV));
            // Generate a random event
            id_random_evt = genere_evenement (val, model->events->nb_evts);
            // save the random number
            trajectory[0] = id_random_evt;
            stop_time = 1;

            do {
               /* re initialize lower and upper state with lower and upper
                * capacity */
               memcpy (states->queues[0], capmins, sizeof (int) * nb_queues);
               memcpy (states->queues[1], capmaxs, sizeof (int) * nb_queues);

               for (indice = 2 * stop_time - 1; indice >= stop_time; indice--) {
                  /* check if we have to realloc the memory */
                  if (current_length_memory < indice) {
                     current_length_memory = current_length_memory * 2;
                     for (n = 0; n < nb_AV; n++)
                        Tab_VA[n] =
                           (int *) realloc (Tab_VA[n],
                                 current_length_memory * sizeof (int));
                     stat =
                        (double *) realloc (stat,
                              current_length_memory *
                              sizeof (double));
                     trajectory =
                        (int *) realloc (trajectory,
                              current_length_memory * sizeof (int));
                  }
                  /* Doing permutations for anthitetic variables */
                  if (max_temps < indice) {
                     for (indice2 = 2 * stop_time - 1; indice2 >= stop_time;
                           indice2--) {
                        stat[indice2] = u01_p (threadID);
                        for (n = 0; n < nb_AV; n++) {
                           Tab_VA[n][indice2] = n;
                        }
                        for (i = 1; i < nb_AV; i++) {
                           place = ((int) (u01_p (threadID) * (nb_AV - 1))) + 1;
                           temp = Tab_VA[place][indice2];
                           Tab_VA[place][indice2] = Tab_VA[i][indice2];
                           Tab_VA[i][indice2] = temp;
                        }
                     }
                     max_temps = indice;
                  }

                  val =
                     (stat[indice] +
                      (double) Tab_VA[num][indice] / (double) nb_AV) -
                     ((int)
                      (stat[indice] +
                       (double) Tab_VA[num][indice] / (double) nb_AV));
                  // Generate a random event
                  id_random_evt = genere_evenement (val, model->events->nb_evts);
                  // save the random number
                  trajectory[indice] = id_random_evt;
                  event = model->events->evts + id_random_evt;
                  // e(x+1) = Transition(e(x),event), doing the event 
                  states =
                     (*((T_ptr_transfct) event->ptr_transition)) (states, event, model);
               }
               // from indice = stop_time-1 to O we take the same trajectory
               // (previous saved event)
               for (indice = stop_time - 1; indice >= 0; indice--) {
                  // take a previous random event
                  id_random_evt = trajectory[indice];
                  event = model->events->evts + id_random_evt;
                  // e(x+1) = Transition(e(x),event), doing the event 
                  states =
                     (*((T_ptr_transfct) event->ptr_transition)) (states, event, model);
               }
               // double the begin time
               stop_time = stop_time << 1;;
               log2stop_time[sample][num] += 1;
               // until we are not at the trajectory max and we have not coupling
               // in all queues
            } while ((2 * stop_time < method->TrajectoryMax) &&
                  !((T_ptr_rewardfct) method->RewardFct->ptr) (states,
                     log2stop_time[sample][num],
                     reward[num],
                     state_saved[sample][num]));
         }

         if (2 * stop_time >= method->TrajectoryMax) {
            log2stop_time[sample][0] = -1;
         }

#pragma omp atomic
            sampleComputed++;

#pragma omp critical
         {
            if (general->output_file != PSI3_DEFAULT_OUTPUT_FD) { /* stdout */
               loadbar (sampleComputed, method->SampleNumber);
            }
         }
      }
#pragma omp barrier
#pragma omp single
      {
         // print the result in output file
         for (i = 0; i < method->SampleNumber; i++) {
            if (log2stop_time[i][0] == -1) { // FIXME
               fprintf (general->output_file, " echec  \n");
            }
            else {
               ((T_ptr_outputfct) (method->OutputFct->ptr))
                  (general->output_file,
                   state_saved[i],
                   i,
                   log2stop_time[i]);
            }
         }

         gettimeofday (&tf, NULL); /* save current time after simulation */

         for (i = 0; i < method->SampleNumber; i++) {
            for (n = 0; n < nb_AV; n++) {
               free (state_saved[i][n]);
            }
            free (state_saved[i]);
         }
         free (state_saved);

         for (i = 0; i < method->SampleNumber; i++) {
            free (log2stop_time[i]);
         }
         free (log2stop_time);
      }

      for (n = 0; n < nb_AV; n++) {
         free (Tab_VA[n]);
      }
      free (Tab_VA);

      for (i = 0; i < states->nb_vectors; i++) {
         free (states->queues[i]);
      }
      free (states->queues);
      free (states);

      free (stat);

      free (trajectory);
   }

   /* Calculate simulation time by substracting end time and beginning time */
   simulation_time = delta_ms (ti, tf);

   simulation->time = simulation_time;

   print_footer (general, model, method, simulation->time);

   print_time (stderr, simulation->time);

   print_perf (stderr, general, simulation);

   return 0;
}

/**
 * \brief       Main 
 * \details     - Load libraries
 *		- Parse config files
 * 		- Initialize data structures
 * 		- Run simulation
 * 		- Write results into output file
 * \return   0.
 */
int
main (int argc, char **argv)
{
   st_model_ptr model = NULL;
   st_backwardMonotoneParallel_ptr method = NULL;
   st_general_ptr general = NULL;
   int rc;

   rc = read_configuration (&general,
                            argv[2],
                            &model,
                            argv[1],
                            (void **) &method,
                            argv[3],
                            (void *) read_method);

   if (rc) {                   /* if reading configuration failed */
      close_psi (general, model, method, free_method);
      return 1;
   }

   /**
    * initialize kernel from the model
    */
   init_kernel (model);

   if (method->Doubling) {
      simulation_doubling (general, method, model);
   }
   else {
      simulation_simple (general, method, model);
   }

   /**
    * clean up the system:
    */
   return close_psi (general, model, method, free_method);
}
