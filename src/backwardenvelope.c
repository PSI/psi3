/**
 * \file        backwardenvelope.c
 * \author      Florian LEVEQUE
 * \author      Jean-Marc.Vincent@imag.fr 
 * \version     1.0.0
 * \date        02/07/2012
 * \brief       Backward Envelop Kernel.
 */

 /* 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <dlfcn.h>
#include <sys/time.h>
#include <time.h>
#include <generator.h>
#include <parser.h>
#include "psi_common.h"

#define ALLOCATION 1000

/*****************************************************************************/
/*             Global variables used by transition or simulation             */

/* TRANSITION ---------- */
/* Model */
/* - Queues: */
int* capmins ;                 // capacity min of queues
int* capmaxs ;                 // capacity max of queues 
int nb_queues;                 // number of queues	

/* Load sharing */
int ls_mode;
int **node_dist;     /* [LOAD SHARING] matrix of distance between two queues :
                      * node_dist[i][j] = distance between i and j */
struct st_neighbors *node_neighbors; /* [LOAD SHARING] array containing for
                                      * each node, the neighbors of the node */

/* SIMULATION ---------- */
double simulation_time;
int **reward;              // reward table
int nb_sample;             // numbers of samples FIXME
int nb_AV;                 // number of anthitetic value
int FLAG;

int *log2stop_time; /* number of iteration to finish the simulation
                     * (coupling time) for each anthitetic variable
                     */
int **state_saved; /* different queues states when coupling for each
                    * anthitetic variable
                    */

/*****************************************************************************/
 
/**
 * \brief      Procedure to free allocated parameters
 * \return     void
 */
void free_method (st_method_ptr method_ptr)
{
   st_backwardEnvelope_ptr method = method_ptr;

   if (method_ptr) {
      method = method_ptr;
   }
   else return;

   if (method->RewardFct != NULL) free (method->RewardFct);
   if (method->EnvInit != NULL) free (method->EnvInit);
   if (method->EnvModif != NULL) free (method->EnvModif);
   if (method->OutputFct != NULL) free (method->OutputFct);

   free (method);
}

/**
 * \brief      Procedure to init the method structure
 * \return     a pointer to a method structure
 *             NULL if failed
 */
st_backwardEnvelope_ptr init_method ()
{
   st_backwardEnvelope_ptr method = (st_backwardEnvelope_ptr)
                                 calloc (1, sizeof(st_backwardEnvelope));
   if (method == NULL) return NULL;

   method->RewardFct = (st_function_ptr) calloc (1, sizeof (st_function));
   if (method->RewardFct == NULL) return NULL;

   method->EnvInit = (st_function_ptr) calloc (1, sizeof (st_function));
   if (method->EnvInit == NULL) return NULL;

   method->EnvModif = (st_function_ptr) calloc (1, sizeof (st_function));
   if (method->EnvModif == NULL) return NULL;

   method->OutputFct = (st_function_ptr) calloc (1, sizeof (st_function));
   if (method->OutputFct == NULL) return NULL;

   method->SampleNumber = 1;
   method->TrajectoryMax = 0;
   method->Antithetic = 0;
   method->Doubling = 0;
   method->InitMemoryLength = 0;

   return method;
}

/**
 * \brief      Procedure to print method parameters to the outout file
 * \param      output_file    file descriptor to the output file
 * \param      method_ptr     method
 * \return     void
 */
void print_method (FILE *output_file, st_method_ptr method_ptr)
{
   st_backwardEnvelope_ptr method = (st_backwardEnvelope_ptr) method_ptr;

   fprintf(output_file, "# Method: backwardenvelope\n");
   fprintf(output_file, "#\tSampleNumber: %d\n", method->SampleNumber);
   fprintf(output_file, "#\tTrajectoryMax: %d\n", method->TrajectoryMax);
   fprintf(output_file, "#\tAntithetic: %d\n", method->Antithetic);
   fprintf(output_file, "#\tDoubling: %d\n", method->Doubling);
   fprintf(output_file, "#\tInitMemoryLength: %d\n", method->InitMemoryLength);
   fprintf(output_file, "#\tCouplingFct: %s\n", method->RewardFct->name);
   fprintf(output_file, "#\tEnvInit: %s\n", method->EnvInit->name);
   fprintf(output_file, "#\tEnvModif: %s\n", method->EnvModif->name);
   fprintf(output_file, "#\tOutputFct: %s\n", method->OutputFct->name);
}

/**
 * \brief      Function to read an initialize method simulation parameters
 * \param      method_param_cfgfile    method config file name
 * \param      general                 pointer to the general structure
 * \param      model                   pointer to the model structure
 * \return     a pointer to a initialized method structure
 *             NULL if failed
 */
st_backwardEnvelope_ptr read_method (char *method_param_cfgfile,
                                     st_general_ptr general,
                                     st_model_ptr model)
{
   int err = 0;
   FILE *method_param_fd;
   char *loadsharing_file_name;
   FILE *loadsharing_file;

#if (DEBUG_LVL >= DEBUG_IO)
   fprintf(stderr, "Opening method parameters file: %s\n", method_param_cfgfile);
#endif
   method_param_fd = fopen(method_param_cfgfile, "r");
   if (method_param_fd == NULL) {
      fprintf(stderr, "Fail to open file %s\n", method_param_cfgfile);
      return NULL;
   }

   st_backwardEnvelope_ptr method = init_method ();

   /**
    * Get samplenumber
    */
   method->SampleNumber = get_int_keyname(method_param_fd, "SampleNumber");
   if (method->SampleNumber <= 0) {
      fprintf(stderr, "Number of sample must be positive\n");
      err++;
   }
#if (DEBUG_LVL >= DEBUG_INFO)
   fprintf(stderr, "SampleNumber: %d\n", method->SampleNumber);
#endif
   nb_sample = method->SampleNumber; /* FIXME */

   /**
    * Get InitMemoryLength: the initial allocated memory
    */
   method->InitMemoryLength = get_int_keyname(method_param_fd, "InitMemoryLength");
   if (method->InitMemoryLength <= 0){
      fprintf (stderr, "InitMemoryLength: automatic default value (%d)\n", ALLOCATION);
      method->InitMemoryLength = ALLOCATION;
   }
#if (DEBUG_LVL >= DEBUG_INFO)
   fprintf(stderr, "InitMemoryLength: %d\n", method->InitMemoryLength);
#endif

   /**
    * Get Antithetic: the number of anthitetic variables
    */
   method->Antithetic = get_int_keyname(method_param_fd, "Antithetic");
   if (method->Antithetic <= 0) {
      fprintf(stderr, "Number of antithetic variables must be positive\n");
      err++;
   }
#if (DEBUG_LVL >= DEBUG_INFO)
   fprintf(stderr, "Antithetic: %d\n", method->Antithetic);
#endif

   nb_AV = method->Antithetic; /* FIXME */


   /**
    * Get TrajectoryMax
    */
   method->TrajectoryMax = get_int_keyname(method_param_fd, "TrajectoryMax");
   if (method->TrajectoryMax <= 0) {
      fprintf(stderr, "TrajectoryMax must be positive\n");
      err++;
   }
   else {
#if (DEBUG_LVL >= DEBUG_INFO)
      fprintf(stderr, "TrajectoryMax: %d\n", method->TrajectoryMax);
#endif
   }

   /**
    * Get Doubling
    */
   method->Doubling = get_bool_keyname (method_param_fd, "Doubling");
   if (method->Doubling != 0) { /* Yes by default */
      method->Doubling = 1;
   }
#if (DEBUG_LVL >= DEBUG_INFO)
   fprintf (stderr, "Doubling: %d\n", method->Doubling);
#endif

   /**
    * Get reward function
    */
   method->RewardFct->name = get_string_keyname (method_param_fd, "CouplingFct");
   if (method->RewardFct->name == NULL) {
      method->RewardFct->name = "Default$reward_ENV";
#if (DEBUG_LVL >= DEBUG_INFO)
      fprintf (stderr, "CouplingFct: automatic default value (%s)\n",
              method->RewardFct->name);
#endif
   }
   method->RewardFct->ptr = (T_ptr_rewardfct)
                                 get_ptr_fct (general->default_lib,
                                              general->user_lib,
                                              method->RewardFct->name);
   if (method->RewardFct->ptr == NULL) {
      err++;
   }
#if (DEBUG_LVL >= DEBUG_INFO)
   fprintf (stderr, "CouplingFct: %s (%p)\n",
           method->RewardFct->name,
           method->RewardFct->ptr);
#endif

   /**
    * Get EnvInit function
    */
   method->EnvInit->name = get_string_keyname (method_param_fd, "EnvInit");
   if (method->EnvInit->name == NULL) {
      method->EnvInit->name = "Default$init_envelop";
#if (DEBUG_LVL >= DEBUG_INFO)
      fprintf (stderr, "EnvInit: automatic default value (%s)\n",
              method->EnvInit->name);
#endif
   }
   method->EnvInit->ptr = (T_ptr_rewardfct)
                                 get_ptr_fct (general->default_lib,
                                              general->user_lib,
                                              method->EnvInit->name);
   if (method->EnvInit->ptr == NULL) {
      err++;
   }
#if (DEBUG_LVL >= DEBUG_INFO)
   fprintf (stderr, "EnvInit: %s (%p)\n",
           method->EnvInit->name,
           method->EnvInit->ptr);
#endif

   /**
    * Get EnvModif function
    */
   method->EnvModif->name = get_string_keyname (method_param_fd, "EnvModif");
   if (method->EnvModif->name == NULL) {
      method->EnvModif->name = "Default$modify";
#if (DEBUG_LVL >= DEBUG_INFO)
      fprintf (stderr, "EnvModif: automatic default value (%s)\n",
              method->EnvModif->name);
#endif
   }
   method->EnvModif->ptr = (T_ptr_rewardfct)
                                 get_ptr_fct (general->default_lib,
                                              general->user_lib,
                                              method->EnvModif->name);
   if (method->EnvModif->ptr == NULL) {
      err++;
   }
#if (DEBUG_LVL >= DEBUG_INFO)
   fprintf (stderr, "EnvModif: %s (%p)\n",
           method->EnvModif->name,
           method->EnvModif->ptr);
#endif

   /**
    * Get Output Function
    */
   method->OutputFct->name = get_string_keyname(method_param_fd, "OutputFct");
   if (method->OutputFct->name == NULL) {
      method->OutputFct->name = "Default$output_B";
#if (DEBUG_LVL >= DEBUG_INFO)
      fprintf(stderr, "OutputFct: automatic default value (%s)\n",
             method->OutputFct->name);
#endif
   }
   method->OutputFct->ptr = (T_ptr_stopfct) get_ptr_fct (general->default_lib,
                                                       general->user_lib,
                                                       method->OutputFct->name);
   if (method->OutputFct->ptr == NULL) {
      err++;
   }
#if (DEBUG_LVL >= DEBUG_INFO)
   fprintf(stderr, "OutputFct: %s (%p)\n",
          method->OutputFct->name,
          method->OutputFct->ptr);
#endif

   /**
    * Get the load sharing
    */
   loadsharing_file_name = get_string_keyname(method_param_fd, "LoadSharing");

   if (loadsharing_file_name != NULL) {
#if (DEBUG_LVL >= DEBUG_INFO)
      fprintf(stderr, "LoadSharing: [%s]\n", loadsharing_file_name);
#endif
      loadsharing_file = fopen (loadsharing_file_name, "r");
      if (loadsharing_file == NULL) {
         fprintf(stderr, "The Load Sharing file does not exist\n");
         err++;
      }
      else {
         //parser the load sharing file
         node_dist = LS_parser (loadsharing_file,
                                node_dist,
                                model->queues->nb_queues,
                                &ls_mode);
         if (node_dist == NULL) {
            err++;
         }
         else {
            //save the configuration of load sharing
            node_neighbors = node_dist_to_node_neighbors(node_dist,
                                                         node_neighbors,
                                                         model->queues->nb_queues);
#if (DEBUG_LVL >= DEBUG_INFO)
            fprintf(stderr, "Load Sharing: nb_queues: %d\n", model->queues->nb_queues);
#endif
         }
         fclose(loadsharing_file);
      }
   }
   else {
      //load sharing is not used
      node_dist = NULL;
      node_neighbors = NULL;
#if (DEBUG_LVL >= DEBUG_INFO)
      fprintf(stderr, "Load Sharing is not used\n");
#endif
   }

   fclose(method_param_fd);

   if (err) {
      free_method (method);
      return NULL;
   }

   return method;
}

/**
 * \brief       Procedure to initialize the kernel from a model
 * \details	Initialize global variables from the model
 * \param    model         model of system.
 * \return   void.
 */
void init_kernel (st_model_ptr model)
{
  /**
   * initialize the random event generator :
   */
  init_generator (model->events->nb_evts, model->events->evts);

  capmins = model->queues->mins;
  capmaxs = model->queues->maxs;
  nb_queues = model->queues->nb_queues;
  FLAG = ALGO_NONMONOTONE;
}

/**
 * \brief       Procedure to free allocated parameters
 * \return   void.
 */
void free_param(){
   int i;
   if (reward != NULL){
      for(i = 0; i < nb_AV; i++) {
	free(reward[i]);
      }
      free(reward);
   }
   if (node_dist != NULL){
     for(i=0;i<nb_queues;i++)
	free(node_dist[i]);
     free(node_dist);
   }
   if (node_neighbors != NULL){
      for (i=0;i<nb_queues;i++){
	      free(node_neighbors[i].neighbors);
      }
      free(node_neighbors);
   }
}

int simulation_simple (st_general_ptr general,
                       st_backwardEnvelope_ptr method,
                       st_model_ptr model)
{

   int current_length_memory = method->InitMemoryLength;
//save time a the beginning and at the end of the simulation  
  struct timeval ti,tf;
//States of the system
  T_state* states=NULL;
//several iterator use in simulation
  int i =0,place,n, temp, num,stop_time = 0,indice;
//current event of the simulation  
  T_event* event;
  int ** Tab_VA;
  double *stat;
  double val;
//back up of the previous event 
  int* trajectory;
//current number of sample
  int sample;
//current random event number
  int id_random_evt;

   int sampleComputed = 0;        /* number of sample computed during the
                                   * simulation */

   st_simulation_ptr simulation;
   simulation = malloc (sizeof (st_simulation));
   simulation->finished = 0;

//initialize the T_state structure thanks to the function defined, allow to do different envelopp  
  states = ((T_ptr_envelop) (method->EnvInit->ptr))(states,2);
  
  log2stop_time=(int*)calloc(nb_AV,sizeof(int));

  Tab_VA = (int **) calloc(nb_AV, sizeof(int*));
  for(n=0; n<nb_AV; n++)  Tab_VA[n]=(int*)calloc(current_length_memory,sizeof(int));

  stat=(double*)calloc(current_length_memory,sizeof(double));
 
  trajectory = (int*)malloc(current_length_memory * sizeof(int));
 
  reward=(int**)calloc(nb_AV, sizeof(int*));
  for (n=0; n< nb_AV; n++)  reward[n]=(int *)calloc(nb_queues,sizeof(int));  
  
  state_saved = (int **) calloc(nb_AV, sizeof(int*));
  for(i=0; i<nb_AV; i++){
	  state_saved[i]=(int*)calloc(nb_queues,sizeof(int));
  }

  //save current time before simulation 
  gettimeofday(&ti,NULL);
  for (sample = 0 ; sample < method->SampleNumber ; sample++){
	  //re-initialize reward table and reward time for each trajectory 
	    for (n=0; n<nb_AV ;n++){
		    log2stop_time[n] = 0;  
		    for (i = 0; i< nb_queues; i++){
			reward[n][i]= 0;
		      }
	    }		    
	    stat[0] = u01();
	    for(n=0; n<nb_AV; n++){
		    Tab_VA[n][0]=n;
	    }
	    for (i=1; i<nb_AV; i++){
		    place = ((int) (u01()*(nb_AV-1)))+1;
		    temp = Tab_VA[place][0];
		    Tab_VA[place][0] = Tab_VA[i][0];
		    Tab_VA[i][0] = temp;
	    }
	    
	    for (num=0; num<nb_AV ; num++){
		    // x modulo y == x - ( ( (int)x/(int)y ) * y )
		    val = (stat[0]+(double)Tab_VA[num][0]/(double)nb_AV) - ((int)(stat[0]+(double)Tab_VA[num][0]/(double)nb_AV)); 
		    //Generate a random event
		    id_random_evt = genere_evenement(val, model->events->nb_evts);
		    //save the random number
		    trajectory[0] =  id_random_evt;
		    stop_time = 1;  
		    
		    do{
			    //re initialize lower and upper state with lower and upper capacity 
			    indice = stop_time -1;
			    states = ((T_ptr_envelop) (method->EnvModif->ptr))(states);
			  //check if we have to realloc the memory 
			    if (current_length_memory < indice){
				current_length_memory = current_length_memory*2;
				for(n=0; n<nb_AV; n++)  Tab_VA[n]=(int*)realloc(Tab_VA[n],current_length_memory*sizeof(int));
				stat=(double*)realloc(stat,current_length_memory*sizeof(double));
				trajectory = (int*)realloc(trajectory,current_length_memory*sizeof(int));
			    }

				stat[indice] = u01();
				for(n=0; n<nb_AV; n++){
					Tab_VA[n][indice]=n;
				}
				for (i=1; i<nb_AV; i++){
					place = ((int) (u01()*(nb_AV-1)))+1;
					temp = Tab_VA[place][indice];
					Tab_VA[place][indice] = Tab_VA[i][indice]; 
					Tab_VA[i][indice] = temp;
				}
			
			    
			    val = (stat[indice]+(double)Tab_VA[num][indice]/(double)nb_AV) - ((int)(stat[indice]+(double)Tab_VA[num][indice]/(double)nb_AV ));				
			    //Generate a random event
			    id_random_evt = genere_evenement(val, model->events->nb_evts);
			    //save the random number
			    trajectory[indice] =  id_random_evt;
			    event = model->events->evts+id_random_evt;
			    // e(x+1) = Transition(e(x),event), doing the event 
			    states = (*((T_ptr_transfct) event->ptr_transition)) (states, event, model);
		    
			    // from indice = stop_time-1 to O we take the same trajectory (previous saved event)
			    for (indice = stop_time-2; indice>=0; indice--){
				//take a previous random event
				id_random_evt = trajectory[indice];
				event = model->events->evts+id_random_evt;
				// e(x+1) = Transition(e(x),event), doing the event 
				states = (*((T_ptr_transfct) event->ptr_transition)) (states, event, model);
				// We check if in previous trajectory, we have already the state because if we detect that 2 vectors 
				//are similar to 2 old ones, it is not necessary to carry on 
			      }      
			      //Increment the begin time
			    stop_time=stop_time+1;
			    log2stop_time[num]+=1;
			    //until we are not at the trajectory max and we have not coupling in all queues
		    }while((stop_time < method->TrajectoryMax) && !(((T_ptr_rewardfct) (method->RewardFct->ptr)) (states,log2stop_time[num],reward[num], state_saved[num])));
	    } 

      sampleComputed++;

	    //print the result in output file
	    if (stop_time >= method->TrajectoryMax){
		fprintf(general->output_file," echec  \n");
	    }
	    else
	    {
         ((T_ptr_outputfct) (method->OutputFct->ptr))
                              (general->output_file, state_saved, sample, log2stop_time);
	    }
       loadbar (sampleComputed, method->SampleNumber);
	} 
 //save current time after simulation 
  gettimeofday(&tf,NULL);
   //Calculate simulation time by substracting end time and beginning time 
   simulation_time = delta_ms (ti, tf);

   simulation->time = simulation_time;

   print_footer (general, model, method, simulation->time);

   print_time (stderr, simulation->time);

   print_perf (stderr, general, simulation);

  //free all the used structures 
  for(n=0; n<nb_AV; n++){
    free(Tab_VA[n]);
    free(state_saved[n]);
  }
  for (i=0;i<states->nb_vectors;i++) free(states->queues[i]);
  free(states->queues);
  free(states);
  free(Tab_VA );
  free(stat);
  free(log2stop_time);
  free(state_saved);   
  free(trajectory);
  return 0;
}

/**
 * \brief       Procedure to run the simulation
 * \details      Run simulation and store updated global variables (e.g state, trajectory etc)
 * \return   void.
 */
int
simulation_doubling (st_general_ptr general,
                     st_backwardEnvelope_ptr method,
                     st_model_ptr model)
{
   int current_length_memory = method->InitMemoryLength;
   //save time a the beginning and at the end of the simulation  
   struct timeval ti,tf;
   //States of the system
   T_state* states=NULL;
   //several iterator use in simulation
   int i =0,place,n, temp, num,stop_time,indice,max_temps,indice2;
   //current event of the simulation  
   T_event* event;
   int ** Tab_VA;
   double *stat;
   double val;
   //back up of the previous event 
   int* trajectory;
   //current number of sample
   int sample;
   //current random event number
   int id_random_evt;

   int sampleComputed = 0;        /* number of sample computed during the
                                   * simulation */

   st_simulation_ptr simulation;
   simulation = malloc (sizeof (st_simulation));
   simulation->finished = 0;

   //initialize the T_state structure thanks to the function defined, allow to do different envelopp  
   states = ((T_ptr_envelop) (method->EnvInit->ptr))(states,2);

   log2stop_time=(int*)calloc(nb_AV,sizeof(int));

   Tab_VA = (int **) calloc(nb_AV, sizeof(int*));
   for(n=0; n<nb_AV; n++)  Tab_VA[n]=(int*)calloc(current_length_memory,sizeof(int));

   stat=(double*)calloc(current_length_memory,sizeof(double));

   trajectory = (int*)malloc(current_length_memory * sizeof(int));

   reward=(int**)calloc(nb_AV, sizeof(int*));
   for (n=0; n< nb_AV; n++)  reward[n]=(int *)calloc(nb_queues,sizeof(int));  

   state_saved = (int **) calloc(nb_AV, sizeof(int*));
   for(i=0; i<nb_AV; i++){
      state_saved[i]=(int*)calloc(nb_queues,sizeof(int));
   }

   //save current time before simulation 
   gettimeofday(&ti,NULL);
   for (sample = 0 ; sample < method->SampleNumber ; sample++){
      //re-initialize reward table and reward time for each trajectory 
      for (n=0; n<nb_AV ;n++){
         log2stop_time[n] = 0;  
         for (i = 0; i< nb_queues; i++){
            reward[n][i]= 0;
         }
      }	
      max_temps = 0;
      stop_time = 1;

      stat[0] = u01();
      for(n=0; n<nb_AV; n++){
         Tab_VA[n][0]=n;
      }
      for (i=1; i<nb_AV; i++){
         place = ((int) (u01()*(nb_AV-1)))+1;
         temp = Tab_VA[place][0];
         Tab_VA[place][0] = Tab_VA[i][0];
         Tab_VA[i][0] = temp;
      }

      for (num=0; num<nb_AV ; num++){
         // x modulo y == x - ( ( (int)x/(int)y ) * y )
         val = (stat[0]+(double)Tab_VA[num][0]/(double)nb_AV) - ((int)(stat[0]+(double)Tab_VA[num][0]/(double)nb_AV)); 
         id_random_evt = genere_evenement(val, model->events->nb_evts);
         trajectory[0] =  id_random_evt;
         stop_time = 1;  
         do{
            //re initialize lower and upper state with lower and upper capacity 
            states = ((T_ptr_envelop) (method->EnvModif->ptr))(states);
            for (indice = 2*stop_time-1; indice >= stop_time; indice--){
               //check if we have to realloc the memory 
               if (current_length_memory < indice){
                  current_length_memory = current_length_memory*2;
                  for(n=0; n<nb_AV; n++)  Tab_VA[n]=(int*)realloc(Tab_VA[n],current_length_memory*sizeof(int));
                  stat=(double*)realloc(stat,current_length_memory*sizeof(double));
                  trajectory = (int*)realloc(trajectory,current_length_memory*sizeof(int));
               }
               if(max_temps < indice){
                  for (indice2 = 2*stop_time-1; indice2 >= stop_time; indice2--){
                     stat[indice2] = u01();
                     //GENERE VAL
                     for(n=0; n<nb_AV; n++){
                        Tab_VA[n][indice2]=n;
                     }
                     for (i=1; i<nb_AV; i++){
                        place = ((int) (u01()*(nb_AV-1)))+1;
                        temp = Tab_VA[place][indice2];
                        Tab_VA[place][indice2] = Tab_VA[i][indice2]; 
                        Tab_VA[i][indice2] = temp;
                     }
                  }
                  max_temps = indice;
               }

               val = (stat[indice]+(double)Tab_VA[num][indice]/(double)nb_AV) - ((int)(stat[indice]+(double)Tab_VA[num][indice]/(double)nb_AV ));				
               //Generate a random event
               id_random_evt = genere_evenement(val, model->events->nb_evts);
               //save the random number
               trajectory[indice] =  id_random_evt;
               event = model->events->evts+id_random_evt;
               // e(x+1) = Transition(e(x),event), doing the event 
               states = (*((T_ptr_transfct) event->ptr_transition)) (states, event, model);
            }
            // from indice = stop_time-1 to O we take the same trajectory (previous saved event)
            for (indice = stop_time-1; indice>=0; indice--){
               //take a previous random event
               id_random_evt = trajectory[indice];
               event = model->events->evts+id_random_evt;
               // e(x+1) = Transition(e(x),event), doing the event 
               states = (*((T_ptr_transfct) event->ptr_transition)) (states, event, model);
            }      
            //double the begin time
            stop_time=stop_time<<1;;
            log2stop_time[num]+=1;
            //until we are not at the trajectory max length and we have not coupling in all queues
         }while((2*stop_time < method->TrajectoryMax) && !(((T_ptr_rewardfct) (method->RewardFct->ptr)) (states,log2stop_time[num],reward[num], state_saved[num])));
      } 

      sampleComputed++;

      //print the result in output file
      if (2*stop_time >= method->TrajectoryMax){
         fprintf(general->output_file," echec  \n");
      }
      else
      {
         ((T_ptr_outputfct) (method->OutputFct->ptr))
                              (general->output_file, state_saved, sample, log2stop_time);
      }
      loadbar (sampleComputed, method->SampleNumber);
   } 
   //save current time after simulation 
   gettimeofday(&tf,NULL);
   //Calculate simulation time by substracting end time and beginning time 
   simulation_time = delta_ms (ti, tf);

   simulation->time = simulation_time;

   print_footer (general, model, method, simulation->time);

   print_time (stderr, simulation->time);

   print_perf (stderr, general, simulation);

   //free all the used structures 
   for(n=0; n<nb_AV; n++){
      free(Tab_VA[n]);
      free(state_saved[n]);
   }
   for (i=0;i<states->nb_vectors;i++) free(states->queues[i]);
   free(states->queues);
   free(states);
   free(Tab_VA );
   free(stat);
   free(log2stop_time);
   free(state_saved);   
   free(trajectory);
   return 0;
}


/**
 * \brief       Main 
 * \details     - Load libraries
 *		- Parse config files
 * 		- Initialize data structures
 * 		- Run simulation
 * 		- Write results into output file
 * \return   0.
 */
int main (int argc, char **argv)
{
   st_model_ptr model = NULL;
   st_backwardEnvelope_ptr method = NULL;
   st_general_ptr general = NULL;
   int rc;

   rc = read_configuration (&general,
                            argv[2],
                            &model,
                            argv[1],
                            (void **) &method,
                            argv[3],
                            (void *) read_method);

   if (rc) {                   /* if reading configuration failed */
      close_psi (general, model, method, free_method);
      return 1;
   }

   /**
    * initialize kernel from the model
    */
   init_kernel (model);

   /**
    * run the simulation
    */
   if (method->Doubling) {
      simulation_doubling (general, method, model);
   }
   else {
      simulation_simple (general, method, model);
   }

   /**
    * clean up the system:
    */
   return close_psi (general, model, method, free_method);
}
