 /**
 * \file          simpleforward.c
 * \author        Minh Quan HO
 * \author        Jean-Marc.Vincent@imag.fr
 * \author        Benjamin.Briot@inria.fr
 * \version       1.3.0
 * \date          01/04/2015
 * \brief         Simple forward kernel.
 */

 /* 
  * This library is free software; you can redistribute it and/or
  * modify it under the terms of the GNU Lesser General Public
  * License as published by the Free Software Foundation; either
  * version 2.1 of the License, or any later version.
  *
  * This library is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  * Lesser General Public License for more details.
  *
  * You should have received a copy of the GNU Lesser General Public
  * License along with this library; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
  */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <dlfcn.h>
#include <sys/time.h>
#include <time.h>
#include <generator.h>
#include <parser.h>
#include "psi_common.h"
#include <math.h>

#define OUTPUT_THRESHOLD 500    /* buffer trajectory before writing out */

/*****************************************************************************/
/*             Global variables used by transition or simulation             */

/* TRANSITION ---------- */
/* Model FIXME: should disapear from this space since it moved to a struct */
/* - Queues: */
int *capmins;                   // capacity min of queues
int *capmaxs;                   // capacity max of queues
int nb_queues;

/* Load sharing */
int ls_mode;
int **node_dist;                /* [LOAD SHARING] matrix of distance between
                                 * two queues : node_dist[i][j] = distance
                                 * between i and j */
struct st_neighbors *node_neighbors;   /* [LOAD SHARING] array containing for
                                        * each node, the neighbors of the node */
int nb_AV;
int FLAG;

/* SIMULATION ---------- */
/* FIXME: will probably move to a struct */
int **log_states;
double *timing_states;
int *events_list;
double simulation_time;

/*****************************************************************************/

/**
 * \brief      Procedure to initialize the kernel from a model
 * \details    Initialize global variables from the model
 * \param      model    system model
 * \return     void
 */
void
init_kernel (st_model_ptr model)
{
   capmins = model->queues->mins;
   capmaxs = model->queues->maxs;
   nb_queues = model->queues->nb_queues;
   FLAG = ALGO_NONMONOTONE;
}

/**
 * \brief      Procedure to free allocated parameters
 * \return     void
 */
void
free_method (st_method_ptr method_ptr)
{
   st_simpleForward_ptr method = method_ptr;

   if (method_ptr) {
      method = method_ptr;
   }
   else
      return;

   if (method->InitialState != NULL) {
      free (method->InitialState);
   }
   if (method->StopFct != NULL) {
      free (method->StopFct);
   }
   if (method->OutputFct != NULL) {
      free (method->OutputFct);
   }
   free (method);
}

/**
 * \brief      Procedure to print method parameters to the outout file
 * \param      output_file    file descriptor to the output file
 * \param      method_ptr     method
 * \return     void
 */
void
print_method (FILE * output_file, st_method_ptr method_ptr)
{
   st_simpleForward_ptr method = (st_simpleForward_ptr) method_ptr;

   fprintf (output_file, "# Method: simpleforward\n");
   fprintf (output_file, "#\tTrajectoryLength: %d\n", method->TrajectoryLength);
   fprintf (output_file, "#\tMaxTime: %f\n", method->MaxTime);

   int elem_vector;
   fprintf (output_file, "#\tTrajectorySeed: [%lu", method->TrajectorySeed[0]);
   for (elem_vector = 1; elem_vector < SEED_VECTOR_SIZE; elem_vector++) {
      fprintf (output_file, ", %lu", method->TrajectorySeed[elem_vector]);
   }
   fprintf (output_file, "]\n");

   fprintf (output_file, "#\tTimingSeed: [%lu", method->TimingSeed[0]);
   for (elem_vector = 1; elem_vector < SEED_VECTOR_SIZE; elem_vector++) {
      fprintf (output_file, ", %lu", method->TimingSeed[elem_vector]);
   }
   fprintf (output_file, "]\n");

   fprintf (output_file, "#\tInitialState: [");
   print_list_int (output_file, method->InitialState, method->InitialState_size);
   fprintf (output_file, "]\n");
}

/**
 * \brief      Procedure to init the method structure
 * \return     a pointer to a method structure
 *             NULL if failed
 */
st_simpleForward_ptr
init_method ()
{
   st_simpleForward_ptr method = (st_simpleForward_ptr)
      calloc (1, sizeof (st_simpleForward));
   if (method == NULL)
      return NULL;
   method->StopFct = (st_function_ptr) calloc (1, sizeof (st_function));
   if (method->StopFct == NULL)
      return NULL;
   method->OutputFct = (st_function_ptr) calloc (1, sizeof (st_function));
   if (method->OutputFct == NULL)
      return NULL;

   method->TrajectoryLength = 0;
   method->MaxTime = -1;
   method->InitialState = NULL;
   method->TimingSeed = NULL;
   method->InitialState_size = 0;
   method->TrajectorySeed = NULL;
   method->UserSeed = 0;
   method->output_threshold = 0;
   method->cpt_transitions = 0;

   return method;
}

/**
 * \brief      Function to read an initialize method simulation parameters
 * \param      method_param_cfgfile    method config file name
 * \param      general                 pointer to the general structure
 * \param      model                   pointer to the model structure
 * \return     a pointer to a initialized method structure
 *             NULL if failed
 */
st_simpleForward_ptr
read_method (char *method_param_cfgfile,
             st_general_ptr general, st_model_ptr model)
{
   int err = 0;
   int rc = 0;
   FILE *method_param_fd;

   PSI3_DEBUG ("Opening method parameters file: %s",
         method_param_cfgfile);

   method_param_fd = fopen (method_param_cfgfile, "r");
   if (method_param_fd == NULL) {
      PSI3_ERROR ("Fail to open file %s\n", method_param_cfgfile);
      return NULL;
   }

   st_simpleForward_ptr method = init_method ();

   /**
    * Get TrajectoryLength
    */
   method->TrajectoryLength =
      get_int_keyname (method_param_fd, "TrajectoryLength");
   if (method->TrajectoryLength <= 0) {
      PSI3_ERROR ("TrajectoryLength must be positive");
      err++;
   }
   else {

   }
   PSI3_DEBUG ("TrajectoryLength: [%d]", method->TrajectoryLength);

   /**
    * Get MaxTime
    */
   method->MaxTime = get_double_keyname (method_param_fd, "MaxTime");

   if (method->MaxTime > 0) {
      PSI3_DEBUG ("MaxTime: [%f]", method->MaxTime);
   }
   else {
      method->MaxTime = INFINITY;
      PSI3_DEBUG ("MaxTime: automatic value [%f] (should be >= 0)", method->MaxTime);
   }

   /**
    * Get initial state(s)
    */
   get_int_list_keyname (method_param_fd,
                         "InitialState",
                         &(method->InitialState), &(method->InitialState_size));

   if (method->InitialState != NULL) { // when user enter an <int> list

      if (method->InitialState_size != model->queues->nb_queues) {
         /* check the size consistency between this list and number of queues */
         fprintf (stderr,
                  " /!\\ : Number of initial states doesn't match with number of "
                  "queues : begin_size = %d - nb_queues = %d\n",
                  method->InitialState_size, model->queues->nb_queues);
         // Missmatch : clear what have been created in this function:
         free (method->InitialState);
         err++;
      }
   }
   else {
      method->InitialState = random_int_list (model->queues->nb_queues,
                                              model->queues->maxs,
                                              model->queues->mins,
                                              general->seed);
      method->InitialState_size = model->queues->nb_queues;
   }

#if (LOG_LVL >= LOG_INFO)
   int i = 0;
   fprintf (stderr, "INFO: InitialState: [");
   for (; i < model->queues->nb_queues;
        fprintf (stderr, " %d", method->InitialState[i++]));
   fprintf (stderr, "]\n");
#endif

   /**
    * Get TrajectorySeed: to reprocude a trajectory
    */
   rc = get_unsigned_long_list_keyname (method_param_fd,
                                        "TrajectorySeed",
                                        &(method->TrajectorySeed),
                                        SEED_VECTOR_SIZE);

   if (rc == 0) {
      method->UserSeed = 1;
      PSI3_DEBUG ("TrajectorySeed: [%lu, %lu, %lu, %lu, %lu, %lu]",
            method->TrajectorySeed[0],
            method->TrajectorySeed[1],
            method->TrajectorySeed[2],
            method->TrajectorySeed[3],
            method->TrajectorySeed[4],
            method->TrajectorySeed[5]);
   }
   else if (rc == -3) {
      PSI3_ERROR ("TrajectorySeed: incorrect size: vector should be size %d",
                  SEED_VECTOR_SIZE);
      err++;
   }
   else if (rc == -1) {
      PSI3_ERROR ("TrajectorySeed: error parsing vector");
      err++;
   }
   else { // (rc == -2) of "~"
      // RANDOM
      PSI3_DEBUG ("TrajectorySeed: Random");
   }

   /**
    * Get TimingSeed: to reprocude a trajectory
    */
   rc = get_unsigned_long_list_keyname (method_param_fd,
                                        "TimingSeed",
                                        &(method->TimingSeed),
                                        SEED_VECTOR_SIZE);

   if (rc == 0) {
      method->UserTimingSeed = 1;
      PSI3_DEBUG ("TimingSeed: [%lu, %lu, %lu, %lu, %lu, %lu]",
            method->TimingSeed[0],
            method->TimingSeed[1],
            method->TimingSeed[2],
            method->TimingSeed[3],
            method->TimingSeed[4],
            method->TimingSeed[5]);
   }
   else if (rc == -3) {
      PSI3_ERROR ("TimingSeed: incorrect size: vector should be size %d",
                  SEED_VECTOR_SIZE);
      err++;
   }
   else if (rc == -1) {
      PSI3_ERROR ("TimingSeed: error parsing vector");
      err++;
   }
   else { // (rc == -2) of "~"
      // RANDOM
      PSI3_DEBUG ("TimingSeed: Random");
   }

   /**
    * Get Stop Function
    */
   method->StopFct->name = get_string_keyname (method_param_fd, "StopFct");
   if (method->StopFct->name == NULL) {
      method->StopFct->name = "Default$stop";
      PSI3_DEBUG ("StopFct: automatic default value (%s)",
                 method->StopFct->name);
   }
   method->StopFct->ptr = (T_ptr_stopfct) get_ptr_fct (general->default_lib,
                                                       general->user_lib,
                                                       method->StopFct->name);
   if (method->StopFct->ptr == NULL) {
      PSI3_ERROR ("StopFct: not found");
      err++;
   }
   PSI3_DEBUG ("StopFct: %s (%p)", method->StopFct->name, method->StopFct->ptr);

   /**
    * Get Output Function
    */
   method->OutputFct->name = get_string_keyname (method_param_fd, "OutputFct");
   if (method->OutputFct->name == NULL) {
      method->OutputFct->name = "Default$output_SF";
      PSI3_DEBUG ("OutputFct: automatic default value (%s)",
                 method->OutputFct->name);
   }
   method->OutputFct->ptr = (T_ptr_stopfct) get_ptr_fct (general->default_lib,
                                                         general->user_lib,
                                                         method->OutputFct->
                                                         name);
   if (method->OutputFct->ptr == NULL) {
      PSI3_ERROR ("OutputFct: not found");
      err++;
   }
   PSI3_DEBUG ("OutputFct: %s (%p)", method->OutputFct->name,
              method->OutputFct->ptr);

   fclose (method_param_fd);

   if (err) {
      free (method->StopFct);
      free (method->OutputFct);
      free (method);
      return NULL;
   }

   return method;
}

/**
 * \brief      Procedure to run the simulation
 * \details    Run simulation and store updated global variables (e.g state,
 *             trajectory etc)
 *
 * \return     1 if ok
 *             -1 if error
 */
int
simulation (st_general_ptr general, st_simpleForward_ptr method,
            st_model_ptr model, st_simulation_ptr *simulationPtr)
{
   /* States of the system */
   T_state *states;
   struct timeval ti, tf;
   T_event *event;
   int n, j, i;
   int cpt_log_size;
   int printLoadbar = 0;

   st_simulation_ptr simulation;
   simulation = malloc (sizeof (st_simulation));
   simulation->finished = 0;
   *simulationPtr = simulation;

   /* Precompute continuous time -------------------------------------------- */

   /* We pre-compute only if there is a time limit: it allows to reduce the
    * trajectory length. The time will be computed during the simulation anyway
    * to associate a timing to each state (and allocate memory respecting the
    * number of states). */

   st_timing_ptr timing;
   timing = psi3_timing_init (0, method->TimingSeed, model);
   if (!method->UserTimingSeed) { // in order to be printed in print_method
      method->TimingSeed = timing->seed;
   }

   if (! isinf (method->MaxTime)) {

      for (i = 0; i < method->TrajectoryLength; i++) {

         psi3_timing_next (timing);

         if (timing->value >= method->MaxTime
               && ((i + 1) <= method->TrajectoryLength)) {

            method->TrajectoryLength = i + 1;
            PSI3_INFO ("Update TrajectoryLength due to MaxTime constraint");
            PSI3_INFO ("TrajectoryLength: [%d]", method->TrajectoryLength);
            break;
         }
      }

      PSI3_DEBUG ("trajectory time: [%f]", timing->value);
      psi3_timing_reset (timing);
   }

   /* Memory usage analyse -------------------------------------------------- */
   /* Estimate the memory usage to decide if we use the outputThreshold
    * system. */
   long stateMemorySize, trajectoryMemorySize;

   stateMemorySize = (model->queues->nb_queues * sizeof (int)) + sizeof (int *);
   trajectoryMemorySize = (stateMemorySize * method->TrajectoryLength) % LONG_MAX;
   PSI3_DEBUG ("Trajectory memory size: [%ld]", trajectoryMemorySize);

   if (trajectoryMemorySize >= 536870912) { /* 512M */
      method->output_threshold = 536870912 / stateMemorySize;
      PSI3_DEBUG ("OutputThreshold: automatic value [%d]",
                  method->output_threshold);
   }
   else {
      method->output_threshold = method->TrajectoryLength;
   }


   int nbTransitions = method->TrajectoryLength - 1;  /* because of initial
                                                       * state */

   PSI3_INFO ("Memory allocation...", method->output_threshold);

   events_list = (int *) malloc (nbTransitions * sizeof (int));

   /* allocate memory for log_states and states */
   states = (T_state *) malloc (sizeof (T_state));
   states->nb_vectors = 1;
   states->splitting = 0;
   states->queues = (int **) malloc (sizeof (int *));
   states->queues[0] = (int *) malloc (model->queues->nb_queues * sizeof (int));

   timing_states = (double *) malloc (method->output_threshold * sizeof (double));

   log_states = (int **) malloc (method->output_threshold * sizeof (int *));

   for (n = 0; n < method->output_threshold; n++) {
      log_states[n] = (int *) malloc (model->queues->nb_queues * sizeof (int));
      /* in case of allocation failure (lack of memory) */
      if (log_states[n] == NULL) {

         PSI3_ERROR ("Memory allocation failed");

         for (j = 0; j < method->output_threshold; j++) {
            free (log_states[j]);
         }

         for (j = 0; j < n - 1; j++) {
            free (log_states[j]);
         }

         return -1;
      }
   }

   PSI3_INFO ("Memory allocation OK", method->output_threshold);

   memcpy (states->queues[0],
           method->InitialState, model->queues->nb_queues * sizeof (int));
   memcpy (log_states[0],
           method->InitialState, model->queues->nb_queues * sizeof (int));

   timing_states[0] = 0;

   if (method->UserSeed) {      /* if user has given a Seed to reproduce a
                                 * trajectory */
      generator_set_state (0, method->TrajectorySeed);
   }
   else {
      method->TrajectorySeed = calloc (SEED_VECTOR_SIZE, sizeof (unsigned long));
      generator_get_state (0, method->TrajectorySeed);
   }

   /* initialize the events_list */
   PSI3_INFO ("Event generation", method->output_threshold);
   for (n = 0; n < nbTransitions; n++) {
      events_list[n] = genere_evenement (u01 (), model->events->nb_evts);
   }
   PSI3_DEBUG ("Event generation OK", method->output_threshold);

   /* Decide if we print a loadbar ------------------------------------ */
   /* If user has specified a outputfile name we print a load bar to show
    * simulation progression */
   if (strcmp (general->output->outputfilename,
               PSI3_DEFAULT_OUTPUT_FILENAME) != 0) {
      printLoadbar = 1;
   }
   else {
      printLoadbar = 0;
   }

   /* Print info... --------------------------------------------------- */
   FILE *output_fd = general->output_file;

   print_header (output_fd, general, model, method, print_method);

   gettimeofday (&ti, NULL);
   n = 0;
   cpt_log_size = 1;            /* because of initial state */

   if (printLoadbar) loadbar (n, nbTransitions);

   do {

      if (method->output_threshold >= 0
            && cpt_log_size >= method->output_threshold) {
         // write out log_states and fetch to the beginning
         ((T_ptr_outputfct) (method->OutputFct->ptr)) (output_fd,
                                                       log_states,
                                                       cpt_log_size,
                                                       events_list,
                                                       timing_states,
                                                       n,
                                                       nbTransitions,
                                                       simulation);
         // reset cpt_log_size
         cpt_log_size = 0;
      }

      /* 1 . get event from events list */
      event = model->events->evts + events_list[n];

      /* 2 . e(x+1) = Transition(e(x),event) */
      states =
         (((T_ptr_transfct) event->ptr_transition)) (states, event, model);
      method->cpt_transitions++;

      /* 3. Store state into log_states */
      memcpy (log_states[cpt_log_size],
              states->queues[0], model->queues->nb_queues * sizeof (int));

      timing_states [cpt_log_size] = psi3_timing_next (timing);

      cpt_log_size++;


      n++;

      if (printLoadbar) loadbar (n, nbTransitions);

      if (((T_ptr_stopfct) method->StopFct->ptr) (states->queues[0])) {
         simulation->finished = 1;
      }

      if (n >= nbTransitions) {
         simulation->finished = 1;
      }

   } while (! simulation->finished);

   // / write the last part of log_states if cpt_log_size != 0
   if (cpt_log_size > 0) {
      ((T_ptr_outputfct) (method->OutputFct->ptr)) (output_fd,
                                                    log_states,
                                                    cpt_log_size,
                                                    events_list,
                                                    timing_states,
                                                    n,
                                                    nbTransitions,
                                                    simulation);
   }

   gettimeofday (&tf, NULL);
   simulation_time = ((double) ((tf.tv_sec - ti.tv_sec) * 1000.0
                                + (tf.tv_usec - ti.tv_usec) / 1000.0));
   simulation->time = simulation_time;

   psi3_timing_close (timing);

   // FREE
   free (states->queues[0]);
   free (states->queues);
   for (n = 0; n < method->output_threshold; n++) {
      free (log_states[n]);
   }
   free (log_states);
   free (states);
   free (events_list);

   return 1;
}

int
maxState (int dimension, st_model_ptr model)
{
   return model->queues->maxs[dimension];
}

int
minState (int dimension, st_model_ptr model)
{
   return model->queues->mins[dimension];
}

void
setState (T_state *state, int dimension, int value)
{
   state->queues[0][dimension] = value;
}

int
nbState (int dimension, st_model_ptr model)
{
   int distance = 0;
   distance = maxState (dimension, model) - minState (dimension, model) + 1;
   return distance;
}

int
nbEvent (st_model_ptr model)
{
   return model->events->nb_evts;
}

int
dimensionNumber (st_model_ptr model)
{
   return model->queues->nb_queues;
}

int
getStatesSpaceSize (st_model_ptr model)
{
   int dimension;
   int statesSpaceSize = 1;

   for (dimension = 0; dimension < dimensionNumber (model); dimension++) {
      statesSpaceSize = statesSpaceSize * nbState (dimension, model);
   }

   return statesSpaceSize;
}

/**
 * Give a position to a state
 */
int
getPosition (T_state *state, st_model_ptr model)
{
   int position = 0;
   int dimension = 0;
   int multiplicator = 1;

   if (dimensionNumber (model) >= 1) {
      position = state->queues[0][dimension]; /* 1st dimension */
   }
   for (dimension = 1; dimension < dimensionNumber (model); dimension++) {
      multiplicator = multiplicator * nbState (dimension - 1, model);
      position = position + (state->queues[0][dimension] * multiplicator);
   }

   return position;
}

int
multiplicator (int dimensionMax, st_model_ptr model)
{
   int dimension;
   int multiplicator = 1;

   for (dimension = 0; dimension <= dimensionMax; dimension++) {

      multiplicator = multiplicator * nbState (dimension, model);
   }

   return multiplicator;
}

/**
 * Give a state to a position
 */
void
getState (int position, T_state *state, st_model_ptr model)
{
   int dimension;
   int quotient = 0;
   
   for (dimension = dimensionNumber (model) - 1; dimension >= 0; dimension--) {

      quotient = position / multiplicator (dimension - 1, model);

      if (quotient >= 1) {
         position = position - (quotient * multiplicator (dimension - 1, model));
         setState (state, dimension, quotient);
      }
      else {
         setState (state, dimension, 0);
      }
   }
}


void
print_state (T_state *state, st_model_ptr model)
{
   int dimension;

   printf ("state [");
   for (dimension = 0; dimension < dimensionNumber (model); dimension++) {
      printf (" %d", state->queues[0][dimension]);
   }
   printf ("]\n");

}

void
print_matrix (double *matrix, int size)
{
   int i, j;

   printf ("    ");
   for (i = 0; i < size; i++) {
      printf ("%5d", i);
   }
   printf ("\n");

   for (i = 0; i < size; i++) {
      printf ("%4d", i);

      for (j = 0; j < size; j++) {
         if (matrix [(i * size) + j] != 0)
            printf (" %1.2f", matrix [(i * size) + j]);
         else
            printf ("     ");
      }

      printf ("\n");
   }
}

void
spread_probabilities (st_model_ptr model)
{
   int i;
   T_event *event = NULL;
   double sum = 0;

   for (i = 0; i < nbEvent (model); i++) {
      event = model->events->evts + i;
      sum = sum + event->rate;
   }

   for (i = 0; i < nbEvent (model); i++) {
      event = model->events->evts + i;

      event->rate = event->rate / sum;
   }
}


int
generate_matrix (st_general_ptr general, st_simpleForward_ptr method,
                  st_model_ptr model)
{
   /* States of the system */
   T_state *state = NULL;
   T_event *event = NULL;
   double *matrix = NULL;

   int statesSpaceSize = 0;

   PSI3_DEBUG ("----- Model info");
   PSI3_DEBUG ("nbQueues: [%d]", dimensionNumber (model));

   statesSpaceSize = getStatesSpaceSize (model);
   PSI3_DEBUG ("States Space size: [%d]", statesSpaceSize);

   PSI3_DEBUG ("----- Memory initialisation");
   state = (T_state *) malloc (sizeof (T_state));
   state->queues = (int **) malloc (sizeof (int *));
   state->queues[0] = (int *) calloc (dimensionNumber (model), sizeof (int));
   state->nb_vectors = 1;

   matrix = calloc (statesSpaceSize * statesSpaceSize, sizeof (double));

   if (matrix == NULL) {
      PSI3_ERROR ("Matrix allocation failed");
      exit (1);
   }

   PSI3_DEBUG ("matrix: [%p]", matrix);
   PSI3_DEBUG ("event: [%p]", event);
   PSI3_DEBUG ("states: [%p]", state);
   PSI3_DEBUG ("statesSpaceSize: [%d]", statesSpaceSize);

//   PSI3_DEBUG ("----- State tests: to position");
//
//   int i, j;
//   for (i = 0; i <= maxState (0, model); i++) {
//      for (j = 0; j <= maxState (1, model); j++) {
//         state->queues[0][0] = i;
//         state->queues[0][1] = j;
//         print_state (state, model);
//         PSI3_DEBUG ("Position: [%d]", getPosition (state, model));
//      }
//   }
//
//   PSI3_DEBUG ("----- State tests: to state");

//   int etat;
//   for (etat = 0; etat < statesSpaceSize; etat++) {
//      PSI3_DEBUG ("Etat: [%d]", etat);
//      getState (etat, state, model);
//      print_state (state, model);
//   }

   PSI3_DEBUG ("----- Events");
   PSI3_DEBUG ("Nb Events: [%d]", nbEvent (model));

   spread_probabilities (model);

   int etat;
   for (etat = 0; etat < statesSpaceSize; etat++) {
      double sum = 0;
      PSI3_DEBUG ("\nEtat entree: [%d]", etat);
      getState (etat, state, model);
      print_state (state, model);

      int i;
      for (i = 0; i < nbEvent (model); i++) {
         event = model->events->evts + i;
         PSI3_DEBUG ("event: [%p]", event);
         (((T_ptr_transfct) event->ptr_transition)) (state, event, model);
         print_state (state, model);
         PSI3_DEBUG ("Etat sortie: [%d]", getPosition (state, model));

         matrix [(etat * statesSpaceSize) + getPosition (state, model)] = event->rate;
         sum = sum + matrix [(etat * statesSpaceSize) + getPosition (state, model)];
      }

      if (sum != 1) {
         matrix [(etat * statesSpaceSize) + getPosition (state, model)] =
            matrix [(etat * statesSpaceSize) + getPosition (state, model)] + (1 - sum);
      }

   }

   print_matrix (matrix, statesSpaceSize);

   return 0;
}

/**
 * \brief      Main
 * \details    - Load libraries
 *             - Parse config files
 *             - Initialize data structures
 *             - Run simulation & write into output file
 * \return     should return 0 if something hapened wrong
 */
int
main (int argc, char **argv)
{
   st_model_ptr model = NULL;
   st_simpleForward_ptr method = NULL;
   st_general_ptr general = NULL;
   int rc;

   rc = read_configuration (&general,
                            argv[2],
                            &model,
                            argv[1],
                            (void **) &method, argv[3], (void *) read_method);

   if (rc) {                    /* if reading configuration failed */
      close_psi (general, model, method, free_method);
      return 1;
   }

   /**
    * initialize kernel from the model
    */
   init_kernel (model);

   /**
    * Generate the Matrix associated to the model
    */
   generate_matrix (general, method, model);

   /**
    * clean up the system:
    */
   return close_psi (general, model, method, free_method);
}
