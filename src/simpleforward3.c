 /**
 * \file          simpleforward3.c
 * \author        Minh Quan HO
 * \author        Jean-Marc.Vincent@imag.fr
 * \author        Benjamin.Briot@inria.fr
 * \version       1.3.0
 * \date          01/04/2015
 * \brief         Simple forward kernel.
 */

 /* 
  * This library is free software; you can redistribute it and/or
  * modify it under the terms of the GNU Lesser General Public
  * License as published by the Free Software Foundation; either
  * version 2.1 of the License, or any later version.
  *
  * This library is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  * Lesser General Public License for more details.
  *
  * You should have received a copy of the GNU Lesser General Public
  * License along with this library; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
  */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <dlfcn.h>
#include <sys/time.h>
#include <time.h>
#include <generator.h>
#include <parser.h>
#include "psi_common.h"
#include <math.h>
#include <omp.h>


/* for dev purpose */
#define LOADBAR 1                // default is 1
#define TIMING_COMPUTATION 1     // default is 1

/*****************************************************************************/
/*             Global variables used by transition or simulation             */

/* TRANSITION ---------- */
/* Model FIXME: should disapear from this space since it moved to a struct */
/* - Queues: */
int *capmins;                   // capacity min of queues
int *capmaxs;                   // capacity max of queues
int nb_queues;

/* Load sharing */
int ls_mode;
int **node_dist;                /* [LOAD SHARING] matrix of distance between
                                 * two queues : node_dist[i][j] = distance
                                 * between i and j */
struct st_neighbors *node_neighbors;   /* [LOAD SHARING] array containing for
                                        * each node, the neighbors of the node */
int nb_AV;
int FLAG;

/* SIMULATION ---------- */
/* FIXME: will probably move to a struct */
int **log_states;
double *timing_states;
int *events_list;
double simulation_time;

int g_cur_event = 0;

typedef struct st_interval {
   int   id;
   int   length;
   int   from;
   int   *beginState;
   int   **trajectory;
   T_event event;
   FILE  *output;
} st_interval, *st_interval_ptr;

st_interval_ptr g_intervals = NULL;

/*****************************************************************************/

/**
 * \brief      Procedure to initialize some kernel components of the kernel
 *             from a model
 * \details    Initialize global variables from the model
 * \param      model    system model
 * \return     void
 */
void
init_kernel (st_model_ptr model)
{
   /**
    * initialize the random event generator :
    */
   init_generator (model->events->nb_evts, model->events->evts);

   capmins = model->queues->mins;
   capmaxs = model->queues->maxs;
   nb_queues = model->queues->nb_queues;
   FLAG = ALGO_NONMONOTONE;
}

/**
 * \brief      Procedure to free allocated parameters
 * \return     void
 */
void
free_method (st_method_ptr method_ptr)
{
   st_simpleForward3_ptr method = method_ptr;

   if (method_ptr) {
      method = method_ptr;
   }
   else
      return;

   if (method->InitialState != NULL) {
      free (method->InitialState);
   }
   if (method->StopFct != NULL) {
      free (method->StopFct);
   }
   if (method->OutputFct != NULL) {
      free (method->OutputFct);
   }
   free (method);
}

/**
 * \brief      Procedure to print method parameters to the outout file
 * \param      output_file    file descriptor to the output file
 * \param      method_ptr     method
 * \return     void
 */
void
print_method (FILE * output_file, st_method_ptr method_ptr)
{
   st_simpleForward3_ptr method = (st_simpleForward3_ptr) method_ptr;

   fprintf (output_file, "# Method: simpleforward3\n");
   fprintf (output_file, "#\tTrajectoryLength: %d\n", method->TrajectoryLength);
   fprintf (output_file, "#\tNumThreads: %d\n", method->NumThreads);
   fprintf (output_file, "#\tGranularity: %d\n", method->Granularity);
   fprintf (output_file, "#\tMaxTime: %f\n", method->MaxTime);
   fprintf (output_file, "#\tTiming: %d\n", method->Timing);

   int elem_vector;
   fprintf (output_file, "#\tTrajectorySeed: [%lu", method->TrajectorySeed[0]);
   for (elem_vector = 1; elem_vector < SEED_VECTOR_SIZE; elem_vector++) {
      fprintf (output_file, ", %lu", method->TrajectorySeed[elem_vector]);
   }
   fprintf (output_file, "]\n");

   fprintf (output_file, "#\tTimingSeed: [%lu", method->TimingSeed[0]);
   for (elem_vector = 1; elem_vector < SEED_VECTOR_SIZE; elem_vector++) {
      fprintf (output_file, ", %lu", method->TimingSeed[elem_vector]);
   }
   fprintf (output_file, "]\n");

   fprintf (output_file, "#\tInitialState: [");
   print_list_int (output_file, method->InitialState, method->InitialState_size);
   fprintf (output_file, "]\n");
}

/**
 * \brief      Procedure to init the method structure
 * \return     a pointer to a method structure
 *             NULL if failed
 */
st_simpleForward3_ptr
init_method ()
{
   st_simpleForward3_ptr method = (st_simpleForward3_ptr)
      calloc (1, sizeof (st_simpleForward3));
   if (method == NULL)
      return NULL;
   method->StopFct = (st_function_ptr) calloc (1, sizeof (st_function));
   if (method->StopFct == NULL)
      return NULL;
   method->OutputFct = (st_function_ptr) calloc (1, sizeof (st_function));
   if (method->OutputFct == NULL)
      return NULL;

   method->TrajectoryLength = 0;
   method->MaxTime = -1;
   method->Granularity = 1;
   method->NumThreads = 1;
   method->InitialState = NULL;
   method->Timing = 1;
   method->TimingSeed = NULL;
   method->InitialState_size = 0;
   method->TrajectorySeed = NULL;
   method->UserSeed = 0;
   method->output_threshold = 0;
   method->cpt_transitions = 0;

   return method;
}

/**
 * \brief      Function to read an initialize method simulation parameters
 * \param      method_param_cfgfile    method config file name
 * \param      general                 pointer to the general structure
 * \param      model                   pointer to the model structure
 * \return     a pointer to a initialized method structure
 *             NULL if failed
 */
st_simpleForward3_ptr
read_method (char *method_param_cfgfile,
             st_general_ptr general, st_model_ptr model)
{
   int err = 0;
   int rc = 0;
   FILE *method_param_fd;

   PSI3_DEBUG ("Opening method parameters file: %s",
         method_param_cfgfile);

   method_param_fd = fopen (method_param_cfgfile, "r");
   if (method_param_fd == NULL) {
      PSI3_ERROR ("Fail to open file %s\n", method_param_cfgfile);
      return NULL;
   }

   st_simpleForward3_ptr method = init_method ();

   /**
    * Get TrajectoryLength
    */
   method->TrajectoryLength =
      get_int_keyname (method_param_fd, "TrajectoryLength");
   if (method->TrajectoryLength <= 0) {
      PSI3_ERROR ("TrajectoryLength must be positive");
      err++;
   }
   else {

   }
   PSI3_DEBUG ("TrajectoryLength: [%d]", method->TrajectoryLength);

   /**
    * Get NumThreads
    */
   rc = get_int_keyname (method_param_fd, "NumThreads");
   PSI3_DEBUG ("GET NUMTHREADS: [%d]", rc);
   if (rc <= 0
         || rc == INT_MIN) { /* we let OMP decide: we get the
                              * OMP number of threads in
                              * parallel region (otherwise this
                              * number is 1) */
#pragma omp parallel
      {
         method->NumThreads = omp_get_num_threads ();
      }
      PSI3_DEBUG ("NumThreads: automatic value [%d]", method->NumThreads);
   }
   else {
      method->NumThreads = rc;
      PSI3_DEBUG ("NumThreads: %d", method->NumThreads);
   }

   /**
    * Get Granularity: depends on TrajectoryLength and NumThreads
    */
   rc = get_int_keyname (method_param_fd, "Granularity");
   PSI3_DEBUG ("GET GRANULARITY: [%d]", rc);
   if (rc <= 0) {
      method->Granularity = method->NumThreads;
      PSI3_DEBUG ("Granularity: automatic value [%d]", method->NumThreads);
   }
   else if (rc > method->TrajectoryLength) {
         method->Granularity = method->TrajectoryLength;
         PSI3_WARNING ("Granularity: > TrajectoryLength: granularity set to [%d]", method->Granularity)
   }
   else {
      method->Granularity = rc;
   }

   /**
    * Get MaxTime
    */
   method->MaxTime = get_double_keyname (method_param_fd, "MaxTime");

   if (method->MaxTime > 0) {
      PSI3_DEBUG ("MaxTime: [%f]", method->MaxTime);
   }
   else {
      method->MaxTime = INFINITY;
      PSI3_DEBUG ("MaxTime: automatic value [%f] (should be >= 0)", method->MaxTime);
   }

   /**
    * Get initial state(s)
    */
   get_int_list_keyname (method_param_fd,
                         "InitialState",
                         &(method->InitialState), &(method->InitialState_size));

   if (method->InitialState != NULL) { // when user enter an <int> list

      if (method->InitialState_size != model->queues->nb_queues) {
         /* check the size consistency between this list and number of queues */
         fprintf (stderr,
                  " /!\\ : Number of initial states doesn't match with number of "
                  "queues : begin_size = %d - nb_queues = %d\n",
                  method->InitialState_size, model->queues->nb_queues);
         // Missmatch : clear what have been created in this function:
         free (method->InitialState);
         err++;
      }
   }
   else {
      method->InitialState = random_int_list (model->queues->nb_queues,
                                              model->queues->maxs,
                                              model->queues->mins,
                                              general->seed);
      method->InitialState_size = model->queues->nb_queues;
   }

#if (LOG_LVL >= LOG_DEBUG)
   int i = 0;
   fprintf (stderr, "INFO: InitialState: [");
   for (; i < model->queues->nb_queues;
        fprintf (stderr, " %d", method->InitialState[i++]));
   fprintf (stderr, "]\n");
#endif

   /**
    * Get TrajectorySeed: to reprocude a trajectory
    */
   rc = get_unsigned_long_list_keyname (method_param_fd,
                                        "TrajectorySeed",
                                        &(method->TrajectorySeed),
                                        SEED_VECTOR_SIZE);

   if (rc == 0) {
      method->UserSeed = 1;
      PSI3_DEBUG ("TrajectorySeed: [%lu, %lu, %lu, %lu, %lu, %lu]",
            method->TrajectorySeed[0],
            method->TrajectorySeed[1],
            method->TrajectorySeed[2],
            method->TrajectorySeed[3],
            method->TrajectorySeed[4],
            method->TrajectorySeed[5]);
   }
   else if (rc == -3) {
      PSI3_ERROR ("TrajectorySeed: incorrect size: vector should be size %d",
                  SEED_VECTOR_SIZE);
      err++;
   }
   else if (rc == -1) {
      PSI3_ERROR ("TrajectorySeed: error parsing vector");
      err++;
   }
   else { // (rc == -2) of "~"
      // RANDOM
      PSI3_DEBUG ("TrajectorySeed: Random");
   }

   /**
    * Get Timing: FIXME: impact the simulation
    */
   rc = get_bool_keyname (method_param_fd, "Timing");
   if (rc != 0) { /* Yes by default */
      method->Timing = 1;
   }
   else {
      method->Timing = 0;
   }
   PSI3_DEBUG ("Timing: %d", method->Timing);

   /**
    * Get TimingSeed: to reprocude a trajectory
    */
   rc = get_unsigned_long_list_keyname (method_param_fd,
                                        "TimingSeed",
                                        &(method->TimingSeed),
                                        SEED_VECTOR_SIZE);

   if (rc == 0) {
      method->UserTimingSeed = 1;
      PSI3_DEBUG ("TimingSeed: [%lu, %lu, %lu, %lu, %lu, %lu]",
            method->TimingSeed[0],
            method->TimingSeed[1],
            method->TimingSeed[2],
            method->TimingSeed[3],
            method->TimingSeed[4],
            method->TimingSeed[5]);
   }
   else if (rc == -3) {
      PSI3_ERROR ("TimingSeed: incorrect size: vector should be size %d",
                  SEED_VECTOR_SIZE);
      err++;
   }
   else if (rc == -1) {
      PSI3_ERROR ("TimingSeed: error parsing vector");
      err++;
   }
   else { // (rc == -2) of "~"
      // RANDOM
      PSI3_DEBUG ("TimingSeed: Random");
   }

   /**
    * Get Stop Function
    */
   method->StopFct->name = get_string_keyname (method_param_fd, "StopFct");
   if (method->StopFct->name == NULL) {
      method->StopFct->name = "Default$stop";
      PSI3_DEBUG ("StopFct: automatic default value (%s)",
                 method->StopFct->name);
   }
   method->StopFct->ptr = (T_ptr_stopfct) get_ptr_fct (general->default_lib,
                                                       general->user_lib,
                                                       method->StopFct->name);
   if (method->StopFct->ptr == NULL) {
      PSI3_ERROR ("StopFct: not found");
      err++;
   }
   PSI3_DEBUG ("StopFct: %s (%p)", method->StopFct->name, method->StopFct->ptr);

   /**
    * Get Output Function
    */
   method->OutputFct->name = get_string_keyname (method_param_fd, "OutputFct");
   if (method->OutputFct->name == NULL) {
      method->OutputFct->name = "Default$output_SFP_timing";
      PSI3_DEBUG ("OutputFct: automatic default value (%s)",
                 method->OutputFct->name);
   }
   method->OutputFct->ptr = (T_ptr_stopfct) get_ptr_fct (general->default_lib,
                                                         general->user_lib,
                                                         method->OutputFct->
                                                         name);
   if (method->OutputFct->ptr == NULL) {
      PSI3_ERROR ("OutputFct: not found");
      err++;
   }
   PSI3_DEBUG ("OutputFct: %s (%p)", method->OutputFct->name,
              method->OutputFct->ptr);

   fclose (method_param_fd);

   if (err) {
      free (method->StopFct);
      free (method->OutputFct);
      free (method);
      return NULL;
   }

   return method;
}

void
init_intervals (int trajectoryLength, int granularity, int *beginState)
{
   PSI3_DEBUG ("Intervals initialisation");

   int intervalId, i, length_avg, length_rest;

   g_intervals = (st_interval_ptr) malloc (granularity * sizeof (st_interval));

   length_avg = (int) (trajectoryLength / granularity);
   length_rest = trajectoryLength % granularity;   // for the last interval

   for (intervalId = 0; intervalId < granularity; intervalId++) {

      g_intervals[intervalId].id = intervalId;
      g_intervals[intervalId].from = length_avg * intervalId;

      if (intervalId == (granularity - 1)) {
         g_intervals[intervalId].length = length_avg + length_rest;
      }
      else {
         g_intervals[intervalId].length = length_avg;
      }

      g_intervals[intervalId].beginState = malloc (nb_queues * sizeof (int));

      /* Interval sub trajectory allocation */
      g_intervals[intervalId].trajectory =
         malloc (g_intervals[intervalId].length * sizeof (int *));

      /* first state of the sub trajectory is the begin state of the
       * trajectory: this is made like this because the first who will write
       * this part of memory is the thread creating the task (TODO: write a
       * good explanation) */
//      g_intervals[intervalId].trajectory[0] = g_intervals[intervalId].beginState;

      /* Here we let malloc to let the thread first touching the page
       * triggering the allocation
       * TODO: check that allocation is alligned (necessary?) */
      for (i = 0; i < g_intervals[intervalId].length; i++) {
         g_intervals[intervalId].trajectory[i] =
            malloc (nb_queues * sizeof (int));
      }

      /* BeginState is set for the first interval */
      if (intervalId == 0) {
         memcpy (g_intervals[intervalId].beginState,
                 beginState,
                 nb_queues * sizeof (int));

      }

      g_intervals[intervalId].output = psi3_get_output (intervalId);

      PSI3_DEBUG ("\tInterval [%d] from [%d] length [%d] beginState [%d, ..., %d]@%p@%p output [%d]",
                  g_intervals[intervalId].id,
                  g_intervals[intervalId].from,
                  g_intervals[intervalId].length,
                  g_intervals[intervalId].beginState[0],
                  g_intervals[intervalId].beginState[nb_queues - 1],
                  g_intervals[intervalId].beginState,
                  g_intervals[intervalId].trajectory[0],
                  g_intervals[intervalId].output);
   }
}

void
compute_interval (st_interval_ptr interval,
                  st_model_ptr model,
                  st_simulation_ptr simulation,
                  st_simpleForward3_ptr method,
                  st_timing_ptr timing)
{
   PSI3_DEBUG ("Thread [%d] START interval [%d] beginState [%d, ..., %d]",
               omp_get_thread_num(),
               interval->id,
               interval->beginState[0],
               interval->beginState[nb_queues - 1]);

   unsigned long iteration = 0; // 0 is beginState FIXME
   unsigned long simulationIteration = interval->from; 
   T_state *states;
   T_event *event;

   states = (T_state *) malloc (sizeof (T_state));
   states->nb_vectors = 1;
   states->splitting = 0;
   states->queues = (int **) malloc (sizeof (int *));
   states->queues[0] = (int *) malloc (model->queues->nb_queues * sizeof (int));

   memcpy (states->queues[0],
           interval->beginState,
           model->queues->nb_queues * sizeof (int));

   if (simulationIteration == 0) { // 1st interval, trajO = beginState 

      memcpy (interval->trajectory[0],
              interval->beginState,
              model->queues->nb_queues * sizeof (int));

      iteration = 1;
   }

   PSI3_DEBUG ("Thread [%d] iteration [%d] simulationIteration [%d] length [%d]",
               omp_get_thread_num(),
               iteration,
               simulationIteration,
               interval->length);

   /**********/
   while ((iteration < interval->length)
           && (((T_ptr_stopfct) method->StopFct->ptr) (states->queues[0]) == 0)) {

      /* 1. get event from events list */
      event = model->events->evts + events_list[g_cur_event];

      /* 2. e(x+1) = Transition(e(x),event) */
      states =
         (((T_ptr_transfct) event->ptr_transition)) (states, event, model);

      method->cpt_transitions++;

      /* 3. Store state into log_states */
      memcpy (interval->trajectory[iteration],
              states->queues[0],
              model->queues->nb_queues * sizeof (int));

#if TIMING_COMPUTATION
      timing_states [g_cur_event + 1] = psi3_timing_next (timing);
#endif

//      cpt_log_size++; FIXME
      iteration++;
      simulationIteration++; // FIXME: maybe compressed
      g_cur_event++;

#if LOADBAR
#pragma omp critical
      {   
         if (simulation->printLoadbar) loadbar (simulationIteration, simulation->length);
      }   
#endif
   }
   /**********/

   PSI3_DEBUG ("Thread [%d] iteration [%d]",
               omp_get_thread_num(),
               iteration);
   PSI3_DEBUG ("Thread [%d] END computation on state [%d, ..., %d]",
               omp_get_thread_num(),
               interval->trajectory[iteration - 1][0],
               interval->trajectory[iteration - 1][nb_queues - 1]);

   if (interval->id >= (method->Granularity - 1)) {
      PSI3_DEBUG ("Thread [%d] last interval", omp_get_thread_num());
      simulation->finished = 1;
   }
   else {
      PSI3_DEBUG ("Thread [%d] copy to interval [%d]@%p",
                  omp_get_thread_num(),
                  interval->id + 1,
                  g_intervals[interval->id + 1].beginState);
      /* copy the last state as begin state of next interval */
      memcpy (g_intervals[interval->id + 1].beginState,
              states->queues[0],
              model->queues->nb_queues * sizeof (int));

#pragma omp task
      {
         compute_interval (g_intervals + (interval->id + 1),
                           model,
                           simulation,
                           method,
                           timing);
      }
   }

   PSI3_DEBUG ("Thread [%d] output interval [%d]@%p trajectory @%p from [%d] length [%d]",
               omp_get_thread_num(),
               interval->id,
               g_intervals[interval->id].beginState,
               interval->trajectory,
               (interval->id * interval->length),
               interval->length);

   /* Output data */
   ((T_ptr_outputfct) (method->OutputFct->ptr))
      (interval->output,
       interval->trajectory,
       events_list,
       interval->from,
       interval->length,
       timing_states);

   PSI3_DEBUG ("Thread [%d] END interval [%d]", omp_get_thread_num(), interval->id);
}

/**
 * \brief      Procedure to run the simulation
 * \details    Run simulation and store updated global variables (e.g state,
 *             trajectory etc)
 *
 * \return     1 if ok
 *             -1 if error
 */
int
simulation (st_general_ptr general,
            st_simpleForward3_ptr method,
            st_model_ptr model)
{
   /* States of the system */
   T_state *states;
   struct timeval ti, tf;
//   T_event *event;
   int n, i;
//   int cpt_log_size;
//   int printLoadbar = 0;

   st_simulation_ptr simulation;
   simulation = malloc (sizeof (st_simulation));
   simulation->finished = 0;

   /* Precompute continuous time -------------------------------------------- */

   /* We pre-compute only if there is a time limit: it allows to reduce the
    * trajectory length. The time will be computed during the simulation anyway
    * to associate a timing to each state (and allocate memory respecting the
    * number of states). */

   st_timing_ptr timing;
   timing = psi3_timing_init (0, method->TimingSeed, model);
   if (!method->UserTimingSeed) { // in order to be printed in print_method
      method->TimingSeed = timing->seed;
   }

   if (! isinf (method->MaxTime)) { // MaxTime set to infinity means no Maxtime

      for (i = 0; i < method->TrajectoryLength; i++) {

         psi3_timing_next (timing);

         if (timing->value >= method->MaxTime
               && ((i + 1) <= method->TrajectoryLength)) {

            method->TrajectoryLength = i + 1;
            PSI3_INFO ("Update TrajectoryLength due to MaxTime constraint");
            PSI3_INFO ("TrajectoryLength: [%d]", method->TrajectoryLength);
            break;
         }
      }

      PSI3_DEBUG ("trajectory time: [%f]", timing->value);
      psi3_timing_reset (timing);
   }

   /* Memory usage analyse -------------------------------------------------- */
   /* Estimate the memory usage to decide if we use the outputThreshold
    * system. */
//   long stateMemorySize, trajectoryMemorySize;
//
//   stateMemorySize = (model->queues->nb_queues * sizeof (int)) + sizeof (int *);
//   trajectoryMemorySize = (stateMemorySize * method->TrajectoryLength) % LONG_MAX;
//   PSI3_DEBUG ("Trajectory memory size: [%ld]", trajectoryMemorySize);

   /* FIXME: for now we consider we have enough memory to store all the trajectory */
//   if (trajectoryMemorySize >= 536870912) { /* 512M */
//      method->output_threshold = 536870912 / stateMemorySize;
//      PSI3_DEBUG ("OutputThreshold: automatic value [%d]",
//                  method->output_threshold);
//   }
//   else {
      method->output_threshold = method->TrajectoryLength;
//   }

   if (method->UserSeed) {      /* if user has given a Seed to reproduce a
                                 * trajectory */
      generator_set_state (0, method->TrajectorySeed);
   }
   else {
      method->TrajectorySeed = calloc (SEED_VECTOR_SIZE, sizeof (unsigned long));
      generator_get_state (0, method->TrajectorySeed);
   }

   print_header (general->output_file, general, model, method, print_method);

   psi3_output_set_multistream (general, method->Granularity);


   init_intervals (method->output_threshold,
                   method->Granularity,
                   method->InitialState); // FIXME: initialState


   int nbTransitions = method->TrajectoryLength - 1;  /* because of initial
                                                       * state */
   simulation->length = nbTransitions;

   PSI3_DEBUG ("Memory allocation...", method->output_threshold);

   events_list = (int *) malloc (nbTransitions * sizeof (int));

   /* allocate memory for log_states and states */
   states = (T_state *) malloc (sizeof (T_state));
   states->nb_vectors = 1;
   states->splitting = 0;
   states->queues = (int **) malloc (sizeof (int *));
   states->queues[0] = (int *) malloc (model->queues->nb_queues * sizeof (int));

   timing_states = (double *) malloc (method->output_threshold * sizeof (double));

   PSI3_DEBUG ("Memory allocation OK", method->output_threshold);

   memcpy (states->queues[0],
           method->InitialState, model->queues->nb_queues * sizeof (int));

   timing_states[0] = 0;

   /* initialize the events_list */
   PSI3_DEBUG ("Event generation", method->output_threshold);
   for (n = 0; n < nbTransitions; n++) {
      events_list[n] = genere_evenement (u01 (), model->events->nb_evts);
   }
   PSI3_DEBUG ("Event generation OK", method->output_threshold);

   /* Decide if we print a loadbar ------------------------------------ */
   /* If user has specified a outputfile name we print a load bar to show
    * simulation progression */
#if LOADBAR
   if (strcmp (general->output->outputfilename,
               PSI3_DEFAULT_OUTPUT_FILENAME) != 0) {
      simulation->printLoadbar = 1;
   }
   else {
      simulation->printLoadbar = 0;
   }
#endif

   gettimeofday (&ti, NULL);

#pragma omp parallel
   {
#pragma omp master
      {
#pragma omp task
         {
            compute_interval (g_intervals,
                              model,
                              simulation,
                              method,
                              timing);
         }
      }
   }

   gettimeofday (&tf, NULL);
   simulation_time = ((double) ((tf.tv_sec - ti.tv_sec) * 1000.0
                                + (tf.tv_usec - ti.tv_usec) / 1000.0));
   simulation->time = simulation_time;
   psi3_timing_close (timing);

   psi3_output_concatenate ();

   print_footer (general, model, method, simulation->time);

   print_time (stderr, simulation->time);

   print_perf (stderr, general, simulation);


//
//   // FREE
//   free (states->queues[0]);
//   free (states->queues);
//   for (n = 0; n < method->output_threshold; n++) {
//      free (log_states[n]);
//   }
//   free (log_states);
//   free (states);
//   free (events_list);

   return 1;
}

/**
 * \brief      Main
 * \details    - Load libraries
 *             - Parse config files
 *             - Initialize data structures
 *             - Run simulation & write into output file
 * \return     should return 0 if something hapened wrong
 */
int
main (int argc, char **argv)
{
   st_model_ptr model = NULL;
   st_simpleForward3_ptr method = NULL;
   st_general_ptr general = NULL;
   int rc;

   rc = read_configuration (&general,
                            argv[2],
                            &model,
                            argv[1],
                            (void **) &method, argv[3], (void *) read_method);

   if (rc) {                    /* if reading configuration failed */
      close_psi (general, model, method, free_method);
      return 1;
   }

   /**
    * initialize kernel from the model
    */
   init_kernel (model);

   /**
    * Run the simulation and write the trajectories into output file:
    */
   simulation (general, method, model);

   /**
    * clean up the system:
    */
   return close_psi (general, model, method, free_method);
}
