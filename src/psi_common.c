/**
 * \file        common.c
 * \author      Florian LEVEQUE
 * \author      Jean-Marc.Vincent@imag.fr 
 * \author      Vincent Danjean <Vincent.Danjean@ens-lyon.org>
 * \version     1.0.0
 * \date        21/03/2013
 * \brief       Common functions for all programs. 
 */

 /* 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "psi_common.h"
#include <parser.h>
#include <generator.h>

#define PSI3_DEFAULT_NAME "libpsi3_default"
#define PSI3_DEFAULT_VERSION "1"
#define PSI3_USER_NAME "libuser"

/**
 * \brief       Procedure to close opened file and libraries
 * \param    default_lib         pointeur of the default library.
 * \param    user_lib	         pointeur of the user library.
 * \param    f			opened file
 * \return   void.
 */
void close_prog (void *default_lib, void *user_lib, st_general_ptr general)
{
#if (DEBUG_LVL) // keep the library opened for callgrind profiling
   if (default_lib != NULL)   dlclose(default_lib);
   if (user_lib != NULL)      dlclose(user_lib);
#endif
   if (general->output_file != NULL) { /* file is opened */
      long fileSize;
      fseek (general->output_file, 0, SEEK_END);
      fileSize = ftell (general->output_file); /* FIXME: seems to be a
                                                * dangerous method if file is
                                                * more than 4gb */
      if (fileSize == 0) {
         remove (general->output->outputfilename);
         PSI3_DEBUG ("Remove file: [%s]", general->output->outputfilename);
      }
      else {
         fclose (general->output_file);
      }
   }
   else {
      PSI3_DEBUG ("File [%s] already closed", general->output->outputfilename);
   }
}

int close_lib(void *default_lib, void *user_lib)
{
  if(default_lib != NULL)  dlclose(default_lib);
  if(user_lib != NULL)     dlclose(user_lib);

  return 0;
}


void loadlibs (void **default_lib, void **user_lib)
{
#if defined(__linux__)
#  define PSI3_DEFAULT_LIBNAME PSI3_DEFAULT_NAME ".so." PSI3_DEFAULT_VERSION
#  define PSI3_USER_LIBNAME PSI3_USER_NAME ".so"
#elif defined(__APPLE__)
#  define PSI3_DEFAULT_LIBNAME PSI3_DEFAULT_NAME ".dylib"
#  define PSI3_USER_LIBNAME PSI3_USER_NAME ".dylib"
#else
#  error System not supported
#endif

#if (DEBUG_LVL >= DEBUG_IO)
   fprintf(stderr, "Loading %s\n", PSI3_DEFAULT_LIBNAME);
#endif

   *default_lib = dlopen (PSI3_DEFAULT_LIBNAME, RTLD_LAZY);

   if (*default_lib == NULL) {
      fprintf (stderr, "Default library %s not found.\n%s\n",
                       PSI3_DEFAULT_LIBNAME,
                       dlerror ());
      close_prog (*default_lib, NULL, NULL);
      return;
   }

#if (DEBUG_LVL >= DEBUG_IO)
   fprintf (stderr, "Loading %s\n", PSI3_USER_LIBNAME);
#endif

   *user_lib = dlopen (PSI3_USER_LIBNAME, RTLD_LAZY);

/** TODO: check how libuser should be handled
 *   if(*user_lib == NULL) {
 *      fprintf (stderr, "User library %s not found.\n%s\n",
 *             PSI3_USER_LIBNAME,
 *             dlerror());
 *      close_prog(*default_lib, user_lib, NULL);
 *      return;
 *   }
 */


   return;
}

/**
 * \brief      Procedure to print the simulation time in a stream  (stdout, a
 *             file ...)
 *
 * \param      stream_out     the stream where the parameters are printed
 * \return     void
 */
void print_time (FILE *output, double simulation_time)
{
   fprintf(output,
           "\n# Total simulation time: %.2f milli-seconds\n",
           simulation_time);
}

void print_perf (FILE *output,
                 st_general_ptr general,
                 st_simulation_ptr simulation)
{
   if (general->output->PrintPerf) {
      fprintf (output, "PERF: time: %.2f\n", simulation->time);
   }
}

void print_header (FILE *output_stream,
                   st_general_ptr general,
                   st_model_ptr model,
                   st_method_ptr method,
                   void (*print_method) (FILE *, st_method_ptr))
{

   print_info (output_stream);

   if (general->output->showModel) {
      print_model (output_stream, *model); /* FIXME: remove ref */
   }

   if (general->output->showParam) {
      print_param (output_stream, general);
      print_method (output_stream, method);
   }

//   fprintf (output_stream, "#\n# Output data:\n");

}

void print_footer (st_general_ptr general,
                   st_model_ptr model,
                   st_method_ptr method,
                   double simulation_time) /* FIXME: simulation struct */
{

   if (general->output->showsimulTime) {
      print_time (general->output_file, simulation_time);
   }
}

void print_info (FILE *output)
{
   fprintf (output, "# PSI3 INFO:\n");
   fprintf (output, "# version: %s\n", PSI3_VERSION);
   fprintf (output, "# build type: %s\n", PSI3_BUILD_TYPE);
   fprintf (output, "# C compiler: %s\n", PSI3_COMPILER);
   fprintf (output, "# compiler options: %s\n", PSI3_C_FLAGS);
   fprintf (output, "\n");
}

void print_param (FILE *fd, st_general_ptr general)
{
   fprintf(fd, "# General Param:\n");

   if (general->seed == -1) {
      fprintf(fd, "#\tSeed: Not set\n");
   }
   else {
      fprintf(fd, "#\tSeed: %d\n", general->seed);
   }
}

int read_configuration (st_general_ptr *general,
                        char *general_file,
                        st_model_ptr *model,
                        char *model_file,
                        st_method_ptr *method,
                        char *method_file,
                        st_method_ptr (*read_method) (char *, st_general_ptr, st_model_ptr))
{

   /**
    * read general parameters file
    */
   *general = read_general_param (general_file);
   if (*general == NULL) {
      fprintf(stderr, "Problem in general parameters file: %s\n", general_file);
      return 1;
   }

   /**
    * Init the Rng (FIXME: should not be here)
    */
   srandom ((*general)->seed); /* FIXME: will disapear */
   generator_seed ((*general)->seed);

   /**
    * Load dynamic libraries
    */
   loadlibs (&((*general)->default_lib), &((*general)->user_lib));

   /**
    * Read model:
    */
   *model = read_model (model_file, (*general)->default_lib, (*general)->user_lib);
   if (*model == NULL) {
      fprintf(stderr, "Problem in model file: %s\n", model_file);
      return 1;
   }

   /**
    * read method parameters file
    */
   *method = (void *) read_method (method_file, *general, *model);
   if (*method == NULL) {
      fprintf(stderr, "Problem in method file: %s\n", method_file);
      return 1;
   }

   return 0;
}

int close_psi (st_general_ptr general,
           st_model_ptr model,
           st_method_ptr method,
           void (*free_method) (st_method_ptr))
{
   close_prog (general->default_lib, general->user_lib, general);
   free_general_param (general);
   free_model (model);
   free_method (method);
   free_generator ();

   return 0;
}

/**
 * \brief       Procedure to show a progress bar of simulation
 * \param    x         the current index we are on.
 * \param    n         the number of indicies to process.
 * \param    r         the number of times we want to update the display.
 * \param    w         the width of the bar.
 * \return   void.
 */
inline void loadBar (int x, int n, int r, int w)
{
   // Calculuate the ratio of complete-to-incomplete.
   float ratio = x/(float)n;
   int   c     = ratio * w;
   int i =0;
   // Only update r times.
   if ( n/r > 0 ){
      if ( x % (n/r) == 0 ){
         // Show the percentage complete.
         fprintf (stderr, "%3d%% [", (int)(ratio*100) );
         // Show the load bar.
         for (i=0; i<c; i++)
            fprintf (stderr, "=");
         for (i=c; i<w; i++)
            fprintf (stderr, " ");
         // ANSI Control codes to go back to the
         // previous line and clear it.
         fprintf (stderr, "]\n\033[F\033[J");
      }
   }
}

#define LOADBAR_BUFFER_SIZE 100
#define LOADBAR_SIZE 20

inline void loadbar (int iteration, int iterationNumber)
{
  //  fprintf (stdout, "LOADBAR: iteration [%d] iterationNumber [%d]\n", iteration, iterationNumber);
   /* we print load bar only 100 times */
  if (((iterationNumber / 100 != 0)
       && (iteration % (iterationNumber / 100) != 0)
       && (iteration != iterationNumber))
      || (iterationNumber <= 0)) { /* condition order matters */
       return;
  }

   char loadbar [LOADBAR_BUFFER_SIZE];
   char *buffer_pos;

   float progress = iteration / (float) (iterationNumber);

   int bar;
   int barNb = progress * LOADBAR_SIZE;

   int rc;

   /* go back to the beginning of the line */
   fprintf (stdout, "\r");

   rc = sprintf (loadbar, "%3d%% [", (int) (progress * 100));

   buffer_pos = (char *) (loadbar + rc);

   for (bar = 0; bar < barNb; bar ++) {
      sprintf (buffer_pos, "=");
      buffer_pos++;
   }
   for (bar = barNb; bar < LOADBAR_SIZE; bar++) {
      sprintf (buffer_pos, " ");
      buffer_pos++;
   }

   sprintf (buffer_pos, "]");

   fprintf (stdout, "%s", loadbar);

   fflush (stdout);

   if (iteration == iterationNumber) /* if finished */
      fprintf (stdout, "\n");
}

double
delta_ms (struct timeval t1, struct timeval t2)
{
   return ((double)
      ((t2.tv_sec - t1.tv_sec) * 1000.0 + (t2.tv_usec - t1.tv_usec) / 1000.0));
}


/**
 * \brief      Function to initialize general simulation parameters
 * \param      param_file    Pointer to parameter config file
 * \param      default_lib    pointeur of the default library.
 * \param      user_lib    pointeur of the user library.
 * \return     0 when all parameters are set correctly, -1 in other case
 */
st_general_ptr
read_general_param (char *general_param_cfgfile)
{
   int err = 0;
   FILE *general_param_fd = NULL;
   char *temp;

   PSI3_DEBUG ("Opening general parameters file: %s", general_param_cfgfile);

   general_param_fd = fopen (general_param_cfgfile, "r");
   if (general_param_fd == NULL) {
      fprintf (stderr, "Fail to open file %s\n", general_param_cfgfile);
      return NULL;
   }

   st_general_ptr general = init_general_param ();

   /**
    * Get seed
    */
   general->seed = get_int_keyname (general_param_fd, "GeneralSeed");
   PSI3_DEBUG ("GOT GENERAL SEED: [%d]", general->seed);
   if (general->seed >= 0) {
      PSI3_DEBUG ("GeneralSeed: %d", general->seed);
   }
   else { /* case "~", nothing... */
      general->seed = time (NULL);
      PSI3_DEBUG ("GeneralSeed: %d (generated)", general->seed);
   }

   /**
    * Get Output filename and open it
    */
   general->output->outputfilename = get_string_keyname (general_param_fd,
                                                         "OutputFileName");
   if (general->output->outputfilename == NULL) {
      /* LENGTH + '\0' */
      general->output->outputfilename = malloc (strlen (PSI3_DEFAULT_OUTPUT_FILENAME) + 1);
      strncpy (general->output->outputfilename, PSI3_DEFAULT_OUTPUT_FILENAME,
               strlen (PSI3_DEFAULT_OUTPUT_FILENAME) + 1);
      general->output_file = PSI3_DEFAULT_OUTPUT_FD;
   }
   else {
      general->output_file = fopen (general->output->outputfilename, "w");
   }
   PSI3_DEBUG ("OutputFileName: %s", general->output->outputfilename);

   if (general->output_file == NULL) {
      fprintf (stderr,
               "Fail to open file %s\n", general->output->outputfilename);
      return NULL;
   }

   /**
    * Get Print Model
    */
   temp = get_string_keyname (general_param_fd, "PrintModel");
   if (temp == NULL) {
      general->output->showModel = 1;
   }
   else {
      general->output->showModel = !strcmp (temp, "Yes");   // Yes = 1, No = 0
      free (temp);
   }
   PSI3_DEBUG ("PrintModel: %d", general->output->showModel);

   /**
    * Get Print Param
    */
   temp = get_string_keyname (general_param_fd, "PrintParam");
   if (temp == NULL) {
      general->output->showParam = 1;
   }
   else {
      general->output->showParam = !strcmp (temp, "Yes");   // Yes = 1, No = 0
      free (temp);
   }
   PSI3_DEBUG ("PrintParam: %d", general->output->showParam);

   /**
    * Get Print SimulationTime
    */
   temp = get_string_keyname (general_param_fd, "PrintSimulationTime");
   if (temp == NULL) {
      general->output->showsimulTime = 1;
   }
   else {
      general->output->showsimulTime = !strcmp (temp, "Yes");  // Yes = 1, No = 
                                                               // 0
      free (temp);
   }
   PSI3_DEBUG ("PrintSimulationTime: %d", general->output->showsimulTime);

   /**
    * Get Print SimulationTime
    */
   temp = get_string_keyname (general_param_fd, "PrintPerf");
   if (temp == NULL) {
      general->output->PrintPerf = 0;
   }
   else {
      general->output->PrintPerf = !strcmp (temp, "Yes");  // Yes = 1
                                                           // No = 0
      free (temp);
   }
   PSI3_DEBUG ("PrintPerf: %d", general->output->PrintPerf);

   fclose (general_param_fd);

   if (err) {
      free (general);
      return NULL;
   }

   return general;
}

int
check_initialState (int *initialState, st_model_ptr model)
{
   int queue;

   for (queue = 0; queue < model->queues->nb_queues; queue++) {
      if (initialState[queue] < model->queues->mins[queue]
            || initialState[queue] > model->queues->maxs[queue]) {
         PSI3_DEBUG ("queue %d: state incorrect (%d)", queue, initialState[queue]);
         return 1;
      }
   }

   return 0;
}
