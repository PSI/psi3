/**
 * \file        psi_common.h
 * \author      Vincent Danjean <Vincent.Danjean@ens-lyon.org>
 * \version     1.0.0
 * \date        21/03/2013
 * \brief       prototypes for functions in psi_common.c
 */

 /* 
  * This library is free software; you can redistribute it and/or
  * modify it under the terms of the GNU Lesser General Public
  * License as published by the Free Software Foundation; either
  * version 2.1 of the License, or any later version.
  * 
  * This library is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  * Lesser General Public License for more details.
  * 
  * You should have received a copy of the GNU Lesser General Public
  * License along with this library; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
  */

#include <psi.h>
#include <dlfcn.h>
#include <stdio.h>
#include <time.h>
#include <sys/time.h>

#include <psi3_output_stream.h>
#include <psi3_log.h>
#include <psi3_timing.h>
#include <generator.h>

#ifndef __PSI_COMMON_H
#define __PSI_COMMON_H

/* FIXME: check redundancy with psi3_log */
#define DEBUG_IO     1
#define DEBUG_INFO   2

#ifndef DEBUG_LVL
#define DEBUG_LVL 0
#endif

#define PSI3_DEFAULT_OUTPUT_FILENAME "stdout"
#define PSI3_DEFAULT_OUTPUT_FD stdout

void close_prog (void *default_lib, void *user_lib, st_general_ptr general);
void loadlibs (void **default_lib, void **user_lib);

void print_time (FILE * f, double simulation_time);
void print_perf (FILE *output,
                 st_general_ptr general,
                 st_simulation_ptr simulation);

void print_header (FILE * output_stream,
                   st_general_ptr general,
                   st_model_ptr model,
                   st_method_ptr method,
                   void (*print_method) (FILE *, st_method_ptr));

void print_footer (st_general_ptr general,
                   st_model_ptr model,
                   st_method_ptr method, double simulation_time);

void print_info (FILE * output);
void print_param (FILE * output, st_general_ptr general);

int read_configuration (st_general_ptr * general,
                        char *general_file,
                        st_model_ptr * model,
                        char *model_file,
                        st_method_ptr * method,
                        char *method_file,
                        void *(*read_method) (char *, st_general_ptr,
                                              st_model_ptr));

int close_psi (st_general_ptr general,
               st_model_ptr model,
               st_method_ptr method, void (*free_method) (st_method_ptr));

//void loadBar (int x, int n, int r, int w);

void loadbar (int iteration, int interationNumber);

double delta_ms (struct timeval t1, struct timeval t2);

st_general_ptr read_general_param (char *general_param_cfgfile);

int check_initialState (int *initialState, st_model_ptr model);
#endif
