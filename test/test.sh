#!/bin/bash
#set -x

# PATH
CUR_PATH=`pwd`

# LOG
LOG="$CUR_PATH/test_log.txt"
MISMATCH1="$CUR_PATH/expected_res.txt"
MISMATCH2="$CUR_PATH/res.txt"

PSI3="psi3_unix"

OUTPUT_FILE="output.txt"

# Available kernels
KERNEL[1]="simpleforward"
KERNEL[2]="simpleforwardparallel"
KERNEL[3]="backwardmonotone"
KERNEL[4]="backwardenvelop"
KERNEL[5]="backwardenvelopsplitting"
KERNEL[6]="multipleforward"
KERNEL[7]="backwardmonotoneparallel"

# Set a table with a list of tests for a specific kernel
list_tests () {

   if [ ! -d "$ROOT_PATH/${KERNEL[$kernel_id]}/input" ]; then
      echo "Error: no dir [$ROOT_PATH/${KERNEL[$kernel_id]}/input]"
      exit 1
   fi

   TESTS_LIST=`find $ROOT_PATH/${KERNEL[$kernel_id]}/input -type f | grep -v "_model" | grep -v "_loadsharing"`

   if [ -z "$TESTS_LIST" ]; then
      echo "Error: no test files"
      exit 1
   fi

   test_id=0
   for test_file in $TESTS_LIST
   do
      TESTS[$test_id]=$test_file
      let test_id=test_id+1
   done

}

# Set a table of result files 
list_results () {

   if [ ! -d "$ROOT_PATH/${KERNEL[$kernel_id]}/output" ]; then
      echo "Error: no dir [$ROOT_PATH/${KERNEL[$kernel_id]}/output]"
      exit 1
   fi

   RESULTS_LIST=`find $ROOT_PATH/${KERNEL[$kernel_id]}/output -type f`

   if [ -z "$RESULTS_LIST" ]; then
      echo "Error: no result files"
      exit 1
   fi

   test_id=0
   for result_file in $RESULTS_LIST
   do
      RESULTS[$test_id]=$result_file
      let test_id=test_id+1
   done

}

check_filetests () {

   if [ ${#TESTS[@]} -eq ${#RESULTS[@]} ]; then

      for t in $TESTS_LIST
      do
         if [ ! -s "${t}_model" ]; then

            echo "Error: missing file: [${t}_model]"
            exit 1

         fi
      done

   else
      echo "Error: Incoherence input/output"
      exit 1
   fi

}

prepare_tests () {

   clean

   TESTS="" # list of test files
   list_tests $kernel_id
   RESULTS="" # list of result files
   list_results $kernel_id

   check_filetests

}

comment_filter () {

   cat $1 | sed '/^#/d' | sed '/^$/d' > $2
}

run () {

   cd $ROOT_PATH/${KERNEL[$kernel_id]}/input &>/dev/null # because of model relative path...

   RC=0 # store return code of PSI

   model_file=${TESTS[$1]}_model
   parameters_file=${TESTS[$1]}
   method_file=$parameters_file

   cmd="$PSI3 -m $model_file -p $parameters_file -k $method_file"
   $PSI3 -m $model_file -p $parameters_file -k $method_file &>$LOG

   RC=$?
}

check () {
   
   PASSED=""

   if [ $RC -ne 0 ];then
      INFO="Exec error"
   else
      if [ -f $OUTPUT_FILE ]; then

         comment_filter $OUTPUT_FILE ${OUTPUT_FILE}_tmp
         comment_filter ${RESULTS[$1]} ${RESULTS[$1]}_tmp

         #      cmp $OUTPUT_FILE ${RESULTS[$1]} &>/dev/null
         cmp ${OUTPUT_FILE}_tmp ${RESULTS[$1]}_tmp &>/dev/null

         if [ $? -eq 0 ]; then
            PASSED="OK"
         else
            INFO="Output file mismatch"
            cp ${RESULTS[$1]} $MISMATCH1
            cp $OUTPUT_FILE $MISMATCH2
         fi

      else
         INFO="No output file"
      fi
   fi

   rm -f ${OUTPUT_FILE}_tmp
   rm -f ${RESULTS[$1]}_tmp
   rm -f $OUTPUT_FILE

   cd - &>/dev/null
}

clean () {

   rm -f $MISMATCH1 $MISMATCH2
   rm -f `find $ROOT_PATH | grep "_tmp"`
   rm -f `find $ROOT_PATH | grep "$OUTPUT_FILE"`

}
 
print_help () {

   echo -e "Test engine for PSI3"
   echo -e ""
   echo -e "options are:"
   echo -e "\t-k [kernel list]"
   echo -e "\t\texecute tests from the given list"
   echo -e ""
   echo -e "\t-d [test root path]"
   echo -e "\t\tpath where are tests directories"
   echo -e ""
   echo -e "\t-h"
   echo -e "\t\tprint this help"

}

if [ $# -eq 0 ]; then
   print_help
fi

while getopts "d:k:h" opt; do
   case $opt in
      d)
         ROOT_PATH="$OPTARG"
         ;;
      k)
         KERNEL_LIST=`echo "$OPTARG" | sed 's/,/ /g'`
         ;;
      h)
         print_help
         ;;
      \?)
         echo "Invalid option: -$OPTARG"
         exit 1
         ;;
   esac
done

for kernel_id in $KERNEL_LIST
do
   echo "Running tests for kernel [${KERNEL[$kernel_id]}]"

   prepare_tests

   testID=0

   for test_name in $TESTS_LIST
   do
      run $testID

      check $testID

      echo -n "TEST: [`basename $test_name`] "

      if [ $PASSED ]; then
         echo "OK"
      else
         echo "FAILED ($INFO)"
         exit 1
      fi

      let testID=testID+1

   done

done

exit 0
