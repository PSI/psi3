/**
 * \file      taskpopfct.c
 * \author    Minh Quan HO
 * \version   1.0
 * \date      30/07/2012
 * \brief     Contains usef-defined task popping functions  
 */

/* 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include <stdio.h>
#include <psi.h>


/**          Task popping function 
 * \details    pop the task whose id_interval is minimum and priority max 
 * \param    taskpool   taskpool where tasks are stored
 * \return   \e T_task*  pointer on the choosen task
 */
T_task* pop_task(T_task_pool* taskpool){
  int min_interval = 100000;
  int max_priority = -1;
  T_task* candidate, *cour;
  
  if(taskpool->nb_tasks <= 0){ 		/// empty taskpool 
    return NULL;
  }else{
    candidate = taskpool->head;
    cour = taskpool->head;
    while(cour != NULL){		/// look for candidate
      if(cour->data.id_interval < min_interval){ // find a new task with smaller id_interval
	min_interval = cour->data.id_interval;
	max_priority = cour->data.priority;
	candidate = cour;			// update candidate
      }
      else{
	if(cour->data.id_interval == min_interval){ // find a task with same min_interval, now look at its priority
	  if(cour->data.priority > max_priority){ // this new task is more priority than current candidate
	    max_priority = cour->data.priority;
	    candidate = cour;			// update candidate
	  }
	}
      }
      cour = cour->next;
    }
    /// now withdraw and return the candidate
    if(candidate == taskpool->head){ 		// candidate at head
      if(candidate->next == NULL){ 		// no task behind => taskpool becomes empty
	taskpool->head = NULL;
	taskpool->queue = NULL;
      }else{ 				//there is a task behind => move head pointer forward
	taskpool->head = candidate->next;
      }
    }else{ 					// candidate not at head
      if(candidate->next == NULL){ 		// no task behind => move queue pointer back
	candidate->prec->next = NULL;
	taskpool->queue = candidate->prec;
      }else{ 				//there is a task behind => modify pointer linkages 
	candidate->prec->next = candidate->next;
	candidate->next->prec = candidate->prec;
      }
    }
    taskpool->nb_tasks--;
    return candidate;
  }
}