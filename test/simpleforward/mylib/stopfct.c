/**
 * \file      stopfct.c
 * \author    Minh Quan HO
 * \version   1.0
 * \date      30/07/2012
 * \brief     Contains usef-defined stop functions  
 */

/* 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include <stdio.h>
#include <psi.h>

int cpt_zero = 0;

/**
 * \brief       Function stop condition of the kernel simple forward
 * \details    	This function watch the first queue of system and stop simulation when this queue goes empty for the first time
 * \param    state     State vector of queues
 * \return   1 to stop simulation, otherwise return 0 (mean keep going simulation)
 */
int stop_when_zero(int* state){
  return (state[0] == 0);
}

/**
 * \brief       Function stop condition of the kernel simple forward
 * \details    	This function watch the first queue of system and stop simulation when this queue goes empty after 1000 times
 *              It's user's responsibility to manage a counter (e.g here cpt_zero)
 * \param    state     State vector of queues
 * \return   1 to stop simulation, otherwise return 0 (mean keep going simulation)
 */
int stop_when_reach_1000_times_zero(int* state){
  if(state[0] == 0) cpt_zero++;
  return (cpt_zero == 1000);
}