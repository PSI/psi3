
test engine: "test.sh"

File tree:
   - one folder per kernel
      - "input" folder containing the 2 kinds of input file
         - test file: simulation parameters
           -> the file name is the test name
         - test file model: the model associated to the test
           -> [test_name]_model
      - "output" folder for the result files
         - test result file, the expected results
           -> [test_name]
   

directory [KERNEL_NAME]
   |
   |-- INPUT
   |     |
   |     |-- test_name
   |     |-- test_name_model
   |
   |-- OUTPUT
   |     |-- test_name
   |



Example:

directory [simpleforward]
   |
   |-- INPUT
   |     |
   |     |-- test1
   |     |-- test1_model
   |
   |-- OUTPUT
   |     |-- test1
   |
